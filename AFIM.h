//
//  FIM.h
//  OpenGL_CGAL
//
//  Created by Sean Dillon on 3/10/16.
//  Copyright (c) 2016 Sean Dillon. All rights reserved.
//

#ifndef __OpenGL_CGAL__AFIM__
#define __OpenGL_CGAL__AFIM__
#include "KernelConstructions.h"
#include "AffineTransformation.h"
#include "FIS.h"
class AFIM:public FIS<Arrangement2,Affine2Arrangement>{
public:
    AFIM(Arrangement2& arr,Arrangement2& global);
};

#endif /* defined(__OpenGL_CGAL__FIM__) */
