//
//  FIS.h
//  OpenGL_CGAL
//
//  Created by Sean Dillon on 3/4/16.
//  Copyright (c) 2016 Sean Dillon. All rights reserved.
//



#ifndef __OpenGL_CGAL__AFIS__
#define __OpenGL_CGAL__AFIS__
#include <vector>
#include "KernelConstructions.h"
#include "AffineTransformation.h"
#include "FIS.h"

class AFIS:public FIS<Point,Affine3>{
public:
    AFIS(){;};
    AFIS(Arrangement2& mesh,Arrangement2& global);
};


#endif /* defined(__OpenGL_CGAL__FIS__) */
