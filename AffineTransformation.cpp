//
//  AffineTransformation.cpp
//  OpenGL_CGAL
//
//  Created by Sean Dillon on 3/4/16.
//  Copyright (c) 2016 Sean Dillon. All rights reserved.
//

#include "AffineTransformation.h"


Affine3::Affine3(Point L[3],Point G[3],K::FT s){
    locals[0]=L[0];locals[1]=L[1];locals[2]=L[2];
    globals[0]=G[0];globals[1]=G[1];globals[2]=G[2];
    //x
    matrix[0][0]=-(G[1].y()*L[2].x() - G[2].y()*L[1].x() - G[1].y()*L[0].x() + G[0].y()*L[1].x() + G[2].y()*L[0].x() - G[0].y()*L[2].x())/(G[1].x()*G[2].y() - G[1].y()*G[2].x() - G[1].x()*G[0].y() + G[1].y()*G[0].x() + G[2].x()*G[0].y() - G[2].y()*G[0].x());
    matrix[0][1]=(G[1].x()*L[2].x() - G[2].x()*L[1].x() - G[1].x()*L[0].x() + G[0].x()*L[1].x() + G[2].x()*L[0].x() - G[0].x()*L[2].x())/(G[1].x()*G[2].y() - G[1].y()*G[2].x() - G[1].x()*G[0].y() + G[1].y()*G[0].x() + G[2].x()*G[0].y() - G[2].y()*G[0].x());
    matrix[0][2]=0;
    matrix[0][3]=(G[1].x()*G[2].y()*L[0].x() - G[1].x()*G[0].y()*L[2].x() - G[1].y()*G[2].x()*L[0].x() + G[1].y()*G[0].x()*L[2].x() + G[2].x()*G[0].y()*L[1].x() - G[2].y()*G[0].x()*L[1].x())/(G[1].x()*G[2].y() - G[1].y()*G[2].x() - G[1].x()*G[0].y() + G[1].y()*G[0].x() + G[2].x()*G[0].y() - G[2].y()*G[0].x());
    //y
    matrix[1][0]=-(G[1].y()*L[2].y() - G[2].y()*L[1].y() - G[1].y()*L[0].y() + G[0].y()*L[1].y() + G[2].y()*L[0].y() - G[0].y()*L[2].y())/(G[1].x()*G[2].y() - G[1].y()*G[2].x() - G[1].x()*G[0].y() + G[1].y()*G[0].x() + G[2].x()*G[0].y() - G[2].y()*G[0].x());
    matrix[1][1]=(G[1].x()*L[2].y() - G[2].x()*L[1].y() - G[1].x()*L[0].y() + G[0].x()*L[1].y() + G[2].x()*L[0].y() - G[0].x()*L[2].y())/(G[1].x()*G[2].y() - G[1].y()*G[2].x() - G[1].x()*G[0].y() + G[1].y()*G[0].x() + G[2].x()*G[0].y() - G[2].y()*G[0].x());
    matrix[1][2]=0;
    matrix[1][3]=(G[1].x()*G[2].y()*L[0].y() - G[1].x()*G[0].y()*L[2].y() - G[1].y()*G[2].x()*L[0].y() + G[1].y()*G[0].x()*L[2].y() + G[2].x()*G[0].y()*L[1].y() - G[2].y()*G[0].x()*L[1].y())/(G[1].x()*G[2].y() - G[1].y()*G[2].x() - G[1].x()*G[0].y() + G[1].y()*G[0].x() + G[2].x()*G[0].y() - G[2].y()*G[0].x());
    //z
    matrix[2][0]=-(G[1].y()*L[2].z() - G[2].y()*L[1].z() - G[1].y()*L[0].z() + G[0].y()*L[1].z() + G[2].y()*L[0].z() - G[0].y()*L[2].z() - G[1].y()*G[2].z()*s + G[1].z()*G[2].y()*s + G[1].y()*G[0].z()*s- G[1].z()*G[0].y()*s- G[2].y()*G[0].z()*s+ G[2].z()*G[0].y()*s)/
    (G[1].x()*G[2].y() - G[1].y()*G[2].x() - G[1].x()*G[0].y() + G[1].y()*G[0].x() + G[2].x()*G[0].y() - G[2].y()*G[0].x());
    matrix[2][1]=(G[1].x()*L[2].z() - G[2].x()*L[1].z() - G[1].x()*L[0].z() + G[0].x()*L[1].z() + G[2].x()*L[0].z() - G[0].x()*L[2].z() - G[1].x()*G[2].z()*s + G[1].z()*G[2].x()*s+ G[1].x()*G[0].z()*s- G[1].z()*G[0].x()*s- G[2].x()*G[0].z()*s+ G[2].z()*G[0].x()*s)/
    (G[1].x()*G[2].y() - G[1].y()*G[2].x() - G[1].x()*G[0].y() + G[1].y()*G[0].x() + G[2].x()*G[0].y() - G[2].y()*G[0].x());
    matrix[2][2]=s;
    matrix[2][3]=(G[1].x()*G[2].y()*L[0].z() - G[1].x()*G[0].y()*L[2].z() - G[1].y()*G[2].x()*L[0].z() + G[1].y()*G[0].x()*L[2].z() + G[2].x()*G[0].y()*L[1].z() - G[2].y()*G[0].x()*L[1].z() - G[1].x()*G[2].y()*G[0].z()*s+ G[1].x()*G[2].z()*G[0].y()*s + G[1].y()*G[2].x()*G[0].z()*s-
                  G[1].y()*G[2].z()*G[0].x()*s - G[1].z()*G[2].x()*G[0].y()*s+ G[1].z()*G[2].y()*G[0].x()*s)/(G[1].x()*G[2].y() - G[1].y()*G[2].x() - G[1].x()*G[0].y() + G[1].y()*G[0].x() + G[2].x()*G[0].y() - G[2].y()*G[0].x());
}

Affine3::Affine3(Point L[3],Point G[3],K::FT S[3]){
    locals[0]=L[0];locals[1]=L[1];locals[2]=L[2];
    globals[0]=G[0];globals[1]=G[1];globals[2]=G[2];
    s[0]=S[0];s[1]=S[1];s[2]=S[2];
    //x
    matrix[0][0]=-(G[1].y()*L[2].x() - G[2].y()*L[1].x() - G[1].y()*L[0].x() + G[0].y()*L[1].x() + G[2].y()*L[0].x() - G[0].y()*L[2].x())/(G[1].x()*G[2].y() - G[1].y()*G[2].x() - G[1].x()*G[0].y() + G[1].y()*G[0].x() + G[2].x()*G[0].y() - G[2].y()*G[0].x());
    matrix[0][1]=(G[1].x()*L[2].x() - G[2].x()*L[1].x() - G[1].x()*L[0].x() + G[0].x()*L[1].x() + G[2].x()*L[0].x() - G[0].x()*L[2].x())/(G[1].x()*G[2].y() - G[1].y()*G[2].x() - G[1].x()*G[0].y() + G[1].y()*G[0].x() + G[2].x()*G[0].y() - G[2].y()*G[0].x());
    matrix[0][2]=0;
    matrix[0][3]=(G[1].x()*G[2].y()*L[0].x() - G[1].x()*G[0].y()*L[2].x() - G[1].y()*G[2].x()*L[0].x() + G[1].y()*G[0].x()*L[2].x() + G[2].x()*G[0].y()*L[1].x() - G[2].y()*G[0].x()*L[1].x())/(G[1].x()*G[2].y() - G[1].y()*G[2].x() - G[1].x()*G[0].y() + G[1].y()*G[0].x() + G[2].x()*G[0].y() - G[2].y()*G[0].x());
     //y
    matrix[1][0]=-(G[1].y()*L[2].y() - G[2].y()*L[1].y() - G[1].y()*L[0].y() + G[0].y()*L[1].y() + G[2].y()*L[0].y() - G[0].y()*L[2].y())/(G[1].x()*G[2].y() - G[1].y()*G[2].x() - G[1].x()*G[0].y() + G[1].y()*G[0].x() + G[2].x()*G[0].y() - G[2].y()*G[0].x());
    matrix[1][1]=(G[1].x()*L[2].y() - G[2].x()*L[1].y() - G[1].x()*L[0].y() + G[0].x()*L[1].y() + G[2].x()*L[0].y() - G[0].x()*L[2].y())/(G[1].x()*G[2].y() - G[1].y()*G[2].x() - G[1].x()*G[0].y() + G[1].y()*G[0].x() + G[2].x()*G[0].y() - G[2].y()*G[0].x());
    matrix[1][2]=0;
    matrix[1][3]=(G[1].x()*G[2].y()*L[0].y() - G[1].x()*G[0].y()*L[2].y() - G[1].y()*G[2].x()*L[0].y() + G[1].y()*G[0].x()*L[2].y() + G[2].x()*G[0].y()*L[1].y() - G[2].y()*G[0].x()*L[1].y())/(G[1].x()*G[2].y() - G[1].y()*G[2].x() - G[1].x()*G[0].y() + G[1].y()*G[0].x() + G[2].x()*G[0].y() - G[2].y()*G[0].x());
}

Affine3::Affine3(std::vector<Point> L,std::vector<Point> G,std::vector<K::FT> S){
    locals[0]=L[0];locals[1]=L[1];locals[2]=L[2];
    globals[0]=G[0];globals[1]=G[1];globals[2]=G[2];
    s[0]=S[0];s[1]=S[1];s[2]=S[2];
    //x
    matrix[0][0]=-(G[1].y()*L[2].x() - G[2].y()*L[1].x() - G[1].y()*L[0].x() + G[0].y()*L[1].x() + G[2].y()*L[0].x() - G[0].y()*L[2].x())/(G[1].x()*G[2].y() - G[1].y()*G[2].x() - G[1].x()*G[0].y() + G[1].y()*G[0].x() + G[2].x()*G[0].y() - G[2].y()*G[0].x());
    matrix[0][1]=(G[1].x()*L[2].x() - G[2].x()*L[1].x() - G[1].x()*L[0].x() + G[0].x()*L[1].x() + G[2].x()*L[0].x() - G[0].x()*L[2].x())/(G[1].x()*G[2].y() - G[1].y()*G[2].x() - G[1].x()*G[0].y() + G[1].y()*G[0].x() + G[2].x()*G[0].y() - G[2].y()*G[0].x());
    matrix[0][2]=0;
    matrix[0][3]=(G[1].x()*G[2].y()*L[0].x() - G[1].x()*G[0].y()*L[2].x() - G[1].y()*G[2].x()*L[0].x() + G[1].y()*G[0].x()*L[2].x() + G[2].x()*G[0].y()*L[1].x() - G[2].y()*G[0].x()*L[1].x())/(G[1].x()*G[2].y() - G[1].y()*G[2].x() - G[1].x()*G[0].y() + G[1].y()*G[0].x() + G[2].x()*G[0].y() - G[2].y()*G[0].x());
    //y
    matrix[1][0]=-(G[1].y()*L[2].y() - G[2].y()*L[1].y() - G[1].y()*L[0].y() + G[0].y()*L[1].y() + G[2].y()*L[0].y() - G[0].y()*L[2].y())/(G[1].x()*G[2].y() - G[1].y()*G[2].x() - G[1].x()*G[0].y() + G[1].y()*G[0].x() + G[2].x()*G[0].y() - G[2].y()*G[0].x());
    matrix[1][1]=(G[1].x()*L[2].y() - G[2].x()*L[1].y() - G[1].x()*L[0].y() + G[0].x()*L[1].y() + G[2].x()*L[0].y() - G[0].x()*L[2].y())/(G[1].x()*G[2].y() - G[1].y()*G[2].x() - G[1].x()*G[0].y() + G[1].y()*G[0].x() + G[2].x()*G[0].y() - G[2].y()*G[0].x());
    matrix[1][2]=0;
    matrix[1][3]=(G[1].x()*G[2].y()*L[0].y() - G[1].x()*G[0].y()*L[2].y() - G[1].y()*G[2].x()*L[0].y() + G[1].y()*G[0].x()*L[2].y() + G[2].x()*G[0].y()*L[1].y() - G[2].y()*G[0].x()*L[1].y())/(G[1].x()*G[2].y() - G[1].y()*G[2].x() - G[1].x()*G[0].y() + G[1].y()*G[0].x() + G[2].x()*G[0].y() - G[2].y()*G[0].x());
}

Point Affine3::Transform(Point& p){
    //simple case
    if(vst==Arrangement2::VerticalScalingType::SINGLE||vst==Arrangement2::VerticalScalingType::FACES)
        return Point(p.x()*matrix[0][0]+p.y()*matrix[0][1]+matrix[0][3],
                     p.x()*matrix[1][0]+p.y()*matrix[1][1]+matrix[1][3],
                     p.x()*matrix[2][0]+p.y()*matrix[2][1]+p.z()*matrix[2][2]+matrix[2][3]);
    //zhao case
    Point result;
    K::FT sf;
    K::FT lambdas[3];
    //compute vf of point according to the transformation of its position
    Triangle_coordinates tc(Point2(locals[0].x(),locals[0].y()),Point2(locals[1].x(),locals[1].y()),Point2(locals[2].x(),locals[2].y()));
    result = Point(matrix[0][0]*p.x()+matrix[0][1]*p.y()+matrix[0][3],matrix[1][0]*p.x()+matrix[1][1]*p.y()+matrix[1][3],0);
    tc(Point2(result.x(),result.y()),lambdas);
    sf = s[0]*lambdas[0]+s[1]*lambdas[1]+s[2]*lambdas[2];
    //initialize the matrix with the new s
     matrix[2][0]=-(globals[1].y()*locals[2].z() - globals[2].y()*locals[1].z() - globals[1].y()*locals[0].z() + globals[0].y()*locals[1].z() + globals[2].y()*locals[0].z() - globals[0].y()*locals[2].z() - globals[1].y()*globals[2].z()*sf + globals[1].z()*globals[2].y()*sf + globals[1].y()*globals[0].z()*sf- globals[1].z()*globals[0].y()*sf- globals[2].y()*globals[0].z()*sf+ globals[2].z()*globals[0].y()*sf)/
     (globals[1].x()*globals[2].y() - globals[1].y()*globals[2].x() - globals[1].x()*globals[0].y() + globals[1].y()*globals[0].x() + globals[2].x()*globals[0].y() - globals[2].y()*globals[0].x());
     matrix[2][1]=(globals[1].x()*locals[2].z() - globals[2].x()*locals[1].z() - globals[1].x()*locals[0].z() + globals[0].x()*locals[1].z() + globals[2].x()*locals[0].z() - globals[0].x()*locals[2].z() - globals[1].x()*globals[2].z()*sf + globals[1].z()*globals[2].x()*sf+ globals[1].x()*globals[0].z()*sf- globals[1].z()*globals[0].x()*sf- globals[2].x()*globals[0].z()*sf+ globals[2].z()*globals[0].x()*sf)/
     (globals[1].x()*globals[2].y() - globals[1].y()*globals[2].x() - globals[1].x()*globals[0].y() + globals[1].y()*globals[0].x() + globals[2].x()*globals[0].y() - globals[2].y()*globals[0].x());
     matrix[2][2]=sf;
     matrix[2][3]=(globals[1].x()*globals[2].y()*locals[0].z() - globals[1].x()*globals[0].y()*locals[2].z() - globals[1].y()*globals[2].x()*locals[0].z() + globals[1].y()*globals[0].x()*locals[2].z() + globals[2].x()*globals[0].y()*locals[1].z() - globals[2].y()*globals[0].x()*locals[1].z() - globals[1].x()*globals[2].y()*globals[0].z()*sf+ globals[1].x()*globals[2].z()*globals[0].y()*sf + globals[1].y()*globals[2].x()*globals[0].z()*sf-
     globals[1].y()*globals[2].z()*globals[0].x()*sf - globals[1].z()*globals[2].x()*globals[0].y()*sf+ globals[1].z()*globals[2].y()*globals[0].x()*sf)/(globals[1].x()*globals[2].y() - globals[1].y()*globals[2].x() - globals[1].x()*globals[0].y() + globals[1].y()*globals[0].x() + globals[2].x()*globals[0].y() - globals[2].y()*globals[0].x());

    return Point(result.x(),result.y(),p.x()*matrix[2][0]+p.y()*matrix[2][1]+p.z()*matrix[2][2]+matrix[2][3]);
}

std::ostream& operator <<(std::ostream& stream,const Affine3& trans) {
    stream<<trans.matrix[0][0]<<","<<trans.matrix[0][1]<<","<<trans.matrix[0][2]<<","<<trans.matrix[0][3]<<std::endl
    <<trans.matrix[1][0]<<","<<trans.matrix[1][1]<<","<<trans.matrix[1][2]<<","<<trans.matrix[1][3]<<std::endl
    <<trans.matrix[2][0]<<","<<trans.matrix[2][1]<<","<<trans.matrix[2][2]<<","<<trans.matrix[2][3]<<std::endl;
    return stream;
}

Affine2Arrangement::Affine2Arrangement(Arrangement2::Vertex_handle LH[3],Arrangement2::Vertex_handle GH[3]){
    locals[0]=LH[0];locals[1]=LH[1];locals[2]=LH[2];
    globals[0]=GH[0];globals[1]=GH[1];globals[2]=GH[2];
    ExactKernel::Point_2 L[3],G[3];
    L[0]=ExactKernel::Point_2(locals[0]->point().x(),locals[0]->point().y());
    L[1]=ExactKernel::Point_2(locals[1]->point().x(),locals[1]->point().y());
    L[2]=ExactKernel::Point_2(locals[2]->point().x(),locals[2]->point().y());
    
    G[0]=ExactKernel::Point_2(globals[0]->point().x(),globals[0]->point().y());
    G[1]=ExactKernel::Point_2(globals[1]->point().x(),globals[1]->point().y());
    G[2]=ExactKernel::Point_2(globals[2]->point().x(),globals[2]->point().y());
    //x
    matrix[0][0]=-(G[1].y()*L[2].x() - G[2].y()*L[1].x() - G[1].y()*L[0].x() + G[0].y()*L[1].x() + G[2].y()*L[0].x() - G[0].y()*L[2].x())/(G[1].x()*G[2].y() - G[1].y()*G[2].x() - G[1].x()*G[0].y() + G[1].y()*G[0].x() + G[2].x()*G[0].y() - G[2].y()*G[0].x());
    matrix[0][1]=(G[1].x()*L[2].x() - G[2].x()*L[1].x() - G[1].x()*L[0].x() + G[0].x()*L[1].x() + G[2].x()*L[0].x() - G[0].x()*L[2].x())/(G[1].x()*G[2].y() - G[1].y()*G[2].x() - G[1].x()*G[0].y() + G[1].y()*G[0].x() + G[2].x()*G[0].y() - G[2].y()*G[0].x());
    matrix[0][2]=(G[1].x()*G[2].y()*L[0].x() - G[1].x()*G[0].y()*L[2].x() - G[1].y()*G[2].x()*L[0].x() + G[1].y()*G[0].x()*L[2].x() + G[2].x()*G[0].y()*L[1].x() - G[2].y()*G[0].x()*L[1].x())/(G[1].x()*G[2].y() - G[1].y()*G[2].x() - G[1].x()*G[0].y() + G[1].y()*G[0].x() + G[2].x()*G[0].y() - G[2].y()*G[0].x());
    //y
    matrix[1][0]=-(G[1].y()*L[2].y() - G[2].y()*L[1].y() - G[1].y()*L[0].y() + G[0].y()*L[1].y() + G[2].y()*L[0].y() - G[0].y()*L[2].y())/(G[1].x()*G[2].y() - G[1].y()*G[2].x() - G[1].x()*G[0].y() + G[1].y()*G[0].x() + G[2].x()*G[0].y() - G[2].y()*G[0].x());
    matrix[1][1]=(G[1].x()*L[2].y() - G[2].x()*L[1].y() - G[1].x()*L[0].y() + G[0].x()*L[1].y() + G[2].x()*L[0].y() - G[0].x()*L[2].y())/(G[1].x()*G[2].y() - G[1].y()*G[2].x() - G[1].x()*G[0].y() + G[1].y()*G[0].x() + G[2].x()*G[0].y() - G[2].y()*G[0].x());
    matrix[1][2]=(G[1].x()*G[2].y()*L[0].y() - G[1].x()*G[0].y()*L[2].y() - G[1].y()*G[2].x()*L[0].y() + G[1].y()*G[0].x()*L[2].y() + G[2].x()*G[0].y()*L[1].y() - G[2].y()*G[0].x()*L[1].y())/(G[1].x()*G[2].y() - G[1].y()*G[2].x() - G[1].x()*G[0].y() + G[1].y()*G[0].x() + G[2].x()*G[0].y() - G[2].y()*G[0].x());
}

Arrangement2 Affine2Arrangement::Transform(Arrangement2& input){
    Arrangement2 t(input);
    for(Arrangement2::Vertex_iterator it= t.vertices_begin();it!=t.vertices_end();it++){
        it->point()=ExactKernel::Point_2(it->point().x()*matrix[0][0]+it->point().y()*matrix[0][1]+matrix[0][2],
                          it->point().x()*matrix[1][0]+it->point().y()*matrix[1][1]+matrix[1][2]);
    }
    return t;
}

std::ostream& operator <<(std::ostream& stream,const Affine2Arrangement& trans) {
    stream<<trans.matrix[0][0]<<","<<trans.matrix[0][1]<<","<<trans.matrix[0][2]<<","<<trans.matrix[0][3]<<std::endl
    <<trans.matrix[1][0]<<","<<trans.matrix[1][1]<<","<<trans.matrix[1][2]<<","<<trans.matrix[1][3]<<std::endl;
    return stream;
}
