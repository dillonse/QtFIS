//
//  AffineTransformation.h
//  OpenGL_CGAL
//
//  Created by Sean Dillon on 3/4/16.
//  Copyright (c) 2016 Sean Dillon. All rights reserved.
//

#ifndef __OpenGL_CGAL__AffineTransformation__
#define __OpenGL_CGAL__AffineTransformation__

#include "KernelConstructions.h"
#include "TransformationObject.h"
/*
 this model is a general version of the Affine 3D trans that covers the Zhao interpolation method.
 for using one s per face (Jeronimo,Hardin), just assign the s factor to all the positions of the s vector
 */
class Affine3:public TO{
public:
    Point globals[3];//the gloabal points that define the transformation
    Point locals[3];//the local points that define the transformation
    K::FT s[3];//the vertical scaling factors of the local points, for case of factor per triangle use same factor in all places
    K::FT matrix[3][4]; //the matrix with which we will multiply p to get the transformed point in homogenous form
    Affine3(){;};
    Affine3(Point L[3],Point G[3],K::FT s);
    Affine3(Point Locals[3],Point Globals[3],K::FT SVector[3]);
    Affine3(std::vector<Point> Locals,std::vector<Point> Globals,std::vector<K::FT> SVector);
    Point Transform(Point& p);
};
std::ostream& operator<<(std::ostream& stream,const Affine3& a);
/*
 this transformation is used for transforming a whole triangulation by transforming the vertices geometry
 its usefull for creating a consistent triangulation via IFS
 */
class Affine2Arrangement{
public:
    Arrangement2::Vertex_handle globals[3];//the gloabal points that define the transformation
    Arrangement2::Vertex_handle locals[3];//the local points that define the transformation
    ExactKernel::FT matrix[2][3]; //the matrix with which we will multiply p to get the transformed point in homogenous form
    Affine2Arrangement(Arrangement2::Vertex_handle Locals[3],Arrangement2::Vertex_handle Globals[3]);
    Arrangement2 Transform(Arrangement2& p);
};
std::ostream& operator<<(std::ostream& stream,const Affine2Arrangement& a);

#endif /* defined(__OpenGL_CGAL__AffineTransformation__) */
