//
//  AffineTransforms.cpp
//  OpenGL_CGAL
//
//  Created by Sean Dillon on 12/26/15.
//  Copyright (c) 2015 Sean Dillon. All rights reserved.
//

#include "AffineTransforms.h"


//Affine Transformation 2D//
AffineTransformation2D::AffineTransformation2D(float a,float b,float c,float s,float d,float e){
    this->a = a;this->b=b;this->c=c;this->s=s;this->d=d;this->e=e;
}

Point2 AffineTransformation2D::transformPoint(Point2 point){
    return Point2(point.x()*this->a + point.y()*this->b+this->d,
                         point.x()*this->c+point.y()*this->s+this->e);
}

//Affine Transformation 3D//
Point AffineTransformation3D::transformPoint(Point point){
    return Point(point.x()*a+point.y()*b+point.z()*c+j,
                 point.x()*d+point.y()*e+point.z()*f+k,
                 point.x()*g+point.y()*h+point.z()*i+l);
}

Point AffineTransformation3D::transformPoint_xy(Point& point_from){
    return Point(point_from.x()*a+point_from.y()*b+point_from.z()*c+j,
                 point_from.x()*d+point_from.y()*e+point_from.z()*f+k,
                 point_from.z());
}

K::FT AffineTransformation3D::transform_z(Point& point_from){
    return  point_from.x()*g+point_from.y()*h+point_from.z()*i+l;
}


std::ostream& operator <<(std::ostream& stream,const AffineTransformation3D& trans) {
    stream<<std::endl<<"("<<trans.a<<","<<trans.b<<","<<trans.c<<")+("<<trans.j<<")"<<std::endl
    <<"("<<trans.d<<","<<trans.e<<","<<trans.f<<")+("<<trans.k<<")"<<std::endl
    <<"("<<trans.g<<","<<trans.h<<","<<trans.i<<")+("<<trans.l<<")"<<std::endl;
    return stream;
}