//
//  AffineTransforms.h
//  OpenGL_CGAL
//
//  Created by Sean Dillon on 12/26/15.
//  Copyright (c) 2015 Sean Dillon. All rights reserved.
//

#ifndef __OpenGL_CGAL__AffineTransforms__
#define __OpenGL_CGAL__AffineTransforms__

#include "KernelConstructions.h"

/**
 Affine2D as
 
 [ a  b ]    [d]
 [ c  s ]  + [e]
 **/

class AffineTransformation2D{
public:
    //data initialized to identity transformation//
    K::FT a=1;
    K::FT b=0;
    K::FT c=0;
    K::FT s=1;
    K::FT d=0;
    K::FT e=0;
    AffineTransformation2D(float a,float b,float c,float s,float d,float e);
    AffineTransformation2D(){};
    Point2 transformPoint(Point2 point);
};

/**
 Affine3D as
 [ a b c ]  [j]
 [ d e f ]  [k]
 [ g h i ]  [l]
 **/



class AffineTransformation3D{
public:
    K::FT a=1;
    K::FT b=0;
    K::FT c=0;
    K::FT d=0;
    K::FT e=1;
    K::FT f=0;
    K::FT g=0;
    K::FT h=0;
    K::FT i=1;
    K::FT j=0;
    K::FT k=0;
    K::FT l=0;
    AffineTransformation3D(float a,float b,float c,float d,float e,float f,float g,float h,float i,float j ,float k, float l):a(a),b(b),c(c),d(d),e(e),f(f),g(g),h(h),i(i),j(j),k(k),l(l){;}
    AffineTransformation3D(){};
    Point transformPoint(Point point);
    Point transformPoint_xy(Point& point);
    Point transformPoint_z(Point& point);
    K::FT transform_z(Point& point);
};
std::ostream& operator<<(std::ostream& stream,const AffineTransformation3D& a);



#endif /* defined(__OpenGL_CGAL__AffineTransforms__) */
