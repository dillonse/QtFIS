//
//  BFIM.cpp
//  OpenGL_CGAL
//
//  Created by Sean Dillon on 3/16/16.
//  Copyright (c) 2016 Sean Dillon. All rights reserved.
//

#include "BFIM.h"
BFIM::BFIM(Arrangement2& arr,Arrangement2& global){
    Arrangement2::Vertex_handle globals[4];
    Arrangement2::Vertex_handle locals[4];
    Arrangement2::Vertex_handle Globals[4];
    //get the globals
    int i=0;
    for(Arrangement2::Vertex_iterator it = global.vertices_begin();it!=global.vertices_end();it++){
        Globals[i]=it;
        i++;
    }
    std::sort(Globals,Globals+4,[](auto i,auto j){return i->data().triangulation_index<j->data().triangulation_index;});
    transformations.reserve(arr.number_of_faces());
    for(Arrangement2::Face_iterator it=arr.faces_begin();it!=arr.faces_end();it++){
        if(it->is_unbounded())continue;
        Arrangement2::Ccb_halfedge_circulator hc = it->outer_ccb();
        locals[0]=hc->target();//->point();
        globals[0]= Globals[hc->target()->data().triangulation_index];//->point();
        hc++;
        locals[1]=hc->target();//->point();
        globals[1]= Globals[hc->target()->data().triangulation_index];//->point();
        hc++;
        locals[2]=hc->target();//->point();
        globals[2] = Globals[hc->target()->data().triangulation_index];//->point();
        hc++;
        locals[3]=hc->target();//->point();
        globals[3] = Globals[hc->target()->data().triangulation_index];//->point();
        transformations.push_back(Bivariate2Arrangement(locals,globals));
    }
    buffer1.push_back(arr);
    std::cout<<"created "<<transformations.size()<<" trans"<<std::endl;
}
