//
//  BFIM.h
//  OpenGL_CGAL
//
//  Created by Sean Dillon on 3/16/16.
//  Copyright (c) 2016 Sean Dillon. All rights reserved.
//

#ifndef __OpenGL_CGAL__BFIM__
#define __OpenGL_CGAL__BFIM__

#include "FIS.h"
#include "KernelConstructions.h"
#include "BivariateTransformation.h"

class BFIM:public FIS<Arrangement2,Bivariate2Arrangement> {
public:
    BFIM(Arrangement2& arr,Arrangement2& global);
};

#endif /* defined(__OpenGL_CGAL__BFIM__) */
