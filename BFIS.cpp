//
//  BFIS.cpp
//  OpenGL_CGAL
//
//  Created by Sean Dillon on 3/17/16.
//  Copyright (c) 2016 Sean Dillon. All rights reserved.
//

#include "BFIS.h"

BFIS::BFIS(Arrangement2& mesh, Arrangement2& global){
    ExacttoK exactToK;
    vst=mesh.vst;ct=mesh.ct;
    Point globals[4];
    Point locals[4];
    Arrangement2::Vertex_handle Globals[4];
    int i=0;
    for(Arrangement2::Vertex_iterator it = global.vertices_begin();it!=global.vertices_end();it++){
        Globals[i++]=it;
    }
    if(ct==Arrangement2::ColoringType::NONE){
        //sort the globals based on their geometry and find the orientation of the third point from the line of the first two
        std::sort(Globals,Globals+4,[](auto i,auto j){return i->point().x()<j->point().x();});
        std::sort(Globals,Globals+2,[](auto i,auto j){return i->point().y()<j->point().y();});
        std::sort(Globals+2,Globals+4,[](auto i,auto j){return i->point().y()>j->point().y();});
        if(vst==Arrangement2::VerticalScalingType::SINGLE){
            //s is the unbounted face s
            K::FT s=mesh.unbounded_face()->data().s;
            for(Arrangement2::Face_iterator it=mesh.faces_begin();it!=mesh.faces_end();it++){
                if(it->is_unbounded())continue;
                Arrangement2::Ccb_halfedge_circulator hc = it->outer_ccb();
                locals[0]=Point(exactToK(hc->target()->point().x()),exactToK(hc->target()->point().y()),hc->target()->data().height);hc++;
                locals[1]=Point(exactToK(hc->target()->point().x()),exactToK(hc->target()->point().y()),hc->target()->data().height);hc++;
                locals[2]=Point(exactToK(hc->target()->point().x()),exactToK(hc->target()->point().y()),hc->target()->data().height);hc++;
                locals[3]=Point(exactToK(hc->target()->point().x()),exactToK(hc->target()->point().y()),hc->target()->data().height);
                //sort the locals same way as the globals
                std::sort(locals,locals+4,[](auto i,auto j){return i.x()<j.x();});
                std::sort(locals,locals+2,[](auto i,auto j){return i.y()<j.y();});
                std::sort(locals+2,locals+4,[](auto i,auto j){return i.y()>j.y();});
                //check if the orientation of the 3rd point relative to line from 1st and 2nd point is the same as the globals
                globals[0]=Point(exactToK(Globals[0]->point().x()),exactToK(Globals[0]->point().y()),Globals[0]->data().height);
                globals[1]=Point(exactToK(Globals[1]->point().x()),exactToK(Globals[1]->point().y()),Globals[1]->data().height);
                globals[2]=Point(exactToK(Globals[2]->point().x()),exactToK(Globals[2]->point().y()),Globals[2]->data().height);
                globals[3]=Point(exactToK(Globals[3]->point().x()),exactToK(Globals[3]->point().y()),Globals[3]->data().height);
                transformations.push_back(Bivariate3(locals,globals,s));
                (transformations.end()-1)->vst=vst;(transformations.end()-1)->ct=ct;
            }
        }
        else if(vst==Arrangement2::VerticalScalingType::FACES){
            //take the s from the face
            for(Arrangement2::Face_iterator it=mesh.faces_begin();it!=mesh.faces_end();it++){
                if(it->is_unbounded())continue;
                K::FT s=it->data().s;
                Arrangement2::Ccb_halfedge_circulator hc = it->outer_ccb();
                locals[0]=Point(exactToK(hc->target()->point().x()),exactToK(hc->target()->point().y()),hc->target()->data().height);hc++;
                locals[1]=Point(exactToK(hc->target()->point().x()),exactToK(hc->target()->point().y()),hc->target()->data().height);hc++;
                locals[2]=Point(exactToK(hc->target()->point().x()),exactToK(hc->target()->point().y()),hc->target()->data().height);hc++;
                locals[3]=Point(exactToK(hc->target()->point().x()),exactToK(hc->target()->point().y()),hc->target()->data().height);
                //sort the locals same way as the globals
                std::sort(locals,locals+4,[](auto i,auto j){return i.x()<j.x();});
                std::sort(locals,locals+2,[](auto i,auto j){return i.y()<j.y();});
                std::sort(locals+2,locals+4,[](auto i,auto j){return i.y()>j.y();});
                globals[0]=Point(exactToK(Globals[0]->point().x()),exactToK(Globals[0]->point().y()),Globals[0]->data().height);
                globals[1]=Point(exactToK(Globals[1]->point().x()),exactToK(Globals[1]->point().y()),Globals[1]->data().height);
                globals[2]=Point(exactToK(Globals[2]->point().x()),exactToK(Globals[2]->point().y()),Globals[2]->data().height);
                globals[3]=Point(exactToK(Globals[3]->point().x()),exactToK(Globals[3]->point().y()),Globals[3]->data().height);
                transformations.push_back(Bivariate3(locals,globals,s));
                (transformations.end()-1)->vst=vst;(transformations.end()-1)->ct=ct;
            }
            
        }
        else if(vst==Arrangement2::VerticalScalingType::VERTICES){
            //gather an array of s one per vertex
            K::FT s[4];
            for(Arrangement2::Face_iterator it=mesh.faces_begin();it!=mesh.faces_end();it++){
                if(it->is_unbounded())continue;
                Arrangement2::Ccb_halfedge_circulator hc = it->outer_ccb();
                locals[0]=Point(exactToK(hc->target()->point().x()),exactToK(hc->target()->point().y()),hc->target()->data().height);s[0]=hc->target()->data().s;hc++;
                locals[1]=Point(exactToK(hc->target()->point().x()),exactToK(hc->target()->point().y()),hc->target()->data().height);s[1]=hc->target()->data().s;hc++;
                locals[2]=Point(exactToK(hc->target()->point().x()),exactToK(hc->target()->point().y()),hc->target()->data().height);s[2]=hc->target()->data().s;hc++;
                locals[3]=Point(exactToK(hc->target()->point().x()),exactToK(hc->target()->point().y()),hc->target()->data().height);s[3]=hc->target()->data().s;
                //sort the locals same way as the globals
                std::sort(locals,locals+4,[](auto i,auto j){return i.x()<j.x();});
                std::sort(locals,locals+2,[](auto i,auto j){return i.y()<j.y();});
                std::sort(locals+2,locals+4,[](auto i,auto j){return i.y()>j.y();});
                //check if the orientation of the 3rd point relative to line from 1st and 2nd point is the same as the globals
                globals[0]=Point(exactToK(Globals[0]->point().x()),exactToK(Globals[0]->point().y()),Globals[0]->data().height);
                globals[1]=Point(exactToK(Globals[1]->point().x()),exactToK(Globals[1]->point().y()),Globals[1]->data().height);
                globals[2]=Point(exactToK(Globals[2]->point().x()),exactToK(Globals[2]->point().y()),Globals[2]->data().height);
                globals[3]=Point(exactToK(Globals[3]->point().x()),exactToK(Globals[3]->point().y()),Globals[3]->data().height);
                transformations.push_back(Bivariate3(locals,globals,s));
                (transformations.end()-1)->vst=vst;(transformations.end()-1)->ct=ct;
            }
            
        }
    }
    else if(ct==Arrangement2::ColoringType::CONSISTENT){
        //sort the globals based on their colors
        int indices[4];
        std::sort(Globals,Globals+4,[](auto i,auto j){return i->data().triangulation_index<j->data().triangulation_index;});
        if(vst==Arrangement2::VerticalScalingType::SINGLE){
            //s from the unbounded face
            K::FT s=mesh.unbounded_face()->data().s;
            for(Arrangement2::Face_iterator it=mesh.faces_begin();it!=mesh.faces_end();it++){
                if(it->is_unbounded())continue;
                Arrangement2::Ccb_halfedge_circulator hc = it->outer_ccb();
                locals[0]=Point(exactToK(hc->target()->point().x()),exactToK(hc->target()->point().y()),hc->target()->data().height);
                indices[0]=hc->target()->data().triangulation_index;
                globals[0]= Point(exactToK(Globals[indices[0]]->point().x()),exactToK(Globals[indices[0]]->point().y()),Globals[indices[0]]->data().height);
                hc++;
                locals[1]=Point(exactToK(hc->target()->point().x()),exactToK(hc->target()->point().y()),hc->target()->data().height);
                indices[1]=hc->target()->data().triangulation_index;
                globals[1]= Point(exactToK(Globals[indices[1]]->point().x()),exactToK(Globals[indices[1]]->point().y()),Globals[indices[1]]->data().height);
                hc++;
                locals[2]=Point(exactToK(hc->target()->point().x()),exactToK(hc->target()->point().y()),hc->target()->data().height);
                indices[2]=hc->target()->data().triangulation_index;
                globals[2]= Point(exactToK(Globals[indices[2]]->point().x()),exactToK(Globals[indices[2]]->point().y()),Globals[indices[2]]->data().height);
                hc++;
                locals[3]=Point(exactToK(hc->target()->point().x()),exactToK(hc->target()->point().y()),hc->target()->data().height);
                indices[3]=hc->target()->data().triangulation_index;
                globals[3]= Point(exactToK(Globals[indices[3]]->point().x()),exactToK(Globals[indices[3]]->point().y()),Globals[indices[3]]->data().height);
                transformations.push_back(Bivariate3(locals,globals,s));
                (transformations.end()-1)->vst=vst;(transformations.end()-1)->ct=ct;
            }
        }else if(vst==Arrangement2::VerticalScalingType::FACES){
            //s from the faces
            for(Arrangement2::Face_iterator it=mesh.faces_begin();it!=mesh.faces_end();it++){
                if(it->is_unbounded())continue;
                K::FT s=it->data().s;
                Arrangement2::Ccb_halfedge_circulator hc = it->outer_ccb();
                locals[0]=Point(exactToK(hc->target()->point().x()),exactToK(hc->target()->point().y()),hc->target()->data().height);
                indices[0]=hc->target()->data().triangulation_index;
                globals[0]= Point(exactToK(Globals[indices[0]]->point().x()),exactToK(Globals[indices[0]]->point().y()),Globals[indices[0]]->data().height);
                hc++;
                locals[1]=Point(exactToK(hc->target()->point().x()),exactToK(hc->target()->point().y()),hc->target()->data().height);
                indices[1]=hc->target()->data().triangulation_index;
                globals[1]= Point(exactToK(Globals[indices[1]]->point().x()),exactToK(Globals[indices[1]]->point().y()),Globals[indices[1]]->data().height);
                hc++;
                locals[2]=Point(exactToK(hc->target()->point().x()),exactToK(hc->target()->point().y()),hc->target()->data().height);
                indices[2]=hc->target()->data().triangulation_index;
                globals[2]= Point(exactToK(Globals[indices[2]]->point().x()),exactToK(Globals[indices[2]]->point().y()),Globals[indices[2]]->data().height);
                hc++;
                locals[3]=Point(exactToK(hc->target()->point().x()),exactToK(hc->target()->point().y()),hc->target()->data().height);
                indices[3]=hc->target()->data().triangulation_index;
                globals[3]= Point(exactToK(Globals[indices[3]]->point().x()),exactToK(Globals[indices[3]]->point().y()),Globals[indices[3]]->data().height);
                transformations.push_back(Bivariate3(locals,globals,s));
                (transformations.end()-1)->vst=vst;(transformations.end()-1)->ct=ct;
            }
        }else if(vst==Arrangement2::VerticalScalingType::VERTICES){
            K::FT s[4];
            for(Arrangement2::Face_iterator it=mesh.faces_begin();it!=mesh.faces_end();it++){
                if(it->is_unbounded())continue;
                Arrangement2::Ccb_halfedge_circulator hc = it->outer_ccb();
                locals[0]=Point(exactToK(hc->target()->point().x()),exactToK(hc->target()->point().y()),hc->target()->data().height);
                s[0]=hc->target()->data().s;
                indices[0]=hc->target()->data().triangulation_index;
                globals[0]= Point(exactToK(Globals[indices[0]]->point().x()),exactToK(Globals[indices[0]]->point().y()),Globals[indices[0]]->data().height);
                hc++;
                locals[1]=Point(exactToK(hc->target()->point().x()),exactToK(hc->target()->point().y()),hc->target()->data().height);
                s[1]=hc->target()->data().s;
                indices[1]=hc->target()->data().triangulation_index;
                globals[1]= Point(exactToK(Globals[indices[1]]->point().x()),exactToK(Globals[indices[1]]->point().y()),Globals[indices[1]]->data().height);
                hc++;
                locals[2]=Point(exactToK(hc->target()->point().x()),exactToK(hc->target()->point().y()),hc->target()->data().height);
                s[2]=hc->target()->data().s;
                indices[2]=hc->target()->data().triangulation_index;
                globals[2]= Point(exactToK(Globals[indices[2]]->point().x()),exactToK(Globals[indices[2]]->point().y()),Globals[indices[2]]->data().height);
                hc++;
                locals[3]=Point(exactToK(hc->target()->point().x()),exactToK(hc->target()->point().y()),hc->target()->data().height);
                s[3]=hc->target()->data().s;
                indices[3]=hc->target()->data().triangulation_index;
                globals[3]= Point(exactToK(Globals[indices[3]]->point().x()),exactToK(Globals[indices[3]]->point().y()),Globals[indices[3]]->data().height);
                transformations.push_back(Bivariate3(locals,globals,s));
                (transformations.end()-1)->vst=vst;(transformations.end()-1)->ct=ct;
            }
        }
    }
    for(Arrangement2::Vertex_iterator it=mesh.vertices_begin();it!=mesh.vertices_end();it++){
        buffer1.push_back(Point(exactToK(it->point().x()),exactToK(it->point().y()),it->data().height));
    }
}
