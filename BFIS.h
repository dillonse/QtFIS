//
//  BFIS.h
//  OpenGL_CGAL
//
//  Created by Sean Dillon on 3/17/16.
//  Copyright (c) 2016 Sean Dillon. All rights reserved.
//

#ifndef __OpenGL_CGAL__BFIS__
#define __OpenGL_CGAL__BFIS__

#include "FIS.h"
#include "BivariateTransformation.h"
#include "KernelConstructions.h"
class BFIS:public FIS<Point,Bivariate3>{
public:
    BFIS(Arrangement2& meshI,Arrangement2& meshJ);
};


#endif /* defined(__OpenGL_CGAL__BFIS__) */
