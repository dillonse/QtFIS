//
//  BilinearTransformation.h
//  OpenGL_CGAL
//
//  Created by Sean Dillon on 3/16/16.
//  Copyright (c) 2016 Sean Dillon. All rights reserved.
//

#ifndef __OpenGL_CGAL__BivariateTransformation__
#define __OpenGL_CGAL__BivariateTransformation__

#include "KernelConstructions.h"
#include "TransformationObject.h"
/* similar as affine3 , this transfomation covers the bilinear fractal interpolation surfaces and counters all the discontinuity problems
 */
class Bivariate3:public TO{
public:
    Point globals[4];//the gloabal points that define the transformation
    Point locals[4];//the local points that define the transformation
    K::FT s[4];//the vertical scaling factors of the local points, for case of factor per triangle use same factor in all places
    K::FT matrix[3][5]; //the matrix with which we will multiply p to get the transformed point in homogenous form
    Bivariate3(Point Locals[4],Point Globals[4],K::FT SVector[4]);
    Bivariate3(Point Locals[4],Point Globals[4],K::FT s);
    Bivariate3(std::vector<Point> Locals,std::vector<Point> Globals,std::vector<K::FT> SVector);
    Point Transform(Point& p);
};
std::ostream& operator<<(std::ostream& stream,const Bivariate3& a);

class Bivariate2Arrangement{
public:
    Arrangement2::Vertex_handle globals[4];//the gloabal points that define the transformation
    Arrangement2::Vertex_handle locals[4];//the local points that define the transformation
    ExactKernel::FT matrix[2][4]; //the matrix with which we will multiply p to get the transformed point in homogenous form
    Bivariate2Arrangement(Arrangement2::Vertex_handle Locals[4],Arrangement2::Vertex_handle Globals[4]);
    Arrangement2 Transform(Arrangement2& p);
    
};
#endif /* defined(__OpenGL_CGAL__BivariateTransformation__) */
