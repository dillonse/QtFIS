//
//  BoostedEntities.h
//  OpenGL_CGAL
//
//  Created by Sean Dillon on 5/23/15.
//  Copyright (c) 2015 Sean Dillon. All rights reserved.
//

#ifndef __OpenGL_CGAL__BoostedEntities__
#define __OpenGL_CGAL__BoostedEntities__

#include <stdio.h>
//CGAL libs//
#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <CGAL/Point_3.h>
#include <CGAL/Vector_3.h>
#include <CGAL/Point_2.h>
#include <CGAL/Vector_2.h>

//boost//
#include <boost/intrusive/unordered_set.hpp>
#include <boost/intrusive/set.hpp>
/*
typedef CGAL::Exact_predicates_exact_constructions_kernel K;  //kernel
typedef CGAL::Point_3<K>                                  Point;    //3d point
typedef CGAL::Vector_3<K>                                 Vector;   //3d vector
typedef CGAL::Point_2<K>                                  Point2;   //2d point
typedef CGAL::Vector_2<K>                                 Vector2;  //2d vector
class BoostedPoint:public Point,public boost::intrusive::unordered_set_base_hook<>{
private:
    Vector normal;
    public:
        BoostedPoint(){};
        BoostedPoint(Point p):Point(p){;};
        BoostedPoint(K::FT x,K::FT y,K::FT z):Point(x,y,z){};
        BoostedPoint(double x,double y,double z):Point(x,y,z){;};
        BoostedPoint(float x,float y,float z):Point(x,y,z){;};
        BoostedPoint(int x,int y,int z):Point(x,y,z){;};
        ~BoostedPoint(){;};
        boost::intrusive::unordered_set_member_hook<> member_hook_; //This is a member hook
    
        friend bool operator== (const BoostedPoint &a, const BoostedPoint &b)
        {  return ((Point)a) == ((Point)b);  }
    
        friend std::size_t hash_value(const BoostedPoint &value)
        { return std::size_t(CGAL::to_double(value[0])+CGAL::to_double(value[1])+CGAL::to_double(value[2]));}
    void setNormal(Vector normal){this->normal=normal;}
    Vector getNormal(){return normal;}
};


class BoostedPoint2:public Point2,public boost::intrusive::set_base_hook<>,public boost::intrusive::unordered_set_base_hook<>{
private:
    Vector2 normal;
public:
    BoostedPoint2(){};
    BoostedPoint2(Point2 p):Point2(p){;};
    BoostedPoint2(K::FT x,K::FT y):Point2(x,y){;};
    BoostedPoint2(double x,double y):Point2(x,y){;};
    BoostedPoint2(float x,float y):Point2(x,y){;};
    BoostedPoint2(int x,int y):Point2(x,y){;};
    ~BoostedPoint2(){;};
    boost::intrusive::set_member_hook<> set_member_hook_; //This is a member hook
    boost::intrusive::unordered_set_member_hook<> unordered_set_member_hook_;
    
    friend bool operator== (const BoostedPoint2 &a, const BoostedPoint2 &b)
    {  return ((Point2)a) == ((Point2)b);  }
    
    //overide operators to sort them accordint to x member//
    friend bool operator > (const BoostedPoint2 &a, const BoostedPoint2 &b)
    {  return a.x() > b.x();  }
    friend bool operator < (const BoostedPoint2 &a, const BoostedPoint2 &b)
    {  return a.x() < b.x();  }
    
    friend std::size_t hash_value(const BoostedPoint2 &value)
    { return std::size_t(CGAL::to_double(value[0])+CGAL::to_double(value[1]));}
    void setNormal(Vector2 normal){this->normal=normal;}
    Vector2 getNormal(){return normal;}
};*/

#endif /* defined(__OpenGL_CGAL__BoostedEntities__) */