//
//  FIS.h
//  OpenGL_CGAL
//
//  Created by Sean Dillon on 3/16/16.
//  Copyright (c) 2016 Sean Dillon. All rights reserved.
//

#ifndef __OpenGL_CGAL__FIS__
#define __OpenGL_CGAL__FIS__

#include <stdio.h>
#include <vector>
#include "IFS.h"
#include "FractalInterpolationObject.h"
template<typename P,typename T>
class FIS:public FIO<P>{
public:
    std::vector<T> transformations;
    std::vector<P> points;
    std::vector<P> buffer1;
    std::vector<P> buffer2;
    void iterate_deterministic(){
        if(buffer1.empty()){
            IFS::iterate_deterministic(buffer2,transformations,buffer1);
            buffer2.clear();
            points=std::vector<P>(buffer1);
        }
        else{
            IFS::iterate_deterministic(buffer1,transformations,buffer2);
            buffer1.clear();
            points=std::vector<P>(buffer2);
        }
    }
    void iterate_stochastic(){
        if(buffer1.empty()){
            IFS::iterate_stochastic(buffer2,transformations,buffer1);
            buffer2.clear();
            points.insert(points.end(),buffer1.begin(),buffer1.end());
        }
        else{
            IFS::iterate_stochastic(buffer1,transformations,buffer2);
            buffer1.clear();
            points.insert(points.end(),buffer2.begin(),buffer2.end());
        }
    }

    void getPoints(std::vector<P>& output){
        output=points;
    }
    void getPoints(std::vector<std::vector<P>>& output){
        output.push_back(points);
    }

    void getInitialPoints(std::vector<P>& output){
        if(buffer1.empty())output=buffer2;
        else output=buffer1;
    }

    void getTransformations(std::vector<T>& output){
        output=transformations;
    }
};
#endif /* defined(__OpenGL_CGAL__FIS__) */
