//
//  FileReader.cpp
//  OpenGL_CGAL
//
//  Created by Sean Dillon on 1/10/16.
//  Copyright (c) 2016 Sean Dillon. All rights reserved.
//

#include "FileIO.h"
#include "Mesh.h"
#include <fstream>
#include <string>
#include <exception>

bool FileIO::Read(RAFIS &afis, std::string filename){
    std::ifstream file(filename);
    if (!file.is_open()){std::cout<<"could not open file "<<filename<<std::endl;return false;}
    std::string line;
    size_t st,local_st;st=local_st=0;
    char mode='0';
    while(getline(file,line)){
        st=0;
        if(line[1]=='t'){
            mode='t';
        }
        else if(line[1]=='p'&&line[2]=='m'){
            mode='r';
        }
        else if(line[1]=='c'){
            mode='c';
        }
        else if(line[1]=='p'&&line[2]!='m'){
            mode='p';
        }
        else if(mode=='t'){
            std::cout<<"case1"<<std::endl;
            afis.transformations.push_back(Affine3());
            (afis.transformations.end()-1)->vst=Arrangement2::VerticalScalingType::SINGLE;
            for(int i=0;i<2;i++){
                for(int j=0;j<4;j++){
                    (afis.transformations.end()-1)->matrix[i][j]=std::stod(line.substr(st),&local_st);
                    st+=local_st;
                }
                getline(file,line);
                st=0;
            }
            for(int j=0;j<4;j++){
                (afis.transformations.end()-1)->matrix[2][j]=std::stod(line.substr(st),&local_st);
                st+=local_st;
            }
            std::cout<<"end_case1"<<std::endl;
        }
        else if(mode=='p'){
            std::cout<<"case2"<<std::endl;
            double x,y,z;
            static int counter=0;
            afis.buffer1.resize(afis.transformations.size());
            afis.buffer2.resize(afis.transformations.size());
            while(line.substr(st).size()>0){
                x=std::stod(line.substr(st),&local_st);
                st+=local_st;
                y=std::stod(line.substr(st),&local_st);
                st+=local_st;
                z=std::stod(line.substr(st),&local_st);
                st+=local_st;
                afis.buffer1[counter].push_back(Point(x,y,z));
            }
            counter++;
            std::cout<<"end_case2"<<std::endl;
        }
        else if(mode=='c'){
            std::cout<<"case3"<<std::endl;
            for(int i=0;i<afis.transformations.size()-1;i++){
                afis.connection_matrix.push_back(std::vector<bool>());
                for(int j=0;j<afis.transformations.size();j++){
                    std::cout<<"i is "<<i<<" and j is "<<j<<std::endl;
                    afis.connection_matrix[i].push_back((std::stod(line.substr(st),&local_st))>0);
                    st+=local_st;
                }
                getline(file,line);
                st=0;
            }
            afis.connection_matrix.push_back(std::vector<bool>());
            std::cout<<"mid"<<std::endl;
            for(int j=0;j<afis.transformations.size();j++){
                afis.connection_matrix[afis.transformations.size()-1].push_back(std::stod(line.substr(st),&local_st));
                st+=local_st;
            }
            std::cout<<"end_case3"<<std::endl;
        }
    }

}

bool FileIO::Read(AFIS &afis, std::string filename){
    std::ifstream file(filename);
    if (!file.is_open()){std::cout<<"could not open file "<<filename<<std::endl;return false;}
    std::string line;
    size_t st,local_st;st=local_st=0;
    char mode='0';
    while(getline(file,line)){
        st=0;
        if(line[1]=='t'){
            mode='t';
        }
        else if(line[1]=='p'){
            mode='p';
        }
        else if(mode=='t'){
            afis.transformations.push_back(Affine3());
            (afis.transformations.end()-1)->vst=Arrangement2::VerticalScalingType::SINGLE;
            for(int i=0;i<2;i++){
                for(int j=0;j<4;j++){
                    (afis.transformations.end()-1)->matrix[i][j]=std::stod(line.substr(st),&local_st);
                    st+=local_st;
                }
                getline(file,line);
                st=0;
            }
            for(int j=0;j<4;j++){
                (afis.transformations.end()-1)->matrix[2][j]=std::stod(line.substr(st),&local_st);
                st+=local_st;
            }

        }
        else if(mode=='p'){
            double x,y,z;
            x=std::stod(line.substr(st),&local_st);
            st+=local_st;
            y=std::stod(line.substr(st),&local_st);
            st+=local_st;
            z=std::stod(line.substr(st),&local_st);
            afis.buffer1.push_back(Point(x,y,z));
        }
    }

}

void FileIO::Write(Arrangement2& data, std::string filename){
    Arrangement2::VerticalScalingType vst=data.vst;
    Arrangement2::ColoringType ct=data.ct;
    Arrangement2::RecurrentIndexingType rit=data.rit;
    std::ofstream file(filename);
    if (!file.is_open()){std::cout<<"could not open file "<<filename<<std::endl;exit(0);}
    std::vector<std::string> metainfo;
    std::string vinfo,finfo,minfo;
    //based on vertical scaling type
    if(vst==Arrangement2::VerticalScalingType::SINGLE){
        minfo.append("s,");
    }
    else if(vst==Arrangement2::VerticalScalingType::FACES){
        finfo.append("s,");
    }
    else if(vst==Arrangement2::VerticalScalingType::VERTICES){
        vinfo.append("s,");
    }
    //based on coloring type
    if(ct==Arrangement2::ColoringType::CONSISTENT){
        vinfo.append("c,");
    }
    else if(ct==Arrangement2::ColoringType::NONE){
        //do nothing
    }
    //based on recurrent indexing type
    if(rit==Arrangement2::RecurrentIndexingType::FACES){
        finfo.append("i,");
    }
    else if(rit==Arrangement2::RecurrentIndexingType::NONE){
        //do nothing
    }
    //if faces carry some info, use faces , otherwise use edges
    vinfo.insert(0,"#v<");
    vinfo.append("x,y,h>");
    metainfo.push_back(vinfo);
    if(finfo.size()>0){
        finfo.insert(0,"#f<");
        finfo.append("p>");
    }
    else{
        finfo.insert(0,"#e<");
        finfo.append("ps,pt>");
    }
    metainfo.push_back(finfo);
    if(minfo.size()>0){
        minfo.insert(0,"#m<");
        *(minfo.end()-1)='>';
        metainfo.push_back(minfo);
    }
    for(size_t i=0;i<metainfo.size();i++){
        std::cout<<metainfo[i]<<std::endl;
    }
    Write(metainfo,data,file);
}

void FileIO::ValuebleTokens(std::string& metainfo,std::vector<std::string>& val_info){
    bool in_brackets=false;
    std::string token;
    for(std::string::iterator it=metainfo.begin();it!=metainfo.end();it++){
        if((*it)=='>'){
            val_info.push_back(token);
            token.clear();
            in_brackets=false;
        }
        else if(in_brackets){
            if((*it)==','){
                val_info.push_back(token);
                token.clear();
            }
            else{
                token+=*it;
            }
        }
        else if((*it)=='<'){
            in_brackets=true;
        }
    }
}

void FileIO::Write(std::vector<std::string>& metainfo,Arrangement2& data,std::ofstream& file){
    std::vector<Arrangement2::Vertex_handle> vhv;
    for(std::vector<std::string>::iterator it=metainfo.begin();it!=metainfo.end();it++){//for each instruction
        //keep only the valueble tokens
        std::vector<std::string> tokens;
        ValuebleTokens(*it,tokens);
        file<<*it<<std::endl;
        for(size_t i=0;i<tokens.size();i++)std::cout<<tokens[i]<<std::endl;
        //write the info depending on the datatype
        if((*it)[1]=='v'){
            for(Arrangement2::Vertex_iterator vi=data.vertices_begin();vi!=data.vertices_end();vi++){
                for(std::vector<std::string>::iterator it=tokens.begin();it!=tokens.end()-1;it++){
                    if(*it=="x")file<<vi->point().x()<<" ";
                    else if(*it=="y")file<<vi->point().y()<<" ";
                    else if(*it=="h")file<<vi->data().height<<" ";
                    else if(*it=="c")file<<vi->data().triangulation_index<<" ";
                    else if(*it=="s")file<<vi->data().s<<" ";
                }
                if(*(tokens.end()-1)=="x")file<<vi->point().x()<<std::endl;
                else if(*(tokens.end()-1)=="y")file<<vi->point().y()<<std::endl;
                else if(*(tokens.end()-1)=="h")file<<vi->data().height<<std::endl;
                else if(*(tokens.end()-1)=="c")file<<vi->data().triangulation_index<<std::endl;
                else if(*(tokens.end()-1)=="s")file<<vi->data().s<<std::endl;
                vhv.push_back(vi);
            }
        }
        else if((*it)[1]=='e'){
            for(Arrangement2::Edge_iterator ei=data.edges_begin();ei!=data.edges_end();ei++){
                for(std::vector<std::string>::iterator it=tokens.begin();it!=tokens.end()-1;it++){
                    if(*it=="ps")
                        file<<std::distance(vhv.begin(),std::find(vhv.begin(),vhv.end(),ei->source()))<<" ";
                    else if(*it=="pt")
                        file<<std::distance(vhv.begin(),std::find(vhv.begin(),vhv.end(),ei->target()))<<" ";
                }
                if(*(tokens.end()-1)=="ps")
                    file<<std::distance(vhv.begin(),std::find(vhv.begin(),vhv.end(),ei->source()))<<std::endl;
                else if(*(tokens.end()-1)=="pt")
                    file<<std::distance(vhv.begin(),std::find(vhv.begin(),vhv.end(),ei->target()))<<std::endl;
            }
        }
        else if((*it)[1]=='f'){
            for(Arrangement2::Face_iterator fi=data.faces_begin();fi!=data.faces_end();fi++){
                if(fi->is_unbounded())continue;
                for(std::vector<std::string>::iterator it=tokens.begin();it!=tokens.end()-1;it++){
                    if(*it=="p"){
                        Arrangement2::Ccb_halfedge_circulator hc=fi->outer_ccb(),end(hc);end--;
                        do{
                            file<<std::distance(vhv.begin(),std::find(vhv.begin(),vhv.end(),hc->target()))<<" ";
                        }while(++hc!=end);
                    }
                    else if(*it=="s"){
                        file<<fi->data().s<<" ";
                    }
                    else if(*it=="i"){
                        file<<fi->data().index<<" ";
                    }
                }
                if(*(tokens.end()-1)=="p"){
                    Arrangement2::Ccb_halfedge_circulator hc=fi->outer_ccb(),end(hc);end--;//end--;
                    do{
                        file<<std::distance(vhv.begin(),std::find(vhv.begin(),vhv.end(),hc->target()))<<" ";
                    }while(++hc!=end);
                    file<<std::distance(vhv.begin(),std::find(vhv.begin(),vhv.end(),(hc)->target()))<<std::endl;
                }
                else if(*(tokens.end()-1)=="s"){
                    file<<fi->data().s<<std::endl;
                }
                else if(*(tokens.end()-1)=="i"){
                    file<<fi->data().index<<std::endl;
                }
            }
        }
        else if((*it)[1]=='m'){
            for(std::vector<std::string>::iterator it=tokens.begin();it!=tokens.end()-1;it++){
                if(*it=="s")file<<data.unbounded_face()->data().s<<" ";
            }
            if(*(tokens.end()-1)=="s")file<<data.unbounded_face()->data().s<<std::endl;
        }
    }
}

void FileIO::ParseMeta(std::vector<std::string> metainfo,Arrangement2::VerticalScalingType& vst,Arrangement2::ColoringType& ct,Arrangement2::RecurrentIndexingType& rit){
    //decide about enums based on metainfo
    for(std::vector<std::string>::iterator it=metainfo.begin();it!=metainfo.end();it++){
        if((*it)[1]=='v'){
            if(it->find("s")!=std::string::npos)vst=Arrangement2::VerticalScalingType::VERTICES;
            if(it->find("c")!=std::string::npos)ct=Arrangement2::ColoringType::CONSISTENT;
            else ct=Arrangement2::ColoringType::NONE;
        }
        else if((*it)[1]=='e'){
            rit=Arrangement2::RecurrentIndexingType::NONE;
        }
        else if((*it)[1]=='f'){
            if(it->find("s")!=std::string::npos)vst=Arrangement2::VerticalScalingType::FACES;
            if(it->find("i")!=std::string::npos)rit=Arrangement2::RecurrentIndexingType::FACES;
        }
        else if((*it)[1]=='m'){
            if(it->find("s")!=std::string::npos)vst=Arrangement2::VerticalScalingType::SINGLE;
        }
    }

    std::cout<<"VST:"<<(vst==Arrangement2::VerticalScalingType::SINGLE?"SINGLE":(vst==Arrangement2::VerticalScalingType::FACES?"FACES":"VERTICES"))
            <<",CT:"<<(ct==Arrangement2::ColoringType::CONSISTENT?"CONSISTENT":"NONE")
           <<",RIT:"<<(rit==Arrangement2::RecurrentIndexingType::FACES?"FACES":"NONE")<<std::endl;
}


bool FileIO::Read(Arrangement2& data, std::string filename){
    std::ifstream file(filename);
    if (!file.is_open()){std::cout<<"could not open file "<<filename<<std::endl;return false;}
    std::vector<std::string> metainfo;
    if(!Read(metainfo,data,file))return false;
    ParseMeta(metainfo,data.vst,data.ct,data.rit);
    Arrangement2::Face_handle fh=data.faces_begin();
    if(fh->is_unbounded())fh++;
    size_t vertices_num=std::distance(fh->outer_ccb(),--Arrangement2::Ccb_halfedge_circulator(fh->outer_ccb()))+1;
    if(vertices_num==3){
        data.pft=Arrangement2::PrimitiveFaceType::TRIANGULAR;
    }
    else if(vertices_num==4){
        data.pft=Arrangement2::PrimitiveFaceType::RECTANGULAR;
    }
    if(!Mesh::validate(data))return false;
    return true;
}

bool FileIO::Read(std::vector<std::string>& metainfo,Arrangement2& data,std::ifstream& file){
    std::string line;
    std::vector<std::string> tokens;
    size_t st,local_st;st=local_st;
    std::vector<Arrangement2::Vertex_handle> vhv;
    std::vector<std::tuple<int,int,Arrangement2::Halfedge_handle>> ev;
    try
    {
        while(getline(file,line)){
            if(line[0]=='#'){
                metainfo.push_back(line);
                tokens.clear();
                ValuebleTokens(line,tokens);
            }
            else if((*(metainfo.end()-1))[1]=='v'){
                double x,y,h,c,s;x=y=h=c=s=0;
                for(std::vector<std::string>::iterator it=tokens.begin();it!=tokens.end();it++){
                    if(*it=="x")x=std::stod(line.substr(st),&local_st);
                    else if(*it=="y")y=std::stod(line.substr(st),&local_st);
                    else if(*it=="h")h=std::stod(line.substr(st),&local_st);
                    else if(*it=="c")c=std::stod(line.substr(st),&local_st);
                    else if(*it=="s")s=std::stod(line.substr(st),&local_st);
                    st+=local_st;
                }
                Arrangement2::Vertex_handle vh= data.insert_in_face_interior(ExactKernel::Point_2(x,y),data.unbounded_face());
                vh->data().height=h;vh->data().triangulation_index=c;vh->data().s=s;
                vhv.push_back(vh);
            }
            else if((*(metainfo.end()-1))[1]=='e'){
                int ps,pt;ps=pt=0;
                for(std::vector<std::string>::iterator it=tokens.begin();it!=tokens.end();it++){
                    if(*it=="ps")ps=std::stod(line.substr(st),&local_st);
                    else if(*it=="pt")pt=std::stod(line.substr(st),&local_st);
                    st+=local_st;
                }
                data.insert_at_vertices(Segment2(vhv[ps]->point(),vhv[pt]->point()),vhv[ps],vhv[pt]);
            }
            else if((*(metainfo.end()-1))[1]=='f'){
                std::vector<int> p;int i;double s;i=s=0;
                for(std::vector<std::string>::iterator it=tokens.begin();it!=tokens.end();it++){
                    if(*it=="p"){
                        while(st<line.size()){
                            p.push_back(std::stod(line.substr(st),&local_st));st+=local_st;
                        }
                    }
                    else if(*it=="i")i=std::stod(line.substr(st),&local_st);
                    else if(*it=="s")s=std::stod(line.substr(st),&local_st);
                    st+=local_st;
                }
                //add the edges//
                Arrangement2::Halfedge_handle hh;
                p.push_back(*(p.begin()));
                for(std::vector<int>::iterator it=p.begin();it!=p.end()-1;it++){
                    std::vector<std::tuple<int,int,Arrangement2::Halfedge_handle>>::iterator ei;
                    if((ei=std::find_if(ev.begin(),ev.end(),[&it](auto i){return (*it==std::get<0>(i)&&*(it+1)==std::get<1>(i))||(*it==std::get<1>(i)&&*(it+1)==std::get<0>(i));}))
                            ==ev.end()){
                        hh=data.insert_at_vertices(Segment2(vhv[*it]->point(),vhv[*(it+1)]->point()),vhv[*it],vhv[*(it+1)]);
                        ev.push_back(std::make_tuple(*it,*(it+1),hh));
                    }
                    else{
                        if(std::get<0>(*ei)==*it){
                            hh=std::get<2>(*ei);
                        }
                        else{
                            hh=std::get<2>(*ei)->twin();
                        }
                    }
                }
                hh->face()->data().s=s;hh->face()->data().index=i;
            }
            else if((*(metainfo.end()-1))[1]=='m'){
                double s=0;
                for(std::vector<std::string>::iterator it=tokens.begin();it!=tokens.end();it++){
                    if(*it=="s")s=std::stod(line.substr(st),&local_st);
                }
                data.unbounded_face()->data().s=s;
            }
            st=0;
        }
    }
    catch (std::exception& e)
    {
        return false;
    }
    return true;
}

bool FileIO::Read(std::vector<Point>& points,std::string filename){
    std::ifstream file(filename);
    size_t st,local_st;st=local_st=0;
    std::string line;
    if (!file.is_open()){std::cout<<"could not open file "<<filename<<std::endl;return false;}
    try
    {
        while(std::getline(file,line)){
            double x=std::stod(line.substr(st),&local_st);st+=local_st;
            double y=std::stod(line.substr(st),&local_st);st+=local_st;
            double z=std::stod(line.substr(st),&local_st);
            points.push_back(Point(x,y,z));
            st=0;
        }
    }
    catch (std::exception& e)
    {
        return false;
    }
    return true;
}


void FileIO::Write(std::vector<std::vector<Point>>& data,std::string filename){
    std::ofstream file(filename);
    if(!file.is_open()){std::cout<<"problem with file "<<filename<<std::endl;exit(0);}
    for(std::vector<std::vector<Point>>::iterator it= data.begin();it!=data.end();it++){
        for(std::vector<Point>::iterator itt=it->begin();itt!=it->end();itt++){
            file<<*itt<<std::endl;
        }
    }
}
