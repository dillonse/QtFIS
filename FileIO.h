//
//  FileReader.h
//  OpenGL_CGAL
//
//  Created by Sean Dillon on 1/10/16.
//  Copyright (c) 2016 Sean Dillon. All rights reserved.
//

#ifndef __OpenGL_CGAL__FileReader__
#define __OpenGL_CGAL__FileReader__

#include <stdio.h>
#include <string>
#include "KernelConstructions.h"
#include "FIS.h"
#include "RFIS.h"
#include "AFIS.h"
#include "RAFIS.h"


class FileIO{
private:
    static void ParseMeta(std::vector<std::string> metainfo,Arrangement2::VerticalScalingType&,Arrangement2::ColoringType&,Arrangement2::RecurrentIndexingType&);
    static void ValuebleTokens(std::string& metainfo,std::vector<std::string>& val_info);
    static void Write(std::vector<std::string>& metainfo,Arrangement2& data,std::ofstream& file);
    static bool Read(std::vector<std::string>& metainfo,Arrangement2& data,std::ifstream& file);
public:
    static void Write(Arrangement2& data,std::string filename);
    static bool Read(Arrangement2& data,std::string filename);
    static bool Read(std::vector<Point>& points,std::string filename);
    static void Write(std::vector<std::vector<Point>>& data,std::string filename);
    static bool Read(AFIS& afis,std::string filename);
    static bool Read(RAFIS &bfis, std::string filename);
    template<typename P,typename T> static void Write(FIS<P,T>& data,std::string filename){
        std::ofstream file(filename);
        if (!file.is_open()){std::cout<<"could not open file "<<filename<<std::endl;}
        //get the transformations
        std::vector<T> transformations;
        data.getTransformations(transformations);
        //get the points
        std::vector<P> points;
        data.getInitialPoints(points);
        //write the transformations first
        file<<"#t<>"<<std::endl;
        if(transformations.size()>0){
            for(typename std::vector<T>::iterator it=transformations.begin();it!=transformations.end();it++){
                file<<*it<<std::endl;
            }
        }
        //write the points
        file<<"#p<>"<<std::endl;
        if(points.size()>0){
            for(typename std::vector<P>::iterator it = points.begin();it!=points.end()-1;it++){
                file<<*it<<",";
            }
            file<<*(points.end()-1)<<std::endl;
        }
    }

    template<typename P,typename T> static void Write(RFIS<P,T>& data,std::string filename){
        std::ofstream file(filename);
        if (!file.is_open()){std::cout<<"could not open file "<<filename<<std::endl;}
        //get the transformations
        std::vector<T> transformations;
        data.getTransformations(transformations);
        //get the points
        std::vector<std::vector<P>> points;
        data.getInitialPoints(points);
        //get the connection matrix
        std::vector<std::vector<bool>> connection_matrix;
        data.getConnectionMatrix(connection_matrix);
        //get the probability matrix
        std::vector<std::vector<float>> probability_matrix;
        data.getProbabilityMatrix(probability_matrix);
        //write the transformations first
        file<<"#t<>"<<std::endl;
        if(transformations.size()>0){
            for(typename std::vector<T>::iterator it=transformations.begin();it!=transformations.end();it++){
                file<<*it<<std::endl;
            }
        }

        //write the connection matrix next
        file<<"#cm<>"<<std::endl;
        if(connection_matrix.size()>0){
            for(std::vector<std::vector<bool>>::iterator it=connection_matrix.begin();it!=connection_matrix.end();it++){
                for(std::vector<bool>::iterator itt=it->begin();itt!=it->end()-1;itt++){
                    file<<*itt<<",";
                }
                file<<*(it->end()-1)<<std::endl;
            }
        }
        //write the probability matrix next
        file<<"#pm<>"<<std::endl;
        if(probability_matrix.size()>0){
            for(std::vector<std::vector<float>>::iterator it=probability_matrix.begin();it!=probability_matrix.end();it++){
                for(std::vector<float>::iterator itt=it->begin();itt!=it->end()-1;itt++){
                    file<<*itt<<",";
                }
                file<<*(it->end()-1)<<std::endl;
            }
        }

        //write the points
        file<<"#p<>"<<std::endl;
        if(points.size()>0){
            for(typename std::vector<std::vector<P>>::iterator it=points.begin();it!=points.end();it++){
                for(typename std::vector<P>::iterator itt = it->begin();itt!=it->end()-1;itt++){
                    file<<*itt<<",";
                }
                file<<*(it->end()-1)<<std::endl;
            }
        }
    }

};

#endif /* defined(__OpenGL_CGAL__FileReader__) */
