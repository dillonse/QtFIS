//
//  FractalInterpolationObject.h
//  OpenGL_CGAL
//
//  Created by Sean Dillon on 4/13/16.
//  Copyright (c) 2016 Sean Dillon. All rights reserved.
//

#ifndef OpenGL_CGAL_FractalInterpolationObject_h
#define OpenGL_CGAL_FractalInterpolationObject_h

#include "KernelConstructions.h"
#include "TransformationObject.h"
template<typename P>
class FIO{
public:
    virtual void iterate_deterministic()=0;
    virtual void iterate_stochastic()=0;
    virtual void getPoints(std::vector<P>&)=0;
    virtual void getPoints(std::vector<std::vector<P>>&)=0;
    Arrangement2::VerticalScalingType vst;
    Arrangement2::ColoringType ct;
    Arrangement2::RecurrentIndexingType rit;
};

#endif
