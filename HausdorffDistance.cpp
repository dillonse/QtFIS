//
//  HausdorffDistance.cpp
//  OpenGL_CGAL
//
//  Created by Sean Dillon on 1/8/16.
//  Copyright (c) 2016 Sean Dillon. All rights reserved.
//

#include "HausdorffDistance.h"
#include "TimePerformance.h"


HausdorffDistance::HausdorffDistance(std::vector<Point> setA,std::vector<Point> setB,bool tA,bool tB){
    if(setB.size()==0&&setA.size()==0)return;
    //create the triangulations
    if(tA){
        //insert to delaunay while normalizing
        dtA = new Delaunay3();
        dtA->insert(setA.begin(),setA.end());
    }
    else{
        //insert to vector while normalizing
        this->vA = new std::vector<Point>(setA);
    }
    
    if(tB){
        //insert to delaunay while normalizing
        dtB = new Delaunay3();
        dtB->insert(setB.begin(),setB.end());
    }
    else{
        //insert to vector while normalizing
        this->vB = new std::vector<Point>(setB);
    }
}

HausdorffDistance::~HausdorffDistance(){
    if(vA!=NULL)delete vA;
    if(vB!=NULL)delete vB;
    if(dtA!=NULL)delete dtA;
    if(dtB!=NULL)delete dtB;
}

void HausdorffDistance::setA(std::vector<Point> v,bool triangulate){
    if(vA!=NULL)delete vA;
    if(dtA!=NULL)delete dtA;
    vA=NULL;dtA=NULL;
    //K::Iso_cuboid_3 c3= CGAL::bounding_box(v.begin(),v.end());
    if(triangulate){
        dtA=new Delaunay3();
        dtA->insert(v.begin(),v.end());
        //for(std::vector<Point>::iterator it=v.begin();it!=v.end();it++)
          //  dtA->insert(Point(it->x(),it->y(),it->z()));
           // dtA->insert(Point((it->x()-c3.xmin())/(c3.xmax()-c3.xmin()),(it->y()-c3.ymin())/(c3.ymax()-c3.ymin()),(it->z()-c3.zmin())/(c3.zmax()-c3.zmin())));
    }
    else{
        vA=new std::vector<Point>();
        for(std::vector<Point>::iterator it=v.begin();it!=v.end();it++)
            vA->push_back(Point(it->x(),it->y(),it->z()));
            //vA->push_back(Point((it->x()-c3.xmin())/(c3.xmax()-c3.xmin()),(it->y()-c3.ymin())/(c3.ymax()-c3.ymin()),(it->z()-c3.zmin())/(c3.zmax()-c3.zmin())));
        
    }
    distanceAB=-1;distanceBA=-1;distanceH=-1;
}

void HausdorffDistance::setB(std::vector<Point> v,bool triangulate){
    if(vB!=NULL){delete vB;vB=NULL;}
    if(dtB!=NULL){delete dtB;dtB=NULL;}
  //  K::Iso_cuboid_3 c3= CGAL::bounding_box(v.begin(),v.end());
    if(triangulate){
        dtB=new Delaunay3();
        dtB->insert(v.begin(),v.end());
        //for(std::vector<Point>::iterator it=v.begin();it!=v.end();it++)
          //  dtB->insert(Point(it->x(),it->y(),it->z()));
            //dtB->insert(Point((it->x()-c3.xmin())/(c3.xmax()-c3.xmin()),(it->y()-c3.ymin())/(c3.ymax()-c3.ymin()),(it->z()-c3.zmin())/(c3.zmax()-c3.zmin())));
    }
    else{
        vB=new std::vector<Point>();
        for(std::vector<Point>::iterator it=v.begin();it!=v.end();it++)
            vB->push_back(Point(it->x(),it->y(),it->z()));
            //vB->push_back(Point((it->x()-c3.xmin())/(c3.xmax()-c3.xmin()),(it->y()-c3.ymin())/(c3.ymax()-c3.ymin()),(it->z()-c3.zmin())/(c3.zmax()-c3.zmin())));
    }
    distanceAB=-1;distanceBA=-1;distanceH=-1;
}


K::FT HausdorffDistance::DistanceAtoB(){
    if(distanceAB!=-1)return distanceAB; //use ready result
    K::FT candidate=0.0;
    //int counter=0;
    if(dtA!=NULL){
        for(Delaunay3::Finite_vertices_iterator it=dtA->finite_vertices_begin();it!=dtA->finite_vertices_end();it++){//for each vertex of A
            //find the distance of the vertex from its nearest neighbour in B (min dist)
            candidate=NNDistancePtoB(it->point()); //CGAL::squared_distance(it->point(),(B.nearest_vertex(it->point()))->point());
            if(candidate>distanceAB)distanceAB=candidate;
          //  std::cout<<"\r"<<counter++<<"/65536";
        }
       // std::cout<<std::endl;
    }
    else if(vA!=NULL){
        for(size_t i=0;i<vA->size();i++){
            candidate=NNDistancePtoB((*vA)[i]);
            if(candidate>distanceAB)distanceAB=candidate;
        }
    }
    return distanceAB;
}

K::FT HausdorffDistance::DistanceBtoA(){
    if(distanceBA!=-1)return distanceBA; //use ready result
    K::FT candidate=0.0;
   // int counter=0;
    if(dtB!=NULL){
        for(Delaunay3::Finite_vertices_iterator it=dtB->finite_vertices_begin();it!=dtB->finite_vertices_end();it++){//for each vertex of A
            //find the distance of the vertex from its nearest neighbour in B (min dist)
            candidate=NNDistancePtoA(it->point()); //CGAL::squared_distance(it->point(),(B.nearest_vertex(it->point()))->point());
            if(candidate>distanceBA)distanceBA=candidate;
            // std::cout<<"\r"<<counter++<<"/65536";
        }
        //std::cout<<std::endl;
    }
    else if(vB!=NULL){
        for(size_t i=0;i<vB->size();i++){
            candidate=NNDistancePtoA((*vB)[i]);
            if(candidate>distanceBA)distanceBA=candidate;
        }
    }
    return distanceBA;
}

K::FT HausdorffDistance::DistanceAB(){
    if(distanceH!=-1)return distanceH;
    if(distanceAB==-1)DistanceAtoB();
    if(distanceBA==-1)DistanceBtoA();
    distanceH=CGAL::max(distanceAB,distanceBA);
    return distanceH;
}

K::FT HausdorffDistance::MDistanceAtoB(){
    distanceAB=0.0;
    //int counter=0;
    if(dtA!=NULL){
        for(Delaunay3::Finite_vertices_iterator it=dtA->finite_vertices_begin();it!=dtA->finite_vertices_end();it++){//for each vertex of A
            //find the distance of the vertex from its nearest neighbour in B (min dist)
            distanceAB+=(NNDistancePtoB(it->point())); //CGAL::squared_distance(it->point(),(B.nearest_vertex(it->point()))->point());
        }
        distanceAB /=(double)(std::distance(dtA->finite_vertices_begin(),dtA->finite_vertices_end()));
        // std::cout<<std::endl;
    }
    else if(vA!=NULL){
        for(size_t i=0;i<vA->size();i++){
            distanceAB+=NNDistancePtoB((*vA)[i])/(double)(vA->size());
        }
    }
    return distanceAB;
    
}

K::FT HausdorffDistance::MDistanceBtoA(){
    distanceBA=0.0;
    if(dtB!=NULL){
        for(Delaunay3::Finite_vertices_iterator it=dtB->finite_vertices_begin();it!=dtB->finite_vertices_end();it++){
            //for each vertex of A
            //find the distance of the vertex from its nearest neighbour in B (min dist)
            distanceBA+=(NNDistancePtoA(it->point()));
        }
        distanceBA/=(double)(std::distance(dtB->finite_vertices_begin(),dtB->finite_vertices_end()));
    }
    else if(vB!=NULL){
        for(size_t i=0;i<vB->size();i++){
            distanceBA+=NNDistancePtoA((*vB)[i])/(double)(vB->size());
        }
    }
    return distanceBA;
}

K::FT HausdorffDistance::MDistanceAB(){
    if(distanceAB==-1)distanceAB=MDistanceAtoB();
    if(distanceBA==-1) distanceBA=MDistanceBtoA();
    distanceH=CGAL::max(distanceAB,distanceBA);
    return distanceH;
}



K::FT HausdorffDistance::NNDistancePtoB(Point& qpoint){
    if(dtB!=NULL){
        return CGAL::squared_distance(qpoint,dtB->nearest_vertex(qpoint)->point());
    }
    else if(vB!=NULL){
        K::FT distance=std::numeric_limits<double>::max();
        K::FT candidate;
        for(size_t i=0;i<vB->size();i++){
            candidate= CGAL::squared_distance(qpoint,(*vB)[i]);
            if(candidate<distance)distance=candidate;
        }
        return distance;
    }
    return 0;
}

K::FT HausdorffDistance::NNDistancePtoA(Point& qpoint){
    if(dtA!=NULL){
        return CGAL::squared_distance(qpoint,dtA->nearest_vertex(qpoint)->point());
    }
    else if(vA!=NULL){
        K::FT distance=std::numeric_limits<double>::max();
        K::FT candidate;
        for(size_t i=0;i<vA->size();i++){
            candidate=CGAL::squared_distance(qpoint,(*vA)[i]);
            if(candidate<distance)distance=candidate;
        }
        return distance;
    }
    return 0;
}


