//
//  HausdorffDistance.h
//  OpenGL_CGAL
//
//  Created by Sean Dillon on 1/8/16.
//  Copyright (c) 2016 Sean Dillon. All rights reserved.
//

#ifndef __OpenGL_CGAL__HausdorffDistance__
#define __OpenGL_CGAL__HausdorffDistance__

#include "KernelConstructions.h"
#include <CGAL/Delaunay_triangulation_3.h>
#include <CGAL/squared_distance_3.h>
#include <CGAL/squared_distance_2.h>

typedef CGAL::Delaunay_triangulation_3<K, CGAL::Fast_location> Delaunay3;

class HausdorffDistance{
private:
    //data
    Delaunay3* dtA=NULL;
    std::vector<Point>* vA=NULL;
    Delaunay3* dtB=NULL;
    std::vector<Point>* vB=NULL;
    K::FT distanceAB=-1;
    K::FT distanceBA=-1;
    K::FT distanceH=-1;
    //functions
    K::FT NNDistancePtoB(Point& qpoint);
    K::FT NNDistancePtoA(Point& qpoint);
public:
    HausdorffDistance(std::vector<Point> setA,std::vector<Point> setB,bool triangulateA=false,bool triangulateB=false);
    ~HausdorffDistance();
    void setA(std::vector<Point> v,bool triangulate=false);
    void setB(std::vector<Point> v,bool triangulate=false);
    K::FT DistanceAtoB();
    K::FT DistanceBtoA();
    K::FT DistanceAB();
    K::FT MDistanceAtoB();
    K::FT MDistanceBtoA();
    K::FT MDistanceAB();
};

#endif /* defined(__OpenGL_CGAL__HausdorffDistance__) */
