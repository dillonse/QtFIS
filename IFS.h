//
//  IFS.h
//  OpenGL_CGAL
//
//  Created by Sean Dillon on 3/4/16.
//  Copyright (c) 2016 Sean Dillon. All rights reserved.
//

#ifndef __OpenGL_CGAL__IFS__
#define __OpenGL_CGAL__IFS__

#include <vector>
#include "Random.h"

class IFS{
public:
    template <class P,class T>
    static void iterate_deterministic(typename std::vector<P>& points,typename std::vector<T>& transformations,std::vector<P>& result){
        for(typename std::vector<P>::iterator it=points.begin();it!=points.end();it++){ //for every point in the points dataset
            for(typename std::vector<T>::iterator itt=transformations.begin();itt!=transformations.end();itt++){
                result.push_back(itt->Transform(*it));
            }
        }
    }
    template <class P,class T>
    static void iterate_stochastic(typename std::vector<P>& points,typename std::vector<T>& transformations,std::vector<P>& result){
        for(typename std::vector<P>::iterator it=points.begin();it!=points.end();it++){ //for every point in the points dataset
            int dice_index=dice::throw_range(0,int(transformations.size()-1));
            result.push_back(transformations[dice_index].Transform(*it));
        }
    }
};


#endif /* defined(__OpenGL_CGAL__IFS__) */
