//
//  IFS1D.cpp
//  OpenGL_CGAL
//
//  Created by Sean Dillon on 12/26/15.
//  Copyright (c) 2015 Sean Dillon. All rights reserved.
//

#include "IFS1D.h"

//Fractal Interpolation 2D//
FractalInterpolation2D::FractalInterpolation2D(Point2* pointsArray,int pointsArraySize,K::FT* verticalScalingVector,int verticalScalingVectorLength,int rec_depth){
    //initialize the interpolation data//
    this->recursionDepth=rec_depth;
    this->interpolantsCount=pointsArraySize;
    for(int i=1;i<=this->recursionDepth;i++){
        this->interpolantsCount+=(pointsArraySize-2)*pow(this->transformationsCount,i);
    }
    interpolation_data = new Point2[this->interpolantsCount];
    //initialize the interpolation data array//
    for(int i=0;i<pointsArraySize;i++){
        interpolation_data[this->interpolantsIndex] = pointsArray[i];
        this->interpolantsIndex++;
    }
    
    //initialize the transformation data//
    this->transformationsCount  = pointsArraySize-1;
    //initialize the transformation data array//
    w = new AffineTransformation2D[this->transformationsCount];
    Point2 P0 = pointsArray[0];
    Point2 PN = pointsArray[pointsArraySize-1];
    for(int i=0;i<transformationsCount;i++){
        w[i].a = (pointsArray[i+1].x() - pointsArray[i].x())/(PN.x()-P0.x());
        w[i].b = 0;
        w[i].c = ((pointsArray[i+1].y() - pointsArray[i].y()) -(verticalScalingVector[i]*(PN.y()-P0.y())))/(PN.x()-P0.x());
        w[i].s = verticalScalingVector[i];
        w[i].d = (PN.x()*pointsArray[i].x() - P0.x()*pointsArray[i+1].x())/(PN.x()-P0.x());
        w[i].e = ((PN.x()*pointsArray[i].y()-P0.x()*pointsArray[i+1].y()) -verticalScalingVector[i]*(PN.x()*P0.y()-P0.x()*PN.y()))/(PN.x()-P0.x());
    }
}

FractalInterpolation2D::~FractalInterpolation2D(){
    delete[] w;
    delete[] interpolation_data;
}

void FractalInterpolation2D::Interpolate(){
    int base=0;
    int limit=this->interpolantsIndex;
    if(this->recursionDepth>0){
        for(int j=0;j<this->transformationsCount;j++){
            for(int k=base+1;k<limit-1;k++){
                this->interpolation_data[this->interpolantsIndex] = w[j].transformPoint(this->interpolation_data[k]);
                this->interpolantsIndex++;
            }
        }
    }
    base=limit;
    for(int i=1;i<this->recursionDepth;i++){
        limit = this->interpolantsIndex;
        for(int j=0;j<this->transformationsCount;j++){
            for(int k=base;k<limit;k++){
                this->interpolation_data[this->interpolantsIndex] = w[j].transformPoint(this->interpolation_data[k]);
                this->interpolantsIndex++;
            }
        }
        base=limit;
    }
}

void FractalInterpolation2D::RenderImmediate(){
    glBegin(GL_LINE_STRIP);
    glColor3f(0.0,0.0,0.0);
    for (int i=0;i<this->interpolantsCount;i++){
        glVertex2d(GLdouble(CGAL::to_double(this->interpolation_data[i].x())),GLdouble(CGAL::to_double(this->interpolation_data[i].y())));
    }
    glEnd();
}

void FractalInterpolation2D::RenderVBO(){
    glColor3f(0.0,0.0,0.0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,VBO_indexID);
    glDrawArrays(GL_LINE_STRIP,0,this->interpolantsCount);
}

void FractalInterpolation2D::InitVBO(){
    SetBuffers();
    BindBuffers();
    IVBO_setup=true;
}


void FractalInterpolation2D::SetBuffers(){
    //no normals in 2D//
    //the points are deduplicated and drawed once per point so no need for index//
    //point buffer from point pool//
    points = new GLfloat[this->interpolantsCount*2];                      //allocate the points buffer//
    for(int i=0;i<this->interpolantsCount;i++){
        points[i*2] = CGAL::to_double(interpolation_data[i].x());
        points[i*2+1] = CGAL::to_double(interpolation_data[i].y());
    }
}

void FractalInterpolation2D::BindBuffers(){
    //memory calculation//
    size_t points_size = this->interpolantsCount*2*sizeof(GLfloat); //get the mem size of the VBO buffer for points//
    
    //buffers generation
    glGenBuffers(1,&VBO_pointsID);                                  //generate the buffer and return its id//
    
    //buffers binding
    glBindBuffer(GL_ARRAY_BUFFER,VBO_pointsID);                 //bind the buffer with the vbo id//
    glBufferData(GL_ARRAY_BUFFER,points_size,&(points[0]),GL_STATIC_DRAW);//set the binded buffer with the points buffer//
    
    //buffers activation//
    glEnableClientState(GL_VERTEX_ARRAY);                   //Activate the points buffer//
    glBindBuffer(GL_ARRAY_BUFFER,VBO_pointsID);
    glVertexPointer(2, GL_FLOAT, 0, 0);                     // Stride of 2 GLfloats//
}

void FractalInterpolation2D::DelBuffers(){
    //delete the buffers//
    glDeleteBuffers(1,&VBO_pointsID);
    //set the IVBO setup flag to false//
    IVBO_setup=false;
}

