//
//  IFS1D.h
//  OpenGL_CGAL
//
//  Created by Sean Dillon on 12/26/15.
//  Copyright (c) 2015 Sean Dillon. All rights reserved.
//

#ifndef __OpenGL_CGAL__IFS1D__
#define __OpenGL_CGAL__IFS1D__

#include "AffineTransforms.h"
#include "KernelConstructions.h"
#include "OGL.h"

class FractalInterpolation2D{
private:
    //data components//
    AffineTransformation2D* w=NULL; //array with the transformations
    Point2* interpolation_data;           //the set of points from interpolation
    int recursionDepth = 4;                //how many times we subdivide the intervals
    int interpolantsIndex = 0;              //the index of the last calculated element in the interpolants array//
    int interpolantsCount=0;              //the count of the interpolants after the interpolation//
    int transformationsCount=0;           //the count of the transformations w[i]//
    //rendering components//
    bool IVBO_setup=false;
    GLuint VBO_pointsID;
    GLuint VBO_indexID;
    GLuint VBO_normalsID;
    GLfloat* points;
    void SetBuffers();
    void BindBuffers();
    void DelBuffers();
    
public:
    //data components//
    FractalInterpolation2D(){;}//for array allocations//
    FractalInterpolation2D(Point2* pointArray,int pointArrayLength,K::FT* verticalScalingVector,int vsvLength,int rec_depth);
    ~FractalInterpolation2D();
    void Interpolate(); //create the set of points//
    //rendering components//
    void InitVBO();         //initialize the VBO rendering data//
    void RenderImmediate();      //render the points in immediate mode//
    void RenderVBO();           //render the points in VBO mode//
};



#endif /* defined(__OpenGL_CGAL__IFS1D__) */
