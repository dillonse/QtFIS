//
//  IFS2D.cpp
//  OpenGL_CGAL
//
//  Created by Sean Dillon on 12/26/15.
//  Copyright (c) 2015 Sean Dillon. All rights reserved.
//

#include "IFS2D.h"

std::pair<std::set<Point>::iterator,bool> ret;

/**********CONSTRUCTOR*****************************/
/**************************************************/

FractalInterpolation3D::FractalInterpolation3D(Point* pointsArray,int pointsArraySize,K::FT* verticalScalingVector,int rec_depth){
    this->transformationsCount  = pow(sqrt(2*pointsArraySize+0.25)-1.5,2);
    //initialize other data//
    //    this->initial_points_count=pointsArraySize;
    this->interpolantsIndex=pointsArraySize;
    //initialize interpolation data//
    this->recursionDepth=rec_depth;
    if(rec_depth==0)this->interpolantsCount=3;
    else this->interpolantsCount=pointsArraySize*((pow(this->transformationsCount,rec_depth)-1)/(this->transformationsCount-1));
    this->interpolation_data.reserve(this->transformationsCount); //= new Point*[this->transformationsCount];
    this->frame.reserve(this->transformationsCount);
    for(int i=0;i<this->transformationsCount;i++){
        this->interpolation_data.push_back(std::vector<Point>());
        this->interpolation_data[i].reserve(this->interpolantsCount); //= new Point[this->interpolantsCount];
        this->frame.push_back(std::vector<std::pair<unsigned int,unsigned int>>());
        for(int j=0;j<this->transformationsCount;j++){
            
            this->frame[i].push_back(std::make_pair(0,0));
        }
    }
    //initialize the transformation data//
    //initialize the transformation data array//
    this->w = new AffineTransformation3D[this->transformationsCount];
    this->indexes_local= new unsigned short*[this->transformationsCount];
    this->indexes_global= new unsigned short*[this->transformationsCount];
    for(int i=0;i<this->transformationsCount;i++){
        this->indexes_local[i] = new unsigned short[3];
        this->indexes_global[i] = new unsigned short[3];
    }
    //initialize the vsv and the initial points//
    this->vsv = new K::FT[pointsArraySize];
    this->initial_points.reserve(pointsArraySize); //= new Point[pointsArraySize];
    for(int i=0;i<pointsArraySize;i++){
        this->vsv[i]=verticalScalingVector[i];
        this->initial_points.push_back(pointsArray[i]);//=pointsArray[i];
    }
    //parse the array as a flatten 2D array//
    int G1_index=0;
    int G2_index=pointsArraySize-(int)(sqrt(2*pointsArraySize+0.25)-0.5);
    int G3_index=pointsArraySize-1;
    Point G1 = pointsArray[0]; //global up
    Point G3 = pointsArray[pointsArraySize-1];// global right
    Point G2=pointsArray[pointsArraySize-(int)(sqrt(2*pointsArraySize+0.25)-0.5)];//global down
    Point LU,LR,LD,GU,GR,GD;//local up,right,down
    K::FT SU,SD,SR;
    std::vector<K::FT> lamdas;
    int limit=1;
    int k=(int)(sqrt(2*pointsArraySize+0.25)-1.5);
    int horizontal_index;
    int vertical_index=1;
    int vertical_index_up=0;
    int row_count=0;
    for(int i=0;i<k;i++){
        horizontal_index=0;
        for(int j=0;j<limit;j++){
            
            switch ((row_count+horizontal_index)%3){
                case 0:
                    GU = G1;
                    GD = G2;
                    GR = G3;
                    indexes_global[i*i+j][0]=G1_index;
                    indexes_global[i*i+j][1]=G2_index;
                    indexes_global[i*i+j][2]=G3_index;
                    break;
                case 1:
                    GU = G2;
                    GD = G3;
                    GR = G1;
                    indexes_global[i*i+j][0]=G2_index;
                    indexes_global[i*i+j][1]=G3_index;
                    indexes_global[i*i+j][2]=G1_index;
                    break;
                case 2:
                    GU = G3;
                    GD = G1;
                    GR = G2;
                    indexes_global[i*i+j][0]=G3_index;
                    indexes_global[i*i+j][1]=G1_index;
                    indexes_global[i*i+j][2]=G2_index;
                    break;
                default:
                    break;
            }
            if(j%2==0){
                //adjusted position
                LU=pointsArray[vertical_index_up+horizontal_index];
                LD=pointsArray[vertical_index+horizontal_index];
                LR = pointsArray[vertical_index+horizontal_index+1];
                SU = this->vsv[vertical_index_up+horizontal_index];
                SD = this->vsv[vertical_index+horizontal_index];
                SR = this->vsv[vertical_index+horizontal_index+1];
                indexes_local[i*i+j][0]=vertical_index_up+horizontal_index;
                indexes_local[i*i+j][1]=vertical_index+horizontal_index;
                indexes_local[i*i+j][2]=vertical_index+horizontal_index+1;
            }
            else{
                //reversed position
                LU=pointsArray[vertical_index_up+horizontal_index];
                LD=pointsArray[vertical_index_up+horizontal_index+1];
                LR = pointsArray[vertical_index+horizontal_index+1];
                SU = this->vsv[vertical_index_up+horizontal_index];
                SD = this->vsv[vertical_index_up+horizontal_index+1];
                SR = this->vsv[vertical_index+horizontal_index+1];
                indexes_local[i*i+j][0]=vertical_index_up+horizontal_index;
                indexes_local[i*i+j][1]=vertical_index_up+horizontal_index+1;
                indexes_local[i*i+j][2]=vertical_index+horizontal_index+1;
                horizontal_index++;
            }
            //calculate their transformation matrix (x-y)//
            //x
            w[i*i+j].a= -(GD.y()*LR.x() - GR.y()*LD.x() - GD.y()*LU.x() + GU.y()*LD.x() + GR.y()*LU.x() - GU.y()*LR.x())/(GD.x()*GR.y() - GD.y()*GR.x() - GD.x()*GU.y() + GD.y()*GU.x() + GR.x()*GU.y() - GR.y()*GU.x());
            w[i*i+j].b=(GD.x()*LR.x() - GR.x()*LD.x() - GD.x()*LU.x() + GU.x()*LD.x() + GR.x()*LU.x() - GU.x()*LR.x())/(GD.x()*GR.y() - GD.y()*GR.x() - GD.x()*GU.y() + GD.y()*GU.x() + GR.x()*GU.y() - GR.y()*GU.x());
            w[i*i+j].c=0;
            w[i*i+j].j=(GD.x()*GR.y()*LU.x() - GD.x()*GU.y()*LR.x() - GD.y()*GR.x()*LU.x() + GD.y()*GU.x()*LR.x() + GR.x()*GU.y()*LD.x() - GR.y()*GU.x()*LD.x())/(GD.x()*GR.y() - GD.y()*GR.x() - GD.x()*GU.y() + GD.y()*GU.x() + GR.x()*GU.y() - GR.y()*GU.x());
            //y
            w[i*i+j].d=-(GD.y()*LR.y() - GR.y()*LD.y() - GD.y()*LU.y() + GU.y()*LD.y() + GR.y()*LU.y() - GU.y()*LR.y())/(GD.x()*GR.y() - GD.y()*GR.x() - GD.x()*GU.y() + GD.y()*GU.x() + GR.x()*GU.y() - GR.y()*GU.x());
            w[i*i+j].e=(GD.x()*LR.y() - GR.x()*LD.y() - GD.x()*LU.y() + GU.x()*LD.y() + GR.x()*LU.y() - GU.x()*LR.y())/(GD.x()*GR.y() - GD.y()*GR.x() - GD.x()*GU.y() + GD.y()*GU.x() + GR.x()*GU.y() - GR.y()*GU.x());
            w[i*i+j].f=0;
            w[i*i+j].k=(GD.x()*GR.y()*LU.y() - GD.x()*GU.y()*LR.y() - GD.y()*GR.x()*LU.y() + GD.y()*GU.x()*LR.y() + GR.x()*GU.y()*LD.y() - GR.y()*GU.x()*LD.y())/(GD.x()*GR.y() - GD.y()*GR.x() - GD.x()*GU.y() + GD.y()*GU.x() + GR.x()*GU.y() - GR.y()*GU.x());
        }
        vertical_index_up=vertical_index;
        vertical_index+=horizontal_index+2;
        limit+=2;
        row_count++;
    }
}

/**********DESTRUCTOR*****************************/
/**************************************************/


FractalInterpolation3D::~FractalInterpolation3D(){
    //delete the interpolation components//
    ClearData();
}

void FractalInterpolation3D::ClearData(){
    for(int i=0;i<this->transformationsCount;i++){
        if(indexes_local!=NULL) delete[] indexes_local[i];
        if(indexes_global!=NULL) delete[] indexes_global[i];
      //  if(interpolation_data!=NULL) delete[] interpolation_data[i];
    }
    delete[] w;
   // delete[] initial_points;
    delete[] vsv;
    indexes_local=NULL;
    indexes_global=NULL;
   // interpolation_data=NULL;
    w=NULL;
 //   initial_points=NULL;
    vsv=NULL;
}

/**********ACCESSORS*********************************/
/****************************************************/

void FractalInterpolation3D::getPoints(std::vector<Point>& container){
  //   if(this->interpolation_data!=NULL){
    std::cout<<"deduplication set haz "<<deduplicationSet.size()<<std::endl;
    container.reserve(deduplicationSet.size());
    std::copy(deduplicationSet.begin(), deduplicationSet.end(), std::back_inserter(container));
    container.insert(container.end(),initial_points.begin(),initial_points.end());
/*        for(int i=0;i<this->transformationsCount;i++){
            for(int j=0;j<interpolation_data[i].size();j++){
                container.push_back(this->interpolation_data[i][j]);
            }
        }*/
  //  }
}

/**********INTERPOLATION*****************************/
/****************************************************/

void FractalInterpolation3D::Interpolate(){
    Point GU,GD,GR,LU,LD,LR;
    Point candidate;
    std::vector<K::FT> lamdas;
    K::FT s;
    int limit=this->interpolantsIndex;
    int distance=0;
    //first iteration//
    if(this->recursionDepth==0){
        for(int j=0;j<this->transformationsCount;j++){      //for each transformation w
            this->interpolation_data[j].push_back(this->initial_points[this->indexes_local[j][0]]);
            this->interpolation_data[j].push_back(this->initial_points[this->indexes_local[j][1]]);
            this->interpolation_data[j].push_back(this->initial_points[this->indexes_local[j][2]]);
        }
        return;
    }
    //compute the amount of points per
    for(int j=0;j<this->transformationsCount;j++){      //for each transformation w
        //correct the matrix according to point//
        ////find the local and globals according tou triangulation
        GU=this->initial_points[this->indexes_global[j][0]];
        GD=this->initial_points[this->indexes_global[j][1]];
        GR=this->initial_points[this->indexes_global[j][2]];
        LU=this->initial_points[this->indexes_local[j][0]];
        LD=this->initial_points[this->indexes_local[j][1]];
        LR=this->initial_points[this->indexes_local[j][2]];
        for(int l=0;l<limit;l++){
            if(l==indexes_global[j][0]||l==indexes_global[j][1]||l==indexes_global[j][2])continue;
            //interpolate each point at that buffer
            ////find the s with barycentruc coordinates
            //////construct the triangle coordinates as a projection on xOy plane
            Triangle_coordinates triangle_coordinates(Point2(LU.x(),LU.y()),Point2(LD.x(),LD.y()),Point2(LR.x(),LR.y()));
            //////query the point on the coordiantes and get barycentric lamdas
            lamdas.clear();
            candidate=w[j].transformPoint_xy(this->initial_points[l]);
            triangle_coordinates(Point2(candidate.x(),candidate.y()),lamdas);
            ///// compute s with weights
            s = vsv[this->indexes_local[j][0]]*lamdas[0]+vsv[this->indexes_local[j][1]]*lamdas[1]+vsv[this->indexes_local[j][2]]*lamdas[2];
            w[j].g=-(GD.y()*LR.z() - GR.y()*LD.z() - GD.y()*LU.z() + GU.y()*LD.z() + GR.y()*LU.z() - GU.y()*LR.z() - GD.y()*GR.z()*s + GD.z()*GR.y()*s + GD.y()*GU.z()*s - GD.z()*GU.y()*s - GR.y()*GU.z()*s + GR.z()*GU.y()*s)/
            (GD.x()*GR.y() - GD.y()*GR.x() - GD.x()*GU.y() + GD.y()*GU.x() + GR.x()*GU.y() - GR.y()*GU.x());
            w[j].h=(GD.x()*LR.z() - GR.x()*LD.z() - GD.x()*LU.z() + GU.x()*LD.z() + GR.x()*LU.z() - GU.x()*LR.z() - GD.x()*GR.z()*s + GD.z()*GR.x()*s + GD.x()*GU.z()*s - GD.z()*GU.x()*s - GR.x()*GU.z()*s + GR.z()*GU.x()*s)/
            (GD.x()*GR.y() - GD.y()*GR.x() - GD.x()*GU.y() + GD.y()*GU.x() + GR.x()*GU.y() - GR.y()*GU.x());
            w[j].i=s;
            w[j].l=(GD.x()*GR.y()*LU.z() - GD.x()*GU.y()*LR.z() - GD.y()*GR.x()*LU.z() + GD.y()*GU.x()*LR.z() + GR.x()*GU.y()*LD.z() - GR.y()*GU.x()*LD.z() - GD.x()*GR.y()*GU.z()*s + GD.x()*GR.z()*GU.y()*s + GD.y()*GR.x()*GU.z()*s -
                    GD.y()*GR.z()*GU.x()*s - GD.z()*GR.x()*GU.y()*s + GD.z()*GR.y()*GU.x()*s)/(GD.x()*GR.y() - GD.y()*GR.x() - GD.x()*GU.y() + GD.y()*GU.x() + GR.x()*GU.y() - GR.y()*GU.x());
            candidate=Point(Point(candidate.x(),candidate.y(),w[j].transform_z(this->initial_points[l])));
            ret=deduplicationSet.insert(candidate);
            if(ret.second==true)
                this->interpolation_data[j].push_back(candidate);
            
        }
        for(int i=0;i<this->transformationsCount;i++){
            this->frame[i][j].second=(unsigned int)this->interpolation_data[j].size(); //all others can read till the end of the stack//
        }
    }
    
    for(int i=1;i<this->recursionDepth;i++){ //for each recursion depth
        for(int j=0;j<this->transformationsCount;j++){      //for each transformation w
            //correct the matrix according to point//
            ////find the local and globals according tou triangulation
            GU=this->initial_points[this->indexes_global[j][0]];
            GD=this->initial_points[this->indexes_global[j][1]];
            GR=this->initial_points[this->indexes_global[j][2]];
            LU=this->initial_points[this->indexes_local[j][0]];
            LD=this->initial_points[this->indexes_local[j][1]];
            LR=this->initial_points[this->indexes_local[j][2]];
            for(int k=0;k<this->transformationsCount;k++){          //get the points of other w buffers (and self)//
                distance = frame[j][k].second-frame[j][k].first;
                for(int l=0;l<distance;l++){                         //interpolate each point at that buffer
                    if(this->interpolation_data[k][frame[j][k].first+l]==GU||this->interpolation_data[k][frame[j][k].first+l]==GD||this->interpolation_data[k][frame[j][k].first+l]==GR)return;
                    ////find the s with barycentruc coordinates
                    //////construct the triangle coordinates as a projection on xOy plane
                    Triangle_coordinates triangle_coordinates(Point2(LU.x(),LU.y()),Point2(LD.x(),LD.y()),Point2(LR.x(),LR.y()));
                    //////query the point on the coordiantes and get barycentric lamdas
                    lamdas.clear();
                    candidate=w[j].transformPoint_xy(this->interpolation_data[k][frame[j][k].first+l]);
                    triangle_coordinates(Point2(candidate.x(),candidate.y()),lamdas);
                    ///// compute s with weights
                    s = vsv[this->indexes_local[j][0]]*lamdas[0]+vsv[this->indexes_local[j][1]]*lamdas[1]+vsv[this->indexes_local[j][2]]*lamdas[2];
                    w[j].g=-(GD.y()*LR.z() - GR.y()*LD.z() - GD.y()*LU.z() + GU.y()*LD.z() + GR.y()*LU.z() - GU.y()*LR.z() - GD.y()*GR.z()*s + GD.z()*GR.y()*s + GD.y()*GU.z()*s - GD.z()*GU.y()*s - GR.y()*GU.z()*s + GR.z()*GU.y()*s)/
                    (GD.x()*GR.y() - GD.y()*GR.x() - GD.x()*GU.y() + GD.y()*GU.x() + GR.x()*GU.y() - GR.y()*GU.x());
                    w[j].h=(GD.x()*LR.z() - GR.x()*LD.z() - GD.x()*LU.z() + GU.x()*LD.z() + GR.x()*LU.z() - GU.x()*LR.z() - GD.x()*GR.z()*s + GD.z()*GR.x()*s + GD.x()*GU.z()*s - GD.z()*GU.x()*s - GR.x()*GU.z()*s + GR.z()*GU.x()*s)/
                    (GD.x()*GR.y() - GD.y()*GR.x() - GD.x()*GU.y() + GD.y()*GU.x() + GR.x()*GU.y() - GR.y()*GU.x());
                    w[j].i=s;
                    w[j].l=(GD.x()*GR.y()*LU.z() - GD.x()*GU.y()*LR.z() - GD.y()*GR.x()*LU.z() + GD.y()*GU.x()*LR.z() + GR.x()*GU.y()*LD.z() - GR.y()*GU.x()*LD.z() - GD.x()*GR.y()*GU.z()*s + GD.x()*GR.z()*GU.y()*s + GD.y()*GR.x()*GU.z()*s -
                            GD.y()*GR.z()*GU.x()*s - GD.z()*GR.x()*GU.y()*s + GD.z()*GR.y()*GU.x()*s)/(GD.x()*GR.y() - GD.y()*GR.x() - GD.x()*GU.y() + GD.y()*GU.x() + GR.x()*GU.y() - GR.y()*GU.x());
                    candidate = Point(candidate.x(),candidate.y(),w[j].transform_z(this->interpolation_data[k][frame[j][k].first+l]));
                    ret=deduplicationSet.insert(candidate);
                    if(ret.second==true)
                        this->interpolation_data[j].push_back(candidate);
                }
            }
        }
        for(int j=0;j<this->transformationsCount;j++){
            for(int k=0;k<this->transformationsCount;k++){
                frame[j][k].first=frame[j][k].second;
                frame[j][k].second=(unsigned int)this->interpolation_data[k].size();
            }
        }
    }
}

K::FT FractalInterpolation3D::vsv_type_0(Point& G,Point& L){
    return (std::abs(L.z())-std::abs(G.z()))/(std::max(std::abs(L.z()),std::abs(G.z())));
    return (L.z()-G.z())/(std::abs(L.z())+std::abs(G.z()));
}

K::FT sign(K::FT num){
    if(num<0)return -1;
    else return 1;
}

K::FT FractalInterpolation3D::vsv_type_1(Point& G,Point& L,int power){
    return pow((std::abs(L.z())-std::abs(G.z())),power)*sign(std::abs(L.z())-std::abs(G.z()))
    /pow((std::max(std::abs(L.z()),std::abs(G.z()))),power);
}