//
//  IFS2D.h
//  OpenGL_CGAL
//
//  Created by Sean Dillon on 12/26/15.
//  Copyright (c) 2015 Sean Dillon. All rights reserved.
//

#ifndef __OpenGL_CGAL__IFS2D__
#define __OpenGL_CGAL__IFS2D__

#include "AffineTransforms.h"
#include "SurfaceRender.h"
#include <set>


class FractalInterpolation3D{
protected:
    //set for deduplication//
    std::set<Point> deduplicationSet;
    //per transformation//
    AffineTransformation3D* w=NULL;         //array with the transformations
    unsigned short** indexes_local=NULL;    //array with indexed to the point pool, indexing local triangle coords//
    unsigned short** indexes_global=NULL;   //array with indexes to point pool,indexing triangular domain coords//
    //per point//
    K::FT* vsv=NULL;                        //vertical scaling factor per point
    //data pool
    std::vector<Point> initial_points;             //buffer of the initial points//
    std::vector<std::vector<Point>> interpolation_data;        //the set of points from interpolation (each transformation w has its own buffer)
    std::vector<std::vector<std::pair<unsigned int,unsigned int>>> frame;
    //indexes and counters
    int recursionDepth=0;                   //the interpolation iterations
    int interpolantsIndex = 0;              //the index of the last calculated element in the interpolants array//
    int interpolantsCount=0;              //the count of the interpolants after the interpolation//
    int transformationsCount=0;           //the count of the transformations w[i]//
    //private functions
    void ClearData();
    K::FT vsv_type_0(Point& G,Point& L);
    K::FT vsv_type_1(Point& G,Point& L,int power);
public:
    //construction & destruction
    FractalInterpolation3D(Point* pointArray,int pointArrayLength,K::FT* verticalScalingVector,int rec_depth);
    FractalInterpolation3D(Point* pointArray,int pointArrayLength,K::FT* verticalScalingVector,int VerticalScalingVectorLength,int rec_depth);
    FractalInterpolation3D(){};
    ~FractalInterpolation3D();
    //begin algorithm
    virtual void Interpolate(); //create the set of points//
    //output
    virtual void getPoints(std::vector<Point>& container);
};





#endif /* defined(__OpenGL_CGAL__IFS2D__) */
