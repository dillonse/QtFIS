//
//  KernelConstructions.h
//  OpenGL_CGAL
//
//  Created by Sean Dillon on 12/26/15.
//  Copyright (c) 2015 Sean Dillon. All rights reserved.
//

#ifndef __OpenGL_CGAL__KernelConstructions__
#define __OpenGL_CGAL__KernelConstructions__

#include <stdio.h>

//CGAL
///KERNEL
#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Exact_predicates_exact_constructions_kernel_with_sqrt.h>
#include <CGAL/Cartesian_converter.h>
///TRIANGULATIONS
#include <CGAL/Triangulation_data_structure_2.h>
#include <CGAL/Delaunay_triangulation_2.h>
#include <CGAL/Triangulation_vertex_base_with_info_2.h>
#include <CGAL/Triangulation_face_base_with_info_2.h>
#include <CGAL/Projection_traits_xy_3.h>
#include <CGAL/Constrained_Delaunay_triangulation_2.h>
#include <CGAL/Constrained_triangulation_face_base_2.h>
#include <CGAL/nearest_neighbor_delaunay_2.h>
///AABB
#include <CGAL/bounding_box.h>
///BARYCENTRICS
#include <CGAL/Barycentric_coordinates_2/Triangle_coordinates_2.h>
///ARANGEMENTS
#include <CGAL/Arr_segment_traits_2.h>
#include <CGAL/Arrangement_2.h>
#include <CGAL/Arr_extended_dcel.h>
#include <CGAL/Arr_naive_point_location.h>
#include <CGAL/Arr_batched_point_location.h>
///INTERPOLATION
#include <CGAL/Interpolation_traits_2.h>
#include <CGAL/natural_neighbor_coordinates_2.h>
#include <CGAL/interpolation_functions.h>
///DISTANCE
#include <CGAL/squared_distance_2.h>
///LINES
#include <CGAL/Line_2.h>
//DIMENSION
#include <CGAL/Dimension.h>
//CENTROID
#include <CGAL/centroid.h>


typedef CGAL::Exact_predicates_exact_constructions_kernel ExactKernel;
typedef CGAL::Exact_predicates_inexact_constructions_kernel InexactKernel;
typedef InexactKernel K;
typedef CGAL::Cartesian_converter<K,ExactKernel> KtoExact;
typedef CGAL::Cartesian_converter<ExactKernel,K> ExacttoK;
typedef CGAL::Cartesian_converter<K,InexactKernel>KtoInexact ;
typedef CGAL::Cartesian_converter<InexactKernel,K> InexacttoK;
typedef CGAL::Cartesian_converter<ExactKernel,InexactKernel> ExacttoInexcat;
typedef CGAL::Cartesian_converter<InexactKernel,ExactKernel> InexacttoExact;

typedef CGAL::Point_3<K>                                  Point;    //3d point
typedef CGAL::Vector_3<K>                                 Vector;   //3d vector
typedef CGAL::Point_2<K>                                  Point2;   //2d point
typedef CGAL::Vector_2<K>                                 Vector2;  //2d vector
typedef CGAL::Projection_traits_xy_3<K>  Gt;

typedef struct vertex_info{
    unsigned int index=0;
    float factor;
}vertex_info;

typedef struct face_info{
    float factor;
    unsigned int index=0;
}face_info;
typedef CGAL::Triangulation_vertex_base_with_info_2<vertex_info,Gt> Vb;
typedef CGAL::Triangulation_face_base_with_info_2<face_info,Gt>     Fb;
typedef CGAL::Constrained_triangulation_face_base_2<Fb>             CFb;
typedef CGAL::Triangulation_data_structure_2<Vb,Fb>                 Tds;
typedef CGAL::Triangulation_2<Gt,Tds>                             Triangulation;
typedef CGAL::Delaunay_triangulation_2<Gt,Tds>                      Delaunay2;
typedef CGAL::Delaunay_triangulation_2<K>                           SimpleDelaunay2;
typedef CGAL::Barycentric_coordinates::Triangle_coordinates_2<K>             Triangle_coordinates;

struct PointHash {
    size_t operator() (const Point& p) const {
        return CGAL::to_double(p.x()+p.y()+p.z());
    }
};

//arrangements

typedef struct arr_vertex_info{
    K::FT height=0;
    K::FT s=0;
    int triangulation_index=0;
}arr_vertex_info;

//placeholders for compilation issues
typedef struct arr_halfedge_info{
//    int triangulation_index=0;
}arr_halfedge_info;

typedef struct arr_face_info{
    unsigned int index=0;
    K::FT s=0;
}arr_face_info;



//typedef CGAL::Arr_basic_insertion_traits_2<K> STs;
typedef CGAL::Arr_segment_traits_2<ExactKernel>                               STs;
typedef CGAL::Arr_extended_dcel<STs,arr_vertex_info,arr_halfedge_info,arr_face_info>      Dcel;
typedef CGAL::Arrangement_2<STs, Dcel>                              Arrangement_2;
typedef STs::Curve_2 Segment2;
typedef CGAL::Arr_naive_point_location<Arrangement_2>                 Arrangment2NaivePL;
class Arrangement2:public Arrangement_2{
public:
    enum class VerticalScalingType{SINGLE,FACES,VERTICES};
    enum class ColoringType{NONE,CONSISTENT};
    enum class RecurrentIndexingType{NONE,FACES};
    enum class PrimitiveFaceType{TRIANGULAR,RECTANGULAR};
    VerticalScalingType vst;
    ColoringType ct;
    RecurrentIndexingType rit;
    PrimitiveFaceType pft;

};

typedef CGAL::Line_2<K>                                             Line2;


#endif /* defined(__OpenGL_CGAL__KernelConstructions__) */
