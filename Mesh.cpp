//
//  TriangularMesh.cpp
//  OpenGL_CGAL
//
//  Created by Sean Dillon on 3/5/16.
//  Copyright (c) 2016 Sean Dillon. All rights reserved.
//

#include "Mesh.h"
#include <set>
#include "Random.h"


void Mesh::createMN(Arrangement2& result,int M,int N){
    result.clear();
    for(int i=0;i<M;i++){
        for(int j=0;j<N;j++){
            Arrangement2::Vertex_handle vh=
                    result.insert_in_face_interior(Arrangement2::Point_2(((double)j)/(double(N-1)),((double)i)/(double(M-1))),result.unbounded_face());
            if(i%2==0&&j%2==0)vh->data().triangulation_index=0;
            if(i%2==0&&j%2==1)vh->data().triangulation_index=1;
            if(i%2==1&&j%2==0)vh->data().triangulation_index=2;
            if(i%2==1&&j%2==1)vh->data().triangulation_index=3;
        }
    }
    Mesh::quadify(result);

}

void Mesh::createMNRec(Arrangement2& result,int M,int N){
    result.clear();
    for(int i=0;i<M;i++){
        for(int j=0;j<N;j++){
            Arrangement2::Vertex_handle vh=
                    result.insert_in_face_interior(Arrangement2::Point_2(((double)j)/(double(N-1)),((double)i)/(double(M-1))),result.unbounded_face());
            if(i<=M/2&&j<=N/2){
                if(i%2==0&&j%2==0)vh->data().triangulation_index=6;
                if(i%2==0&&j%2==1)vh->data().triangulation_index=7;
                if(i%2==1&&j%2==0)vh->data().triangulation_index=3;
                if(i%2==1&&j%2==1)vh->data().triangulation_index=4;
            }
            else if(i<=M/2&&j>N/2){
                if(i%2==0&&j%2==0)vh->data().triangulation_index=8;
                if(i%2==0&&j%2==1)vh->data().triangulation_index=7;
                if(i%2==1&&j%2==0)vh->data().triangulation_index=5;
                if(i%2==1&&j%2==1)vh->data().triangulation_index=4;
            }
            else if(i>M/2&&j<=N/2){
                if(i%2==0&&j%2==0)vh->data().triangulation_index=0;
                if(i%2==0&&j%2==1)vh->data().triangulation_index=1;
                if(i%2==1&&j%2==0)vh->data().triangulation_index=3;
                if(i%2==1&&j%2==1)vh->data().triangulation_index=4;
            }
            else{
                if(i%2==0&&j%2==0)vh->data().triangulation_index=2;
                if(i%2==0&&j%2==1)vh->data().triangulation_index=1;
                if(i%2==1&&j%2==0)vh->data().triangulation_index=5;
                if(i%2==1&&j%2==1)vh->data().triangulation_index=4;//3;

            }

        }
    }
    Mesh::quadify(result);

}

void Mesh::addCollinearFrame(Arrangement2& I, Arrangement2 &J){
    ExacttoK exactToK;
    //
    std::vector<Arrangement2::Vertex_handle> vhv;
    std::vector<Arrangement2::Vertex_handle> cvhv;
    std::vector<std::pair<Arrangement2::Vertex_handle,Arrangement2::Vertex_handle>> connections;
    std::vector<std::pair<Arrangement2::Vertex_handle,ExactKernel::Point_2>>displacements;
    //itterate over convex hull
    Arrangement2::Ccb_halfedge_circulator hc=(*I.unbounded_face()->inner_ccbs_begin()),end(hc);
    do{
        //check if the points: source,target,next target are collinear
        if(CGAL::collinear(hc->source()->point(),hc->target()->point(),hc->next()->target()->point())){
            //create a vector from source to target
            Vector2 v(exactToK(hc->source()->point()),exactToK(hc->target()->point()));
            //get the perpedicular direction
            CGAL::Orientation o=CGAL::POSITIVE;
            v=v.perpendicular(o);
            std::pair<double,double> appr_perp_dir=std::make_pair(CGAL::to_double(v.direction().dx()),CGAL::to_double(v.direction().dy()));
            appr_perp_dir.first/=std::sqrt(CGAL::to_double(v.squared_length()));
            appr_perp_dir.second/=std::sqrt(CGAL::to_double(v.squared_length()));
            //get the distance between two of the points
            K::FT offset=std::sqrt(CGAL::to_double(CGAL::squared_distance(hc->source()->point(),hc->target()->point())));
            //add that point on the data with location new:original+direction*length
            ExactKernel::Point_2 offsetPoint(hc->target()->point().x()+offset*appr_perp_dir.first,hc->target()->point().y()+offset*appr_perp_dir.second);
            Arrangement2::Vertex_handle vh=I.insert_in_face_interior(offsetPoint,I.unbounded_face());
            vhv.push_back(vh);
            connections.push_back(std::make_pair(hc->target(),vh));
        }
        else{
            //create a vector from source to target
            Vector2 v1(exactToK(hc->source()->point()),exactToK(hc->target()->point()));
            Vector2 v2(exactToK(hc->target()->point()),exactToK(hc->next()->target()->point()));
            //get the perpedicular direction
            CGAL::Orientation o=CGAL::POSITIVE;
            v1=v1.perpendicular(o);
            std::pair<double,double> appr_perp_dir1=std::make_pair(CGAL::to_double(v1.direction().dx()),CGAL::to_double(v1.direction().dy()));
            appr_perp_dir1.first/=std::sqrt(CGAL::to_double(v1.squared_length()));
            appr_perp_dir1.second/=std::sqrt(CGAL::to_double(v1.squared_length()));
            v2=v2.perpendicular(o);
            std::pair<double,double> appr_perp_dir2=std::make_pair(CGAL::to_double(v2.direction().dx()),CGAL::to_double(v2.direction().dy()));
            appr_perp_dir2.first/=std::sqrt(CGAL::to_double(v2.squared_length()));
            appr_perp_dir2.second/=std::sqrt(CGAL::to_double(v2.squared_length()));
            //get the distance between two of the points
            K::FT offset1=std::sqrt(CGAL::to_double(CGAL::squared_distance(hc->source()->point(),hc->target()->point())));
            K::FT offset2=std::sqrt(CGAL::to_double(CGAL::squared_distance(hc->target()->point(),hc->next()->target()->point())));
            //add that point on the data with location new:original+direction*length
            ExactKernel::Point_2 offsetPoint1(hc->target()->point().x()+offset1*appr_perp_dir1.first,hc->target()->point().y()+offset2*appr_perp_dir1.second);
            ExactKernel::Point_2 offsetPoint2(hc->target()->point().x()+offset2*appr_perp_dir2.first,hc->target()->point().y()+offset2*appr_perp_dir2.second);
            ExactKernel::Point_2 offsetPoint12(hc->target()->point().x()+offset1*appr_perp_dir1.first+offset2*appr_perp_dir2.first,
                                 hc->target()->point().y()+offset1*appr_perp_dir1.second+offset2*appr_perp_dir2.second);
            Arrangement2::Vertex_handle vh=I.insert_in_face_interior(offsetPoint1,I.unbounded_face());
            vhv.push_back(vh);connections.push_back(std::make_pair(hc->target(),vh));
            vh=I.insert_in_face_interior(offsetPoint12,I.unbounded_face());
            vh->data().height=hc->target()->data().height;
            cvhv.push_back(vh);
            vhv.push_back(vh);
            vh=I.insert_in_face_interior(offsetPoint2,I.unbounded_face());
            vhv.push_back(vh);connections.push_back(std::make_pair(hc->target(),vh));
        }
    }while(++hc!=end);
    for(std::vector<std::pair<Arrangement2::Vertex_handle,Arrangement2::Vertex_handle>>::iterator it=connections.begin();it!=connections.end();it++){
        I.insert_at_vertices(Segment2(it->first->point(),it->second->point()),it->first,it->second);
    }
   vhv.push_back(*vhv.begin());
    std::reverse(cvhv.begin(),cvhv.end());
    cvhv.push_back(*(cvhv.begin()));
    std::reverse(cvhv.begin(),cvhv.end());
    cvhv.push_back(*(cvhv.begin()+1));
    std::vector<Arrangement2::Vertex_handle>::iterator ci=cvhv.begin();
    for(std::vector<Arrangement2::Vertex_handle>::iterator it=vhv.begin();it!=vhv.end()-1;it++){
        I.insert_at_vertices(Segment2((*it)->point(),(*(it+1))->point()),*it,*(it+1));
        if(std::find(cvhv.begin()+1,cvhv.end(),*it)!=cvhv.end())ci++;
        else{
            (*it)->data().height=std::sqrt(CGAL::to_double(CGAL::squared_distance((*(ci))->point(),(*it)->point())))/
                    (std::sqrt(CGAL::to_double(CGAL::squared_distance((*ci)->point(),(*(ci+1))->point()))))
                    *(*(ci+1))->data().height+
             std::sqrt(CGAL::to_double(CGAL::squared_distance((*(ci+1))->point(),(*it)->point())))/(std::sqrt(CGAL::to_double(CGAL::squared_distance((*ci)->point(),(*(ci+1))->point()))))*(*ci)->data().height;
        }
    }
    //change the convex hull geometry of the J mesh
    //get the AABB of the corners of the CH of I
    //create a vector of poinst of the CH Corners
    std::vector<Point2> chc;
    for(std::vector<Arrangement2::Vertex_handle>::iterator ci=cvhv.begin();ci!=cvhv.end();ci++){
        chc.push_back(exactToK((*ci)->point()));
    }
    K::Iso_rectangle_2 r2=CGAL::bounding_box(chc.begin(),chc.end());
    hc=(*J.unbounded_face()->inner_ccbs_begin());
    end=Arrangement2::Ccb_halfedge_circulator(hc);
    K::FT offset=r2.max().x()-1;
    do{
        //check if the points: source,target,next target are collinear
        if(CGAL::collinear(hc->source()->point(),hc->target()->point(),hc->next()->target()->point())){
            //create a vector from source to target
            Vector2 v(exactToK(hc->source()->point()),exactToK(hc->target()->point()));
            //get the perpedicular direction
            CGAL::Orientation o=CGAL::POSITIVE;
            v=v.perpendicular(o);
            std::pair<double,double> appr_perp_dir=std::make_pair(CGAL::to_double(v.direction().dx()),CGAL::to_double(v.direction().dy()));
            appr_perp_dir.first/=std::sqrt(CGAL::to_double(v.squared_length()));
            appr_perp_dir.second/=std::sqrt(CGAL::to_double(v.squared_length()));
            //get the distance between two of the points
            //add that point on the data with location new:original+direction*length
            ExactKernel::Point_2 offsetPoint(hc->target()->point().x()+offset*appr_perp_dir.first,hc->target()->point().y()+offset*appr_perp_dir.second);
            displacements.push_back(std::make_pair(hc->target(),offsetPoint));

        }else{
            //create a vector from source to target
            Vector2 v1(exactToK(hc->source()->point()),exactToK(hc->target()->point()));
            Vector2 v2(exactToK(hc->target()->point()),exactToK(hc->next()->target()->point()));
            //get the perpedicular direction
            CGAL::Orientation o=CGAL::POSITIVE;
            v1=v1.perpendicular(o);
            std::pair<double,double> appr_perp_dir1=std::make_pair(CGAL::to_double(v1.direction().dx()),CGAL::to_double(v1.direction().dy()));
            appr_perp_dir1.first/=std::sqrt(CGAL::to_double(v1.squared_length()));
            appr_perp_dir1.second/=std::sqrt(CGAL::to_double(v1.squared_length()));
            v2=v2.perpendicular(o);
            std::pair<double,double> appr_perp_dir2=std::make_pair(CGAL::to_double(v2.direction().dx()),CGAL::to_double(v2.direction().dy()));
            appr_perp_dir2.first/=std::sqrt(CGAL::to_double(v2.squared_length()));
            appr_perp_dir2.second/=std::sqrt(CGAL::to_double(v2.squared_length()));
            //get the distance between two of the points
            K::FT offset1=offset;
            K::FT offset2=offset;
            //add that point on the data with location new:original+direction*length
            ExactKernel::Point_2 offsetPoint12(hc->target()->point().x()+offset1*appr_perp_dir1.first+offset2*appr_perp_dir2.first,
                                 hc->target()->point().y()+offset1*appr_perp_dir1.second+offset2*appr_perp_dir2.second);
            displacements.push_back(std::make_pair(hc->target(),offsetPoint12));
        }

    }while(++hc!=end);
    for(std::vector<std::pair<Arrangement2::Vertex_handle,ExactKernel::Point_2>>::iterator it=displacements.begin();it!=displacements.end();it++){
        it->first->point()=it->second;
    }
}

void Mesh::unify(std::vector<Arrangement2>& data,Arrangement2& result){
    std::vector<Arrangement2::Vertex_handle> vhv;
    std::vector<std::tuple<Arrangement2::Vertex_handle,Arrangement2::Vertex_handle,Arrangement2::Halfedge_handle>> ev;
    for(std::vector<Arrangement2>::iterator it=data.begin();it!=data.end();it++){//foreach list member
        for(Arrangement2::Face_iterator fi=it->faces_begin();fi!=it->faces_end();fi++){//foreach edge of the arr
            if(fi->is_unbounded())continue;
            //gather the vertices of the face
            Arrangement2::Ccb_halfedge_circulator hc=fi->outer_ccb(),end(hc);
            std::vector<Arrangement2::Vertex_handle> vertices;
            do{
                //check if the vertice exists on the vhv
                std::vector<Arrangement2::Vertex_handle>::iterator vh;
                if((vh=std::find_if(vhv.begin(),vhv.end(),[&hc](auto i){return hc->target()->point()==i->point();}))==vhv.end()){
                    Arrangement2::Vertex_handle vhl =result.insert_in_face_interior(ExactKernel::Point_2(hc->target()->point().x(),hc->target()->point().y()),result.unbounded_face());
                    vhl->data().height=hc->target()->data().height;
                    vhl->data().s=hc->target()->data().s;
                    vhl->data().triangulation_index=hc->target()->data().triangulation_index;
                    vhv.push_back(vhl);
                    vertices.push_back(vhl);
                }
                else{
                    vertices.push_back(*vh);
                }
            }while(++hc!=end);
            //add the outer ccb of the face
            Arrangement2::Halfedge_handle hh;
            vertices.push_back(*vertices.begin());
            for(std::vector<Arrangement2::Vertex_handle>::iterator vi=vertices.begin();vi!=vertices.end()-1;vi++){
                //check if the edge exists on the ev
                std::vector<std::tuple<Arrangement2::Vertex_handle,Arrangement2::Vertex_handle,Arrangement2::Halfedge_handle>>::iterator eh;
                if((eh=std::find_if(ev.begin(),ev.end(),
                                    [&vi](auto i){return ((*vi)->point()==std::get<0>(i)->point()&&((*(vi+1))->point()==std::get<1>(i)->point()))||((*vi)->point()==std::get<1>(i)->point()&&((*(vi+1))->point()==std::get<0>(i)->point()));}))==ev.end()){
                    std::cout<<"inserting "<<(*vi)->point()<<" and "<<(*(vi+1))->point()<<std::endl;
                    hh= result.insert_at_vertices(Segment2((*vi)->point(),(*(vi+1))->point()),*vi,*(vi+1));
                    ev.push_back(std::make_tuple(*vi,*(vi+1),hh));
                }
                else{
                    if(std::get<0>(*eh)->point()==(*vi)->point()){
                        hh=std::get<2>(*eh);
                    }
                    else{
                        hh=std::get<2>(*eh)->twin();
                    }
                }
            }
            hh->face()->data().s=fi->data().s;
            hh->face()->data().index=fi->data().index;
        }
    }
}

void Mesh::sampleHeightsJFromI(Arrangement2& I,Arrangement2& J){
    if(I.number_of_vertices()==0||J.number_of_vertices()==0)return;
    ExacttoK exactToK;
    std::vector<Point> points;
    for(Arrangement2::Vertex_iterator vi=I.vertices_begin();vi!=I.vertices_end();vi++){
        points.push_back(Point(exactToK(vi->point().x()),exactToK(vi->point().y()),vi->data().height));
    }
    Delaunay2 dt(points.begin(),points.end());
    for(Arrangement2::Vertex_iterator it =J.vertices_begin();it!=J.vertices_end();it++){
         it->data().height=dt.nearest_vertex(Point(exactToK(it->point().x()),exactToK(it->point().y()),it->data().height))->point().z();
    }
}

bool Mesh::validate(Arrangement2& mesh){
    //check for isolated vertices
    if(mesh.number_of_isolated_vertices()>0)return false;
    //check for faces num
    if(mesh.number_of_faces()==1||mesh.number_of_unbounded_faces()> 1)return false;
    //get the number of faces of the first face
    Arrangement2::Face_handle fh=mesh.faces_begin();
    size_t vertices_num;
    if(mesh.pft==Arrangement2::PrimitiveFaceType::TRIANGULAR){
        vertices_num=3;
    }
    else if(mesh.pft==Arrangement2::PrimitiveFaceType::RECTANGULAR){
        vertices_num=4;
    }
    while(++fh!=mesh.faces_end()){
        if(fh->is_unbounded())continue;
        if(std::distance(fh->outer_ccb(),--Arrangement2::Ccb_halfedge_circulator(fh->outer_ccb()))+size_t(1)!=vertices_num)
            return false;
    }
    return true;
}

bool Mesh::validate(Arrangement2& I,Arrangement2& J){
    ExacttoK exactToK;
    std::vector<Point> points;
    for(Arrangement2::Vertex_iterator vi=I.vertices_begin();vi!=I.vertices_end();vi++){
        points.push_back(Point(exactToK(vi->point().x()),exactToK(vi->point().y()),vi->data().height));
    }
    Delaunay2 dt(points.begin(),points.end());
    for(Arrangement2::Vertex_iterator it =J.vertices_begin();it!=J.vertices_end();it++){
        if(CGAL::squared_distance(Point(exactToK(it->point().x()),exactToK(it->point().y()),it->data().height),dt.nearest_vertex(Point(exactToK(it->point().x()),exactToK(it->point().y()),it->data().height))->point())>0.1f){
            return false;
        }
    }
    return true;
}

void Mesh::clear(Arrangement2& mesh){
    for(Arrangement2::Vertex_iterator vi=mesh.vertices_begin();vi!=mesh.vertices_end();vi++){
        if(vi->degree()>0){
            Arrangement2::Halfedge_around_vertex_circulator hc(vi->incident_halfedges()),end(hc);
            std::vector<Arrangement2::Halfedge_handle> halfedges;
            do{
                if(std::find_if(halfedges.begin(),halfedges.end(),[hc](auto i){return (hc->target()==i->target()&& hc->source()==i->source())||
                                (hc->target()==i->source()&& hc->source()==i->target()); })==halfedges.end()){
                    halfedges.push_back(hc);
                }
            }while(++hc!=end);
            for(std::vector<Arrangement2::Halfedge_handle>::iterator itt=halfedges.begin();itt!=halfedges.end();itt++){
                mesh.remove_edge(*itt,false,false);
            }
        }
    }
}

bool Mesh::quadify(Arrangement2& mesh){
    if(mesh.number_of_vertices()==0)return false;
    //clean the mesh (nice pun huh)
    Mesh::clear(mesh);
    //gather vh on vector
    std::vector<Arrangement2::Vertex_handle> vhv;
    for(Arrangement2::Vertex_handle vh=mesh.vertices_begin();vh!=mesh.vertices_end();vh++){
        vhv.push_back(vh);
    }
    //sort by x,y
    std::sort(vhv.begin(),vhv.end(),[](auto i,auto j){if(i->point().y()==j->point().y())return i->point().x()<j->point().x();return i->point().y()<j->point().y();});
    //partition
    std::vector<std::vector<Arrangement2::Vertex_handle>> vvhv;
    std::vector<Arrangement2::Vertex_handle>::iterator bound=vhv.begin();
    std::vector<Arrangement2::Vertex_handle>::iterator probound;
    while(bound!=vhv.end()){
        probound=bound;
        bound = std::stable_partition(bound, vhv.end(),[bound](auto i){return i->point().y()==(*bound)->point().y();});
        vvhv.push_back(std::vector<Arrangement2::Vertex_handle>(probound,bound));
    }
   //add connections as follows: from west to east, from north to south
    for(size_t i=0;i<vvhv.size()-1;i++){
        for(size_t j=0;j<vvhv[i].size()-1;j++){
            mesh.insert_at_vertices(Segment2(vvhv[i][j]->point(),vvhv[i][j+1]->point()),vvhv[i][j],vvhv[i][j+1]);//west to east
            mesh.insert_at_vertices(Segment2(vvhv[i][j]->point(),vvhv[i+1][j]->point()),vvhv[i][j],vvhv[i+1][j]);//north to south
        }
    }
    //close the north south
    for(size_t i=0;i<vvhv.size()-1;i++){
         mesh.insert_at_vertices(Segment2(vvhv[i][vvhv[i].size()-1]->point(),vvhv[i+1][vvhv[i].size()-1]->point()),vvhv[i][vvhv[i].size()-1],vvhv[i+1][vvhv[i].size()-1]);//north to south
    }
    //close the west east
    //close the north south
    for(size_t j=0;j<vvhv.size()-1;j++){
         mesh.insert_at_vertices(Segment2(vvhv[vvhv.size()-1][j]->point(),vvhv[vvhv.size()-1][j+1]->point()),vvhv[vvhv.size()-1][j],vvhv[vvhv.size()-1][j+1]);//west to east
    }
    return true;
}

bool Mesh::triangulateX(Arrangement2& mesh){
    //do same as in quadify
    if(mesh.number_of_vertices()==0)return false;
    //clean the mesh (nice pun huh)
    Mesh::clear(mesh);
    //gather vh on vector
    std::vector<Arrangement2::Vertex_handle> vhv;
    for(Arrangement2::Vertex_handle vh=mesh.vertices_begin();vh!=mesh.vertices_end();vh++){
        vhv.push_back(vh);
    }
    //sort by x,y
    std::sort(vhv.begin(),vhv.end(),[](auto i,auto j){if(i->point().y()==j->point().y())return i->point().x()<j->point().x();return i->point().y()<j->point().y();});
    //partition
    std::vector<std::vector<Arrangement2::Vertex_handle>> vvhv;
    std::vector<Arrangement2::Vertex_handle>::iterator bound=vhv.begin();
    std::vector<Arrangement2::Vertex_handle>::iterator probound;
    while(bound!=vhv.end()){
        probound=bound;
        bound = std::stable_partition(bound, vhv.end(),[bound](auto i){return i->point().y()==(*bound)->point().y();});
        vvhv.push_back(std::vector<Arrangement2::Vertex_handle>(probound,bound));
    }
   //add connections as follows: from west to east, from north to south, and cross
    for(size_t i=0;i<vvhv.size()-1;i++){
        for(size_t j=0;j<vvhv[i].size()-1;j++){
            if(((i%2)+j)%2==0){//northwest to southeast
                if((j+1)<vvhv[i+1].size())
                    mesh.insert_at_vertices(Segment2(vvhv[i][j]->point(),vvhv[i+1][j+1]->point()),vvhv[i][j],vvhv[i+1][j+1]);
            }
            else{//southwest to northeast
                mesh.insert_at_vertices(Segment2(vvhv[i+1][j]->point(),vvhv[i][j+1]->point()),vvhv[i+1][j],vvhv[i][j+1]);
            }
            mesh.insert_at_vertices(Segment2(vvhv[i][j]->point(),vvhv[i][j+1]->point()),vvhv[i][j],vvhv[i][j+1]);//west to east
            mesh.insert_at_vertices(Segment2(vvhv[i][j]->point(),vvhv[i+1][j]->point()),vvhv[i][j],vvhv[i+1][j]);//north to south
        }
    }
    //close the north south
    for(size_t i=0;i<vvhv.size()-1;i++){
         mesh.insert_at_vertices(Segment2(vvhv[i][vvhv[i].size()-1]->point(),vvhv[i+1][vvhv[i].size()-1]->point()),vvhv[i][vvhv[i].size()-1],vvhv[i+1][vvhv[i].size()-1]);//north to south
    }
    //close the west east
    //close the north south
    for(size_t j=0;j<vvhv.size()-1;j++){
         mesh.insert_at_vertices(Segment2(vvhv[vvhv.size()-1][j]->point(),vvhv[vvhv.size()-1][j+1]->point()),vvhv[vvhv.size()-1][j],vvhv[vvhv.size()-1][j+1]);//west to east
    }
    return true;
}

bool Mesh::triangulatePL(Arrangement2& mesh){
    //do same as in quadify
    if(mesh.number_of_vertices()==0)return false;
    //clean the mesh (nice pun huh)
    Mesh::clear(mesh);
    //gather vh on vector
    std::vector<Arrangement2::Vertex_handle> vhv;
    for(Arrangement2::Vertex_handle vh=mesh.vertices_begin();vh!=mesh.vertices_end();vh++){
        vhv.push_back(vh);
    }
    //sort by x,y
    std::sort(vhv.begin(),vhv.end(),[](auto i,auto j){if(i->point().y()==j->point().y())return i->point().x()<j->point().x();return i->point().y()<j->point().y();});
    //partition
    std::vector<std::vector<Arrangement2::Vertex_handle>> vvhv;
    std::vector<Arrangement2::Vertex_handle>::iterator bound=vhv.begin();
    std::vector<Arrangement2::Vertex_handle>::iterator probound;
    while(bound!=vhv.end()){
        probound=bound;
        bound = std::stable_partition(bound, vhv.end(),[bound](auto i){return i->point().y()==(*bound)->point().y();});
        vvhv.push_back(std::vector<Arrangement2::Vertex_handle>(probound,bound));
    }
   //add connections as follows: from west to east, from north to south, and cross
    for(size_t i=0;i<vvhv.size()-1;i++){
        for(size_t j=0;j<vvhv[i].size()-1;j++){
            mesh.insert_at_vertices(Segment2(vvhv[i+1][j]->point(),vvhv[i][j+1]->point()),vvhv[i+1][j],vvhv[i][j+1]);
            mesh.insert_at_vertices(Segment2(vvhv[i][j]->point(),vvhv[i][j+1]->point()),vvhv[i][j],vvhv[i][j+1]);//west to east
            mesh.insert_at_vertices(Segment2(vvhv[i][j]->point(),vvhv[i+1][j]->point()),vvhv[i][j],vvhv[i+1][j]);//north to south
        }
    }
    //close the north south
    for(size_t i=0;i<vvhv.size()-1;i++){
         mesh.insert_at_vertices(Segment2(vvhv[i][vvhv[i].size()-1]->point(),vvhv[i+1][vvhv[i].size()-1]->point()),vvhv[i][vvhv[i].size()-1],vvhv[i+1][vvhv[i].size()-1]);//north to south
    }
    //close the west east
    //close the north south
    for(size_t j=0;j<vvhv.size()-1;j++){
         mesh.insert_at_vertices(Segment2(vvhv[vvhv.size()-1][j]->point(),vvhv[vvhv.size()-1][j+1]->point()),vvhv[vvhv.size()-1][j],vvhv[vvhv.size()-1][j+1]);//west to east
    }
    return true;
}

bool Mesh::triangulatePR(Arrangement2& mesh){
    //do same as in quadify
    if(mesh.number_of_vertices()==0)return false;
    //clean the mesh (nice pun huh)
    Mesh::clear(mesh);
    //gather vh on vector
    std::vector<Arrangement2::Vertex_handle> vhv;
    for(Arrangement2::Vertex_handle vh=mesh.vertices_begin();vh!=mesh.vertices_end();vh++){
        vhv.push_back(vh);
    }
    //sort by x,y
    std::sort(vhv.begin(),vhv.end(),[](auto i,auto j){if(i->point().y()==j->point().y())return i->point().x()<j->point().x();return i->point().y()<j->point().y();});
    //partition
    std::vector<std::vector<Arrangement2::Vertex_handle>> vvhv;
    std::vector<Arrangement2::Vertex_handle>::iterator bound=vhv.begin();
    std::vector<Arrangement2::Vertex_handle>::iterator probound;
    while(bound!=vhv.end()){
        probound=bound;
        bound = std::stable_partition(bound, vhv.end(),[bound](auto i){return i->point().y()==(*bound)->point().y();});
        vvhv.push_back(std::vector<Arrangement2::Vertex_handle>(probound,bound));
    }
   //add connections as follows: from west to east, from north to south, and cross
    for(size_t i=0;i<vvhv.size()-1;i++){
        for(size_t j=0;j<vvhv[i].size()-1;j++){
            if((j+1)<vvhv[i+1].size())
                mesh.insert_at_vertices(Segment2(vvhv[i][j]->point(),vvhv[i+1][j+1]->point()),vvhv[i][j],vvhv[i+1][j+1]);
            mesh.insert_at_vertices(Segment2(vvhv[i][j]->point(),vvhv[i][j+1]->point()),vvhv[i][j],vvhv[i][j+1]);//west to east
            mesh.insert_at_vertices(Segment2(vvhv[i][j]->point(),vvhv[i+1][j]->point()),vvhv[i][j],vvhv[i+1][j]);//north to south
        }
    }
    //close the north south
    for(size_t i=0;i<vvhv.size()-1;i++){
         mesh.insert_at_vertices(Segment2(vvhv[i][vvhv[i].size()-1]->point(),vvhv[i+1][vvhv[i].size()-1]->point()),vvhv[i][vvhv[i].size()-1],vvhv[i+1][vvhv[i].size()-1]);//north to south
    }
    //close the west east
    //close the north south
    for(size_t j=0;j<vvhv.size()-1;j++){
         mesh.insert_at_vertices(Segment2(vvhv[vvhv.size()-1][j]->point(),vvhv[vvhv.size()-1][j+1]->point()),vvhv[vvhv.size()-1][j],vvhv[vvhv.size()-1][j+1]);//west to east
    }
    return true;
}

void Mesh::createRandomTriangular(Arrangement2& arr,Arrangement2& global){
    Arrangement2::Vertex_handle v1 = arr.insert_in_face_interior(ExactKernel::Point_2(0,1), arr.unbounded_face());
    Arrangement2::Vertex_handle v2 = arr.insert_in_face_interior(ExactKernel::Point_2(0,0.5), arr.unbounded_face());
    Arrangement2::Vertex_handle v3 = arr.insert_in_face_interior(ExactKernel::Point_2(0,0), arr.unbounded_face());
    Arrangement2::Vertex_handle v4 = arr.insert_in_face_interior(ExactKernel::Point_2(0.5,0), arr.unbounded_face());
    Arrangement2::Vertex_handle v5 = arr.insert_in_face_interior(ExactKernel::Point_2(1,0), arr.unbounded_face());
    Arrangement2::Vertex_handle v6 = arr.insert_in_face_interior(ExactKernel::Point_2(0.5,0.5), arr.unbounded_face());
    v1->data().triangulation_index=0;
    v2->data().triangulation_index=2;
    v3->data().triangulation_index=1;
    v4->data().triangulation_index=0;
    v5->data().triangulation_index=2;
    v6->data().triangulation_index=1;
    arr.insert_at_vertices(Segment2(v1->point(),v2->point()),v1,v2);
    arr.insert_at_vertices(Segment2(v2->point(),v3->point()),v2,v3);
    arr.insert_at_vertices(Segment2(v3->point(),v4->point()),v3,v4);
    arr.insert_at_vertices(Segment2(v4->point(),v5->point()),v4,v5);
    arr.insert_at_vertices(Segment2(v5->point(),v6->point()),v5,v6);
    arr.insert_at_vertices(Segment2(v6->point(),v2->point()),v6,v2);
    arr.insert_at_vertices(Segment2(v6->point(),v1->point()),v6,v1);
    arr.insert_at_vertices(Segment2(v6->point(),v4->point()),v6,v4);
    arr.insert_at_vertices(Segment2(v2->point(),v4->point()),v2,v4);
    
    //random stuff
    v1->data().height=dice::throw_range(-1,1);v1->data().s=dice::throw_range(-1,1);
    v2->data().height=dice::throw_range(-1,1);v2->data().s=dice::throw_range(-1,1);
    v3->data().height=dice::throw_range(-1,1);v3->data().s=dice::throw_range(-1,1);
    v4->data().height=dice::throw_range(-1,1);v4->data().s=dice::throw_range(-1,1);
    v5->data().height=dice::throw_range(-1,1);v5->data().s=dice::throw_range(-1,1);
    v6->data().height=dice::throw_range(-1,1);v6->data().s=dice::throw_range(-1,1);
    //
    Arrangement2::Vertex_handle v11 =
            global.insert_in_face_interior(ExactKernel::Point_2(0,1),global.unbounded_face());v11->data().triangulation_index=0;v11->data().height=v1->data().height;
    Arrangement2::Vertex_handle v22 =
            global.insert_in_face_interior(ExactKernel::Point_2(0,0),global.unbounded_face());v22->data().triangulation_index=1;v22->data().height=v3->data().height;
    Arrangement2::Vertex_handle v33 =
            global.insert_in_face_interior(ExactKernel::Point_2(1,0),global.unbounded_face());v33->data().triangulation_index=2;v33->data().height=v5->data().height;
    global.insert_at_vertices(Segment2(v11->point(),v22->point()),v11,v22);
    global.insert_at_vertices(Segment2(v22->point(),v33->point()),v22,v33);
    global.insert_at_vertices(Segment2(v33->point(),v11->point()),v33,v11);
    
    //more random stuff
    for(Arrangement2::Face_iterator fi=arr.faces_begin();fi!=arr.faces_end();fi++){
        fi->data().s=dice::throw_range(-1,1);
        fi->data().index=0;
    }
}


void Mesh::createTriangular(Arrangement2& arr,Arrangement2& global){
    Arrangement2::Vertex_handle v1 = arr.insert_in_face_interior(ExactKernel::Point_2(0,1), arr.unbounded_face());
    Arrangement2::Vertex_handle v2 = arr.insert_in_face_interior(ExactKernel::Point_2(0,0.5), arr.unbounded_face());
    Arrangement2::Vertex_handle v3 = arr.insert_in_face_interior(ExactKernel::Point_2(0,0), arr.unbounded_face());
    Arrangement2::Vertex_handle v4 = arr.insert_in_face_interior(ExactKernel::Point_2(0.5,0), arr.unbounded_face());
    Arrangement2::Vertex_handle v5 = arr.insert_in_face_interior(ExactKernel::Point_2(1,0), arr.unbounded_face());
    Arrangement2::Vertex_handle v6 = arr.insert_in_face_interior(ExactKernel::Point_2(0.5,0.5), arr.unbounded_face());
    v1->data().triangulation_index=0;
    v2->data().triangulation_index=2;
    v3->data().triangulation_index=1;
    v4->data().triangulation_index=0;
    v5->data().triangulation_index=2;
    v6->data().triangulation_index=1;
    arr.insert_at_vertices(Segment2(v1->point(),v2->point()),v1,v2);
    arr.insert_at_vertices(Segment2(v2->point(),v3->point()),v2,v3);
    arr.insert_at_vertices(Segment2(v3->point(),v4->point()),v3,v4);
    arr.insert_at_vertices(Segment2(v4->point(),v5->point()),v4,v5);
    arr.insert_at_vertices(Segment2(v5->point(),v6->point()),v5,v6);
    arr.insert_at_vertices(Segment2(v6->point(),v2->point()),v6,v2);
    arr.insert_at_vertices(Segment2(v6->point(),v1->point()),v6,v1);
    arr.insert_at_vertices(Segment2(v6->point(),v4->point()),v6,v4);
    arr.insert_at_vertices(Segment2(v2->point(),v4->point()),v2,v4);
    
    v1 = global.insert_in_face_interior(ExactKernel::Point_2(0,1),global.unbounded_face());v1->data().triangulation_index=0;
    v2 = global.insert_in_face_interior(ExactKernel::Point_2(0,0),global.unbounded_face());v2->data().triangulation_index=1;
    v3 = global.insert_in_face_interior(ExactKernel::Point_2(1,0),global.unbounded_face());v3->data().triangulation_index=2;
    global.insert_at_vertices(Segment2(v1->point(),v2->point()),v1,v2);
    global.insert_at_vertices(Segment2(v2->point(),v3->point()),v2,v3);
    global.insert_at_vertices(Segment2(v3->point(),v1->point()),v3,v1);
}

void Mesh::createRectangular1(std::vector<Arrangement2>& arr,std::vector<Arrangement2>& globals){
    arr.push_back(Arrangement2());
    Arrangement2::Vertex_handle v1 = arr[0].insert_in_face_interior(ExactKernel::Point_2(0,1), arr[0].unbounded_face()); v1->data().triangulation_index=1;
    Arrangement2::Vertex_handle v2 = arr[0].insert_in_face_interior(ExactKernel::Point_2(0,2.0/3.0), arr[0].unbounded_face()); v2->data().triangulation_index=0;
    Arrangement2::Vertex_handle v3 = arr[0].insert_in_face_interior(ExactKernel::Point_2(0,1.0/3.0), arr[0].unbounded_face()); v3->data().triangulation_index=1;
    Arrangement2::Vertex_handle v4 = arr[0].insert_in_face_interior(ExactKernel::Point_2(0,0), arr[0].unbounded_face()); v4->data().triangulation_index=0;
    Arrangement2::Vertex_handle v5 = arr[0].insert_in_face_interior(ExactKernel::Point_2(1.0/3.0,0), arr[0].unbounded_face()); v5->data().triangulation_index=2;
    Arrangement2::Vertex_handle v6 = arr[0].insert_in_face_interior(ExactKernel::Point_2(2.0/3.0,0), arr[0].unbounded_face()); v6->data().triangulation_index=0;
    Arrangement2::Vertex_handle v7 = arr[0].insert_in_face_interior(ExactKernel::Point_2(1,0), arr[0].unbounded_face()); v7->data().triangulation_index=2;
    Arrangement2::Vertex_handle v8 = arr[0].insert_in_face_interior(ExactKernel::Point_2(2.0/3.0,1.0/3.0), arr[0].unbounded_face()); v8->data().triangulation_index=1;
    Arrangement2::Vertex_handle v9 = arr[0].insert_in_face_interior(ExactKernel::Point_2(1.0/3.0,2.0/3.0), arr[0].unbounded_face()); v9->data().triangulation_index=2;
    Arrangement2::Vertex_handle v10 = arr[0].insert_in_face_interior(ExactKernel::Point_2(1.0/3.0,1.0/3.0),arr[0].unbounded_face());v10->data().triangulation_index=0;
    arr[0].insert_at_vertices(Segment2(v1->point(),v2->point()),v1,v2);
    arr[0].insert_at_vertices(Segment2(v2->point(),v3->point()),v2,v3);
    arr[0].insert_at_vertices(Segment2(v3->point(),v4->point()),v3,v4);
    arr[0].insert_at_vertices(Segment2(v4->point(),v5->point()),v4,v5);
    arr[0].insert_at_vertices(Segment2(v5->point(),v6->point()),v5,v6);
    arr[0].insert_at_vertices(Segment2(v6->point(),v7->point()),v6,v7);
    arr[0].insert_at_vertices(Segment2(v7->point(),v8->point()),v7,v8);
    arr[0].insert_at_vertices(Segment2(v8->point(),v9->point()),v8,v9);
    arr[0].insert_at_vertices(Segment2(v9->point(),v1->point()),v9,v1);
    
    arr[0].insert_at_vertices(Segment2(v2->point(),v9->point()),v2,v9);
    arr[0].insert_at_vertices(Segment2(v3->point(),v9->point()),v3,v9);
    arr[0].insert_at_vertices(Segment2(v3->point(),v10->point()),v3,v10);
    arr[0].insert_at_vertices(Segment2(v9->point(),v10->point()),v9,v10);
    arr[0].insert_at_vertices(Segment2(v8->point(),v10->point()),v8,v10);
    arr[0].insert_at_vertices(Segment2(v3->point(),v5->point()),v3,v5);
    arr[0].insert_at_vertices(Segment2(v5->point(),v10->point()),v5,v10);
    arr[0].insert_at_vertices(Segment2(v5->point(),v8->point()),v5,v8);
    arr[0].insert_at_vertices(Segment2(v6->point(),v8->point()),v6,v8);
    
    globals.push_back(Arrangement2());
    v1 = globals[0].insert_in_face_interior(ExactKernel::Point_2(0,1),globals[0].unbounded_face()); v1->data().triangulation_index=1;
    v2 = globals[0].insert_in_face_interior(ExactKernel::Point_2(0,0),globals[0].unbounded_face()); v2->data().triangulation_index=0;
    v3 = globals[0].insert_in_face_interior(ExactKernel::Point_2(1,0),globals[0].unbounded_face()); v3->data().triangulation_index=2;
    globals[0].insert_at_vertices(Segment2(v1->point(),v2->point()),v1,v2);
    globals[0].insert_at_vertices(Segment2(v2->point(),v3->point()),v2,v3);
    globals[0].insert_at_vertices(Segment2(v3->point(),v1->point()),v3,v1);
    arr.push_back(Arrangement2());
    
    v1 = arr[1].insert_in_face_interior(ExactKernel::Point_2(0,1), arr[1].unbounded_face()); v1->data().triangulation_index=1;
    v2 = arr[1].insert_in_face_interior(ExactKernel::Point_2(1.0/3.0,1), arr[1].unbounded_face()); v2->data().triangulation_index=3;
    v3 = arr[1].insert_in_face_interior(ExactKernel::Point_2(2.0/3.0,1), arr[1].unbounded_face()); v3->data().triangulation_index=1;
    v4 = arr[1].insert_in_face_interior(ExactKernel::Point_2(1,1), arr[1].unbounded_face()); v4->data().triangulation_index=3;
    v5 = arr[1].insert_in_face_interior(ExactKernel::Point_2(1,2.0/3.0), arr[1].unbounded_face()); v5->data().triangulation_index=2;
    v6 = arr[1].insert_in_face_interior(ExactKernel::Point_2(1,1.0/3.0), arr[1].unbounded_face()); v6->data().triangulation_index=3;
    v7 = arr[1].insert_in_face_interior(ExactKernel::Point_2(1,0), arr[1].unbounded_face()); v7->data().triangulation_index=2;
    v8 = arr[1].insert_in_face_interior(ExactKernel::Point_2(2.0/3.0,1.0/3.0), arr[1].unbounded_face()); v8->data().triangulation_index=1;
    v9 = arr[1].insert_in_face_interior(ExactKernel::Point_2(1.0/3.0,2.0/3.0), arr[1].unbounded_face()); v9->data().triangulation_index=2;
    v10 = arr[1].insert_in_face_interior(ExactKernel::Point_2(2.0/3.0,2.0/3.0), arr[1].unbounded_face()); v10->data().triangulation_index=3;
    
    arr[1].insert_at_vertices(Segment2(v1->point(),v2->point()),v1,v2);
    arr[1].insert_at_vertices(Segment2(v2->point(),v3->point()),v2,v3);
    arr[1].insert_at_vertices(Segment2(v3->point(),v4->point()),v3,v4);
    arr[1].insert_at_vertices(Segment2(v4->point(),v5->point()),v4,v5);
    arr[1].insert_at_vertices(Segment2(v5->point(),v6->point()),v5,v6);
    arr[1].insert_at_vertices(Segment2(v6->point(),v7->point()),v6,v7);
    arr[1].insert_at_vertices(Segment2(v7->point(),v8->point()),v7,v8);
    arr[1].insert_at_vertices(Segment2(v8->point(),v9->point()),v8,v9);
    arr[1].insert_at_vertices(Segment2(v9->point(),v1->point()),v9,v1);
    
    arr[1].insert_at_vertices(Segment2(v2->point(),v9->point()),v2,v9);
    arr[1].insert_at_vertices(Segment2(v3->point(),v9->point()),v3,v9);
    arr[1].insert_at_vertices(Segment2(v3->point(),v10->point()),v3,v10);
    arr[1].insert_at_vertices(Segment2(v9->point(),v10->point()),v9,v10);
    arr[1].insert_at_vertices(Segment2(v8->point(),v10->point()),v8,v10);
    arr[1].insert_at_vertices(Segment2(v3->point(),v5->point()),v3,v5);
    arr[1].insert_at_vertices(Segment2(v5->point(),v10->point()),v5,v10);
    arr[1].insert_at_vertices(Segment2(v5->point(),v8->point()),v5,v8);
    arr[1].insert_at_vertices(Segment2(v6->point(),v8->point()),v6,v8);
    
    globals.push_back(Arrangement2());
    v1 = globals[1].insert_in_face_interior(ExactKernel::Point_2(0,1),globals[1].unbounded_face()); v1->data().triangulation_index=1;
    v2 = globals[1].insert_in_face_interior(ExactKernel::Point_2(1,1),globals[1].unbounded_face()); v2->data().triangulation_index=3;
    v3 = globals[1].insert_in_face_interior(ExactKernel::Point_2(1,0),globals[1].unbounded_face()); v3->data().triangulation_index=2;
    
    globals[1].insert_at_vertices(Segment2(v1->point(),v2->point()),v1,v2);
    globals[1].insert_at_vertices(Segment2(v2->point(),v3->point()),v2,v3);
    globals[1].insert_at_vertices(Segment2(v3->point(),v1->point()),v3,v1);
    
    //add the face indexing
    int colors[3];
    int pattern1[]={0,1,2};
    int pattern2[]={1,2,3};
    for(std::vector<Arrangement2>::iterator it = arr.begin();it!=arr.end();it++){
        for(Arrangement2::Face_iterator fi=it->faces_begin();fi!=it->faces_end();fi++){
            if(fi->is_unbounded())continue;
            Arrangement2::Ccb_halfedge_circulator hc= fi->outer_ccb();
            colors[0]=hc->target()->data().triangulation_index;hc++;
            colors[1]=hc->target()->data().triangulation_index;hc++;
            colors[2]=hc->target()->data().triangulation_index;
            std::sort(colors,colors+3);
            if(std::equal(colors,colors+3,pattern1))fi->data().index=0;
            else if(std::equal(colors,colors+3,pattern2))fi->data().index=1;
        }
    }
    for(std::vector<Arrangement2>::iterator it = globals.begin();it!=globals.end();it++){
        for(Arrangement2::Face_iterator fi=it->faces_begin();fi!=it->faces_end();fi++){
            if(fi->is_unbounded())continue;
            Arrangement2::Ccb_halfedge_circulator hc= fi->outer_ccb();
            colors[0]=hc->target()->data().triangulation_index;hc++;
            colors[1]=hc->target()->data().triangulation_index;hc++;
            colors[2]=hc->target()->data().triangulation_index;
            std::sort(colors,colors+3);
            if(std::equal(colors,colors+3,pattern1))fi->data().index=0;
            else if(std::equal(colors,colors+3,pattern2))fi->data().index=1;
        }
    }
}

void Mesh::createRectangular2(std::vector<Arrangement2>& arr,std::vector<Arrangement2>& globals){
    arr.push_back(Arrangement2());
    Arrangement2::Vertex_handle v1 = arr[0].insert_in_face_interior(ExactKernel::Point_2(0,1), arr[0].unbounded_face()); v1->data().triangulation_index=1;
    Arrangement2::Vertex_handle v2 = arr[0].insert_in_face_interior(ExactKernel::Point_2(0,2.0/3.0), arr[0].unbounded_face()); v2->data().triangulation_index=0;
    Arrangement2::Vertex_handle v3 = arr[0].insert_in_face_interior(ExactKernel::Point_2(0,1.0/3.0), arr[0].unbounded_face()); v3->data().triangulation_index=1;
    Arrangement2::Vertex_handle v4 = arr[0].insert_in_face_interior(ExactKernel::Point_2(0,0), arr[0].unbounded_face()); v4->data().triangulation_index=0;
    Arrangement2::Vertex_handle v5 = arr[0].insert_in_face_interior(ExactKernel::Point_2(1.0/3.0,0), arr[0].unbounded_face()); v5->data().triangulation_index=2;
    Arrangement2::Vertex_handle v6 = arr[0].insert_in_face_interior(ExactKernel::Point_2(2.0/3.0,0), arr[0].unbounded_face()); v6->data().triangulation_index=0;
    Arrangement2::Vertex_handle v7 = arr[0].insert_in_face_interior(ExactKernel::Point_2(1,0), arr[0].unbounded_face()); v7->data().triangulation_index=2;
    Arrangement2::Vertex_handle v8 = arr[0].insert_in_face_interior(ExactKernel::Point_2(2.0/3.0,1.0/3.0), arr[0].unbounded_face()); v8->data().triangulation_index=1;
    Arrangement2::Vertex_handle v9 = arr[0].insert_in_face_interior(ExactKernel::Point_2(1.0/3.0,2.0/3.0), arr[0].unbounded_face()); v9->data().triangulation_index=2;
    Arrangement2::Vertex_handle v10 = arr[0].insert_in_face_interior(ExactKernel::Point_2(1.0/3.0,1.0/3.0),arr[0].unbounded_face());v10->data().triangulation_index=3;
    arr[0].insert_at_vertices(Segment2(v1->point(),v2->point()),v1,v2);
    arr[0].insert_at_vertices(Segment2(v2->point(),v3->point()),v2,v3);
    arr[0].insert_at_vertices(Segment2(v3->point(),v4->point()),v3,v4);
    arr[0].insert_at_vertices(Segment2(v4->point(),v5->point()),v4,v5);
    arr[0].insert_at_vertices(Segment2(v5->point(),v6->point()),v5,v6);
    arr[0].insert_at_vertices(Segment2(v6->point(),v7->point()),v6,v7);
    arr[0].insert_at_vertices(Segment2(v7->point(),v8->point()),v7,v8);
    arr[0].insert_at_vertices(Segment2(v8->point(),v9->point()),v8,v9);
    arr[0].insert_at_vertices(Segment2(v9->point(),v1->point()),v9,v1);
    
    arr[0].insert_at_vertices(Segment2(v2->point(),v9->point()),v2,v9);
    arr[0].insert_at_vertices(Segment2(v3->point(),v9->point()),v3,v9);
    arr[0].insert_at_vertices(Segment2(v3->point(),v10->point()),v3,v10);
    arr[0].insert_at_vertices(Segment2(v9->point(),v10->point()),v9,v10);
    arr[0].insert_at_vertices(Segment2(v8->point(),v10->point()),v8,v10);
    arr[0].insert_at_vertices(Segment2(v3->point(),v5->point()),v3,v5);
    arr[0].insert_at_vertices(Segment2(v5->point(),v10->point()),v5,v10);
    arr[0].insert_at_vertices(Segment2(v5->point(),v8->point()),v5,v8);
    arr[0].insert_at_vertices(Segment2(v6->point(),v8->point()),v6,v8);
    
    globals.push_back(Arrangement2());
    v1 = globals[0].insert_in_face_interior(ExactKernel::Point_2(0,1),globals[0].unbounded_face()); v1->data().triangulation_index=1;
    v2 = globals[0].insert_in_face_interior(ExactKernel::Point_2(0,0),globals[0].unbounded_face()); v2->data().triangulation_index=0;
    v3 = globals[0].insert_in_face_interior(ExactKernel::Point_2(1,0),globals[0].unbounded_face()); v3->data().triangulation_index=2;
    globals[0].insert_at_vertices(Segment2(v1->point(),v2->point()),v1,v2);
    globals[0].insert_at_vertices(Segment2(v2->point(),v3->point()),v2,v3);
    globals[0].insert_at_vertices(Segment2(v3->point(),v1->point()),v3,v1);
    arr.push_back(Arrangement2());
    
    v1 = arr[1].insert_in_face_interior(ExactKernel::Point_2(0,1), arr[1].unbounded_face()); v1->data().triangulation_index=1;
    v2 = arr[1].insert_in_face_interior(ExactKernel::Point_2(1.0/3.0,1), arr[1].unbounded_face()); v2->data().triangulation_index=3;
    v3 = arr[1].insert_in_face_interior(ExactKernel::Point_2(2.0/3.0,1), arr[1].unbounded_face()); v3->data().triangulation_index=1;
    v4 = arr[1].insert_in_face_interior(ExactKernel::Point_2(1,1), arr[1].unbounded_face()); v4->data().triangulation_index=3;
    v5 = arr[1].insert_in_face_interior(ExactKernel::Point_2(1,2.0/3.0), arr[1].unbounded_face()); v5->data().triangulation_index=2;
    v6 = arr[1].insert_in_face_interior(ExactKernel::Point_2(1,1.0/3.0), arr[1].unbounded_face()); v6->data().triangulation_index=3;
    v7 = arr[1].insert_in_face_interior(ExactKernel::Point_2(1,0), arr[1].unbounded_face()); v7->data().triangulation_index=2;
    v8 = arr[1].insert_in_face_interior(ExactKernel::Point_2(2.0/3.0,1.0/3.0), arr[1].unbounded_face()); v8->data().triangulation_index=1;
    v9 = arr[1].insert_in_face_interior(ExactKernel::Point_2(1.0/3.0,2.0/3.0), arr[1].unbounded_face()); v9->data().triangulation_index=2;
    v10 = arr[1].insert_in_face_interior(ExactKernel::Point_2(2.0/3.0,2.0/3.0), arr[1].unbounded_face()); v10->data().triangulation_index=0;
    
    arr[1].insert_at_vertices(Segment2(v1->point(),v2->point()),v1,v2);
    arr[1].insert_at_vertices(Segment2(v2->point(),v3->point()),v2,v3);
    arr[1].insert_at_vertices(Segment2(v3->point(),v4->point()),v3,v4);
    arr[1].insert_at_vertices(Segment2(v4->point(),v5->point()),v4,v5);
    arr[1].insert_at_vertices(Segment2(v5->point(),v6->point()),v5,v6);
    arr[1].insert_at_vertices(Segment2(v6->point(),v7->point()),v6,v7);
    arr[1].insert_at_vertices(Segment2(v7->point(),v8->point()),v7,v8);
    arr[1].insert_at_vertices(Segment2(v8->point(),v9->point()),v8,v9);
    arr[1].insert_at_vertices(Segment2(v9->point(),v1->point()),v9,v1);
    
    arr[1].insert_at_vertices(Segment2(v2->point(),v9->point()),v2,v9);
    arr[1].insert_at_vertices(Segment2(v3->point(),v9->point()),v3,v9);
    arr[1].insert_at_vertices(Segment2(v3->point(),v10->point()),v3,v10);
    arr[1].insert_at_vertices(Segment2(v9->point(),v10->point()),v9,v10);
    arr[1].insert_at_vertices(Segment2(v8->point(),v10->point()),v8,v10);
    arr[1].insert_at_vertices(Segment2(v3->point(),v5->point()),v3,v5);
    arr[1].insert_at_vertices(Segment2(v5->point(),v10->point()),v5,v10);
    arr[1].insert_at_vertices(Segment2(v5->point(),v8->point()),v5,v8);
    arr[1].insert_at_vertices(Segment2(v6->point(),v8->point()),v6,v8);
    
    globals.push_back(Arrangement2());
    v1 = globals[1].insert_in_face_interior(ExactKernel::Point_2(0,1),globals[1].unbounded_face()); v1->data().triangulation_index=1;
    v2 = globals[1].insert_in_face_interior(ExactKernel::Point_2(1,1),globals[1].unbounded_face()); v2->data().triangulation_index=3;
    v3 = globals[1].insert_in_face_interior(ExactKernel::Point_2(1,0),globals[1].unbounded_face()); v3->data().triangulation_index=2;
    
    globals[1].insert_at_vertices(Segment2(v1->point(),v2->point()),v1,v2);
    globals[1].insert_at_vertices(Segment2(v2->point(),v3->point()),v2,v3);
    globals[1].insert_at_vertices(Segment2(v3->point(),v1->point()),v3,v1);
}

void Mesh::createRectangular3(Arrangement2& arr,Arrangement2& globals){
    Arrangement2::Vertex_handle v1 = arr.insert_in_face_interior(ExactKernel::Point_2(0,1), arr.unbounded_face()); v1->data().triangulation_index=0;
    Arrangement2::Vertex_handle v2 = arr.insert_in_face_interior(ExactKernel::Point_2(1.0/3.0,1), arr.unbounded_face()); v2->data().triangulation_index=1;
    Arrangement2::Vertex_handle v3 = arr.insert_in_face_interior(ExactKernel::Point_2(2.0/3.0,1), arr.unbounded_face()); v3->data().triangulation_index=0;
    Arrangement2::Vertex_handle v4 = arr.insert_in_face_interior(ExactKernel::Point_2(1,1), arr.unbounded_face()); v4->data().triangulation_index=1;
    
    Arrangement2::Vertex_handle v5 = arr.insert_in_face_interior(ExactKernel::Point_2(1,2.0/3.0), arr.unbounded_face()); v5->data().triangulation_index=2;
    Arrangement2::Vertex_handle v6 = arr.insert_in_face_interior(ExactKernel::Point_2(1,1.0/3.0), arr.unbounded_face()); v6->data().triangulation_index=1;
    Arrangement2::Vertex_handle v7 = arr.insert_in_face_interior(ExactKernel::Point_2(1,0), arr.unbounded_face()); v7->data().triangulation_index=2;
    
    Arrangement2::Vertex_handle v8 = arr.insert_in_face_interior(ExactKernel::Point_2(2.0/3.0,0), arr.unbounded_face()); v8->data().triangulation_index=3;
    Arrangement2::Vertex_handle v9 = arr.insert_in_face_interior(ExactKernel::Point_2(1.0/3.0,0), arr.unbounded_face()); v9->data().triangulation_index=2;
    Arrangement2::Vertex_handle v10 = arr.insert_in_face_interior(ExactKernel::Point_2(0,0), arr.unbounded_face()); v10->data().triangulation_index=3;
    
    Arrangement2::Vertex_handle v11 = arr.insert_in_face_interior(ExactKernel::Point_2(0,1.0/3.0), arr.unbounded_face()); v11->data().triangulation_index=0;
    Arrangement2::Vertex_handle v12 = arr.insert_in_face_interior(ExactKernel::Point_2(0,2.0/3.0), arr.unbounded_face()); v12->data().triangulation_index=3;
    
    Arrangement2::Vertex_handle v13 = arr.insert_in_face_interior(ExactKernel::Point_2(1.0/3.0,2.0/3.0), arr.unbounded_face()); v13->data().triangulation_index=2;
    Arrangement2::Vertex_handle v14 = arr.insert_in_face_interior(ExactKernel::Point_2(2.0/3.0,2.0/3.0), arr.unbounded_face()); v14->data().triangulation_index=3;
    
    Arrangement2::Vertex_handle v15 = arr.insert_in_face_interior(ExactKernel::Point_2(2.0/3.0,1.0/3.0), arr.unbounded_face()); v15->data().triangulation_index=0;
    Arrangement2::Vertex_handle v16 = arr.insert_in_face_interior(ExactKernel::Point_2(1.0/3.0,1.0/3.0), arr.unbounded_face()); v16->data().triangulation_index=1;
    
    arr.insert_at_vertices(Segment2(v1->point(),v2->point()),v1,v2);
    arr.insert_at_vertices(Segment2(v2->point(),v3->point()),v2,v3);
    arr.insert_at_vertices(Segment2(v3->point(),v4->point()),v3,v4);
    arr.insert_at_vertices(Segment2(v4->point(),v5->point()),v4,v5);
    arr.insert_at_vertices(Segment2(v5->point(),v6->point()),v5,v6);
    arr.insert_at_vertices(Segment2(v6->point(),v7->point()),v6,v7);
    arr.insert_at_vertices(Segment2(v7->point(),v8->point()),v7,v8);
    arr.insert_at_vertices(Segment2(v8->point(),v9->point()),v8,v9);
    arr.insert_at_vertices(Segment2(v9->point(),v10->point()),v9,v10);
    arr.insert_at_vertices(Segment2(v10->point(),v11->point()),v10,v11);
    arr.insert_at_vertices(Segment2(v11->point(),v12->point()),v11,v12);
    arr.insert_at_vertices(Segment2(v12->point(),v1->point()),v12,v1);
    
    arr.insert_at_vertices(Segment2(v2->point(),v13->point()),v2,v13);
    arr.insert_at_vertices(Segment2(v3->point(),v14->point()),v3,v14);
    arr.insert_at_vertices(Segment2(v12->point(),v13->point()),v12,v13);
    arr.insert_at_vertices(Segment2(v13->point(),v14->point()),v13,v14);
    arr.insert_at_vertices(Segment2(v14->point(),v5->point()),v14,v5);
    arr.insert_at_vertices(Segment2(v13->point(),v16->point()),v13,v16);
    arr.insert_at_vertices(Segment2(v14->point(),v15->point()),v14,v15);
    arr.insert_at_vertices(Segment2(v11->point(),v16->point()),v11,v16);
    arr.insert_at_vertices(Segment2(v16->point(),v15->point()),v16,v15);
    arr.insert_at_vertices(Segment2(v15->point(),v6->point()),v15,v6);
    arr.insert_at_vertices(Segment2(v16->point(),v9->point()),v16,v9);
    arr.insert_at_vertices(Segment2(v15->point(),v8->point()),v15,v8);
    
    v1 = globals.insert_in_face_interior(ExactKernel::Point_2(0,1), globals.unbounded_face()); v1->data().triangulation_index=0;
    v2 = globals.insert_in_face_interior(ExactKernel::Point_2(1,1), globals.unbounded_face()); v2->data().triangulation_index=1;
    v3 = globals.insert_in_face_interior(ExactKernel::Point_2(1,0), globals.unbounded_face()); v3->data().triangulation_index=2;
    v4 = globals.insert_in_face_interior(ExactKernel::Point_2(0,0), globals.unbounded_face()); v4->data().triangulation_index=3;
    
    globals.insert_at_vertices(Segment2(v1->point(),v2->point()),v1,v2);
    globals.insert_at_vertices(Segment2(v2->point(),v3->point()),v2,v3);
    globals.insert_at_vertices(Segment2(v3->point(),v4->point()),v3,v4);
    globals.insert_at_vertices(Segment2(v4->point(),v1->point()),v4,v1);
    
}

void Mesh::createRectangular4(std::vector<Arrangement2>& arr,std::vector<Arrangement2>& globals){
    //q00
    arr.push_back(Arrangement2());
    globals.push_back(Arrangement2());
    Arrangement2::Vertex_handle v1 = arr[0].insert_in_face_interior(ExactKernel::Point_2(0,3.0/6.0), arr[0].unbounded_face()); v1->data().triangulation_index=3;
    Arrangement2::Vertex_handle v2 = arr[0].insert_in_face_interior(ExactKernel::Point_2(1.0/6.0,3.0/6.0), arr[0].unbounded_face()); v2->data().triangulation_index=4;
    Arrangement2::Vertex_handle v3 = arr[0].insert_in_face_interior(ExactKernel::Point_2(2.0/6.0,3.0/6.0), arr[0].unbounded_face()); v3->data().triangulation_index=3;
    Arrangement2::Vertex_handle v4 = arr[0].insert_in_face_interior(ExactKernel::Point_2(3.0/6.0,3.0/6.0), arr[0].unbounded_face()); v4->data().triangulation_index=4;
    
    Arrangement2::Vertex_handle v5 = arr[0].insert_in_face_interior(ExactKernel::Point_2(0,2.0/6.0), arr[0].unbounded_face()); v5->data().triangulation_index=6;
    Arrangement2::Vertex_handle v6 = arr[0].insert_in_face_interior(ExactKernel::Point_2(1.0/6.0,2.0/6.0), arr[0].unbounded_face()); v6->data().triangulation_index=7;
    Arrangement2::Vertex_handle v7 = arr[0].insert_in_face_interior(ExactKernel::Point_2(2.0/6.0,2.0/6.0), arr[0].unbounded_face()); v7->data().triangulation_index=6;
    Arrangement2::Vertex_handle v8 = arr[0].insert_in_face_interior(ExactKernel::Point_2(3.0/6.0,2.0/6.0), arr[0].unbounded_face()); v8->data().triangulation_index=7;
    
    Arrangement2::Vertex_handle v9 = arr[0].insert_in_face_interior(ExactKernel::Point_2(0,1.0/6.0), arr[0].unbounded_face()); v9->data().triangulation_index=3;
    Arrangement2::Vertex_handle v10 = arr[0].insert_in_face_interior(ExactKernel::Point_2(1.0/6.0,1.0/6.0), arr[0].unbounded_face()); v10->data().triangulation_index=4;
    Arrangement2::Vertex_handle v11 = arr[0].insert_in_face_interior(ExactKernel::Point_2(2.0/6.0,1.0/6.0), arr[0].unbounded_face()); v11->data().triangulation_index=3;
    Arrangement2::Vertex_handle v12 = arr[0].insert_in_face_interior(ExactKernel::Point_2(3.0/6.0,1.0/6.0), arr[0].unbounded_face()); v12->data().triangulation_index=4;
    
    Arrangement2::Vertex_handle v13 = arr[0].insert_in_face_interior(ExactKernel::Point_2(0,0.0/6.0), arr[0].unbounded_face()); v13->data().triangulation_index=6;
    Arrangement2::Vertex_handle v14 = arr[0].insert_in_face_interior(ExactKernel::Point_2(1.0/6.0,0.0/6.0), arr[0].unbounded_face()); v14->data().triangulation_index=7;
    Arrangement2::Vertex_handle v15 = arr[0].insert_in_face_interior(ExactKernel::Point_2(2.0/6.0,0.0/6.0), arr[0].unbounded_face()); v15->data().triangulation_index=6;
    Arrangement2::Vertex_handle v16 = arr[0].insert_in_face_interior(ExactKernel::Point_2(3.0/6.0,0.0/6.0), arr[0].unbounded_face()); v16->data().triangulation_index=7;
    
    arr[0].insert_at_vertices(Segment2(v1->point(),v2->point()),v1,v2);
    arr[0].insert_at_vertices(Segment2(v2->point(),v3->point()),v2,v3);
    arr[0].insert_at_vertices(Segment2(v3->point(),v4->point()),v3,v4);
    
    arr[0].insert_at_vertices(Segment2(v5->point(),v6->point()),v5,v6);
    arr[0].insert_at_vertices(Segment2(v6->point(),v7->point()),v6,v7);
    arr[0].insert_at_vertices(Segment2(v7->point(),v8->point()),v7,v8);
    
    arr[0].insert_at_vertices(Segment2(v9->point(),v10->point()),v9,v10);
    arr[0].insert_at_vertices(Segment2(v10->point(),v11->point()),v10,v11);
    arr[0].insert_at_vertices(Segment2(v11->point(),v12->point()),v11,v12);
    
    arr[0].insert_at_vertices(Segment2(v13->point(),v14->point()),v13,v14);
    arr[0].insert_at_vertices(Segment2(v14->point(),v15->point()),v14,v15);
    arr[0].insert_at_vertices(Segment2(v15->point(),v16->point()),v15,v16);
    
    arr[0].insert_at_vertices(Segment2(v1->point(),v5->point()),v1,v5);
    arr[0].insert_at_vertices(Segment2(v5->point(),v9->point()),v5,v9);
    arr[0].insert_at_vertices(Segment2(v9->point(),v13->point()),v9,v13);
    
    arr[0].insert_at_vertices(Segment2(v2->point(),v6->point()),v2,v6);
    arr[0].insert_at_vertices(Segment2(v6->point(),v10->point()),v6,v10);
    arr[0].insert_at_vertices(Segment2(v10->point(),v14->point()),v10,v14);
    
    arr[0].insert_at_vertices(Segment2(v3->point(),v7->point()),v3,v7);
    arr[0].insert_at_vertices(Segment2(v7->point(),v11->point()),v7,v11);
    arr[0].insert_at_vertices(Segment2(v11->point(),v15->point()),v11,v15);
    
    arr[0].insert_at_vertices(Segment2(v4->point(),v8->point()),v4,v8);
    arr[0].insert_at_vertices(Segment2(v8->point(),v12->point()),v8,v12);
    arr[0].insert_at_vertices(Segment2(v12->point(),v16->point()),v12,v16);
    
    v1 = globals[0].insert_in_face_interior(ExactKernel::Point_2(0,0.5), globals[0].unbounded_face()); v1->data().triangulation_index=3;
    v2 = globals[0].insert_in_face_interior(ExactKernel::Point_2(3.0/6.0,0.5), globals[0].unbounded_face()); v2->data().triangulation_index=4;
    v3 = globals[0].insert_in_face_interior(ExactKernel::Point_2(3.0/6.0,0.0), globals[0].unbounded_face()); v3->data().triangulation_index=7;
    v4 = globals[0].insert_in_face_interior(ExactKernel::Point_2(0.0/6.0,0.0), globals[0].unbounded_face()); v4->data().triangulation_index=6;
    
    globals[0].insert_at_vertices(Segment2(v1->point(),v2->point()),v1,v2);
    globals[0].insert_at_vertices(Segment2(v2->point(),v3->point()),v2,v3);
    globals[0].insert_at_vertices(Segment2(v3->point(),v4->point()),v3,v4);
    globals[0].insert_at_vertices(Segment2(v4->point(),v1->point()),v4,v1);
    //q01
    arr.push_back(Arrangement2());
    globals.push_back(Arrangement2());
    v1 = arr[1].insert_in_face_interior(ExactKernel::Point_2(0.5+0,3.0/6.0),arr[1].unbounded_face()); v1->data().triangulation_index=4;
    v2 = arr[1].insert_in_face_interior(ExactKernel::Point_2(0.5+1.0/6.0,3.0/6.0),arr[1].unbounded_face()); v2->data().triangulation_index=5;
    v3 = arr[1].insert_in_face_interior(ExactKernel::Point_2(0.5+2.0/6.0,3.0/6.0),arr[1].unbounded_face()); v3->data().triangulation_index=4;
    v4 = arr[1].insert_in_face_interior(ExactKernel::Point_2(0.5+3.0/6.0,3.0/6.0),arr[1].unbounded_face()); v4->data().triangulation_index=5;
    
    v5 = arr[1].insert_in_face_interior(ExactKernel::Point_2(0.5+0,2.0/6.0),arr[1].unbounded_face()); v5->data().triangulation_index=7;
    v6 = arr[1].insert_in_face_interior(ExactKernel::Point_2(0.5+1.0/6.0,2.0/6.0),arr[1].unbounded_face()); v6->data().triangulation_index=8;
    v7 = arr[1].insert_in_face_interior(ExactKernel::Point_2(0.5+2.0/6.0,2.0/6.0),arr[1].unbounded_face()); v7->data().triangulation_index=7;
    v8 = arr[1].insert_in_face_interior(ExactKernel::Point_2(0.5+3.0/6.0,2.0/6.0),arr[1].unbounded_face()); v8->data().triangulation_index=8;
    
    v9 = arr[1].insert_in_face_interior(ExactKernel::Point_2(0.5+0,1.0/6.0), arr[1].unbounded_face()); v9->data().triangulation_index=4;
    v10=arr[1].insert_in_face_interior(ExactKernel::Point_2(0.5+1.0/6.0,1.0/6.0),arr[1].unbounded_face()); v10->data().triangulation_index=5;
    v11=arr[1].insert_in_face_interior(ExactKernel::Point_2(0.5+2.0/6.0,1.0/6.0),arr[1].unbounded_face()); v11->data().triangulation_index=4;
    v12=arr[1].insert_in_face_interior(ExactKernel::Point_2(0.5+3.0/6.0,1.0/6.0),arr[1].unbounded_face()); v12->data().triangulation_index=5;
    
    v13 = arr[1].insert_in_face_interior(ExactKernel::Point_2(0.5+0,0.0/6.0), arr[1].unbounded_face()); v13->data().triangulation_index=7;
    v14 =arr[1].insert_in_face_interior(ExactKernel::Point_2(0.5+1.0/6.0,0.0/6.0),arr[1].unbounded_face()); v14->data().triangulation_index=8;
    v15=arr[1].insert_in_face_interior(ExactKernel::Point_2(0.5+2.0/6.0,0.0/6.0), arr[1].unbounded_face());v15->data().triangulation_index=7;
    v16=arr[1].insert_in_face_interior(ExactKernel::Point_2(0.5+3.0/6.0,0.0/6.0),arr[1].unbounded_face()); v16->data().triangulation_index=8;
    
    arr[1].insert_at_vertices(Segment2(v1->point(),v2->point()),v1,v2);
    arr[1].insert_at_vertices(Segment2(v2->point(),v3->point()),v2,v3);
    arr[1].insert_at_vertices(Segment2(v3->point(),v4->point()),v3,v4);
    
    arr[1].insert_at_vertices(Segment2(v5->point(),v6->point()),v5,v6);
    arr[1].insert_at_vertices(Segment2(v6->point(),v7->point()),v6,v7);
    arr[1].insert_at_vertices(Segment2(v7->point(),v8->point()),v7,v8);
    
    arr[1].insert_at_vertices(Segment2(v9->point(),v10->point()),v9,v10);
    arr[1].insert_at_vertices(Segment2(v10->point(),v11->point()),v10,v11);
    arr[1].insert_at_vertices(Segment2(v11->point(),v12->point()),v11,v12);
    
    arr[1].insert_at_vertices(Segment2(v13->point(),v14->point()),v13,v14);
    arr[1].insert_at_vertices(Segment2(v14->point(),v15->point()),v14,v15);
    arr[1].insert_at_vertices(Segment2(v15->point(),v16->point()),v15,v16);
    
    arr[1].insert_at_vertices(Segment2(v1->point(),v5->point()),v1,v5);
    arr[1].insert_at_vertices(Segment2(v5->point(),v9->point()),v5,v9);
    arr[1].insert_at_vertices(Segment2(v9->point(),v13->point()),v9,v13);
    
    arr[1].insert_at_vertices(Segment2(v2->point(),v6->point()),v2,v6);
    arr[1].insert_at_vertices(Segment2(v6->point(),v10->point()),v6,v10);
    arr[1].insert_at_vertices(Segment2(v10->point(),v14->point()),v10,v14);
    
    arr[1].insert_at_vertices(Segment2(v3->point(),v7->point()),v3,v7);
    arr[1].insert_at_vertices(Segment2(v7->point(),v11->point()),v7,v11);
    arr[1].insert_at_vertices(Segment2(v11->point(),v15->point()),v11,v15);
    
    arr[1].insert_at_vertices(Segment2(v4->point(),v8->point()),v4,v8);
    arr[1].insert_at_vertices(Segment2(v8->point(),v12->point()),v8,v12);
    arr[1].insert_at_vertices(Segment2(v12->point(),v16->point()),v12,v16);
    
    v1 = globals[1].insert_in_face_interior(ExactKernel::Point_2(0.5+0,0.5), globals[1].unbounded_face()); v1->data().triangulation_index=4;
    v2 = globals[1].insert_in_face_interior(ExactKernel::Point_2(0.5+3.0/6.0,0.5), globals[1].unbounded_face()); v2->data().triangulation_index=5;
    v3 = globals[1].insert_in_face_interior(ExactKernel::Point_2(0.5+3.0/6.0,0.0), globals[1].unbounded_face()); v3->data().triangulation_index=8;
    v4 = globals[1].insert_in_face_interior(ExactKernel::Point_2(0.5+0.0/6.0,0.0), globals[1].unbounded_face()); v4->data().triangulation_index=7;
    
    globals[1].insert_at_vertices(Segment2(v1->point(),v2->point()),v1,v2);
    globals[1].insert_at_vertices(Segment2(v2->point(),v3->point()),v2,v3);
    globals[1].insert_at_vertices(Segment2(v3->point(),v4->point()),v3,v4);
    globals[1].insert_at_vertices(Segment2(v4->point(),v1->point()),v4,v1);
    
    ///////
    //q10
    arr.push_back(Arrangement2());
    globals.push_back(Arrangement2());
    v1=arr[2].insert_in_face_interior(ExactKernel::Point_2(0,0.5+3.0/6.0), arr[2].unbounded_face()); v1->data().triangulation_index=0;
    v2=arr[2].insert_in_face_interior(ExactKernel::Point_2(1.0/6.0,0.5+3.0/6.0), arr[2].unbounded_face()); v2->data().triangulation_index=1;
    v3=arr[2].insert_in_face_interior(ExactKernel::Point_2(2.0/6.0,0.5+3.0/6.0), arr[2].unbounded_face()); v3->data().triangulation_index=0;
    v4=arr[2].insert_in_face_interior(ExactKernel::Point_2(3.0/6.0,0.5+3.0/6.0), arr[2].unbounded_face()); v4->data().triangulation_index=1;
    
    v5 = arr[2].insert_in_face_interior(ExactKernel::Point_2(0,0.5+2.0/6.0), arr[2].unbounded_face()); v5->data().triangulation_index=3;
    v6=arr[2].insert_in_face_interior(ExactKernel::Point_2(1.0/6.0,0.5+2.0/6.0), arr[2].unbounded_face()); v6->data().triangulation_index=4;
    v7=arr[2].insert_in_face_interior(ExactKernel::Point_2(2.0/6.0,0.5+2.0/6.0), arr[2].unbounded_face()); v7->data().triangulation_index=3;
    v8=arr[2].insert_in_face_interior(ExactKernel::Point_2(3.0/6.0,0.5+2.0/6.0), arr[2].unbounded_face()); v8->data().triangulation_index=4;
    
    v9 = arr[2].insert_in_face_interior(ExactKernel::Point_2(0,0.5+1.0/6.0), arr[2].unbounded_face()); v9->data().triangulation_index=0;
    v10=arr[2].insert_in_face_interior(ExactKernel::Point_2(1.0/6.0,0.5+1.0/6.0),arr[2].unbounded_face()); v10->data().triangulation_index=1;
    v11=arr[2].insert_in_face_interior(ExactKernel::Point_2(2.0/6.0,0.5+1.0/6.0),arr[2].unbounded_face()); v11->data().triangulation_index=0;
    v12=arr[2].insert_in_face_interior(ExactKernel::Point_2(3.0/6.0,0.5+1.0/6.0),arr[2].unbounded_face()); v12->data().triangulation_index=1;
    
    v13 = arr[2].insert_in_face_interior(ExactKernel::Point_2(0,0.5+0.0/6.0), arr[2].unbounded_face()); v13->data().triangulation_index=3;
    v14=arr[2].insert_in_face_interior(ExactKernel::Point_2(1.0/6.0,0.5+0.0/6.0), arr[2].unbounded_face()); v14->data().triangulation_index=4;
    v15=arr[2].insert_in_face_interior(ExactKernel::Point_2(2.0/6.0,0.5+0.0/6.0),arr[2].unbounded_face()); v15->data().triangulation_index=3;
    v16=arr[2].insert_in_face_interior(ExactKernel::Point_2(3.0/6.0,0.5+0.0/6.0),arr[2].unbounded_face()); v16->data().triangulation_index=4;
    
    arr[2].insert_at_vertices(Segment2(v1->point(),v2->point()),v1,v2);
    arr[2].insert_at_vertices(Segment2(v2->point(),v3->point()),v2,v3);
    arr[2].insert_at_vertices(Segment2(v3->point(),v4->point()),v3,v4);
    
    arr[2].insert_at_vertices(Segment2(v5->point(),v6->point()),v5,v6);
    arr[2].insert_at_vertices(Segment2(v6->point(),v7->point()),v6,v7);
    arr[2].insert_at_vertices(Segment2(v7->point(),v8->point()),v7,v8);
    
    arr[2].insert_at_vertices(Segment2(v9->point(),v10->point()),v9,v10);
    arr[2].insert_at_vertices(Segment2(v10->point(),v11->point()),v10,v11);
    arr[2].insert_at_vertices(Segment2(v11->point(),v12->point()),v11,v12);
    
    arr[2].insert_at_vertices(Segment2(v13->point(),v14->point()),v13,v14);
    arr[2].insert_at_vertices(Segment2(v14->point(),v15->point()),v14,v15);
    arr[2].insert_at_vertices(Segment2(v15->point(),v16->point()),v15,v16);
    
    arr[2].insert_at_vertices(Segment2(v1->point(),v5->point()),v1,v5);
    arr[2].insert_at_vertices(Segment2(v5->point(),v9->point()),v5,v9);
    arr[2].insert_at_vertices(Segment2(v9->point(),v13->point()),v9,v13);
    
    arr[2].insert_at_vertices(Segment2(v2->point(),v6->point()),v2,v6);
    arr[2].insert_at_vertices(Segment2(v6->point(),v10->point()),v6,v10);
    arr[2].insert_at_vertices(Segment2(v10->point(),v14->point()),v10,v14);
    
    arr[2].insert_at_vertices(Segment2(v3->point(),v7->point()),v3,v7);
    arr[2].insert_at_vertices(Segment2(v7->point(),v11->point()),v7,v11);
    arr[2].insert_at_vertices(Segment2(v11->point(),v15->point()),v11,v15);
    
    arr[2].insert_at_vertices(Segment2(v4->point(),v8->point()),v4,v8);
    arr[2].insert_at_vertices(Segment2(v8->point(),v12->point()),v8,v12);
    arr[2].insert_at_vertices(Segment2(v12->point(),v16->point()),v12,v16);
    
    v1 = globals[2].insert_in_face_interior(ExactKernel::Point_2(0,0.5+0.5), globals[2].unbounded_face()); v1->data().triangulation_index=0;
    v2 = globals[2].insert_in_face_interior(ExactKernel::Point_2(3.0/6.0,0.5+0.5), globals[2].unbounded_face()); v2->data().triangulation_index=1;
    v3 = globals[2].insert_in_face_interior(ExactKernel::Point_2(3.0/6.0,0.5+0.0), globals[2].unbounded_face()); v3->data().triangulation_index=4;
    v4 = globals[2].insert_in_face_interior(ExactKernel::Point_2(0.0/6.0,0.5+0.0), globals[2].unbounded_face()); v4->data().triangulation_index=3;
    
    globals[2].insert_at_vertices(Segment2(v1->point(),v2->point()),v1,v2);
    globals[2].insert_at_vertices(Segment2(v2->point(),v3->point()),v2,v3);
    globals[2].insert_at_vertices(Segment2(v3->point(),v4->point()),v3,v4);
    globals[2].insert_at_vertices(Segment2(v4->point(),v1->point()),v4,v1);
    //q11
    arr.push_back(Arrangement2());
    globals.push_back(Arrangement2());
    v1 = arr[3].insert_in_face_interior(ExactKernel::Point_2(0.5+0,0.5+3.0/6.0),arr[3].unbounded_face()); v1->data().triangulation_index=1;
    v2=arr[3].insert_in_face_interior(ExactKernel::Point_2(0.5+1.0/6.0,.5+3.0/6.0),arr[3].unbounded_face());v2->data().triangulation_index=2;
    v3=arr[3].insert_in_face_interior(ExactKernel::Point_2(0.5+2.0/6.0,.5+3.0/6.0),arr[3].unbounded_face());v3->data().triangulation_index=1;
    v4=arr[3].insert_in_face_interior(ExactKernel::Point_2(0.5+3.0/6.0,.5+3.0/6.0),arr[3].unbounded_face());v4->data().triangulation_index=2;
    
    v5 = arr[3].insert_in_face_interior(ExactKernel::Point_2(0.5+0,.5+2.0/6.0),arr[3].unbounded_face()); v5->data().triangulation_index=4;
    v6=arr[3].insert_in_face_interior(ExactKernel::Point_2(0.5+1.0/6.0,.5+2.0/6.0),arr[3].unbounded_face());v6->data().triangulation_index=5;
    v7=arr[3].insert_in_face_interior(ExactKernel::Point_2(0.5+2.0/6.0,.5+2.0/6.0),arr[3].unbounded_face());v7->data().triangulation_index=4;
    v8=arr[3].insert_in_face_interior(ExactKernel::Point_2(0.5+3.0/6.0,.5+2.0/6.0),arr[3].unbounded_face());v8->data().triangulation_index=5;
    
    v9 = arr[3].insert_in_face_interior(ExactKernel::Point_2(0.5+0,.5+1.0/6.0), arr[3].unbounded_face()); v9->data().triangulation_index=1;
    v10=arr[3].insert_in_face_interior(ExactKernel::Point_2(.5+1.0/6.0,.5+1.0/6.0),arr[3].unbounded_face());v10->data().triangulation_index=2;
    v11=arr[3].insert_in_face_interior(ExactKernel::Point_2(.5+2.0/6.0,.5+1.0/6.0),arr[3].unbounded_face());v11->data().triangulation_index=1;
    v12=arr[3].insert_in_face_interior(ExactKernel::Point_2(.5+3.0/6.0,.5+1.0/6.0),arr[3].unbounded_face());v12->data().triangulation_index=2;
    
    v13 = arr[3].insert_in_face_interior(ExactKernel::Point_2(.5+0,.5+0.0/6.0), arr[3].unbounded_face()); v13->data().triangulation_index=4;
    v14=arr[3].insert_in_face_interior(ExactKernel::Point_2(.5+1.0/6.0,.5+0.0/6.0),arr[3].unbounded_face());v14->data().triangulation_index=5;
    v15=arr[3].insert_in_face_interior(ExactKernel::Point_2(.5+2.0/6.0,.5+0.0/6.0),arr[3].unbounded_face());v15->data().triangulation_index=4;
    v16=arr[3].insert_in_face_interior(ExactKernel::Point_2(.5+3.0/6.0,.5+0.0/6.0),arr[3].unbounded_face());v16->data().triangulation_index=5;
    
    arr[3].insert_at_vertices(Segment2(v1->point(),v2->point()),v1,v2);
    arr[3].insert_at_vertices(Segment2(v2->point(),v3->point()),v2,v3);
    arr[3].insert_at_vertices(Segment2(v3->point(),v4->point()),v3,v4);
    
    arr[3].insert_at_vertices(Segment2(v5->point(),v6->point()),v5,v6);
    arr[3].insert_at_vertices(Segment2(v6->point(),v7->point()),v6,v7);
    arr[3].insert_at_vertices(Segment2(v7->point(),v8->point()),v7,v8);
    
    arr[3].insert_at_vertices(Segment2(v9->point(),v10->point()),v9,v10);
    arr[3].insert_at_vertices(Segment2(v10->point(),v11->point()),v10,v11);
    arr[3].insert_at_vertices(Segment2(v11->point(),v12->point()),v11,v12);
    
    arr[3].insert_at_vertices(Segment2(v13->point(),v14->point()),v13,v14);
    arr[3].insert_at_vertices(Segment2(v14->point(),v15->point()),v14,v15);
    arr[3].insert_at_vertices(Segment2(v15->point(),v16->point()),v15,v16);
    
    arr[3].insert_at_vertices(Segment2(v1->point(),v5->point()),v1,v5);
    arr[3].insert_at_vertices(Segment2(v5->point(),v9->point()),v5,v9);
    arr[3].insert_at_vertices(Segment2(v9->point(),v13->point()),v9,v13);
    
    arr[3].insert_at_vertices(Segment2(v2->point(),v6->point()),v2,v6);
    arr[3].insert_at_vertices(Segment2(v6->point(),v10->point()),v6,v10);
    arr[3].insert_at_vertices(Segment2(v10->point(),v14->point()),v10,v14);
    
    arr[3].insert_at_vertices(Segment2(v3->point(),v7->point()),v3,v7);
    arr[3].insert_at_vertices(Segment2(v7->point(),v11->point()),v7,v11);
    arr[3].insert_at_vertices(Segment2(v11->point(),v15->point()),v11,v15);
    
    arr[3].insert_at_vertices(Segment2(v4->point(),v8->point()),v4,v8);
    arr[3].insert_at_vertices(Segment2(v8->point(),v12->point()),v8,v12);
    arr[3].insert_at_vertices(Segment2(v12->point(),v16->point()),v12,v16);
    
    v1 = globals[3].insert_in_face_interior(ExactKernel::Point_2(0.5+0,0.5+0.5), globals[3].unbounded_face()); v1->data().triangulation_index=1;
    v2 = globals[3].insert_in_face_interior(ExactKernel::Point_2(0.5+3.0/6.0,0.5+0.5), globals[3].unbounded_face()); v2->data().triangulation_index=2;
    v3 = globals[3].insert_in_face_interior(ExactKernel::Point_2(0.5+3.0/6.0,0.0+0.5), globals[3].unbounded_face()); v3->data().triangulation_index=5;
    v4 = globals[3].insert_in_face_interior(ExactKernel::Point_2(0.5+0.0/6.0,0.0+0.5), globals[3].unbounded_face()); v4->data().triangulation_index=4;
    
    globals[3].insert_at_vertices(Segment2(v1->point(),v2->point()),v1,v2);
    globals[3].insert_at_vertices(Segment2(v2->point(),v3->point()),v2,v3);
    globals[3].insert_at_vertices(Segment2(v3->point(),v4->point()),v3,v4);
    globals[3].insert_at_vertices(Segment2(v4->point(),v1->point()),v4,v1);
}

void Mesh::createWang1(Arrangement2& arr,Arrangement2& globals){
    Arrangement2::Vertex_handle v1 = arr.insert_in_face_interior(ExactKernel::Point_2(0,1), arr.unbounded_face()); v1->data().triangulation_index=0;
    Arrangement2::Vertex_handle v2 = arr.insert_in_face_interior(ExactKernel::Point_2(1.0/3.0,1), arr.unbounded_face()); v2->data().triangulation_index=1;
    Arrangement2::Vertex_handle v3 = arr.insert_in_face_interior(ExactKernel::Point_2(2.0/3.0,1), arr.unbounded_face()); v3->data().triangulation_index=0;
    Arrangement2::Vertex_handle v4 = arr.insert_in_face_interior(ExactKernel::Point_2(1,1), arr.unbounded_face()); v4->data().triangulation_index=1;
    
    v1->data().height=1;v2->data().height=2;v3->data().height=3;v4->data().height=1;
    
    Arrangement2::Vertex_handle v5 = arr.insert_in_face_interior(ExactKernel::Point_2(1,2.0/3.0), arr.unbounded_face()); v5->data().triangulation_index=2;
    Arrangement2::Vertex_handle v6 = arr.insert_in_face_interior(ExactKernel::Point_2(1,1.0/3.0), arr.unbounded_face()); v6->data().triangulation_index=1;
    Arrangement2::Vertex_handle v7 = arr.insert_in_face_interior(ExactKernel::Point_2(1,0), arr.unbounded_face()); v7->data().triangulation_index=2;
    
    v5->data().height=6;v6->data().height=3;v7->data().height=2;
    
    Arrangement2::Vertex_handle v8 = arr.insert_in_face_interior(ExactKernel::Point_2(2.0/3.0,0), arr.unbounded_face()); v8->data().triangulation_index=3;
    Arrangement2::Vertex_handle v9 = arr.insert_in_face_interior(ExactKernel::Point_2(1.0/3.0,0), arr.unbounded_face()); v9->data().triangulation_index=2;
    Arrangement2::Vertex_handle v10 = arr.insert_in_face_interior(ExactKernel::Point_2(0,0), arr.unbounded_face()); v10->data().triangulation_index=3;
    
    v8->data().height=1;v9->data().height=3;v10->data().height=1;
    
    Arrangement2::Vertex_handle v11 = arr.insert_in_face_interior(ExactKernel::Point_2(0,1.0/3.0), arr.unbounded_face()); v11->data().triangulation_index=0;
    Arrangement2::Vertex_handle v12 = arr.insert_in_face_interior(ExactKernel::Point_2(0,2.0/3.0), arr.unbounded_face()); v12->data().triangulation_index=3;
    
    v11->data().height=4;v12->data().height=3;
    
    Arrangement2::Vertex_handle v13 = arr.insert_in_face_interior(ExactKernel::Point_2(1.0/3.0,2.0/3.0), arr.unbounded_face()); v13->data().triangulation_index=2;
    Arrangement2::Vertex_handle v14 = arr.insert_in_face_interior(ExactKernel::Point_2(2.0/3.0,2.0/3.0), arr.unbounded_face()); v14->data().triangulation_index=3;
    
    v13->data().height=1;v14->data().height=2;
    
    Arrangement2::Vertex_handle v15 = arr.insert_in_face_interior(ExactKernel::Point_2(2.0/3.0,1.0/3.0), arr.unbounded_face()); v15->data().triangulation_index=0;
    Arrangement2::Vertex_handle v16 = arr.insert_in_face_interior(ExactKernel::Point_2(1.0/3.0,1.0/3.0), arr.unbounded_face()); v16->data().triangulation_index=1;
    
    v15->data().height=3;v16->data().height=0;
    
    v1->data().s=0.5;v2->data().s=0.5;v3->data().s=0.5;v4->data().s=0.5;v5->data().s=0.5;v6->data().s=0.5;v7->data().s=0.5;v8->data().s=0.5;
    v9->data().s=0.5;v10->data().s=0.5;v11->data().s=0.5;v12->data().s=0.5;v13->data().s=0.5;v14->data().s=0.5;v15->data().s=0.5;v16->data().s=0.5;
    
    arr.insert_at_vertices(Segment2(v1->point(),v2->point()),v1,v2);
    arr.insert_at_vertices(Segment2(v2->point(),v3->point()),v2,v3);
    arr.insert_at_vertices(Segment2(v3->point(),v4->point()),v3,v4);
    arr.insert_at_vertices(Segment2(v4->point(),v5->point()),v4,v5);
    arr.insert_at_vertices(Segment2(v5->point(),v6->point()),v5,v6);
    arr.insert_at_vertices(Segment2(v6->point(),v7->point()),v6,v7);
    arr.insert_at_vertices(Segment2(v7->point(),v8->point()),v7,v8);
    arr.insert_at_vertices(Segment2(v8->point(),v9->point()),v8,v9);
    arr.insert_at_vertices(Segment2(v9->point(),v10->point()),v9,v10);
    arr.insert_at_vertices(Segment2(v10->point(),v11->point()),v10,v11);
    arr.insert_at_vertices(Segment2(v11->point(),v12->point()),v11,v12);
    arr.insert_at_vertices(Segment2(v12->point(),v1->point()),v12,v1);
    
    arr.insert_at_vertices(Segment2(v2->point(),v13->point()),v2,v13);
    arr.insert_at_vertices(Segment2(v3->point(),v14->point()),v3,v14);
    arr.insert_at_vertices(Segment2(v12->point(),v13->point()),v12,v13);
    arr.insert_at_vertices(Segment2(v13->point(),v14->point()),v13,v14);
    arr.insert_at_vertices(Segment2(v14->point(),v5->point()),v14,v5);
    arr.insert_at_vertices(Segment2(v13->point(),v16->point()),v13,v16);
    arr.insert_at_vertices(Segment2(v14->point(),v15->point()),v14,v15);
    arr.insert_at_vertices(Segment2(v11->point(),v16->point()),v11,v16);
    arr.insert_at_vertices(Segment2(v16->point(),v15->point()),v16,v15);
    arr.insert_at_vertices(Segment2(v15->point(),v6->point()),v15,v6);
    arr.insert_at_vertices(Segment2(v16->point(),v9->point()),v16,v9);
    arr.insert_at_vertices(Segment2(v15->point(),v8->point()),v15,v8);
    
    v1 = globals.insert_in_face_interior(ExactKernel::Point_2(0,1), globals.unbounded_face()); v1->data().triangulation_index=0;
    v2 = globals.insert_in_face_interior(ExactKernel::Point_2(1,1), globals.unbounded_face()); v2->data().triangulation_index=1;
    v3 = globals.insert_in_face_interior(ExactKernel::Point_2(1,0), globals.unbounded_face()); v3->data().triangulation_index=2;
    v4 = globals.insert_in_face_interior(ExactKernel::Point_2(0,0), globals.unbounded_face()); v4->data().triangulation_index=3;
    
    v1->data().height=1;v2->data().height=1;v3->data().height=2;v4->data().height=1;
    v1->data().s=-1;
    globals.insert_at_vertices(Segment2(v1->point(),v2->point()),v1,v2);
    globals.insert_at_vertices(Segment2(v2->point(),v3->point()),v2,v3);
    globals.insert_at_vertices(Segment2(v3->point(),v4->point()),v3,v4);
    globals.insert_at_vertices(Segment2(v4->point(),v1->point()),v4,v1);
    
}


