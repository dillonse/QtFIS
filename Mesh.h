//
//  TriangularMesh.h
//  OpenGL_CGAL
//
//  Created by Sean Dillon on 3/5/16.
//  Copyright (c) 2016 Sean Dillon. All rights reserved.
//

#ifndef __OpenGL_CGAL__TriangularMesh__
#define __OpenGL_CGAL__TriangularMesh__
#include "KernelConstructions.h"

class Mesh{
public:
    static void createMN(Arrangement2& result,int M,int N);
    static void createMNRec(Arrangement2& result,int M,int N);
    static void unify(std::vector<Arrangement2>& data,Arrangement2& result);
    static void addCollinearFrame(Arrangement2& I,Arrangement2& J);
    static void sampleHeightsJFromI(Arrangement2& I,Arrangement2& J);
    static bool validate(Arrangement2& mesh);
    static bool validate(Arrangement2& I,Arrangement2& J);
    static void clear(Arrangement2& mesh);
    static bool quadify(Arrangement2& mesh);
    static bool triangulateX(Arrangement2& mesh);
    static bool triangulatePL(Arrangement2& mesh);
    static bool triangulatePR(Arrangement2& mesh);
    static void createTriangular(Arrangement2& arr, Arrangement2& global);
    static void createRandomTriangular(Arrangement2& arr, Arrangement2& global);
    static void createRectangular1(std::vector<Arrangement2>& arr,std::vector<Arrangement2>& globals);
    static void createRectangular2(std::vector<Arrangement2>& arr,std::vector<Arrangement2>& globals);
    static void createRectangular3(Arrangement2& arr,Arrangement2& globals);
    static void createRectangular4(std::vector<Arrangement2>& arr,std::vector<Arrangement2>& globals);
    static void createWang1(Arrangement2& arr,Arrangement2& globals);
};

#endif /* defined(__OpenGL_CGAL__TriangularMesh__) */
