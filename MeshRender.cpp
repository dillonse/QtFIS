//
//  MeshRender.cpp
//  OpenGL_CGAL
//
//  Created by Sean Dillon on 3/10/16.
//  Copyright (c) 2016 Sean Dillon. All rights reserved.
//

#include "MeshRender.h"
#include <iterator>

void MeshRender::BindBuffers(){
    //buffers generation
    glGenBuffers(1,&VBO_pointsID);                                  //generate the points buffer and return its id//
    glGenBuffers(1,&VBO_indexID);
    glGenBuffers(1,&VBO_colourIndexID);
    

    //buffers binding
    //points
    glBindBuffer(GL_ARRAY_BUFFER,VBO_pointsID);                 //bind the buffer with the vbo id//
    glBufferData(GL_ARRAY_BUFFER,points.size()*sizeof(points[0]),&(points[0]),GL_STATIC_DRAW);//set the binded buffer with the points buffer//
    //indexes
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,VBO_indexID);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER,indexes.size()*sizeof(indexes[0]),&(indexes[0]),GL_STATIC_DRAW);
    //colour indexes//
    glBindBuffer(GL_ARRAY_BUFFER,VBO_colourIndexID);
    glBufferData(GL_ARRAY_BUFFER,colour_indexes.size()*sizeof(colour_indexes[0]),&(this->colour_indexes[0]),GL_STATIC_DRAW);

    //isolated
    if(isolated_points.size()>0){
        glGenBuffers(1,&VBO_isolatedPointsID);
        glGenBuffers(1,&VBO_isolatedIndexID);
        glGenBuffers(1,&VBO_isolatedColourIndexID);
        //isolated points
        glBindBuffer(GL_ARRAY_BUFFER,VBO_isolatedPointsID);                 //bind the buffer with the vbo id//
        glBufferData(GL_ARRAY_BUFFER,isolated_points.size()*sizeof(isolated_points[0]),&(isolated_points[0]),GL_STATIC_DRAW);//set the binded buffer with the points buffer//

        //isolated indices
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,VBO_isolatedIndexID);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER,isolated_indexes.size()*sizeof(isolated_indexes[0]),&(isolated_indexes[0]),GL_STATIC_DRAW);

        //isolated colours//
        glBindBuffer(GL_ARRAY_BUFFER,VBO_isolatedColourIndexID);
        glBufferData(GL_ARRAY_BUFFER,isolated_colour_indexes.size()*sizeof(isolated_colour_indexes[0]),&(this->isolated_colour_indexes[0]),GL_STATIC_DRAW);
    }
    
    //buffers activation//
    //points
    glEnableClientState(GL_VERTEX_ARRAY);                   //Activate the points buffer//
    glBindBuffer(GL_ARRAY_BUFFER,VBO_pointsID);
    glVertexPointer(3, GL_FLOAT,0, 0);                     // Stride of 3 GLfloats//
    //indexes
    glEnableClientState(GL_INDEX_ARRAY);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,VBO_indexID);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,VBO_floorIndexID);
    //colours//
    glEnableClientState(GL_COLOR_ARRAY);
    glBindBuffer(GL_ARRAY_BUFFER,VBO_colourIndexID);
    glColorPointer(3,GL_FLOAT,0,0);
}

void MeshRender::RenderVBO(){
    if(!this->drawEnabled)return;

    glDisable(GL_LIGHTING);
    if(this->drawMeshParameter){
        //close lights and set the wireframe mode//
        if(this->drawColoursParameter){
            glEnableClientState(GL_COLOR_ARRAY);
            glBindBuffer(GL_ARRAY_BUFFER,VBO_colourIndexID);
            glColorPointer(3,GL_FLOAT,0,0);
        }
        else{
            glDisableClientState(GL_COLOR_ARRAY);
        }
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        glLineWidth(2);
        glBindBuffer(GL_ARRAY_BUFFER,VBO_pointsID);
        glVertexPointer(3, GL_FLOAT,0, 0);
        ////set the indices
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,VBO_indexID);
        //set the colors
        glLineWidth(2);
        glDrawElements(GL_LINES,(GLsizei)indexes.size(),GL_UNSIGNED_INT, 0);

        //isolated
        if(isolated_points.size()>0){
            if(this->drawColoursParameter){
                glEnableClientState(GL_COLOR_ARRAY);
                glBindBuffer(GL_ARRAY_BUFFER,VBO_isolatedColourIndexID);
                glColorPointer(3,GL_FLOAT,0,0);
            }
            else{
                glDisableClientState(GL_COLOR_ARRAY);
            }
            glBindBuffer(GL_ARRAY_BUFFER,VBO_isolatedPointsID);
            glVertexPointer(3, GL_FLOAT,0, 0);
            ////set the indices
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,VBO_isolatedIndexID);
            //set the colors
            glPointSize(4.0);
            glDrawElements(GL_POINTS,(GLsizei)isolated_indexes.size(),GL_UNSIGNED_INT, 0);
        }
    }
    //print the vsfs
    if(this->drawGammaValuesParameter){
        print_vsfs();
    }
    glDisableClientState(GL_COLOR_ARRAY);
}


void MeshRender::DelBuffers(){
    //delete point buffers
    glDeleteBuffers(1,&VBO_pointsID);
    //delete index buffers
    glDeleteBuffers(1,&VBO_indexID);
    //delete colour buffers
    glDeleteBuffers(1,&VBO_colourIndexID);
}

void MeshRender::SetData(std::vector<Arrangement2> arr){
    //compute the AABB on xy and height
    Arrangement2::VerticalScalingType vst=arr[0].vst;
    ExacttoK exactToK;
    std::vector<Point> BBPoints;
    for(std::vector<Arrangement2>::iterator itt= arr.begin();itt!=arr.end();itt++){
        for(Arrangement2::Vertex_iterator it= itt->vertices_begin();it!=itt->vertices_end();it++){
            BBPoints.push_back(Point(exactToK(it->point().x()),exactToK(it->point().y()),it->data().height));
        }
    }
    if(BBPoints.size()==0)return;
    K::Iso_cuboid_3 c3= CGAL::bounding_box(BBPoints.begin(),BBPoints.end());
    AABB=c3;
    BBPoints.clear();
    size_t unique_points=0;
    for(std::vector<Arrangement2>::iterator itt= arr.begin();itt!=arr.end();itt++){
        unique_points+=std::distance(itt->vertices_begin(),itt->vertices_end());
    }
    points.reserve(unique_points*3*2);
    colour_indexes.reserve(unique_points*3);
    for(std::vector<Arrangement2>::iterator itt= arr.begin();itt!=arr.end();itt++){
        if(vst==Arrangement2::VerticalScalingType::VERTICES){
            for(Arrangement2::Vertex_iterator it = itt->vertices_begin();it!=itt->vertices_end();it++){
                vs_factors.push_back(std::make_pair(CGAL::to_double(it->data().s),Point(exactToK(it->point().x()),AABB.zmax(),exactToK(it->point().y()))));
            }
        }else if(vst==Arrangement2::VerticalScalingType::FACES){
            for(Arrangement2::Face_iterator it = itt->faces_begin();it!=itt->faces_end();it++){
                if(it->is_unbounded())continue;
                Point sum(0,0,0);int counter=0;
                Arrangement2::Ccb_halfedge_circulator hc = it->outer_ccb(),end(hc);
                do{
                    counter++;
                    sum=Point(sum.x()+exactToK(hc->target()->point().x()),sum.y()+hc->target()->data().height,sum.z()+exactToK(hc->target()->point().y()));
                }while((++hc)!=end);
                sum=Point(sum.x()/counter,AABB.zmax(),sum.z()/counter);
                vs_factors.push_back(std::make_pair(CGAL::to_double(it->data().s),sum));
            }
        }
        else if(vst==Arrangement2::VerticalScalingType::SINGLE){
            Point sum(0,0,0);int counter=0;
            for(Arrangement2::Vertex_iterator it = itt->vertices_begin();it!=itt->vertices_end();it++){
                counter++;
                sum=Point(sum.x()+exactToK(it->point().x()),0,sum.z()+exactToK(it->point().y()));
            }
            sum=Point(sum.x()/counter,AABB.zmax(),sum.z()/counter);
            vs_factors.push_back(std::make_pair(CGAL::to_double(itt->unbounded_face()->data().s),sum));
        }
    }
    int i=0;
    int j=0;
    for(std::vector<Arrangement2>::iterator itt= arr.begin();itt!=arr.end();itt++){
        if(itt->ct==Arrangement2::ColoringType::NONE)this->drawColoursParameter=false;
        for(Arrangement2::Vertex_iterator it = itt->vertices_begin();it!=itt->vertices_end();it++){
            if(it->degree()==0){
                isolated_points.push_back(CGAL::to_double(it->point().x()));
                isolated_points.push_back(CGAL::to_double(it->data().height));
                isolated_points.push_back(CGAL::to_double(it->point().y()));
                it->data().height=j++;

                if(it->data().triangulation_index==0){
                    isolated_colour_indexes.push_back(1);isolated_colour_indexes.push_back(0);isolated_colour_indexes.push_back(0);
                }
                else if(it->data().triangulation_index==1){
                    isolated_colour_indexes.push_back(0);isolated_colour_indexes.push_back(1);isolated_colour_indexes.push_back(0);
                }
                else if(it->data().triangulation_index==2){
                    isolated_colour_indexes.push_back(0);isolated_colour_indexes.push_back(0);isolated_colour_indexes.push_back(1);
                }
                else if(it->data().triangulation_index==3){
                    isolated_colour_indexes.push_back(0);isolated_colour_indexes.push_back(1);isolated_colour_indexes.push_back(1);
                }
                else if(it->data().triangulation_index==4){
                    isolated_colour_indexes.push_back(1);isolated_colour_indexes.push_back(0);isolated_colour_indexes.push_back(1);
                }
                else if(it->data().triangulation_index==5){
                    isolated_colour_indexes.push_back(1);isolated_colour_indexes.push_back(1);isolated_colour_indexes.push_back(0);
                }
                else if(it->data().triangulation_index==6){
                    isolated_colour_indexes.push_back(0.5);isolated_colour_indexes.push_back(0.5);isolated_colour_indexes.push_back(0);
                }
                else if(it->data().triangulation_index==7){
                    isolated_colour_indexes.push_back(0);isolated_colour_indexes.push_back(0.5);isolated_colour_indexes.push_back(0.5);
                }
                else if(it->data().triangulation_index==8){
                    isolated_colour_indexes.push_back(0.5);isolated_colour_indexes.push_back(0);isolated_colour_indexes.push_back(0.5);
                }
            }else{
                points.push_back(CGAL::to_double(it->point().x()));
                points.push_back(CGAL::to_double(it->data().height));
                points.push_back(CGAL::to_double(it->point().y()));
                it->data().height=i++;

                if(it->data().triangulation_index==0){
                    colour_indexes.push_back(1);colour_indexes.push_back(0);colour_indexes.push_back(0);
                }
                else if(it->data().triangulation_index==1){
                    colour_indexes.push_back(0);colour_indexes.push_back(1);colour_indexes.push_back(0);
                }
                else if(it->data().triangulation_index==2){
                    colour_indexes.push_back(0);colour_indexes.push_back(0);colour_indexes.push_back(1);
                }
                else if(it->data().triangulation_index==3){
                    colour_indexes.push_back(0);colour_indexes.push_back(1);colour_indexes.push_back(1);
                }
                else if(it->data().triangulation_index==4){
                    colour_indexes.push_back(1);colour_indexes.push_back(0);colour_indexes.push_back(1);
                }
                else if(it->data().triangulation_index==5){
                    colour_indexes.push_back(1);colour_indexes.push_back(1);colour_indexes.push_back(0);
                }
                else if(it->data().triangulation_index==6){
                    colour_indexes.push_back(0.5);colour_indexes.push_back(0.5);colour_indexes.push_back(0);
                }
                else if(it->data().triangulation_index==7){
                    colour_indexes.push_back(0);colour_indexes.push_back(0.5);colour_indexes.push_back(0.5);
                }
                else if(it->data().triangulation_index==8){
                    colour_indexes.push_back(0.5);colour_indexes.push_back(0);colour_indexes.push_back(0.5);
                }
            }
        }
    }
    for(std::vector<Arrangement2>::iterator itt= arr.begin();itt!=arr.end();itt++){
        for(Arrangement2::Edge_iterator it=itt->edges_begin();it!=itt->edges_end();it++){
            indexes.push_back(CGAL::to_double(it->source()->data().height));
            indexes.push_back(CGAL::to_double(it->target()->data().height));
        }
    }
    for(std::vector<Arrangement2>::iterator itt= arr.begin();itt!=arr.end();itt++){
        for(Arrangement2::Face_iterator fi=itt->faces_begin();fi!=itt->faces_end();fi++){
            for(Arrangement2::Isolated_vertex_iterator it=fi->isolated_vertices_begin();it!=fi->isolated_vertices_end();it++){
                isolated_indexes.push_back(CGAL::to_double(it->data().height));
            }
        }
    }
    BindBuffers();
}


void MeshRender::SetData(Arrangement2 A){
    std::vector<Arrangement2> AV;
    AV.push_back(A);
    this->SetData(AV);
}

void MeshRender::print_vsfs(){
    for(size_t i=0;i<vs_factors.size();i++){
        glRasterPos3f(CGAL::to_double(vs_factors[i].second.x()),CGAL::to_double(vs_factors[i].second.y()),CGAL::to_double(vs_factors[i].second.z()));
        std::stringstream stream;
        stream << std::fixed << std::setprecision(2) << vs_factors[i].first;
        std::string y= stream.str();
        for(std::string::iterator it=y.begin();it!=y.end();it++){
            char c=*it;
            glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12,c);
        }
    }
}
