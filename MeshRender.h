//
//  MeshRender.h
//  OpenGL_CGAL
//
//  Created by Sean Dillon on 3/10/16.
//  Copyright (c) 2016 Sean Dillon. All rights reserved.
//

#ifndef __OpenGL_CGAL__MeshRender__
#define __OpenGL_CGAL__MeshRender__

#include "OGL.h"
#include "KernelConstructions.h"
#include <stdio.h>
#include <iterator>


class MeshRender:public RenderObject{
protected:
    bool IVBO_setup=false;
    ////IVBO IDs//
    GLuint VBO_pointsID=0;
    GLuint VBO_isolatedPointsID=0;
    GLuint VBO_indexID=0;
    GLuint VBO_isolatedIndexID=0;
    GLuint VBO_normalsID=0;
    GLuint VBO_AABBpointsID=0;
    GLuint VBO_floorIndexID=0;
    GLuint VBO_colourIndexID=0;
    GLuint VBO_isolatedColourIndexID=0;
    ////Data buffers//
    std::vector<GLfloat> isolated_points;
    std::vector<GLfloat> points;
    std::vector<GLfloat> normals;
    std::vector<GLuint> indexes;
    std::vector<GLuint> isolated_indexes;
    std::vector<GLfloat> AABB_points;
    std::vector<GLuint> floor_indexes;
    std::vector<GLfloat> colour_indexes;
    std::vector<GLfloat> isolated_colour_indexes;
    std::vector<std::pair<GLfloat,Point>> vs_factors;
    ////IVBO functions
    void BindBuffers();
    void DelBuffers();
    ////counters and value placeholders//
    GLfloat floor=0;
    GLfloat ceiling=0;
    //print the vs factors on points//
    void print_vsfs();
public:
    bool drawEnabled=true;
    bool drawGammaValuesParameter=true;
    bool drawMeshParameter=true;
    bool drawColoursParameter=true;
    void RenderImmediate();      //render the points in immediate mode//
    void RenderVBO();           //render the points in VBO mode//
    void SetData(std::vector<Arrangement2> A);
    void SetData(Arrangement2 A);
    void Normalize(Point scale,Point trans);
};


#endif /* defined(__OpenGL_CGAL__MeshRender__) */
