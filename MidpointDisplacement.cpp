//
//  MidpointDisplacement.cpp
//  OpenGL_CGAL
//
//  Created by Sean Dillon on 5/22/15.
//

#include "MidpointDisplacement.h"
#include "Random.h"

#define DIMENSION 3         //space dimension//

//fractal noise
double Hurst = 0.5;  //Hurst parameter in [0,1] where 0 means more roughness//
double Sigma = 1;

Vector normalize(Vector v){
    double length = sqrt(CGAL::to_double(v[0]*v[0]+v[1]*v[1]+v[2]*v[2]));
    return Vector(v[0]/length,v[1]/length,v[2]/length);
}

MD::MD(Point p1,Point p2,Point p3,Point p4){
    //insert the points at the point pool and create the initial box
    points.push_back(p1);points.push_back(p2);points.push_back(p3);points.push_back(p4);
    std::sort(points.begin(),points.end(),[](auto i,auto j){return i.x()<j.x();});
    std::sort(points.begin(),points.begin()+2,[](auto i,auto j){return i.y()<j.y();});
    std::sort(points.begin()+2,points.end(),[](auto i,auto j){return i.y()>j.y();});
    buffer1.push_back(std::make_tuple(0,1,2,3));
}

void MD::iterate(){
    std::vector<std::tuple<int,int,int,int>>* read;
    std::vector<std::tuple<int,int,int,int>>* write;
    if(!buffer1.empty()){
        read=&buffer1;
        write=&buffer2;
    }
    else{
        read=&buffer2;
        write=&buffer1;
    }
    std::vector<std::tuple<int,int,int,int>>::iterator end=read->end();
    for(std::vector<std::tuple<int,int,int,int>>::iterator it=read->begin();it!=end;it++){
        Point p0=points[std::get<0>(*it)];Point p1=points[std::get<1>(*it)];Point p2=points[std::get<2>(*it)];Point p3=points[std::get<3>(*it)];
        //compute midpoint
        Point midpoint=CGAL::midpoint(CGAL::midpoint(p0,p1),CGAL::midpoint(p2,p3));
        //add noise to the z coord of the midpoint//
        K::FT deltaX=CGAL::abs(p0.x()-p3.x());
        //midpoint=Point(midpoint.x(),midpoint.y(),midpoint.z()+pow(0.5,Hurst/deltaX)*Sigma*dice::GaussianNoise(0,1));
        midpoint=Point(midpoint.x(),midpoint.y(),midpoint.z()+sqrt(1-exp2(2*Hurst-2))*pow(CGAL::to_double(deltaX),Hurst)*Sigma*dice::GaussianNoise(0,1));
        //compute side midpoints
        Point p01=CGAL::midpoint(p0,p1);Point p12=CGAL::midpoint(p1,p2);Point p23=CGAL::midpoint(p2,p3);Point p30=CGAL::midpoint(p3,p0);
        //add points to point pool
        points.push_back(p01);points.push_back(p12);points.push_back(p23);points.push_back(p30);
        points.push_back(midpoint);
        //create boxes and add them to list
        write->push_back(std::make_tuple(std::get<0>(*it),points.size()-5,points.size()-1,points.size()-2));
        write->push_back(std::make_tuple(points.size()-5,std::get<1>(*it),points.size()-4,points.size()-1));
        write->push_back(std::make_tuple(points.size()-2,points.size()-1,points.size()-3,std::get<3>(*it)));
        write->push_back(std::make_tuple(points.size()-1,points.size()-4,std::get<2>(*it),points.size()-3));
    }
    read->clear();
}

void MD::getPoints(std::vector<Point>& output){
    output=points;
}


/*
void MD::ComputeGrid(){
    //compute usefull parameters//
    int iterations = log2(gridSize);        //compute the iterations//
    double DeltaX = gridScale/2;             //compute the initial ||Δx|| //
    int indexOffset = gridSize/2;           //compute the initial index offset//
    Point notComputed = Point(-1,-1,-1);
    //compute the height of the grid//
    int loopLimit;
    for(int it=0;it<iterations;it++){           //for all iterations//
        loopLimit = exp2(it+1)+1;
        //compute the odd and odd case//
        for(int i=0;i<loopLimit;i++){         //over x with offset
            for (int j=0;j<loopLimit;j++){   //over y with offset
                Point& p = grid[i*indexOffset][j*indexOffset];                  //get a reference to the point
                if(p!=notComputed||(even(i)||even(j)))continue;      //computed or not both odds go to next itteration//
                //compute height x way
                grid[i*indexOffset][j*indexOffset] =
                CGAL::midpoint(CGAL::midpoint(grid[(i-1)*indexOffset][(j-1)*indexOffset],grid[(i-1)*indexOffset][(j+1)*indexOffset]),CGAL::midpoint(grid[(i+1)*indexOffset][(j-1)*indexOffset],grid[(i+1)*indexOffset][(j+1)*indexOffset]));
                //add noise to the z coord of the point//
               // grid[i*indexOffset][j*indexOffset]=
               // Point(grid[i*indexOffset][j*indexOffset][0],grid[i*indexOffset][j*indexOffset][1],
               //              grid[i*indexOffset][j*indexOffset][2]+sqrt(1-exp2(2*Hurst-2))*pow(DeltaX,Hurst)*Sigma*Gauss());
                grid[i*indexOffset][j*indexOffset] =
                Point(grid[i*indexOffset][j*indexOffset][0],grid[i*indexOffset][j*indexOffset][1],
                      grid[i*indexOffset][j*indexOffset][2]+pow(0.5,it*Hurst)*Sigma*Gauss());
                grid[i*indexOffset][j*indexOffset]=BoostedPoint(grid[i*indexOffset][j*indexOffset]);//cast it as boosted point
            }
        }
        //compute the odd xor odd case//
        for(int i=0;i<loopLimit;i++){         //over x with offset
            for(int j=0;j<loopLimit;j++){   //over y with offset
                Point& p = grid[i*indexOffset][j*indexOffset];                  //get a reference to the point
                if(p!=notComputed||((odd(i)&&odd(j)))||((even(i)&&even(j))))continue;      //computed or not xor odds go to next itteration//
                //compute height cross way
                if(i*indexOffset==0){grid[i*indexOffset][j*indexOffset] =CGAL::midpoint(grid[i*indexOffset][(j-1)*indexOffset],grid[i*indexOffset][(j+1)*indexOffset]);}
                    else if(i*indexOffset==gridSize-1){grid[i*indexOffset][j*indexOffset] = CGAL::midpoint(grid[i*indexOffset][(j-1)*indexOffset],grid[i*indexOffset][(j+1)*indexOffset]);}
                    else if(j==0){grid[i*indexOffset][j*indexOffset] =CGAL::midpoint(grid[(i-1)*indexOffset][j*indexOffset],grid[(i+1)*indexOffset][j*indexOffset]);}
                    else if(j*indexOffset==gridSize-1){grid[i*indexOffset][j*indexOffset] =CGAL::midpoint(grid[(i-1)*indexOffset][j*indexOffset],grid[(i+1)*indexOffset][j*indexOffset]);}
                    else {grid[i*indexOffset][j*indexOffset] = CGAL::midpoint(CGAL::midpoint(grid[i*indexOffset][(j-1)*indexOffset],grid[i*indexOffset][(j+1)*indexOffset]),CGAL::midpoint(grid[(i-1)*indexOffset][j*indexOffset],grid[(i+1)*indexOffset][j*indexOffset]));}
                Point(grid[i*indexOffset][j*indexOffset][0],grid[i*indexOffset][j*indexOffset][1],
                      grid[i*indexOffset][j*indexOffset][2]+sqrt(1-exp2(2*Hurst-2))*pow(DeltaX,Hurst)*Sigma*Gauss());
                grid[i*indexOffset][j*indexOffset]=BoostedPoint(grid[i*indexOffset][j*indexOffset]); //cast it as boostedPoint
            }
        }
        indexOffset/=2;
        DeltaX/=2;
    }
}


*/









