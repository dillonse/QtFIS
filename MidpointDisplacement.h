//
//  MidpointDisplacement.h
//  OpenGL_CGAL
//
//  Created by Sean Dillon on 5/22/15.
//

#ifndef __OpenGL_CGAL__MidpointDisplacement__
#define __OpenGL_CGAL__MidpointDisplacement__

#include <stdio.h>
#include "KernelConstructions.h"



class MD{
private:
    std::vector<std::tuple<int,int,int,int>> buffer1;
    std::vector<std::tuple<int,int,int,int>> buffer2;
    std::vector<Point> points;
    
public:
    MD(Point p1,Point p2,Point p3,Point p4);
    void iterate();
    void getPoints(std::vector<Point>& output);
};


#endif /* defined(__OpenGL_CGAL__MidpointDisplacement__) */
