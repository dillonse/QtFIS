//
//  OGL.h
//  OpenGL_CGAL
//
//  Created by Sean Dillon on 12/26/15.
//  Copyright (c) 2015 Sean Dillon. All rights reserved.
//

#ifndef OpenGL_CGAL_OGL_h
#define OpenGL_CGAL_OGL_h

#ifdef __APPLE__
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <GLUT/glut.h>
#else
#include "GL/glew.h"
#include "GL/freeglut.h"
#endif
#include "KernelConstructions.h"

class RenderObject{
public:
    virtual void RenderVBO()=0;
    K::Iso_cuboid_3 AABB;
    GLfloat NormalizedTranslateX=0;
    GLfloat NormalizedTranslateY=0;
    GLfloat NormalizedTranslateZ=0;
    GLfloat NormalizedScaleX=1;
    GLfloat NormalizedScaleY=1;
    GLfloat NormalizedScaleZ=1;
};


#endif
