#-------------------------------------------------
#
# Project created by QtCreator 2016-04-20T20:45:21
#
#-------------------------------------------------


#path

INCLUDEPATH+=/Users/seandillon/OpenGL_CGAL

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QtFIS
TEMPLATE = app



SOURCES += main.cpp\
        mainwindow.cpp \
    ../OpenGL_CGAL/AffineTransformation.cpp \
    ../OpenGL_CGAL/AFIM.cpp \
    ../OpenGL_CGAL/AFIS.cpp \
    ../OpenGL_CGAL/BFIM.cpp \
    ../OpenGL_CGAL/BFIS.cpp \
    ../OpenGL_CGAL/BivariateTransformation.cpp \
    ../OpenGL_CGAL/FileIO.cpp \
    ../OpenGL_CGAL/FIS.cpp \
    ../OpenGL_CGAL/HausdorffDistance.cpp \
    ../OpenGL_CGAL/IFS.cpp \
    #../OpenGL_CGAL/main.cpp \
    ../OpenGL_CGAL/Mesh.cpp \
    ../OpenGL_CGAL/MeshRender.cpp \
    ../OpenGL_CGAL/MidpointDisplacement.cpp \
    ../OpenGL_CGAL/RAFIM.cpp \
    ../OpenGL_CGAL/RAFIS.cpp \
    ../OpenGL_CGAL/Random.cpp \
    ../OpenGL_CGAL/RBFIM.cpp \
    ../OpenGL_CGAL/RBFIS.cpp \
    ../OpenGL_CGAL/Regression.cpp \
    ../OpenGL_CGAL/RFIS.cpp \
    ../OpenGL_CGAL/Sampling.cpp \
    ../OpenGL_CGAL/SceneInteractor.cpp \
    ../OpenGL_CGAL/SceneRender.cpp \
    ../OpenGL_CGAL/SurfaceRender.cpp \
    ../OpenGL_CGAL/TimePerformance.cpp \
    qscenerender.cpp \
    uihandler.cpp \
    scenemanager.cpp

HEADERS  += mainwindow.h \
    ../OpenGL_CGAL/AffineTransformation.h \
    ../OpenGL_CGAL/AFIM.h \
    ../OpenGL_CGAL/AFIS.h \
    ../OpenGL_CGAL/BFIM.h \
    ../OpenGL_CGAL/BFIS.h \
    ../OpenGL_CGAL/BivariateTransformation.h \
    ../OpenGL_CGAL/FileIO.h \
    ../OpenGL_CGAL/FIS.h \
    ../OpenGL_CGAL/FractalInterpolationObject.h \
    ../OpenGL_CGAL/HausdorffDistance.h \
    ../OpenGL_CGAL/IFS.h \
    ../OpenGL_CGAL/KernelConstructions.h \
    ../OpenGL_CGAL/Mesh.h \
    ../OpenGL_CGAL/MeshRender.h \
    ../OpenGL_CGAL/MidpointDisplacement.h \
    ../OpenGL_CGAL/OGL.h \
    ../OpenGL_CGAL/RAFIM.h \
    ../OpenGL_CGAL/RAFIS.h \
    ../OpenGL_CGAL/Random.h \
    ../OpenGL_CGAL/RBFIM.h \
    ../OpenGL_CGAL/RBFIS.h \
    ../OpenGL_CGAL/Regression.h \
    ../OpenGL_CGAL/RFIS.h \
    ../OpenGL_CGAL/RIFS.h \
    ../OpenGL_CGAL/Sampling.h \
    ../OpenGL_CGAL/SceneInteractor.h \
    ../OpenGL_CGAL/SceneRender.h \
    ../OpenGL_CGAL/SurfaceRender.h \
    ../OpenGL_CGAL/TimePerformance.h \
    ../OpenGL_CGAL/TransformationObject.h \
    qscenerender.h \
    uihandler.h \
    scenemanager.h

FORMS    += mainwindow.ui

#glut
mac: LIBS += -framework GLUT
else:unix|win32: LIBS += -lGLUT

#cgal
win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../opt/local/lib/release/ -lCGAL.11.0.1
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../opt/local/lib/debug/ -lCGAL.11.0.1
else:unix: LIBS += -L$$PWD/../../../opt/local/lib/ -lCGAL.11.0.1

INCLUDEPATH += $$PWD/../../../opt/local/include
DEPENDPATH += $$PWD/../../../opt/local/include

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../opt/local/lib/release/ -lCGAL_Core.11.0.1
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../opt/local/lib/debug/ -lCGAL_Core.11.0.1
else:unix: LIBS += -L$$PWD/../../../opt/local/lib/ -lCGAL_Core.11.0.1

INCLUDEPATH += $$PWD/../../../opt/local/include
DEPENDPATH += $$PWD/../../../opt/local/include


#boost


win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../opt/local/lib/release/ -lboost_thread-mt
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../opt/local/lib/debug/ -lboost_thread-mt
else:unix: LIBS += -L$$PWD/../../../opt/local/lib/ -lboost_thread-mt

INCLUDEPATH += $$PWD/../../../opt/local/include
DEPENDPATH += $$PWD/../../../opt/local/include

#gmp


win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../opt/local/lib/release/ -lgmp.10
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../opt/local/lib/debug/ -lgmp.10
else:unix: LIBS += -L$$PWD/../../../opt/local/lib/ -lgmp.10

INCLUDEPATH += $$PWD/../../../opt/local/include
DEPENDPATH += $$PWD/../../../opt/local/include

#mpfr

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../opt/local/lib/release/ -lmpfr.4
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../opt/local/lib/debug/ -lmpfr.4
else:unix: LIBS += -L$$PWD/../../../opt/local/lib/ -lmpfr.4

INCLUDEPATH += $$PWD/../../../opt/local/include
DEPENDPATH += $$PWD/../../../opt/local/include

#c++14
CONFIG += c++14

#extra cause macports and troubles
#libiconv
win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../usr/lib/release/ -liconv.2
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../usr/lib/debug/ -liconv.2
else:unix: LIBS += -L/usr/lib/ -liconv.2

INCLUDEPATH += $$PWD/../../../usr/include
DEPENDPATH += $$PWD/../../../usr/include

