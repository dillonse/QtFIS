#include <QApplication>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "qscenerender.h"
#include "uihandler.h"
#include "scenemanager.h"


#include "KernelConstructions.h"
#include "SurfaceRender.h"
#include "MeshRender.h"
#include "FileIO.h"
#include "Sampling.h"

int main(int argc, char *argv[])
{
    //set up the application process
    QApplication a(argc, argv);
    QMainWindow mainwindow;
    Ui::MainWindow ui;
    ui.setupUi(&mainwindow);

    SceneManager sm;
    UIHandler uihandler(ui);
    //sm.setUIHandler(&uihandler);
    sm.setSceneRender(ui.openGLWidget);
    mainwindow.show();

    return a.exec();
}
