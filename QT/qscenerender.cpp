#include "qscenerender.h"
#include <QMouseEvent>

QSceneRender::QSceneRender(){
    this->initGLUTFunctions();
    this->initTransformations();
}

QSceneRender::QSceneRender(QWidget * widget):QOpenGLWidget(widget)
{
    this->initGLUTFunctions();
    this->setParent(widget);
    this->initTransformations();
}

void QSceneRender::initializeGL(){
    this->initRender();
    this->normalize();
}

void QSceneRender::resizeGL(int w,int h){
    this->reshape(w,h);
    this->SceneRender::width=this->QOpenGLWidget::width();
    this->SceneRender::height=this->QOpenGLWidget::height();
}

void QSceneRender::paintGL(){
    this->display();
}

void QSceneRender::mousePressEvent(QMouseEvent* event){
    Qt::MouseButton qmb= event->button();
    int gmb=0;
    if(qmb==Qt::RightButton)
        gmb=GLUT_RIGHT_BUTTON;
    else if(qmb==Qt::LeftButton)
        gmb=GLUT_LEFT_BUTTON;
    this->readMouseButtons(gmb,GLUT_DOWN,event->x(),event->y());
    QDeltaTimer.restart();
}

void QSceneRender::mouseReleaseEvent(QMouseEvent *event){
     Qt::MouseButton qmb= event->button();
     int gmb=0;
     if(qmb==Qt::RightButton)
         gmb=GLUT_RIGHT_BUTTON;
     else if(qmb==Qt::LeftButton)
         gmb=GLUT_LEFT_BUTTON;
     this->readMouseButtons(gmb,GLUT_UP,event->x(),event->y());
     deltaTime=QDeltaTimer.restart()/1000000.0f;
}

void QSceneRender::mouseMoveEvent(QMouseEvent *event){
    this->readMouseDrag(event->x(),event->y());
    deltaTime=QDeltaTimer.restart()/1000000.0f;
    this->updateScene();
    this->update();
}

void QSceneRender::wheelEvent(QWheelEvent* event){
    int x=event->angleDelta().x();
    int y=event->angleDelta().y();
    if(y>0)this->readMouseButtons(3,GLUT_DOWN,x,y);
    else if(y<0)this->readMouseButtons(4,GLUT_DOWN,x,-y);
    else if(y==0)this->readMouseButtons(3,GLUT_UP,x,y);
    deltaTime=QDeltaTimer.restart()/1000000.0f;
    this->updateScene();
    this->update();
}

void QSceneRender::normalizeGL(){
    this->normalize();
}
