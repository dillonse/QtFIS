#ifndef QSCENERENDER_H
#define QSCENERENDER_H

#include <qopenglwidget.h>
#include "SceneRender.h"
#include "SceneInteractor.h"
#include <QElapsedTimer>

class QSceneRender:public QOpenGLWidget,public SceneRender,public SceneInteractor
{
    QElapsedTimer QDeltaTimer;
    void mousePressEvent(QMouseEvent* event);
    void mouseReleaseEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void wheelEvent(QWheelEvent* event);
public:
    QSceneRender();
    QSceneRender(QWidget*widget);
    void initializeGL();
    void resizeGL(int w, int h);
    void paintGL();
    void normalizeGL();
};

#endif // QSCENERENDER_H
