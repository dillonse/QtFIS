#include "scenemanager.h"
#include <QApplication>
#include <QDateTime>
#include <QDir>
#include <QFileDialog>
#include <QMessageBox>
#include <QStandardPaths>
#include "FileIO.h"
#include "Sampling.h"
#include "Regression.h"
#include "TimePerformance.h"
#include "AFIS.h"
#include "RAFIS.h"
#include "BFIS.h"
#include "RBFIS.h"
#include "RBFIM.h"
#include "BFIM.h"
#include "uihandler.h"
#include "Mesh.h"

//rendering components
QSceneRender* SceneManager::scene=NULL;
SurfaceRender* SceneManager::originalSurfaceRender=NULL;
SurfaceRender* SceneManager::reconstructedSurfaceRender=NULL;
SurfaceRender* SceneManager::surfaceRender=NULL;
MeshRender* SceneManager::meshRenderI=new MeshRender();
MeshRender* SceneManager::meshRenderJ=new MeshRender();
MeshRender* SceneManager::meshRender=SceneManager::meshRenderI;
//interpolation components
Arrangement2 SceneManager::I;
Arrangement2 SceneManager::J;
std::vector<Point> SceneManager::surface;
std::vector<std::vector<Point>> SceneManager::reconstructedSurface;
FIO<Point>* SceneManager::fio=NULL;
//user interaction components
pointList SceneManager::uiPointsI;
pointList SceneManager::uiPointsJ;
pointList* SceneManager::uiPoints=&SceneManager::uiPointsI;
faceList SceneManager::uiFacesI;
faceList SceneManager::uiFacesJ;
faceList* SceneManager::uiFaces=&SceneManager::uiFacesI;
Arrangement2* SceneManager::A=&SceneManager::I;
//file IO
QString SceneManager::meshWorkingDirectory;

SceneManager::SceneManager()
{
}

void SceneManager::setSceneRender(QSceneRender* sr){
    SceneManager::scene=sr;
    SceneManager::scene->drawAxisParameter=UIHandler::gui.drawAxisButton->isChecked();
    SceneManager::scene->drawHeighValuesParameter=UIHandler::gui.drawHeightValuesButton->isChecked();
}

//Tools
void SceneManager::refreshMeshRender(){
    delete SceneManager::meshRenderI;
    delete SceneManager::meshRenderJ;
    SceneManager::meshRenderI =new MeshRender();
    SceneManager::meshRenderJ =new MeshRender();
    SceneManager::scene->makeCurrent();
    SceneManager::meshRenderI->SetData(I);
    SceneManager::meshRenderJ->SetData(J);
    SceneManager::scene->doneCurrent();
    if(UIHandler::gui.domainsButton->isChecked())
        SceneManager::meshRender=SceneManager::meshRenderJ;
    else if(UIHandler::gui.intervalsButton->isChecked())
        SceneManager::meshRender=SceneManager::meshRenderI;
    SceneManager::refreshRenders();
}
void SceneManager::refreshRenders(){
    //update the render list
    SceneManager::scene->makeCurrent();
    SceneManager::scene->removeRenders();
    if(SceneManager::meshRender!=NULL)
        SceneManager::scene->addRender(*SceneManager::meshRender,Point(0,0,0),Point(0,0,0));
    if(SceneManager::surfaceRender!=NULL)
        SceneManager::scene->addRender(*SceneManager::surfaceRender,Point(0,0,0),Point(0,0,0));
    //update the scene rendering//
    SceneManager::scene->normalizeGL();
    SceneManager::scene->doneCurrent();
    //update the surface drawing parameters
    if(SceneManager::meshRender!=NULL){
        SceneManager::meshRender->drawEnabled=UIHandler::gui.meshButton->isChecked();
        SceneManager::meshRenderI->drawGammaValuesParameter=UIHandler::gui.drawGammaValues->isChecked();
        SceneManager::meshRenderJ->drawGammaValuesParameter=false;
    }
    if(SceneManager::surfaceRender!=NULL){
        SceneManager::surfaceRender->drawEnabled=UIHandler::gui.surfaceButton->isChecked();
        SceneManager::surfaceRender->drawFillParameter=UIHandler::gui.drawFrontBackButton->isChecked();
        SceneManager::surfaceRender->drawFloorLinesParameter=UIHandler::gui.drawFloorLinesButton->isChecked();
        SceneManager::surfaceRender->drawWireframeParameter=UIHandler::gui.drawWireframeButton->isChecked();
    }
    //update the scene
    SceneManager::scene->update();
}
void SceneManager::refreshFacesDataList(pointList* uiPoints,faceList* uiFaces,Arrangement2* A){
    //uiFaces against A (remove)
    //for each face in the list
    std::vector<Arrangement2::Face_handle> fhv;
    for(Arrangement2::Face_iterator fi=A->faces_begin();fi!=A->faces_end();fi++){
        if(fi->is_unbounded())continue;
        fhv.push_back(fi);
    }
    for(faceList::iterator it=uiFaces->begin();it!=uiFaces->end();it++){
        //check if the handler is in the arrangement
        if(std::find(fhv.begin(),fhv.end(),std::get<4>(*it))==fhv.end()){
            delete std::get<0>(*it)->parentWidget();
            delete std::get<1>(*it)->parentWidget();
            delete std::get<2>(*it)->parentWidget();
            delete std::get<3>(*it)->parentWidget();
            std::get<3>(*it)=NULL;
        }
    }
    uiFaces->resize(std::distance(uiFaces->begin(),std::remove_if(uiFaces->begin(),uiFaces->end(),[](auto i){return std::get<3>(i)==NULL;})));
    //A against uiFaces (add)
    for(std::vector<Arrangement2::Face_handle>::iterator fh=fhv.begin();fh!=fhv.end();fh++){
        //check if the face handle is in the list
        if(std::find_if(uiFaces->begin(),uiFaces->end(),[fh](auto i){return (*fh)==std::get<4>(i);})==uiFaces->end()){
            //add the face to the list
            //Index
            QCheckBox* Index= new QCheckBox(); Index->setText(QString((std::to_string(uiFaces->size()+1)).c_str()));
            Index->setSizePolicy(QSizePolicy::Preferred,QSizePolicy::Preferred);
            QFrame* IndexFrame=new QFrame();
            QHBoxLayout *IndexLayout = new QHBoxLayout;
            IndexLayout->addWidget(Index);
            IndexFrame->setLayout(IndexLayout);
            IndexFrame->setMaximumHeight(24);IndexFrame->setMinimumHeight(24);IndexFrame->setMinimumWidth(40);IndexFrame->layout()->setSpacing(0);
            IndexFrame->layout()->setContentsMargins(0,0,0,0);
            UIHandler::gui.facesIndexingFrame->layout()->addWidget(IndexFrame);
            //VI
            QTextBrowser* VerticesIndices= new QTextBrowser();VerticesIndices->setSizePolicy(QSizePolicy::Preferred,QSizePolicy::Preferred);
            Arrangement2::Ccb_halfedge_circulator hc=(*fh)->outer_ccb(),end(hc);
            QString text;
            pointList::iterator result;
            do{
                if((result= std::find_if(uiPoints->begin(),uiPoints->end(),
                                         [hc](auto i){return std::get<6>(i)==hc->target();}))!= uiPoints->end()){
                    text+=QString::number(std::distance(uiPoints->begin(),result));
                    text+=",";
                }
            }while(++hc!=end);
            if(!text.isEmpty())text.chop(1);
            VerticesIndices->setText(text);
            VerticesIndices->setSizePolicy(QSizePolicy::Preferred,QSizePolicy::Preferred);
            QFrame* VIFrame=new QFrame();
            QHBoxLayout *VILayout = new QHBoxLayout;
            VILayout->addWidget(VerticesIndices);
            VIFrame->setLayout(VILayout);
            VIFrame->setMaximumHeight(24);VIFrame->setMinimumHeight(24);VIFrame->setMinimumWidth(40);VIFrame->layout()->setSpacing(0);
            VIFrame->layout()->setContentsMargins(0,0,0,0);
            UIHandler::gui.facesVerticeIndexingFrame->layout()->addWidget(VIFrame);
            //DI
            QSpinBox* DomainIndex= new QSpinBox();DomainIndex->setRange(0,std::numeric_limits<int>().max());
            DomainIndex->setValue(CGAL::to_double((*fh)->data().index));DomainIndex->setSizePolicy(QSizePolicy::Preferred,QSizePolicy::Preferred);
            QFrame* DIFrame=new QFrame();
            QHBoxLayout *DILayout = new QHBoxLayout;
            DILayout->addWidget(DomainIndex);
            DIFrame->setLayout(DILayout);
            DIFrame->setMaximumHeight(24);DIFrame->setMinimumHeight(24);DIFrame->setMinimumWidth(40);DIFrame->layout()->setSpacing(0);
            DIFrame->layout()->setContentsMargins(0,0,0,0);
            UIHandler::gui.facesDomainIndexFrame->layout()->addWidget(DIFrame);
            //SF
            QDoubleSpinBox* ScalingFactor = new QDoubleSpinBox(); ScalingFactor->setRange(-1,1);
            ScalingFactor->setValue(CGAL::to_double((*fh)->data().s));ScalingFactor->setSizePolicy(QSizePolicy::Preferred,QSizePolicy::Preferred);
            QFrame* SFFrame=new QFrame();
            QHBoxLayout *SFLayout = new QHBoxLayout;
            SFLayout->addWidget(ScalingFactor);
            SFFrame->setLayout(SFLayout);
            SFFrame->setMaximumHeight(24);SFFrame->setMinimumHeight(24);SFFrame->setMinimumWidth(40);SFFrame->layout()->setSpacing(0);
            SFFrame->layout()->setContentsMargins(0,0,0,0);
            UIHandler::gui.facesScalingFactorFrame->layout()->addWidget(SFFrame);
            uiFaces->push_back(std::make_tuple(Index,VerticesIndices,DomainIndex,ScalingFactor,(*fh)));
            IndexFrame->hide();VIFrame->hide();DIFrame->hide();SFFrame->hide();
            //signals
            QObject::connect(DomainIndex,SIGNAL(valueChanged(int)),UIHandler::instance,SLOT(setFacesDomainIndex(int)));
            QObject::connect(ScalingFactor,SIGNAL(valueChanged(double)),UIHandler::instance,SLOT(setFacesScalingFactor(double)));
        }
    }
}

void SceneManager::refreshMeshDataList(){
    //hide the widgets of the current list
    for(pointList::iterator it=uiPoints->begin();it!=uiPoints->end();it++){
        std::get<0>(*it)->parentWidget()->hide();
        std::get<1>(*it)->parentWidget()->hide();
        std::get<2>(*it)->parentWidget()->hide();
        std::get<3>(*it)->parentWidget()->hide();
        std::get<4>(*it)->parentWidget()->hide();
        std::get<5>(*it)->parentWidget()->hide();
    }
    for(faceList::iterator it=uiFaces->begin();it!=uiFaces->end();it++){
        std::get<0>(*it)->parentWidget()->hide();
        std::get<1>(*it)->parentWidget()->hide();
        std::get<2>(*it)->parentWidget()->hide();
        std::get<3>(*it)->parentWidget()->hide();
    }
    //set the pointer based on UI
    if(UIHandler::gui.domainsButton->isChecked()){
        SceneManager::uiPoints=&SceneManager::uiPointsJ;
        SceneManager::uiFaces=&SceneManager::uiFacesJ;
        SceneManager::A=&SceneManager::J;
    }
    else if(UIHandler::gui.intervalsButton->isChecked()){
        SceneManager::uiPoints=&SceneManager::uiPointsI;
        SceneManager::uiFaces=&SceneManager::uiFacesI;
        SceneManager::A=&SceneManager::I;
    }
    //show the data
    for(pointList::iterator it=uiPoints->begin();it!=uiPoints->end();it++){
        std::get<0>(*it)->parentWidget()->show();std::get<0>(*it)->show();
        std::get<1>(*it)->parentWidget()->show();std::get<1>(*it)->show();
        std::get<2>(*it)->parentWidget()->show();std::get<2>(*it)->show();
        std::get<3>(*it)->parentWidget()->show();std::get<3>(*it)->show();
        std::get<4>(*it)->parentWidget()->show();std::get<4>(*it)->show();
        std::get<5>(*it)->parentWidget()->show();std::get<5>(*it)->show();
        std::get<3>(*it)->setValue(CGAL::to_double(std::get<6>(*it)->data().height));
    }
    for(faceList::iterator it=uiFaces->begin();it!=uiFaces->end();it++){
        std::get<0>(*it)->parentWidget()->show();std::get<0>(*it)->show();
        std::get<1>(*it)->parentWidget()->show();std::get<1>(*it)->show();
        std::get<2>(*it)->parentWidget()->show();std::get<2>(*it)->show();
        std::get<3>(*it)->parentWidget()->show();std::get<3>(*it)->show();
    }
    //update the mesh tab
    UIHandler::gui.meshVerticalScalingScalingFactorDoubleSpinBox->setValue(CGAL::to_double(SceneManager::A->unbounded_face()->data().s));
    if(A->vst==Arrangement2::VerticalScalingType::SINGLE){
        UIHandler::gui.meshVerticalScalingScalingFactorDoubleSpinBox->setEnabled(true);
        UIHandler::gui.meshVerticalScalingSingleRadioButton->setChecked(true);
    }
    else if(A->vst==Arrangement2::VerticalScalingType::FACES){
        UIHandler::gui.meshVerticalScalingScalingFactorDoubleSpinBox->setEnabled(false);
        UIHandler::gui.meshVerticalScalingFacesRadioButton->setChecked(true);
    }
    else if(A->vst==Arrangement2::VerticalScalingType::VERTICES){
        UIHandler::gui.meshVerticalScalingScalingFactorDoubleSpinBox->setEnabled(false);
        UIHandler::gui.meshVerticalScalingVerticesRadioButton->setChecked(true);
    }
    if(A->ct==Arrangement2::ColoringType::CONSISTENT){
        UIHandler::gui.meshDomainIndexingColouringColoursRadioButton->setChecked(true);
    }
    else{
        UIHandler::gui.meshDomainIndexingColouringFaceIndicesRadioButton->setChecked(true);
        SceneManager::setMeshDomainIndexingVertexColoringDomainIndices(true);
    }
}

void SceneManager::cleanMeshDataList(){
    uiPoints=NULL;
    for(pointList::iterator it=uiPointsI.begin();it!=uiPointsI.end();it++){
        delete std::get<0>(*it)->parentWidget();
        delete std::get<1>(*it)->parentWidget();
        delete std::get<2>(*it)->parentWidget();
        delete std::get<3>(*it)->parentWidget();
        delete std::get<4>(*it)->parentWidget();
        delete std::get<5>(*it)->parentWidget();
    }
    uiPointsI.clear();
    for(pointList::iterator it=uiPointsJ.begin();it!=uiPointsJ.end();it++){
        delete std::get<0>(*it)->parentWidget();
        delete std::get<1>(*it)->parentWidget();
        delete std::get<2>(*it)->parentWidget();
        delete std::get<3>(*it)->parentWidget();
        delete std::get<4>(*it)->parentWidget();
        delete std::get<5>(*it)->parentWidget();
    }
    uiPointsJ.clear();
    uiFaces=NULL;
    for(faceList::iterator it=uiFacesI.begin();it!=uiFacesI.end();it++){
        delete std::get<0>(*it)->parentWidget();
        delete std::get<1>(*it)->parentWidget();
        delete std::get<2>(*it)->parentWidget();
        delete std::get<3>(*it)->parentWidget();
    }
    uiFacesI.clear();
    for(faceList::iterator it=uiFacesJ.begin();it!=uiFacesJ.end();it++){
        delete std::get<0>(*it)->parentWidget();
        delete std::get<1>(*it)->parentWidget();
        delete std::get<2>(*it)->parentWidget();
        delete std::get<3>(*it)->parentWidget();
    }
    uiFacesJ.clear();
}

//Visualization

void SceneManager::setVisualizationAxis(bool b){
    scene->drawAxisParameter=b;
    scene->update();
}

void SceneManager::setVisualizationHeights(bool b){
    scene->drawHeighValuesParameter=b;
    scene->update();
}

void SceneManager::setVisualizationGammas(bool b){
    meshRenderI->drawGammaValuesParameter=b;
    scene->update();
}

void SceneManager::setVisualizationLines(bool b){
    if(SceneManager::surfaceRender!=NULL)
        SceneManager::surfaceRender->drawFloorLinesParameter=b;
    scene->update();
}

void SceneManager::setVisualizationWireframe(bool b){
    if(SceneManager::surfaceRender!=NULL)
        SceneManager::surfaceRender->drawWireframeParameter=b;
    scene->update();
}

void SceneManager::setVisualizationFill(bool b){
    if(SceneManager::surfaceRender!=NULL)
        SceneManager::surfaceRender->drawFillParameter=b;
    scene->update();
}

void SceneManager::setVisualizationMesh(bool b){
    if(meshRender!=NULL)meshRender->drawEnabled=b;
    scene->update();
}

void SceneManager::setVisualizationSurface(bool b){
    if(SceneManager::surfaceRender!=NULL)
        SceneManager::surfaceRender->drawEnabled=b;
    scene->update();
}

void SceneManager::setVisualizationReconstructed(bool b){
    if(SceneManager::reconstructedSurfaceRender!=NULL&&b){
        SceneManager::surfaceRender=SceneManager::reconstructedSurfaceRender;
        SceneManager::refreshRenders();
    }
}

void SceneManager::setVisualizationOriginal(bool b){
    if(SceneManager::surfaceRender!=NULL&&b){
        SceneManager::surfaceRender=originalSurfaceRender;
        SceneManager::refreshRenders();
    }
}

void SceneManager::setVisualizationDomains(bool b){
    if(SceneManager::meshRenderJ!=NULL&&b){
        SceneManager::meshRender=SceneManager::meshRenderJ;
        SceneManager::refreshRenders();
        SceneManager::refreshMeshDataList();
        UIHandler::gui.verticesScalingFactorFrame->hide();
        UIHandler::gui.facesScalingFactorFrame->hide();
        UIHandler::gui.DomainColoringBox->hide();
        UIHandler::gui.VerticalScalingBox->hide();
        UIHandler::gui.MeshFromPointsBox->hide();
    }
}

void SceneManager::setVisualizationIntervals(bool b){
    if(SceneManager::meshRenderI!=NULL&&b){
        SceneManager::meshRender=SceneManager::meshRenderI;
        SceneManager::refreshRenders();
        SceneManager::refreshMeshDataList();
        UIHandler::gui.verticesScalingFactorFrame->show();
        UIHandler::gui.facesScalingFactorFrame->show();
        UIHandler::gui.DomainColoringBox->show();
        UIHandler::gui.VerticalScalingBox->show();
        UIHandler::gui.MeshFromPointsBox->show();
    }

}

void SceneManager::setVisualizationResetPosition(){
    SceneManager::scene->initTransformations();
    SceneManager::scene->update();
}

void SceneManager::setVisualizationPrint(){
    static QString lastLocation;
    static int counter=0;
    QImage image=scene->grabFramebuffer();
    QString fileName=QFileDialog::getSaveFileName(UIHandler::gui.centralWidget, QString("Save File"),
                           (lastLocation.isEmpty()?QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation):lastLocation)+"/frame"+QString::number(counter)+".png",
                            QString("Images (*.png *.eps *.ps *bmp)"));

    QFileInfo fileinfo(fileName);
    if(fileinfo.suffix().isEmpty())fileName+=".png";
    lastLocation= fileinfo.path();
    fileName =QDir::toNativeSeparators(fileName);
    image.save(fileName);
    counter++;
}
//Interpolation

void SceneManager::setInterpolationIterate(){
    /*if((!Mesh::validate(I))||(!Mesh::validate(J))||(!Mesh::validate(I,J))){
        QMessageBox::critical(UIHandler::gui.openGLWidget,"Bad Mesh","Intervals or domains mesh is not valid for interpolation: make sure it does not contain isolated vertices or degenerate faces");
        return;
    }*/
    timer t,s;
    s.setStart();
    static int iterations=0;
    if(fio==NULL){
        iterations=0;
        Arrangement2 tempI(I),tempJ(J);
        if(UIHandler::gui.CollinearFrameButton->isChecked())
            Mesh::addCollinearFrame(tempI,tempJ);
        if(UIHandler::gui.IFSButton->isChecked()){
            if(tempI.pft==Arrangement2::PrimitiveFaceType::TRIANGULAR){
                fio=new AFIS(tempI,tempJ);
            }
            else if(tempI.pft==Arrangement2::PrimitiveFaceType::RECTANGULAR){
                fio=new BFIS(tempI,tempJ);
            }
        }
        else if(UIHandler::gui.RIFSButton->isChecked()){
            if(tempI.pft==Arrangement2::PrimitiveFaceType::TRIANGULAR){
                fio=new RAFIS(tempI,tempJ);
            }
            else if(tempI.pft==Arrangement2::PrimitiveFaceType::RECTANGULAR){
                fio=new RBFIS(tempI,tempJ);
            }
        }
        //update the ui
        UIHandler::gui.reconstructedButton->setEnabled(true);
        UIHandler::gui.reconstructedButton->setChecked(true);
        UIHandler::gui.drawFloorLinesButton->setEnabled(true);
        UIHandler::gui.drawFrontBackButton->setEnabled(true);
        UIHandler::gui.drawWireframeButton->setEnabled(true);
        UIHandler::gui.surfaceButton->setEnabled(true);
        UIHandler::gui.RIFSButton->setEnabled(false);
        UIHandler::gui.IFSButton->setEnabled(false);
        UIHandler::gui.heightsButton->setEnabled(false);
        UIHandler::gui.gammaButton->setEnabled(false);
        UIHandler::gui.CollinearFrameButton->setEnabled(false);
    }
    s.setEnd();
    UIHandler::gui.surfaceButton->setChecked(true);
    UIHandler::gui.reconstructedButton->setChecked(true);
    t.setStart();
    if(UIHandler::gui.deterministicButton->isChecked()){
        fio->iterate_deterministic();
    }
    else if(UIHandler::gui.stochasticButton->isChecked()){
        fio->iterate_stochastic();
    }
    t.setEnd();
    //update the logs
    iterations++;
    std::vector<Point> v;
    fio->getPoints(v);
    t.setEnd();
    std::ofstream log("log.txt", std::ofstream::out | std::ofstream::app);
    if (!log.is_open()){std::cout<<"could not open file "<<"logfile.txt"<<std::endl;exit(0);}
    log<<"fnum "<<I.number_of_faces()-1<<", inum "<<iterations<<", pnum "<<v.size()<<", t "<<t.getInterval()/(double)CLOCKS_PER_SEC<<", s "<<s.getInterval()/(double)CLOCKS_PER_SEC<<std::endl;
    //update the renderer
    SceneManager::reconstructedSurface.clear();
    fio->getPoints(SceneManager::reconstructedSurface);
    std::cout<<"rs is "<<SceneManager::reconstructedSurface.size()<<std::endl;
    if(UIHandler::gui.CollinearFrameButton->isChecked()&&UIHandler::gui.RIFSButton->isChecked()){
        //remove the frame data
        std::vector<Point> centroids;
        for(std::vector<std::vector<Point>>::iterator it=SceneManager::reconstructedSurface.begin();it!=SceneManager::reconstructedSurface.end();it++){
            //compute a centroid
            Point centroid =CGAL::centroid(it->begin(),it->end(),CGAL::Dimension_tag<0>());
            centroids.push_back(centroid);
        }
        int side = std::sqrt(centroids.size());
        std::vector<Point> erase_centroids(side*4);
        //sort centroids by x
        std::sort(centroids.begin(),centroids.end(),[](auto i,auto j){return i.x()<j.x();});
        erase_centroids.resize(std::distance(erase_centroids.begin(),std::copy(centroids.begin(),centroids.begin()+side,erase_centroids.begin())));
        std::reverse(centroids.begin(),centroids.end());
        erase_centroids.resize(std::distance(erase_centroids.begin(),std::copy(centroids.begin(),centroids.begin()+side,erase_centroids.end())));
        std::sort(centroids.begin(),centroids.end(),[](auto i,auto j){return i.y()<j.y();});
        erase_centroids.resize(std::distance(erase_centroids.begin(),std::copy(centroids.begin(),centroids.begin()+side,erase_centroids.end())));
        std::reverse(centroids.begin(),centroids.end());
        erase_centroids.resize(std::distance(erase_centroids.begin(),std::copy(centroids.begin(),centroids.begin()+side,erase_centroids.end())));
        SceneManager::reconstructedSurface.resize(std::distance(SceneManager::reconstructedSurface.begin(),std::remove_if(SceneManager::reconstructedSurface.begin(),SceneManager::reconstructedSurface.end(),
                                                                                                                          [&erase_centroids](auto i){Point c=CGAL::centroid(i.begin(),i.end(),CGAL::Dimension_tag<0>());
            return std::find(erase_centroids.begin(),erase_centroids.end(),c)!=erase_centroids.end();})));
    }
    std::vector<Delaunay2> dtv;
    for(std::vector<std::vector<Point>>::iterator it=SceneManager::reconstructedSurface.begin();it!=SceneManager::reconstructedSurface.end();it++){
        for(std::vector<Point>::iterator itt=it->begin();itt!=it->end();itt++){
            std::cout<<"pontoi et "<<(*itt)<<std::endl;
        }
        dtv.push_back(Delaunay2(it->begin(),it->end()));
    }
    delete SceneManager::reconstructedSurfaceRender;
    SceneManager::reconstructedSurfaceRender = new SurfaceRender();
    //update the scene
    SceneManager::scene->makeCurrent();
    SceneManager::reconstructedSurfaceRender->SetData(dtv);
    SceneManager::scene->doneCurrent();
    SceneManager::surfaceRender=SceneManager::reconstructedSurfaceRender;
    SceneManager::refreshRenders();
}

void SceneManager::setInterpolationClear(){
    //delete the reconstructed
    delete SceneManager::reconstructedSurfaceRender;
    delete SceneManager::fio;
    SceneManager::reconstructedSurfaceRender=NULL;
    SceneManager::fio=NULL;
    //update the scene
    SceneManager::surfaceRender=SceneManager::originalSurfaceRender;
    SceneManager::refreshRenders();
    //update the ui
    UIHandler::gui.RIFSButton->setEnabled(true);
    UIHandler::gui.IFSButton->setEnabled(true);
    if(I.pft==Arrangement2::PrimitiveFaceType::RECTANGULAR&&UIHandler::gui.meshDomainIndexingColouringFaceIndicesRadioButton->isChecked())
        UIHandler::gui.CollinearFrameButton->setEnabled(true);
    if(SceneManager::surfaceRender==NULL){
        UIHandler::gui.drawFloorLinesButton->setEnabled(false);
        UIHandler::gui.drawFrontBackButton->setEnabled(false);
        UIHandler::gui.drawWireframeButton->setEnabled(false);
    }
    else{
        UIHandler::gui.heightsButton->setEnabled(true);
        UIHandler::gui.gammaButton->setEnabled(true);
    }
    UIHandler::gui.reconstructedButton->setEnabled(false);
    UIHandler::gui.originalButton->setChecked(true);
    //normalize scene
    SceneManager::scene->normalizeGL();
}

//Sampling
void SceneManager::setSamplingHeights(){
    Sampling::SampleHeights(SceneManager::I,SceneManager::J,SceneManager::surface);
    SceneManager::refreshMeshRender();
}

void SceneManager::setVerticalScalingVector(){
    std::vector<std::vector<Point>> mesh_points;
    std::vector<std::vector<Point>> global_points;
    Regression::Sample(SceneManager::I,SceneManager::J,SceneManager::surface,mesh_points,global_points);
    Regression::CalculateVSV(SceneManager::I,SceneManager::J,mesh_points,global_points);
}

void SceneManager::setSamplingResize(){
    //create a RBFIM
    SceneManager::I.clear();
    Mesh::createMNRec(SceneManager::I,UIHandler::gui.MspinBox->value(),UIHandler::gui.NspinBox->value());
    //Mesh::createMN(SceneManager::I,UIHandler::gui.MspinBox->value(),UIHandler::gui.NspinBox->value());
    SceneManager::J.clear();
    Mesh::createMNRec(SceneManager::J,3,3);
    //Mesh::createMN(SceneManager::J,2,2);
    SceneManager::refreshMeshRender();
}

//vertices

void SceneManager::setVerticesAll(){
    for(pointList::iterator it=SceneManager::uiPoints->begin();it!=uiPoints->end();it++){
        std::get<0>(*it)->setChecked(true);
    }
}

void SceneManager::setVerticesNone(){
    for(pointList::iterator it=SceneManager::uiPoints->begin();it!=uiPoints->end();it++){
        std::get<0>(*it)->setChecked(false);
    }
}

void SceneManager::setVerticesClear(){
    for(pointList::iterator it=SceneManager::uiPoints->begin();it!=uiPoints->end();it++){
        if(std::get<0>(*it)->isChecked()){
            std::get<1>(*it)->clear();
            std::get<2>(*it)->clear();
            std::get<3>(*it)->clear();
            std::get<4>(*it)->clear();
            std::get<5>(*it)->clear();
        }
    }
}

void SceneManager::setVerticesAdd(){
    Arrangement2::Vertex_handle vh=A->insert_in_face_interior(ExactKernel::Point_2(0,0),A->unbounded_face());
    //Index
     QCheckBox* Index = new QCheckBox();
     Index->setText(QString((std::to_string(uiPoints->size())).c_str()));
     QFrame* IndexFrame=new QFrame();
     QHBoxLayout *IndexLayout = new QHBoxLayout;
     IndexLayout->addWidget(Index);
     IndexFrame->setLayout(IndexLayout);
     IndexFrame->setMaximumHeight(24);IndexFrame->setMinimumHeight(24);IndexFrame->setMinimumWidth(40);IndexFrame->layout()->setSpacing(0);
     IndexFrame->layout()->setContentsMargins(0,0,0,0);
     UIHandler::gui.verticesIndexingFrame->layout()->addWidget(IndexFrame);
     //X
     QDoubleSpinBox* X = new QDoubleSpinBox();
     X->setValue(0.0);
     QFrame* XFrame=new QFrame();
     QHBoxLayout *XLayout = new QHBoxLayout;
     XLayout->addWidget(X);
     XFrame->setLayout(XLayout);
     XFrame->setMaximumHeight(24);XFrame->setMinimumHeight(24);XFrame->setMinimumWidth(40);XFrame->layout()->setSpacing(0);
     XFrame->layout()->setContentsMargins(0,0,0,0);
     UIHandler::gui.verticesXFrame->layout()->addWidget(XFrame);
     //Y
     QDoubleSpinBox* Y = new QDoubleSpinBox();
     Y->setValue(0.0);
     QFrame* YFrame=new QFrame();
     QHBoxLayout *YLayout = new QHBoxLayout;
     YLayout->addWidget(Y);
     YFrame->setLayout(YLayout);
     YFrame->setMaximumHeight(24);YFrame->setMinimumHeight(24);YFrame->setMinimumWidth(40);YFrame->layout()->setSpacing(0);
     YFrame->layout()->setContentsMargins(0,0,0,0);
     UIHandler::gui.verticesYFrame->layout()->addWidget(YFrame);
     //Height
     QDoubleSpinBox* Height = new QDoubleSpinBox();
     Height->setValue(0.0);
     QFrame* HeightFrame=new QFrame();
     QHBoxLayout *HeightLayout = new QHBoxLayout;
     HeightLayout->addWidget(Height);
     HeightFrame->setLayout(HeightLayout);
     HeightFrame->setMaximumHeight(24);HeightFrame->setMinimumHeight(24);HeightFrame->setMinimumWidth(40);HeightFrame->layout()->setSpacing(0);
     HeightFrame->layout()->setContentsMargins(0,0,0,0);
     UIHandler::gui.verticesHeightFrame->layout()->addWidget(HeightFrame);
     //Color
     QSpinBox* Colour = new QSpinBox();
     Colour->setValue(0);
     Colour->setRange(0,std::numeric_limits<int>().max());
     QFrame* ColourFrame=new QFrame();
     QHBoxLayout *ColourLayout = new QHBoxLayout;
     ColourLayout->addWidget(Colour);
     ColourFrame->setLayout(ColourLayout);
     ColourFrame->setMaximumHeight(24);ColourFrame->setMinimumHeight(24);ColourFrame->setMinimumWidth(40);ColourFrame->layout()->setSpacing(0);
     ColourFrame->layout()->setContentsMargins(0,0,0,0);
     UIHandler::gui.verticesColourFrame->layout()->addWidget(ColourFrame);
     //Scaling Factor
     QDoubleSpinBox* ScalingFactor = new QDoubleSpinBox();
     ScalingFactor->setValue(0.0);
     ScalingFactor->setRange(-1,1);
     QFrame* SFFrame=new QFrame();
     QHBoxLayout *SFLayout = new QHBoxLayout;
     SFLayout->addWidget(ScalingFactor);
     SFFrame->setLayout(SFLayout);
     SFFrame->setMaximumHeight(24);SFFrame->setMinimumHeight(24);SFFrame->setMinimumWidth(40);SFFrame->layout()->setSpacing(0);
     SFFrame->layout()->setContentsMargins(0,0,0,0);
     UIHandler::gui.verticesScalingFactorFrame->layout()->addWidget(SFFrame);
     QObject::connect(X,SIGNAL(valueChanged(double)),UIHandler::instance,SLOT(setVerticesX(double)));
     QObject::connect(Y,SIGNAL(valueChanged(double)),UIHandler::instance,SLOT(setVerticesY(double)));
     QObject::connect(Height,SIGNAL(valueChanged(double)),UIHandler::instance,SLOT(setVerticesHeights(double)));
     QObject::connect(Colour,SIGNAL(valueChanged(int)),UIHandler::instance,SLOT(setVerticesColour(int)));
     QObject::connect(ScalingFactor,SIGNAL(valueChanged(double)),UIHandler::instance,SLOT(setVerticesScalingFactor(double)));
     uiPoints->push_back(std::make_tuple(Index,X,Y,Height,Colour,ScalingFactor,vh));
     Mesh::sampleHeightsJFromI(SceneManager::I,SceneManager::J);
     SceneManager::refreshMeshRender();
     SceneManager::refreshMeshDataList();
}

void SceneManager::setVerticesRemove(){
    std::vector<pointList::iterator> to_remove;
    for(pointList::iterator it=SceneManager::uiPoints->begin();it!=uiPoints->end();it++){
        if(std::get<0>(*it)->isChecked()){
            if(std::get<6>(*it)->degree()>0){
                Arrangement2::Halfedge_around_vertex_circulator hc(std::get<6>(*it)->incident_halfedges()),end(hc);
                std::vector<Arrangement2::Halfedge_handle> halfedges;
                do{
                    if(std::find_if(halfedges.begin(),halfedges.end(),[hc](auto i){return (hc->target()==i->target()&& hc->source()==i->source())||
                                    (hc->target()==i->source()&& hc->source()==i->target()); })==halfedges.end()){
                        halfedges.push_back(hc);
                    }
                }while(++hc!=end);
                for(std::vector<Arrangement2::Halfedge_handle>::iterator itt=halfedges.begin();itt!=halfedges.end();itt++){

                    A->remove_edge(*itt,false,false);
                }
            }
           CGAL::remove_vertex(*A,std::get<6>(*it));
            delete std::get<0>(*it)->parentWidget();
            delete std::get<1>(*it)->parentWidget();
            delete std::get<2>(*it)->parentWidget();
            delete std::get<3>(*it)->parentWidget();
            delete std::get<4>(*it)->parentWidget();
            delete std::get<5>(*it)->parentWidget();
            std::get<0>(*it)=NULL;
        }
    }
    uiPoints->resize(std::distance(uiPoints->begin(),std::remove_if(uiPoints->begin(),uiPoints->end(),[](auto i){return std::get<0>(i)==NULL;})));
    SceneManager::refreshFacesDataList();
    SceneManager::refreshMeshRender();
}

void SceneManager::setVerticesX(QDoubleSpinBox* sb,double d){
    pointList::iterator result;
    if((result= std::find_if(SceneManager::uiPoints->begin(),SceneManager::uiPoints->end(),
                             [sb](auto i){return std::get<1>(i)==sb;}))!= SceneManager::uiPoints->end()){
        std::get<6>(*result)->point()=ExactKernel::Point_2(d,std::get<6>(*result)->point().y());
        Mesh::sampleHeightsJFromI(SceneManager::I,SceneManager::J);
        std::get<3>(*result)->setValue(CGAL::to_double(std::get<6>(*result)->data().height));
        SceneManager::refreshMeshRender();
    }
}

void SceneManager::setVerticesY(QDoubleSpinBox* sb,double d){
    pointList::iterator result;
    if((result= std::find_if(SceneManager::uiPoints->begin(),SceneManager::uiPoints->end(),
                             [sb](auto i){return std::get<2>(i)==sb;}))!= SceneManager::uiPoints->end()){
        std::get<6>(*result)->point()=ExactKernel::Point_2(std::get<6>(*result)->point().x(),d);
        Mesh::sampleHeightsJFromI(SceneManager::I,SceneManager::J);
        std::get<3>(*result)->setValue(CGAL::to_double(std::get<6>(*result)->data().height));
        SceneManager::refreshMeshRender();
    }
}

void SceneManager::setVerticesHeight(QDoubleSpinBox* sb,double d){
    pointList::iterator result;
    if((result= std::find_if(SceneManager::uiPoints->begin(),SceneManager::uiPoints->end(),
                             [sb](auto i){return std::get<3>(i)==sb;}))!= SceneManager::uiPoints->end()){
        std::get<6>(*result)->data().height=d;
        Mesh::sampleHeightsJFromI(SceneManager::I,SceneManager::J);
        std::get<3>(*result)->setValue(CGAL::to_double(std::get<6>(*result)->data().height));
        SceneManager::refreshMeshRender();
    }
}

void SceneManager::setVerticesColour(QSpinBox* sb,int i){
    pointList::iterator result;
    if((result= std::find_if(SceneManager::uiPoints->begin(),SceneManager::uiPoints->end(),
                             [sb](auto i){return std::get<4>(i)==sb;}))!= SceneManager::uiPoints->end()){
        std::get<6>(*result)->data().triangulation_index=i;
        SceneManager::refreshMeshRender();
    }
}

void SceneManager::setVerticesScalingFactor(QDoubleSpinBox* sb,double d){
    pointList::iterator result;
    if((result= std::find_if(SceneManager::uiPoints->begin(),SceneManager::uiPoints->end(),
                             [sb](auto i){return std::get<5>(i)==sb;}))!= SceneManager::uiPoints->end()){
        std::get<6>(*result)->data().s=d;
        SceneManager::refreshMeshRender();
    }
}

//faces

void SceneManager::setFacesAll(){
    for(faceList::iterator it=SceneManager::uiFaces->begin();it!=uiFaces->end();it++){
        std::get<0>(*it)->setChecked(true);
    }
}

void SceneManager::setFacesNone(){
    for(faceList::iterator it=SceneManager::uiFaces->begin();it!=uiFaces->end();it++){
        std::get<0>(*it)->setChecked(false);
    }

}

void SceneManager::setFacesClear(){
    for(faceList::iterator it=SceneManager::uiFaces->begin();it!=uiFaces->end();it++){
        if(std::get<0>(*it)->isChecked()){
            std::get<2>(*it)->clear();
            std::get<3>(*it)->clear();
        }
    }
}

void SceneManager::setFacesDomainIndex(QSpinBox* sb,int i){
    faceList::iterator result;
    if((result= std::find_if(SceneManager::uiFaces->begin(),SceneManager::uiFaces->end(),
                             [sb](auto i){return std::get<2>(i)==sb;}))!= SceneManager::uiFaces->end()){
        std::get<4>(*result)->data().index=i;
        SceneManager::refreshMeshRender();
    }

}

void SceneManager::setFacesScalingFactor(QDoubleSpinBox* sb,double d){
    faceList::iterator result;
    if((result= std::find_if(SceneManager::uiFaces->begin(),SceneManager::uiFaces->end(),
                             [sb](auto i){return std::get<3>(i)==sb;}))!= SceneManager::uiFaces->end()){
        std::get<4>(*result)->data().s=d;
        SceneManager::refreshMeshRender();
    }
}

//mesh

void SceneManager::setMeshVerticalScalingSingle(bool b){
    UIHandler::gui.meshVerticalScalingScalingFactorDoubleSpinBox->setEnabled(b);
    A->vst=Arrangement2::VerticalScalingType::SINGLE;
    SceneManager::refreshMeshRender();
}
void SceneManager::setMeshVerticalScalingFaces(bool b){
    A->vst=Arrangement2::VerticalScalingType::FACES;
    SceneManager::refreshMeshRender();
}

void SceneManager::setMeshVerticalScalingVertices(bool b){
    A->vst=Arrangement2::VerticalScalingType::VERTICES;
    SceneManager::refreshMeshRender();
}

void SceneManager::setMeshVerticalScalingFactor(double d){
    I.unbounded_face()->data().s=d;
    J.unbounded_face()->data().s=d;
    SceneManager::refreshMeshRender();
}

void SceneManager::setMeshDomainIndexingVertexColoringDomainIndices(bool b){
    if(b){
        A->ct=Arrangement2::ColoringType::NONE;
        A->rit=Arrangement2::RecurrentIndexingType::FACES;
        if(A->pft==Arrangement2::PrimitiveFaceType::RECTANGULAR)
            UIHandler::gui.CollinearFrameButton->setEnabled(true);
        SceneManager::meshRender->drawColoursParameter=false;
        SceneManager::scene->update();
    }
    else{
        A->ct=Arrangement2::ColoringType::CONSISTENT;
        A->rit=Arrangement2::RecurrentIndexingType::NONE;
        SceneManager::meshRender->drawColoursParameter=true;
        SceneManager::scene->update();
    }
}

void SceneManager::setMeshDomainIndexingVertexColoringVertexColours(bool b){
    if(b){
        A->ct=Arrangement2::ColoringType::CONSISTENT;
        A->rit=Arrangement2::RecurrentIndexingType::NONE;
        UIHandler::gui.CollinearFrameButton->setEnabled(false);
        UIHandler::gui.CollinearFrameButton->setChecked(false);
        SceneManager::meshRender->drawColoursParameter=true;
        SceneManager::scene->update();
    }
    else{
        A->ct=Arrangement2::ColoringType::NONE;
        A->rit=Arrangement2::RecurrentIndexingType::FACES;
        SceneManager::meshRender->drawColoursParameter=false;
        SceneManager::scene->update();
    }
}

void SceneManager::setMeshClear(){
    Mesh::clear(I);
    Mesh::clear(J);
    SceneManager::refreshMeshRender();
    SceneManager::refreshFacesDataList(&uiPointsI,&uiFacesI,&I);
    SceneManager::refreshFacesDataList(&uiPointsJ,&uiFacesJ,&J);
    SceneManager::refreshMeshDataList();
}

void SceneManager::setMeshQuadify(){
    I.pft=Arrangement2::PrimitiveFaceType::RECTANGULAR;
    J.pft=Arrangement2::PrimitiveFaceType::RECTANGULAR;
    if(!Mesh::validate(I,J)||!Mesh::quadify(I)||!Mesh::quadify(J)||!Mesh::validate(I)||!Mesh::validate(J)){
        Mesh::clear(I);Mesh::clear(J);
     QMessageBox::critical(UIHandler::gui.openGLWidget,"Bad Mesh","Intervals or domains mesh is not valid for quadification");
        return;
    }
    SceneManager::refreshMeshRender();
    SceneManager::refreshFacesDataList(&uiPointsI,&uiFacesI,&I);
    SceneManager::refreshFacesDataList(&uiPointsJ,&uiFacesJ,&J);
    SceneManager::refreshMeshDataList();
}

void SceneManager::setMeshTriangulate(){
    I.pft=Arrangement2::PrimitiveFaceType::TRIANGULAR;
    J.pft=Arrangement2::PrimitiveFaceType::TRIANGULAR;
    if(Mesh::validate(I,J)){
        if(UIHandler::gui.triangulationRadioButtonCross->isChecked()){
            if(!Mesh::triangulateX(I)||!Mesh::triangulateX(J)||!Mesh::validate(I)||!Mesh::validate(J)){
                QMessageBox::critical(UIHandler::gui.openGLWidget,"Bad Mesh","Intervals or domains mesh is not valid for triangulation");
                 Mesh::clear(I);Mesh::clear(J);
                 return;
            }
        }
        else if(UIHandler::gui.triangulationRadioButtonNWSE->isChecked()){
            if(!Mesh::triangulatePL(I)||!Mesh::triangulatePL(J)||!Mesh::validate(I)||!Mesh::validate(J)){
                QMessageBox::critical(UIHandler::gui.openGLWidget,"Bad Mesh","Intervals or domains mesh is not valid for triangulation");
                 Mesh::clear(I);Mesh::clear(J);
                 return;
            }
        }
        else if(UIHandler::gui.triangulationRadioButtonSWNE->isChecked()){
            if(!Mesh::triangulatePR(I)||!Mesh::triangulatePR(J)||!Mesh::validate(I)||!Mesh::validate(J)){
                QMessageBox::critical(UIHandler::gui.openGLWidget,"Bad Mesh","Intervals or domains mesh is not valid for triangulation");
                 Mesh::clear(I);Mesh::clear(J);
                 return;
            }
        }
    }
    else{
        QMessageBox::critical(UIHandler::gui.openGLWidget,"Bad Mesh","Intervals or domains mesh is not valid for triangulation");
         Mesh::clear(I);Mesh::clear(J);
         return;
    }
    SceneManager::refreshMeshRender();
    SceneManager::refreshFacesDataList(&uiPointsI,&uiFacesI,&I);
    SceneManager::refreshFacesDataList(&uiPointsJ,&uiFacesJ,&J);
    SceneManager::refreshMeshDataList();
}

//Toolbar

void SceneManager::setToolBarFileLoadSurface(){
    //get the file path from dialogue
    QString filename = QFileDialog::getOpenFileName();
    std::string std_filename=filename.toUtf8().constData();
    //update the data
    std::vector<Point> newSurface;
    if(!FileIO::Read(newSurface,std_filename)){
        if(!filename.isEmpty()){
            QMessageBox::critical(UIHandler::gui.openGLWidget,"Bad Surface File","Surface file: "+filename+" is invalid");
        }
        return;
    }
    SceneManager::surface=std::vector<Point>(newSurface);
    Sampling::NormalizeSurfaceCoodrinates(SceneManager::surface);
    //update the surface render
    delete SceneManager::originalSurfaceRender;
    delete SceneManager::fio;
    SceneManager::originalSurfaceRender=NULL;
    SceneManager::fio=NULL;
    SceneManager::originalSurfaceRender =new SurfaceRender();
    SceneManager::surfaceRender=originalSurfaceRender;
    Delaunay2 dt(SceneManager::surface.begin(),SceneManager::surface.end());
    //update the scene renders//
    SceneManager::scene->makeCurrent();
    surfaceRender->SetData(dt);
    SceneManager::scene->doneCurrent();
    SceneManager::refreshRenders();
    //update the ui
    UIHandler::gui.drawFloorLinesButton->setEnabled(true);
    UIHandler::gui.drawFrontBackButton->setEnabled(true);
    UIHandler::gui.drawWireframeButton->setEnabled(true);
    UIHandler::gui.drawHeightValuesButton->setEnabled(true);
    UIHandler::gui.surfaceButton->setEnabled(true);
    UIHandler::gui.originalButton->setEnabled(true);
    UIHandler::gui.originalButton->setChecked(true);
    UIHandler::gui.surfaceButton->setChecked(true);
    if(SceneManager::meshRender!=NULL){
        UIHandler::gui.heightsButton->setEnabled(true);
        UIHandler::gui.gammaButton->setEnabled(true);
    }
}

void SceneManager::setToolBarFileLoadMesh(){
    //get the file path from dialogue
    QString filename = QFileDialog::getExistingDirectory();
    QFileInfo fileinfo(filename);
    std::string filenameI= (filename+"/"+fileinfo.baseName()+"."+fileinfo.suffix()+"I").toUtf8().constData();
    std::string filenameJ= (filename+"/"+fileinfo.baseName()+"."+fileinfo.suffix()+"J").toUtf8().constData();
    //update the data
    Arrangement2 newI,newJ;
    if(!FileIO::Read(newI,filenameI)){
        if(!filename.isEmpty())
            QMessageBox::critical(UIHandler::gui.openGLWidget,"Bad Mesh File","Mesh file: "+filename+"/"+fileinfo.baseName()+"."+fileinfo.suffix()+"I"" is invalid");
        return;
    }
    if(!FileIO::Read(newJ,filenameJ)){
        if(!filename.isEmpty())
            QMessageBox::critical(UIHandler::gui.openGLWidget,"Bad Mesh File","Mesh file: "+filename+"/"+fileinfo.baseName()+"."+fileinfo.suffix()+"J"" is invalid");
        return;
    }
    SceneManager::I=Arrangement2(newI);
    SceneManager::J=Arrangement2(newJ);
    J.unbounded_face()->data().s=I.unbounded_face()->data().s;
    //update the mesh render
    delete SceneManager::meshRenderI;
    delete SceneManager::meshRenderJ;
    delete SceneManager::reconstructedSurfaceRender;
    delete SceneManager::fio;
    SceneManager::reconstructedSurfaceRender=NULL;
    SceneManager::fio=NULL;
    SceneManager::meshRenderI =new MeshRender();
    SceneManager::meshRenderJ =new MeshRender();
    meshRenderJ->drawGammaValuesParameter=false;
    //update the scene renders//
    SceneManager::scene->makeCurrent();
    meshRenderI->SetData(I);
    meshRenderJ->SetData(J);
    SceneManager::scene->doneCurrent();
    meshRender=meshRenderI;
    SceneManager::refreshRenders();
    //update the gui buttons
    //iteration
    UIHandler::gui.deterministicButton->setEnabled(true);
    UIHandler::gui.stochasticButton->setEnabled(true);
    UIHandler::gui.IFSButton->setEnabled(true);
    UIHandler::gui.RIFSButton->setEnabled(true);
    UIHandler::gui.iterateButton->setEnabled(true);
    UIHandler::gui.clearButton->setEnabled(true);
    //visualization
    UIHandler::gui.drawHeightValuesButton->setEnabled(true);
    UIHandler::gui.drawGammaValues->setEnabled(true);
    UIHandler::gui.meshButton->setEnabled(true);
    UIHandler::gui.originalButton->setChecked(true);
    UIHandler::gui.reconstructedButton->setEnabled(false);
    UIHandler::gui.meshButton->setChecked(true);
    UIHandler::gui.domainsButton->setEnabled(true);
    UIHandler::gui.intervalsButton->setEnabled(true);
    UIHandler::gui.intervalsButton->setChecked(true);
    if(SceneManager::surfaceRender!=NULL){
        UIHandler::gui.heightsButton->setEnabled(true);
        UIHandler::gui.gammaButton->setEnabled(true);
    }
    //vertices/faces UI
    //update the list of vertices
    SceneManager::cleanMeshDataList();
    //I
    for(Arrangement2::Vertex_iterator it=I.vertices_begin();it!=I.vertices_end();it++){
        QCheckBox* Index= new QCheckBox();
        Index->setSizePolicy(QSizePolicy::Preferred,QSizePolicy::Preferred);
        QDoubleSpinBox* X=new QDoubleSpinBox();X->setRange(std::numeric_limits<double>().lowest(),std::numeric_limits<double>().max());
        X->setValue(CGAL::to_double(it->point().x()));X->setSizePolicy(QSizePolicy::Preferred,QSizePolicy::Preferred);
        QDoubleSpinBox* Y=new QDoubleSpinBox();Y->setRange(std::numeric_limits<double>().lowest(),std::numeric_limits<double>().max());
        Y->setValue(CGAL::to_double(it->point().y()));Y->setSizePolicy(QSizePolicy::Preferred,QSizePolicy::Preferred);
        QDoubleSpinBox* Height=new QDoubleSpinBox();Height->setRange(std::numeric_limits<double>().lowest(),std::numeric_limits<double>().max());
        Height->setValue(CGAL::to_double(it->data().height));Height->setSizePolicy(QSizePolicy::Preferred,QSizePolicy::Preferred);
        QSpinBox* Colour = new QSpinBox;Colour->setRange(0,std::numeric_limits<int>().max());
        Colour->setValue(it->data().triangulation_index);Colour->setSizePolicy(QSizePolicy::Preferred,QSizePolicy::Preferred);
        QDoubleSpinBox* ScalingFactor = new QDoubleSpinBox();ScalingFactor->setRange(-1,1);
        ScalingFactor->setValue(CGAL::to_double(it->data().s));ScalingFactor->setSizePolicy(QSizePolicy::Preferred,QSizePolicy::Preferred);
        Index->hide();X->hide();Y->hide();Height->hide();Colour->hide();ScalingFactor->hide();
        SceneManager::uiPointsI.push_back(std::make_tuple(Index,X,Y,Height,Colour,ScalingFactor,it));
        //singals
        QObject::connect(X,SIGNAL(valueChanged(double)),UIHandler::instance,SLOT(setVerticesX(double)));
        QObject::connect(Y,SIGNAL(valueChanged(double)),UIHandler::instance,SLOT(setVerticesY(double)));
        QObject::connect(Height,SIGNAL(valueChanged(double)),UIHandler::instance,SLOT(setVerticesHeights(double)));
        QObject::connect(Colour,SIGNAL(valueChanged(int)),UIHandler::instance,SLOT(setVerticesColour(int)));
        QObject::connect(ScalingFactor,SIGNAL(valueChanged(double)),UIHandler::instance,SLOT(setVerticesScalingFactor(double)));
    }
    std::sort(uiPointsI.begin(),uiPointsI.end(),[](auto i,auto j){if(std::get<6>(i)->point().y()==std::get<6>(j)->point().y())
            return std::get<6>(i)->point().x()<std::get<6>(j)->point().x();return std::get<6>(i)->point().y()<std::get<6>(j)->point().y();});
    for(pointList::iterator it=uiPointsI.begin();it!=uiPointsI.end();it++){
        //Index
         std::get<0>(*it)->setText(QString((std::to_string(std::distance(uiPointsI.begin(),it))).c_str()));
         QFrame* IndexFrame=new QFrame();
         QHBoxLayout *IndexLayout = new QHBoxLayout;
         IndexLayout->addWidget(std::get<0>(*it));
         IndexFrame->setLayout(IndexLayout);
         IndexFrame->setMaximumHeight(24);IndexFrame->setMinimumHeight(24);IndexFrame->setMinimumWidth(40);IndexFrame->layout()->setSpacing(0);
         IndexFrame->layout()->setContentsMargins(0,0,0,0);
         UIHandler::gui.verticesIndexingFrame->layout()->addWidget(IndexFrame);
         //X
         QFrame* XFrame=new QFrame();
         QHBoxLayout *XLayout = new QHBoxLayout;
         XLayout->addWidget(std::get<1>(*it));
         XFrame->setLayout(XLayout);
         XFrame->setMaximumHeight(24);XFrame->setMinimumHeight(24);XFrame->setMinimumWidth(40);XFrame->layout()->setSpacing(0);
         XFrame->layout()->setContentsMargins(0,0,0,0);
         UIHandler::gui.verticesXFrame->layout()->addWidget(XFrame);
         //Y
         QFrame* YFrame=new QFrame();
         QHBoxLayout *YLayout = new QHBoxLayout;
         YLayout->addWidget(std::get<2>(*it));
         YFrame->setLayout(YLayout);
         YFrame->setMaximumHeight(24);YFrame->setMinimumHeight(24);YFrame->setMinimumWidth(40);YFrame->layout()->setSpacing(0);
         YFrame->layout()->setContentsMargins(0,0,0,0);
         UIHandler::gui.verticesYFrame->layout()->addWidget(YFrame);
         //Height
         QFrame* HeightFrame=new QFrame();
         QHBoxLayout *HeightLayout = new QHBoxLayout;
         HeightLayout->addWidget(std::get<3>(*it));
         HeightFrame->setLayout(HeightLayout);
         HeightFrame->setMaximumHeight(24);HeightFrame->setMinimumHeight(24);HeightFrame->setMinimumWidth(40);HeightFrame->layout()->setSpacing(0);
         HeightFrame->layout()->setContentsMargins(0,0,0,0);
         UIHandler::gui.verticesHeightFrame->layout()->addWidget(HeightFrame);
         //Color
         QFrame* ColourFrame=new QFrame();
         QHBoxLayout *ColourLayout = new QHBoxLayout;
         ColourLayout->addWidget(std::get<4>(*it));
         ColourFrame->setLayout(ColourLayout);
         ColourFrame->setMaximumHeight(24);ColourFrame->setMinimumHeight(24);ColourFrame->setMinimumWidth(40);ColourFrame->layout()->setSpacing(0);
         ColourFrame->layout()->setContentsMargins(0,0,0,0);
         UIHandler::gui.verticesColourFrame->layout()->addWidget(ColourFrame);
         //Scaling Factor
         QFrame* SFFrame=new QFrame();
         QHBoxLayout *SFLayout = new QHBoxLayout;
         SFLayout->addWidget(std::get<5>(*it));
         SFFrame->setLayout(SFLayout);
         SFFrame->setMaximumHeight(24);SFFrame->setMinimumHeight(24);SFFrame->setMinimumWidth(40);SFFrame->layout()->setSpacing(0);
         SFFrame->layout()->setContentsMargins(0,0,0,0);
         UIHandler::gui.verticesScalingFactorFrame->layout()->addWidget(SFFrame);
         IndexFrame->hide();XFrame->hide();YFrame->hide();HeightFrame->hide();ColourFrame->hide();SFFrame->hide();
    }
    //J
    for(Arrangement2::Vertex_iterator it=J.vertices_begin();it!=J.vertices_end();it++){
        QCheckBox* Index= new QCheckBox(); Index->setText(QString((std::to_string(std::distance(J.vertices_begin(),it))).c_str()));
        Index->setSizePolicy(QSizePolicy::Preferred,QSizePolicy::Preferred);
        QDoubleSpinBox* X=new QDoubleSpinBox();X->setRange(std::numeric_limits<double>().lowest(),std::numeric_limits<double>().max());
        X->setValue(CGAL::to_double(it->point().x()));X->setSizePolicy(QSizePolicy::Preferred,QSizePolicy::Preferred);
        QDoubleSpinBox* Y=new QDoubleSpinBox();Y->setRange(std::numeric_limits<double>().lowest(),std::numeric_limits<double>().max());
        Y->setValue(CGAL::to_double(it->point().y()));Y->setSizePolicy(QSizePolicy::Preferred,QSizePolicy::Preferred);
        QDoubleSpinBox* Height=new QDoubleSpinBox();Height->setRange(std::numeric_limits<double>().lowest(),std::numeric_limits<double>().max());
        Height->setValue(CGAL::to_double(it->data().height));Height->setSizePolicy(QSizePolicy::Preferred,QSizePolicy::Preferred);
        QSpinBox* Colour = new QSpinBox;Colour->setRange(0,std::numeric_limits<int>().max());
        Colour->setValue(it->data().triangulation_index);Colour->setSizePolicy(QSizePolicy::Preferred,QSizePolicy::Preferred);
        QDoubleSpinBox* ScalingFactor = new QDoubleSpinBox();ScalingFactor->setRange(-1,1);
        ScalingFactor->setValue(CGAL::to_double(it->data().s));ScalingFactor->setSizePolicy(QSizePolicy::Preferred,QSizePolicy::Preferred);
        Index->hide();X->hide();Y->hide();Height->hide();Colour->hide();ScalingFactor->hide();
        SceneManager::uiPointsJ.push_back(std::make_tuple(Index,X,Y,Height,Colour,ScalingFactor,it));
        //singals
        QObject::connect(X,SIGNAL(valueChanged(double)),UIHandler::instance,SLOT(setVerticesX(double)));
        QObject::connect(Y,SIGNAL(valueChanged(double)),UIHandler::instance,SLOT(setVerticesY(double)));
        QObject::connect(Height,SIGNAL(valueChanged(double)),UIHandler::instance,SLOT(setVerticesHeights(double)));
        QObject::connect(Colour,SIGNAL(valueChanged(int)),UIHandler::instance,SLOT(setVerticesColour(int)));
        QObject::connect(ScalingFactor,SIGNAL(valueChanged(double)),UIHandler::instance,SLOT(setVerticesScalingFactor(double)));
    }
    std::sort(uiPointsJ.begin(),uiPointsJ.end(),[](auto i,auto j){if(std::get<6>(i)->point().y()==std::get<6>(j)->point().y())
            return std::get<6>(i)->point().x()<std::get<6>(j)->point().x();return std::get<6>(i)->point().y()<std::get<6>(j)->point().y();});
    for(pointList::iterator it=uiPointsJ.begin();it!=uiPointsJ.end();it++){
        //Index
         std::get<0>(*it)->setText(QString((std::to_string(std::distance(uiPointsJ.begin(),it))).c_str()));
         QFrame* IndexFrame=new QFrame();
         QHBoxLayout *IndexLayout = new QHBoxLayout;
         IndexLayout->addWidget(std::get<0>(*it));
         IndexFrame->setLayout(IndexLayout);
         IndexFrame->setMaximumHeight(24);IndexFrame->setMinimumHeight(24);IndexFrame->setMinimumWidth(40);IndexFrame->layout()->setSpacing(0);
         IndexFrame->layout()->setContentsMargins(0,0,0,0);;
         UIHandler::gui.verticesIndexingFrame->layout()->addWidget(IndexFrame);
         //X
         QFrame* XFrame=new QFrame();
         QHBoxLayout *XLayout = new QHBoxLayout;
         XLayout->addWidget(std::get<1>(*it));
         XFrame->setLayout(XLayout);
         XFrame->setMaximumHeight(24);XFrame->setMinimumHeight(24);XFrame->setMinimumWidth(40);XFrame->layout()->setSpacing(0);
         XFrame->layout()->setContentsMargins(0,0,0,0);
         UIHandler::gui.verticesXFrame->layout()->addWidget(XFrame);
         //Y
         QFrame* YFrame=new QFrame();
         QHBoxLayout *YLayout = new QHBoxLayout;
         YLayout->addWidget(std::get<2>(*it));
         YFrame->setLayout(YLayout);
         YFrame->setMaximumHeight(24);YFrame->setMinimumHeight(24);YFrame->setMinimumWidth(40);YFrame->layout()->setSpacing(0);
         YFrame->layout()->setContentsMargins(0,0,0,0);
         UIHandler::gui.verticesYFrame->layout()->addWidget(YFrame);
         //Height
         QFrame* HeightFrame=new QFrame();
         QHBoxLayout *HeightLayout = new QHBoxLayout;
         HeightLayout->addWidget(std::get<3>(*it));
         HeightFrame->setLayout(HeightLayout);
         HeightFrame->setMaximumHeight(24);HeightFrame->setMinimumHeight(24);HeightFrame->setMinimumWidth(40);HeightFrame->layout()->setSpacing(0);
         HeightFrame->layout()->setContentsMargins(0,0,0,0);
         UIHandler::gui.verticesHeightFrame->layout()->addWidget(HeightFrame);
         //Color
         QFrame* ColourFrame=new QFrame();
         QHBoxLayout *ColourLayout = new QHBoxLayout;
         ColourLayout->addWidget(std::get<4>(*it));
         ColourFrame->setLayout(ColourLayout);
         ColourFrame->setMaximumHeight(24);ColourFrame->setMinimumHeight(24);ColourFrame->setMinimumWidth(40);ColourFrame->layout()->setSpacing(0);
         ColourFrame->layout()->setContentsMargins(0,0,0,0);
         UIHandler::gui.verticesColourFrame->layout()->addWidget(ColourFrame);
         //Scaling Factor
         QFrame* SFFrame=new QFrame();
         QHBoxLayout *SFLayout = new QHBoxLayout;
         SFLayout->addWidget(std::get<5>(*it));
         SFFrame->setLayout(SFLayout);
         SFFrame->setMaximumHeight(24);SFFrame->setMinimumHeight(24);SFFrame->setMinimumWidth(40);SFFrame->layout()->setSpacing(0);
         SFFrame->layout()->setContentsMargins(0,0,0,0);
         UIHandler::gui.verticesScalingFactorFrame->layout()->addWidget(SFFrame);
         IndexFrame->hide();XFrame->hide();YFrame->hide();HeightFrame->hide();ColourFrame->hide();SFFrame->hide();
    }
    SceneManager::uiPoints=&SceneManager::uiPointsI;
    if(I.number_of_vertices()>0){
        UIHandler::gui.verticesIndexingFrame->show();
        UIHandler::gui.verticesXFrame->show();
        UIHandler::gui.verticesYFrame->show();
        UIHandler::gui.verticesHeightFrame->show();
        UIHandler::gui.verticesColourFrame->show();
        UIHandler::gui.verticesScalingFactorFrame->show();
    }
    //update the list of faces
    //I
    for(Arrangement2::Face_iterator it=I.faces_begin();it!=I.faces_end();it++){
        if(it->is_unbounded())continue;
        //Index
         QCheckBox* Index= new QCheckBox(); Index->setText(QString((std::to_string(std::distance(I.faces_begin(),it))).c_str()));
         Index->setSizePolicy(QSizePolicy::Preferred,QSizePolicy::Preferred);
         Index->setText(QString((std::to_string(std::distance(I.faces_begin(),it))).c_str()));
         QFrame* IndexFrame=new QFrame();
         QHBoxLayout *IndexLayout = new QHBoxLayout;
         IndexLayout->addWidget(Index);
         IndexFrame->setLayout(IndexLayout);
         IndexFrame->setMaximumHeight(24);IndexFrame->setMinimumHeight(24);IndexFrame->setMinimumWidth(40);IndexFrame->layout()->setSpacing(0);
         IndexFrame->layout()->setContentsMargins(0,0,0,0);
         UIHandler::gui.facesIndexingFrame->layout()->addWidget(IndexFrame);
        //VI
        QTextBrowser* VerticesIndices= new QTextBrowser();VerticesIndices->setSizePolicy(QSizePolicy::Preferred,QSizePolicy::Preferred);
        Arrangement2::Ccb_halfedge_circulator hc=it->outer_ccb(),end(hc);
        QString text;
        pointList::iterator result;
        do{
            if((result= std::find_if(SceneManager::uiPointsI.begin(),SceneManager::uiPointsI.end(),
                                     [hc](auto i){return std::get<6>(i)==hc->target();}))!= SceneManager::uiPointsI.end()){
                text+=QString::number(std::distance(uiPointsI.begin(),result));
                text+=",";
            }
        }while(++hc!=end);
        if(!text.isEmpty())text.chop(1);
        VerticesIndices->setText(text);
        VerticesIndices->setSizePolicy(QSizePolicy::Preferred,QSizePolicy::Preferred);
        QFrame* VIFrame=new QFrame();
        QHBoxLayout *VILayout = new QHBoxLayout;
        VILayout->addWidget(VerticesIndices);
        VIFrame->setLayout(VILayout);
        VIFrame->setMaximumHeight(24);VIFrame->setMinimumHeight(24);VIFrame->setMinimumWidth(40);VIFrame->layout()->setSpacing(0);
        VIFrame->layout()->setContentsMargins(0,0,0,0);
        UIHandler::gui.facesVerticeIndexingFrame->layout()->addWidget(VIFrame);
        //DI
        QSpinBox* DomainIndex= new QSpinBox();DomainIndex->setRange(0,std::numeric_limits<int>().max());
        DomainIndex->setValue(CGAL::to_double(it->data().index));DomainIndex->setSizePolicy(QSizePolicy::Preferred,QSizePolicy::Preferred);
        QFrame* DIFrame=new QFrame();
        QHBoxLayout *DILayout = new QHBoxLayout;
        DILayout->addWidget(DomainIndex);
        DIFrame->setLayout(DILayout);
        DIFrame->setMaximumHeight(24);DIFrame->setMinimumHeight(24);DIFrame->setMinimumWidth(40);DIFrame->layout()->setSpacing(0);
        DIFrame->layout()->setContentsMargins(0,0,0,0);
        UIHandler::gui.facesDomainIndexFrame->layout()->addWidget(DIFrame);
        //SF
        QDoubleSpinBox* ScalingFactor = new QDoubleSpinBox(); ScalingFactor->setRange(-1,1);
        ScalingFactor->setValue(CGAL::to_double(it->data().s));ScalingFactor->setSizePolicy(QSizePolicy::Preferred,QSizePolicy::Preferred);
        QFrame* SFFrame=new QFrame();
        QHBoxLayout *SFLayout = new QHBoxLayout;
        SFLayout->addWidget(ScalingFactor);
        SFFrame->setLayout(SFLayout);
        SFFrame->setMaximumHeight(24);SFFrame->setMinimumHeight(24);SFFrame->setMinimumWidth(40);SFFrame->layout()->setSpacing(0);
        SFFrame->layout()->setContentsMargins(0,0,0,0);
        UIHandler::gui.facesScalingFactorFrame->layout()->addWidget(SFFrame);
        SceneManager::uiFacesI.push_back(std::make_tuple(Index,VerticesIndices,DomainIndex,ScalingFactor,it));
        IndexFrame->hide();VIFrame->hide();DIFrame->hide();SFFrame->hide();
        //signals
        QObject::connect(DomainIndex,SIGNAL(valueChanged(int)),UIHandler::instance,SLOT(setFacesDomainIndex(int)));
        QObject::connect(ScalingFactor,SIGNAL(valueChanged(double)),UIHandler::instance,SLOT(setFacesScalingFactor(double)));
    }
    //J
    for(Arrangement2::Face_iterator it=J.faces_begin();it!=J.faces_end();it++){
        if(it->is_unbounded())continue;
        //Index
         QCheckBox* Index= new QCheckBox(); Index->setText(QString((std::to_string(std::distance(J.faces_begin(),it))).c_str()));
         Index->setSizePolicy(QSizePolicy::Preferred,QSizePolicy::Preferred);
         Index->setText(QString((std::to_string(std::distance(J.faces_begin(),it))).c_str()));
         QFrame* IndexFrame=new QFrame();
         QHBoxLayout *IndexLayout = new QHBoxLayout;
         IndexLayout->addWidget(Index);
         IndexFrame->setLayout(IndexLayout);
         IndexFrame->setMaximumHeight(24);IndexFrame->setMinimumHeight(24);IndexFrame->setMinimumWidth(40);IndexFrame->layout()->setSpacing(0);
         IndexFrame->layout()->setContentsMargins(0,0,0,0);
         UIHandler::gui.facesIndexingFrame->layout()->addWidget(IndexFrame);
        //VI
        QTextBrowser* VerticesIndices= new QTextBrowser();VerticesIndices->setSizePolicy(QSizePolicy::Preferred,QSizePolicy::Preferred);
        Arrangement2::Ccb_halfedge_circulator hc=it->outer_ccb(),end(hc);
        QString text;
        pointList::iterator result;
        do{
            if((result= std::find_if(SceneManager::uiPointsJ.begin(),SceneManager::uiPointsJ.end(),
                                     [hc](auto i){return std::get<6>(i)==hc->target();}))!= SceneManager::uiPointsJ.end()){
                text+=QString::number(std::distance(uiPointsJ.begin(),result));
                text+=",";
            }
        }while(++hc!=end);
        if(!text.isEmpty())text.chop(1);
        VerticesIndices->setText(text);
        VerticesIndices->setSizePolicy(QSizePolicy::Preferred,QSizePolicy::Preferred);
        QFrame* VIFrame=new QFrame();
        QHBoxLayout *VILayout = new QHBoxLayout;
        VILayout->addWidget(VerticesIndices);
        VIFrame->setLayout(VILayout);
        VIFrame->setMaximumHeight(24);VIFrame->setMinimumHeight(24);VIFrame->setMinimumWidth(40);VIFrame->layout()->setSpacing(0);
        VIFrame->layout()->setContentsMargins(0,0,0,0);
        UIHandler::gui.facesVerticeIndexingFrame->layout()->addWidget(VIFrame);
        //DI
        QSpinBox* DomainIndex= new QSpinBox();DomainIndex->setRange(0,std::numeric_limits<int>().max());
        DomainIndex->setValue(CGAL::to_double(it->data().index));DomainIndex->setSizePolicy(QSizePolicy::Preferred,QSizePolicy::Preferred);
        QFrame* DIFrame=new QFrame();
        QHBoxLayout *DILayout = new QHBoxLayout;
        DILayout->addWidget(DomainIndex);
        DIFrame->setLayout(DILayout);
        DIFrame->setMaximumHeight(24);DIFrame->setMinimumHeight(24);DIFrame->setMinimumWidth(40);DIFrame->layout()->setSpacing(0);
        DIFrame->layout()->setContentsMargins(0,0,0,0);
        UIHandler::gui.facesDomainIndexFrame->layout()->addWidget(DIFrame);
        //SF
        QDoubleSpinBox* ScalingFactor = new QDoubleSpinBox(); ScalingFactor->setRange(-1,1);
        ScalingFactor->setValue(CGAL::to_double(it->data().s));ScalingFactor->setSizePolicy(QSizePolicy::Preferred,QSizePolicy::Preferred);
        QFrame* SFFrame=new QFrame();
        QHBoxLayout *SFLayout = new QHBoxLayout;
        SFLayout->addWidget(ScalingFactor);
        SFFrame->setLayout(SFLayout);
        SFFrame->setMaximumHeight(24);SFFrame->setMinimumHeight(24);SFFrame->setMinimumWidth(40);SFFrame->layout()->setSpacing(0);
        SFFrame->layout()->setContentsMargins(0,0,0,0);
        UIHandler::gui.facesScalingFactorFrame->layout()->addWidget(SFFrame);
        SceneManager::uiFacesJ.push_back(std::make_tuple(Index,VerticesIndices,DomainIndex,ScalingFactor,it));
        IndexFrame->hide();VIFrame->hide();DIFrame->hide();SFFrame->hide();
        //signals
        QObject::connect(DomainIndex,SIGNAL(valueChanged(int)),UIHandler::instance,SLOT(setFacesDomainIndex(int)));
        QObject::connect(ScalingFactor,SIGNAL(valueChanged(double)),UIHandler::instance,SLOT(setFacesScalingFactor(double)));
    }
    SceneManager::uiFaces=&SceneManager::uiFacesI;
    if(I.number_of_faces()>0){
        UIHandler::gui.facesIndexingFrame->show();
        UIHandler::gui.facesVerticeIndexingFrame->show();
        UIHandler::gui.facesDomainIndexFrame->show();
        UIHandler::gui.facesScalingFactorFrame->show();
    }
    SceneManager::A=&SceneManager::I;
    SceneManager::refreshMeshDataList();
    //sampling
    //@TODO when sampling is impelemented//
}

void SceneManager::setToolBarFileSaveMesh(){
    QString filename = QFileDialog::getExistingDirectory();
    QFileInfo fileinfo(filename);
    std::string filenameI= (filename+"/"+fileinfo.baseName()+"."+fileinfo.suffix()+"I").toUtf8().constData();
    std::string filenameJ= (filename+"/"+fileinfo.baseName()+"."+fileinfo.suffix()+"J").toUtf8().constData();
    FileIO::Write(I,filenameI);
    FileIO::Write(J,filenameJ);
}

void SceneManager::setToolBarFileLoadIFSRIFS(){
    QString filename = QFileDialog::getOpenFileName();
    std::string std_filename=filename.toUtf8().constData();
    RAFIS* afis=new RAFIS();
    FileIO::Read(*afis,std_filename);
    std::cout<<"siz is "<<afis->points.size()<<std::endl;
    SceneManager::fio=afis;
    //update the gui buttons
    //iteration
    UIHandler::gui.deterministicButton->setEnabled(true);
    UIHandler::gui.stochasticButton->setEnabled(true);
    UIHandler::gui.IFSButton->setEnabled(true);
    UIHandler::gui.RIFSButton->setEnabled(true);
    UIHandler::gui.iterateButton->setEnabled(true);
    UIHandler::gui.clearButton->setEnabled(true);
    //visualization
    UIHandler::gui.drawHeightValuesButton->setEnabled(true);
    UIHandler::gui.drawGammaValues->setEnabled(true);
    UIHandler::gui.meshButton->setEnabled(true);
    UIHandler::gui.originalButton->setChecked(true);
    UIHandler::gui.reconstructedButton->setEnabled(false);
    UIHandler::gui.meshButton->setChecked(true);
    UIHandler::gui.domainsButton->setEnabled(true);
    UIHandler::gui.intervalsButton->setEnabled(true);
    UIHandler::gui.intervalsButton->setChecked(true);
    if(SceneManager::surfaceRender!=NULL){
        UIHandler::gui.heightsButton->setEnabled(true);
        UIHandler::gui.gammaButton->setEnabled(true);
    }
    //update the ui
    UIHandler::gui.reconstructedButton->setEnabled(true);
    UIHandler::gui.reconstructedButton->setChecked(true);
    UIHandler::gui.drawFloorLinesButton->setEnabled(true);
    UIHandler::gui.drawFrontBackButton->setEnabled(true);
    UIHandler::gui.drawWireframeButton->setEnabled(true);
    UIHandler::gui.surfaceButton->setEnabled(true);
    UIHandler::gui.RIFSButton->setEnabled(false);
    UIHandler::gui.IFSButton->setEnabled(false);
    UIHandler::gui.heightsButton->setEnabled(false);
    UIHandler::gui.gammaButton->setEnabled(false);
    UIHandler::gui.CollinearFrameButton->setEnabled(false);
}

void SceneManager::setToolBarExportIFSRIFS(){
    if(I.number_of_faces()==1)return;
    QString filename = QFileDialog::getSaveFileName(UIHandler::gui.centralWidget, QString("Save File"),
                                                    QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation),
                                                     QString("Text (*.txt)"));

    Arrangement2 tempI(I),tempJ(J);
    if(UIHandler::gui.CollinearFrameButton->isChecked())
        Mesh::addCollinearFrame(tempI,tempJ);
    if(UIHandler::gui.IFSButton->isChecked()){
        if(tempI.pft==Arrangement2::PrimitiveFaceType::TRIANGULAR){
            AFIS local_fio(tempI,tempJ);
            FileIO::Write(local_fio,filename.toUtf8().constData());
        }
        else if(tempI.pft==Arrangement2::PrimitiveFaceType::RECTANGULAR){
            BFIS local_fio(tempI,tempJ);
            FileIO::Write(local_fio,filename.toUtf8().constData());
        }
    }
    else if(UIHandler::gui.RIFSButton->isChecked()){
        if(tempI.pft==Arrangement2::PrimitiveFaceType::TRIANGULAR){
            RAFIS local_fio(tempI,tempJ);
            FileIO::Write(local_fio,filename.toUtf8().constData());
        }
        else if(tempI.pft==Arrangement2::PrimitiveFaceType::RECTANGULAR){
            RBFIS local_fio(tempI,tempJ);
            FileIO::Write(local_fio,filename.toUtf8().constData());
        }
    }
}

void SceneManager::setToolBarFileExportSurface(){
    QString filename = QFileDialog::getSaveFileName(UIHandler::gui.centralWidget, QString("Save File"),
                                                    QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation),
                                                     QString("Text (*.txt)"));
    FileIO::Write(SceneManager::reconstructedSurface,filename.toUtf8().constData());
}
