#ifndef SCENEMANAGER_H
#define SCENEMANAGER_H
#include "mainwindow.h"
#include "qscenerender.h"
#include "SurfaceRender.h"
#include "MeshRender.h"
#include "FractalInterpolationObject.h"
#include <QTextBrowser>
#include <QCheckBox>
#include <QDoubleSpinBox>
#include <QSpinBox>
//<Index,X,Y,Height,Color,VSF,Vertex_handle>
typedef std::vector<std::tuple<QCheckBox*,QDoubleSpinBox*,QDoubleSpinBox*,QDoubleSpinBox*,QSpinBox*,QDoubleSpinBox*,Arrangement2::Vertex_handle>> pointList;
//Index,Vertices(indexed),Domain Index,VSF,Face_handle
typedef std::vector<std::tuple<QCheckBox*,QTextBrowser*,QSpinBox*,QDoubleSpinBox*,Arrangement2::Face_handle>> faceList;

class SceneManager
{
private:
    //rendering components
    static QSceneRender*scene;
    static SurfaceRender* originalSurfaceRender;
    static SurfaceRender* reconstructedSurfaceRender;
    static SurfaceRender* surfaceRender;
    static MeshRender* meshRenderI;
    static MeshRender* meshRenderJ;
    static MeshRender* meshRender;
    //interpolation components
    static Arrangement2 I,J;
    static std::vector<Point> surface;
    static std::vector<std::vector<Point>> reconstructedSurface;
    static FIO<Point>* fio;
    //user interaction components
    static Arrangement2* A;
    static pointList uiPointsI;
    static pointList uiPointsJ;
    static pointList* uiPoints;
    static faceList uiFacesI;
    static faceList uiFacesJ;
    static faceList *uiFaces;
    //file IO components
    static QString meshWorkingDirectory;
public:
    SceneManager();
    static void setSceneRender(QSceneRender* sr);
    //tools
    //refresh
    static void refreshMeshRender();
    static void refreshRenders();
    static void refreshMeshDataList();
    static void refreshFacesDataList(pointList* plist=SceneManager::uiPoints,faceList* flist=SceneManager::uiFaces,Arrangement2* arr=SceneManager::A);
    //clean
    static void cleanMeshDataList();
    //visualization
    ///draw parameters
    static void setVisualizationAxis(bool);
    static void setVisualizationHeights(bool);
    static void setVisualizationGammas(bool);
    static void setVisualizationLines(bool);
    static void setVisualizationWireframe(bool);
    static void setVisualizationFill(bool);
    ///draw item parameters
    static void setVisualizationMesh(bool);
    static void setVisualizationSurface(bool);
    static void setVisualizationReconstructed(bool);
    static void setVisualizationOriginal(bool);
    static void setVisualizationDomains(bool);
    static void setVisualizationIntervals(bool);
    ///action buttons
    static void setVisualizationResetPosition();
    static void setVisualizationPrint();
    //interpolation
    ///action buttons
    static void setInterpolationIterate();
    static void setInterpolationClear();
    //sampling
    static void setSamplingHeights();
    static void setVerticalScalingVector();
    static void setSamplingResize();
    //vertices
    static void setVerticesAll();
    static void setVerticesNone();
    static void setVerticesClear();
    static void setVerticesAdd();
    static void setVerticesRemove();
    static void setVerticesX(QDoubleSpinBox*,double);
    static void setVerticesY(QDoubleSpinBox*,double);
    static void setVerticesHeight(QDoubleSpinBox*,double);
    static void setVerticesColour(QSpinBox*,int);
    static void setVerticesScalingFactor(QDoubleSpinBox*,double);
    //faces
    static void setFacesAll();
    static void setFacesNone();
    static void setFacesClear();
    static void setFacesDomainIndex(QSpinBox*,int);
    static void setFacesScalingFactor(QDoubleSpinBox*,double);
    //mesh
    static void setMeshVerticalScalingSingle(bool);
    static void setMeshVerticalScalingFaces(bool);
    static void setMeshVerticalScalingVertices(bool);
    static void setMeshVerticalScalingFactor(double);
    static void setMeshDomainIndexingVertexColoringDomainIndices(bool);
    static void setMeshDomainIndexingVertexColoringVertexColours(bool);
    static void setMeshClear();
    static void setMeshQuadify();
    static void setMeshTriangulate();
    //toolbar
    static void setToolBarFileLoadSurface();
    static void setToolBarFileSaveMesh();
    static void setToolBarFileLoadMesh();
    static void setToolBarFileLoadIFSRIFS();
    static void setToolBarExportIFSRIFS();
    static void setToolBarFileExportSurface();
};

#endif // SCENEMANAGER_H
