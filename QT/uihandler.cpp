#include "uihandler.h"
#include "scenemanager.h"

Ui::MainWindow UIHandler::gui;

UIHandler* UIHandler::instance=NULL;

UIHandler::UIHandler(){

}

UIHandler::UIHandler(Ui::MainWindow& ui){
    instance=this;
    gui=ui;
    //Interpolation
    ///radio buttons
    ///click buttons
    QObject::connect(ui.iterateButton,&QRadioButton::clicked,this,&UIHandler::setInterpolationIterate);
    QObject::connect(ui.clearButton,&QRadioButton::clicked,this,&UIHandler::setInterpolationClear);
    //Visualization
    ///radio buttons
    QObject::connect(ui.originalButton,&QRadioButton::toggled,this,&UIHandler::setVisualizationOriginal);
    QObject::connect(ui.reconstructedButton,&QRadioButton::toggled,this,&UIHandler::setVisualizationReconstructed);
    QObject::connect(ui.domainsButton,&QRadioButton::toggled,this,&UIHandler::setVisualizationDomains);
    QObject::connect(ui.intervalsButton,&QRadioButton::toggled,this,&UIHandler::setVisualizationIntervals);
    ///checkbox buttons
    QObject::connect(ui.surfaceButton,&QCheckBox::stateChanged,this,&UIHandler::setVisualizationSurface);
    QObject::connect(ui.meshButton,&QCheckBox::stateChanged,this,&UIHandler::setVisualizationMesh);
    QObject::connect(ui.drawAxisButton,&QCheckBox::stateChanged,this,&UIHandler::setVisualizationAxis);
    QObject::connect(ui.drawHeightValuesButton,&QCheckBox::stateChanged,this,&UIHandler::setVisualizationHeights);
    QObject::connect(ui.drawGammaValues,&QCheckBox::stateChanged,this,&UIHandler::setVisualizationGammas);
    QObject::connect(ui.drawFloorLinesButton,&QCheckBox::stateChanged,this,&UIHandler::setVisualizationLines);
    QObject::connect(ui.drawWireframeButton,&QCheckBox::stateChanged,this,&UIHandler::setVisualizationWireframe);
    QObject::connect(ui.drawFrontBackButton,&QCheckBox::stateChanged,this,&UIHandler::setVisualizationFill);
    ///push buttons
    QObject::connect(ui.resetPositionButton,&QCheckBox::clicked,this,&UIHandler::setVisualizationResetPosition);
    QObject::connect(ui.printButton,&QCheckBox::clicked,this,&UIHandler::setVisualizationPrint);
    //Sampling
    QObject::connect(ui.resizeButton,&QCheckBox::clicked,this,&UIHandler::setSamplingResize);
    QObject::connect(ui.heightsButton,&QCheckBox::clicked,this,&UIHandler::setSamplingHeights);
    QObject::connect(ui.gammaButton,&QCheckBox::clicked,this,&UIHandler::setVerticalScalingVector);
    QObject::connect(ui.MspinBox,SIGNAL(valueChanged(double)),this,SLOT(setMSpinBox(double)));
    QObject::connect(ui.NspinBox,SIGNAL(valueChanged(double)),this,SLOT(setNSpinBox(double)));
    //vertices
    QObject::connect(ui.verticesAllButton,&QCheckBox::clicked,this,&UIHandler::setVerticesAll);
    QObject::connect(ui.verticesNoneButton,&QCheckBox::clicked,this,&UIHandler::setVerticesNone);
    QObject::connect(ui.verticesClearButton,&QCheckBox::clicked,this,&UIHandler::setVerticesClear);
    QObject::connect(ui.verticesAddButton,&QCheckBox::clicked,this,&UIHandler::setVerticesAdd);
    QObject::connect(ui.verticesRemoveButton,&QCheckBox::clicked,this,&UIHandler::setVerticesRemove);
    //faces
    QObject::connect(ui.facesAllButton,&QCheckBox::clicked,this,&UIHandler::setFacesAll);
    QObject::connect(ui.facesNoneButton,&QCheckBox::clicked,this,&UIHandler::setFacesNone);
    QObject::connect(ui.facesClearButton,&QCheckBox::clicked,this,&UIHandler::setFacesClear);
    //mesh
    QObject::connect(ui.meshVerticalScalingSingleRadioButton,&QRadioButton::toggled,this,&UIHandler::setMeshVerticalScalingSingle);
    QObject::connect(ui.meshVerticalScalingFacesRadioButton,&QRadioButton::toggled,this,&UIHandler::setMeshVerticalScalingFaces);
    QObject::connect(ui.meshVerticalScalingVerticesRadioButton,&QRadioButton::toggled,this,&UIHandler::setMeshVerticalScalingVertices);
    QObject::connect(ui.meshVerticalScalingScalingFactorDoubleSpinBox,SIGNAL(valueChanged(double)),this,SLOT(setMeshVerticalScalingFactor(double)));
    QObject::connect(ui.meshDomainIndexingColouringFaceIndicesRadioButton,&QRadioButton::toggled,this,&UIHandler::setMeshUseFaceIndices);
    QObject::connect(ui.meshDomainIndexingColouringColoursRadioButton,&QRadioButton::toggled,this,&UIHandler::setMeshUseVertexColours);
    QObject::connect(ui.meshClearButton,&QCheckBox::clicked,this,&UIHandler::setMeshClear);
    QObject::connect(ui.meshQuadifyButton,&QCheckBox::clicked,this,&UIHandler::setMeshQuadify);
    QObject::connect(ui.meshTriangulateButton,&QCheckBox::clicked,this,&UIHandler::setMeshTriangulate);
    //Menu
    ///File
    ////Mesh
    QObject::connect(ui.actionOpenSurface_2,&QAction::triggered,this,&UIHandler::setToolbarFileLoadSurface);
    ui.actionExport_Mesh->setShortcut(Qt::Key_S | Qt::CTRL);
    QObject::connect(ui.actionExport_Mesh_2,&QAction::triggered,this,&UIHandler::setToolbarFileSaveMesh);
    QObject::connect(ui.actionOpen_Mesh_2,&QAction::triggered,this,&UIHandler::setToolbarFileLoadMesh);
    QObject::connect(ui.actionOpen_Mesh_2,&QAction::triggered,this,&UIHandler::setToolbarFileLoadMesh);
    QObject::connect(ui.actionImport_IFS_RIFS,&QAction::triggered,this,&UIHandler::setToolbarFileLoadIFSRIFS);
    QObject::connect(ui.actionExport_Surface_2,&QAction::triggered,this,&UIHandler::setToolbarFileExportSurface);
}
//interpolation

void UIHandler::setInterpolationIterate(){
    SceneManager::setInterpolationIterate();
}

void UIHandler::setInterpolationClear(){
    SceneManager::setInterpolationClear();
}

//visualization

void UIHandler::setVisualizationReconstructed(){
    SceneManager::setVisualizationReconstructed(true);
    SceneManager::setVisualizationOriginal(false);
}

void UIHandler::setVisualizationOriginal(){
    SceneManager::setVisualizationOriginal(true);
    SceneManager::setVisualizationReconstructed(false);
}

void UIHandler::setVisualizationSurface(int state){
    if(state==Qt::Checked){
        SceneManager::setVisualizationSurface(true);
    }
    else if(state==Qt::Unchecked){
        SceneManager::setVisualizationSurface(false);
    }
}
void UIHandler::setVisualizationMesh(int state){
    if(state==Qt::Checked){
        SceneManager::setVisualizationMesh(true);
    }
    else if(state==Qt::Unchecked){
        SceneManager::setVisualizationMesh(false);
    }
}
void UIHandler::setVisualizationAxis(int state){
    if(state==Qt::Checked){
        SceneManager::setVisualizationAxis(true);
    }
    else if(state==Qt::Unchecked){
        SceneManager::setVisualizationAxis(false);
    }
}
void UIHandler::setVisualizationHeights(int state){
    if(state==Qt::Checked){
        SceneManager::setVisualizationHeights(true);
    }
    else if(state==Qt::Unchecked){
        SceneManager::setVisualizationHeights(false);
    }
}
void UIHandler::setVisualizationGammas(int state){
    if(state==Qt::Checked){
        SceneManager::setVisualizationGammas(true);
    }
    else if(state==Qt::Unchecked){
        SceneManager::setVisualizationGammas(false);
    }

}
void UIHandler::setVisualizationLines(int state){
    if(state==Qt::Checked){
        SceneManager::setVisualizationLines(true);
    }
    else if(state==Qt::Unchecked){
        SceneManager::setVisualizationLines(false);
    }
}

void UIHandler::setVisualizationWireframe(int state){
    if(state==Qt::Checked){
        SceneManager::setVisualizationWireframe(true);
    }
    else if(state==Qt::Unchecked){
        SceneManager::setVisualizationWireframe(false);
    }
}

void UIHandler::setVisualizationFill(int state){
    if(state==Qt::Checked){
        SceneManager::setVisualizationFill(true);
    }
    else if(state==Qt::Unchecked){
        SceneManager::setVisualizationFill(false);
    }
}

void UIHandler::setVisualizationResetPosition(){
    SceneManager::setVisualizationResetPosition();
}

void UIHandler::setVisualizationPrint(){
    SceneManager::setVisualizationPrint();
}

void UIHandler::setVisualizationDomains(){
    SceneManager::setVisualizationDomains(true);
}

void UIHandler::setVisualizationIntervals(){
    SceneManager::setVisualizationIntervals(true);
}

//toolbar
void UIHandler::setToolbarFileLoadSurface(){
    SceneManager::setToolBarFileLoadSurface();
}
void UIHandler::setToolbarFileLoadMesh(){
    SceneManager::setToolBarFileLoadMesh();
}

void UIHandler::setToolbarFileLoadIFSRIFS(){
    SceneManager::setToolBarFileLoadIFSRIFS();
}

void UIHandler::setToolbarFileSaveMesh(){
    SceneManager::setToolBarFileSaveMesh();
}

void UIHandler::setToolbarFileExportIFSRIFS(){
    SceneManager::setToolBarExportIFSRIFS();
}

void UIHandler::setToolbarFileExportSurface(){
    SceneManager::setToolBarFileExportSurface();
}

//sampling
void UIHandler::setSamplingHeights(){
    SceneManager::setSamplingHeights();
}

void UIHandler::setVerticalScalingVector(){
    SceneManager::setVerticalScalingVector();
}

void UIHandler::setMSpinBox(double M){
    //@TODO
}

void UIHandler::setNSpinBox(double N){
    //@TODO
}

void UIHandler::setSamplingResize(){
    SceneManager::setSamplingResize();
}

//vertices

void UIHandler::setVerticesAll(){
    SceneManager::setVerticesAll();
}

void UIHandler::setVerticesNone(){
    SceneManager::setVerticesNone();
}

void UIHandler::setVerticesClear(){
    SceneManager::setVerticesClear();
}

void UIHandler::setVerticesAdd(){
    SceneManager::setVerticesAdd();
}

void UIHandler::setVerticesRemove(){
    SceneManager::setVerticesRemove();
}

void UIHandler::setVerticesX(double d){
    QDoubleSpinBox* dsbp=qobject_cast<QDoubleSpinBox*>(sender());
    SceneManager::setVerticesX(dsbp,d);
}

void UIHandler::setVerticesY(double d){
    QDoubleSpinBox* dsbp=qobject_cast<QDoubleSpinBox*>(sender());
    SceneManager::setVerticesY(dsbp,d);
}

void UIHandler::setVerticesHeights(double d){
    QDoubleSpinBox* dsbp=qobject_cast<QDoubleSpinBox*>(sender());
    SceneManager::setVerticesHeight(dsbp,d);
}

void UIHandler::setVerticesColour(int i){
    QSpinBox* dsbp=qobject_cast<QSpinBox*>(sender());
    SceneManager::setVerticesColour(dsbp,i);
}

void UIHandler::setVerticesScalingFactor(double d){
    QDoubleSpinBox* dsbp=qobject_cast<QDoubleSpinBox*>(sender());
    SceneManager::setVerticesScalingFactor(dsbp,d);
}

//faces

void UIHandler::setFacesAll(){
    SceneManager::setFacesAll();
}

void UIHandler::setFacesNone(){
    SceneManager::setFacesNone();
}

void UIHandler::setFacesClear(){
    SceneManager::setFacesClear();
}

void UIHandler::setFacesDomainIndex(int i){
    QSpinBox* dsbp=qobject_cast<QSpinBox*>(sender());
    SceneManager::setFacesDomainIndex(dsbp,i);
}

void UIHandler::setFacesScalingFactor(double d){
    QDoubleSpinBox* dsbp=qobject_cast<QDoubleSpinBox*>(sender());
    SceneManager::setFacesScalingFactor(dsbp,d);
}

//mesh

void UIHandler::setMeshVerticalScalingSingle(bool b){
    SceneManager::setMeshVerticalScalingSingle(b);
}

void UIHandler::setMeshVerticalScalingFaces(bool b){
    SceneManager::setMeshVerticalScalingFaces(b);
}

void UIHandler::setMeshVerticalScalingVertices(bool b){
    SceneManager::setMeshVerticalScalingVertices(b);
}

void UIHandler::setMeshVerticalScalingFactor(double d){
    SceneManager::setMeshVerticalScalingFactor(d);
}

void UIHandler::setMeshUseFaceIndices(bool b){
    SceneManager::setMeshDomainIndexingVertexColoringDomainIndices(b);
}

void UIHandler::setMeshUseVertexColours(bool b){
    SceneManager::setMeshDomainIndexingVertexColoringVertexColours(b);
}

void UIHandler::setMeshClear(){
    SceneManager::setMeshClear();
}

void UIHandler::setMeshQuadify(){
    SceneManager::setMeshQuadify();
}

void UIHandler::setMeshTriangulate(){
    SceneManager::setMeshTriangulate();
}
