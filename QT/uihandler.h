
#ifndef UIHANDLER_H
#define UIHANDLER_H

#include <QObject>
#include "ui_mainwindow.h"
#include "scenemanager.h"


class UIHandler:public QObject
{
    Q_OBJECT
public:
    UIHandler();
    UIHandler(Ui::MainWindow& ui);
    static Ui::MainWindow gui;
    static UIHandler* instance;
public slots:
    //interoplation parameters
    void setInterpolationIterate();
    void setInterpolationClear();
    //visualization parameters
    void setVisualizationSurface(int state);
    void setVisualizationMesh(int state);
    void setVisualizationAxis(int state);
    void setVisualizationHeights(int state);
    void setVisualizationGammas(int state);
    void setVisualizationLines(int state);
    void setVisualizationWireframe(int state);
    void setVisualizationFill(int state);
    void setVisualizationResetPosition();
    void setVisualizationPrint();
    void setVisualizationReconstructed();
    void setVisualizationOriginal();
    void setVisualizationDomains();
    void setVisualizationIntervals();
    //sampling parameters
    void setSamplingHeights();
    void setVerticalScalingVector();
    void setMSpinBox(double);
    void setNSpinBox(double);
    void setSamplingResize();
    //vertices
    void setVerticesAll();
    void setVerticesNone();
    void setVerticesClear();
    void setVerticesAdd();
    void setVerticesRemove();
    void setVerticesX(double);
    void setVerticesY(double);
    void setVerticesHeights(double);
    void setVerticesColour(int);
    void setVerticesScalingFactor(double);
    //faces
    void setFacesAll();
    void setFacesNone();
    void setFacesClear();
    void setFacesDomainIndex(int);
    void setFacesScalingFactor(double);
    //mesh
    void setMeshVerticalScalingSingle(bool);
    void setMeshVerticalScalingFaces(bool);
    void setMeshVerticalScalingVertices(bool);
    void setMeshVerticalScalingFactor(double);
    void setMeshUseFaceIndices(bool);
    void setMeshUseVertexColours(bool);
    void setMeshClear();
    void setMeshQuadify();
    void setMeshTriangulate();
    //toolbar
    //file
    void setToolbarFileLoadSurface();
    void setToolbarFileLoadIFSRIFS();
    void setToolbarFileLoadMesh();
    void setToolbarFileSaveMesh();
    void setToolbarFileExportIFSRIFS();
    void setToolbarFileExportSurface();

};

#endif // UIHANDLER_H
