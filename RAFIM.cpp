//
//  RFIM.cpp
//  OpenGL_CGAL
//
//  Created by Sean Dillon on 3/10/16.
//  Copyright (c) 2016 Sean Dillon. All rights reserved.
//

#include "RAFIM.h"
#include "RIFS.h"
#include <iterator>

RAFIM::RAFIM(Arrangement2& mesh,Arrangement2& globalArrangement){
    ExacttoK exactToK;
    Arrangement2::Vertex_handle globals[3];
    Arrangement2::Vertex_handle locals[3];
    std::vector<Arrangement2::Vertex_handle> Globals;
    //get the globals in a vector and sort them based on their triangulation index
    for(Arrangement2::Vertex_iterator it=globalArrangement.vertices_begin();it!=globalArrangement.vertices_end();it++){
        Globals.push_back(it);
    }
    //sorting and deduplication
    std::sort(Globals.begin(),Globals.end(),[](auto i,auto j){return i->data().triangulation_index<j->data().triangulation_index;});
    Globals.resize(std::distance(Globals.begin(),std::unique(Globals.begin(),Globals.end(),
                                                             [](auto i,auto j){return i->data().triangulation_index==j->data().triangulation_index;})));
    //create the triangulations
    std::vector<Point> temp;
    for(Arrangement2::Face_iterator it=mesh.faces_begin();it!=mesh.faces_end();it++){
        if(it->is_unbounded())continue;
        Arrangement2::Ccb_halfedge_circulator hc = it->outer_ccb();
        locals[0]=hc->target();//Point2(hc->target()->point().x(),hc->target()->point().y());
        //indices[0]=hc->target()->data().triangulation_index;
        globals[0]=Globals[hc->target()->data().triangulation_index];//Point2(Globals[indices[0]]->point().x(),Globals[indices[0]]->point().y());
        hc++;
        locals[1]=hc->target();//Point2(hc->target()->point().x(),hc->target()->point().y());
        //indices[1]=hc->target()->data().triangulation_index;
        globals[1]=Globals[hc->target()->data().triangulation_index];
        hc++;
        locals[2]=hc->target();//Point2(hc->target()->point().x(),hc->target()->point().y());
        globals[2]=Globals[hc->target()->data().triangulation_index];//Point2(Globals[indices[2]]->point().x(),Globals[indices[2]]->point().y());
        //create and add the transformation to the container
        transformations.push_back(Affine2Arrangement(locals,globals));
    }
    //create the connection matrix
    connection_matrix.reserve(transformations.size());
    for(size_t i=0;i<transformations.size();i++){
        connection_matrix.push_back(std::vector<bool>());
        connection_matrix[connection_matrix.size()-1].reserve(transformations.size());
        for(size_t j=0;j<transformations.size();j++){
            //compute the median of the local points
            Point2 median(
                          exactToK((transformations[j].locals[0]->point().x()+transformations[j].locals[1]->point().x()+transformations[j].locals[2]->point().x())/3),
                          exactToK((transformations[j].locals[0]->point().y()+transformations[j].locals[1]->point().y()+transformations[j].locals[2]->point().y())/3));
            
            K::Triangle_2 t(Point2(exactToK(transformations[i].globals[0]->point().x()),exactToK(transformations[i].globals[0]->point().y())),
                            Point2(exactToK(transformations[i].globals[1]->point().x()),exactToK(transformations[i].globals[1]->point().y())),
                            Point2(exactToK(transformations[i].globals[2]->point().x()),exactToK(transformations[i].globals[2]->point().y())));
            if(((t.oriented_side(median)!=CGAL::ON_POSITIVE_SIDE)&&t.orientation()==CGAL::NEGATIVE)||
               ((t.oriented_side(median)!=CGAL::ON_NEGATIVE_SIDE)&&t.orientation()==CGAL::POSITIVE)){
                connection_matrix[i].push_back(true);
            }
            else{
                connection_matrix[i].push_back(false);
            }
        }
    }
    //create the probability matrix
    probability_matrix.reserve(transformations.size());
    for(size_t i=0;i<transformations.size();i++){
        probability_matrix.push_back(std::vector<float>());
        probability_matrix[probability_matrix.size()-1].reserve(transformations.size());
        float accumulator=0.0;
        for(size_t j=0;j<transformations.size();j++){//accumulate the row of the cmatrix
            if(connection_matrix[j][i])accumulator+=1.0f;
        }
        for(size_t j=0;j<transformations.size();j++){//init the values of pmatrix based on cmatric
            if(connection_matrix[j][i])probability_matrix[i].push_back(1.0f/accumulator);
            else probability_matrix[i].push_back(0.0f);
        }
    }
    //create the data vector
    buffer1.reserve(transformations.size());
    buffer2.reserve(transformations.size());
    for(size_t i=0;i<transformations.size();i++){
        buffer1.push_back(std::vector<Arrangement2>());
        buffer2.push_back(std::vector<Arrangement2>());
        Arrangement2 arr;
        Arrangement2::Vertex_handle v1 = arr.insert_in_face_interior(transformations[i].locals[0]->point(),arr.unbounded_face());
        v1->data().height=transformations[i].locals[0]->data().height;
        v1->data().triangulation_index=transformations[i].locals[0]->data().triangulation_index;
        Arrangement2::Vertex_handle v2 = arr.insert_in_face_interior(transformations[i].locals[1]->point(),arr.unbounded_face());
        v2->data().height=transformations[i].locals[1]->data().height;
        v2->data().triangulation_index=transformations[i].locals[1]->data().triangulation_index;
        Arrangement2::Vertex_handle v3 = arr.insert_in_face_interior(transformations[i].locals[2]->point(),arr.unbounded_face());
        v3->data().height=transformations[i].locals[2]->data().height;
        v3->data().triangulation_index=transformations[i].locals[2]->data().triangulation_index;
        arr.insert_at_vertices(Segment2(v1->point(),v2->point()),v1,v2);
        arr.insert_at_vertices(Segment2(v2->point(),v3->point()),v2,v3);
        arr.insert_at_vertices(Segment2(v3->point(),v1->point()),v3,v1);
        buffer1[i].push_back(arr);
    }
    points = std::vector<std::vector<Arrangement2>>(buffer1);
}
