//
//  RFIM.h
//  OpenGL_CGAL
//
//  Created by Sean Dillon on 3/10/16.
//  Copyright (c) 2016 Sean Dillon. All rights reserved.
//

#ifndef __OpenGL_CGAL__RAFIM__
#define __OpenGL_CGAL__RAFIM__
#include "AffineTransformation.h"
#include "KernelConstructions.h"
#include "RFIS.h"
class RAFIM:public RFIS<Arrangement2,Affine2Arrangement>{
public:
    RAFIM(Arrangement2& mesh ,Arrangement2& globals);
};

#endif /* defined(__OpenGL_CGAL__RFIM__) */
