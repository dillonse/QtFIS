//
//  RAFIS.cpp
//  OpenGL_CGAL
//
//  Created by Sean Dillon on 3/16/16.
//  Copyright (c) 2016 Sean Dillon. All rights reserved.
//

#include "RAFIS.h"

RAFIS::RAFIS(Arrangement2& mesh, Arrangement2& globalArrangement){
    ExacttoK exactToK;
    vst=mesh.vst;
    ct=mesh.ct;
    Point globals[3];
    Point locals[3];
    if(ct==Arrangement2::ColoringType::NONE){
        //the mapping from I to J is done via face indices//
        if(vst==Arrangement2::VerticalScalingType::SINGLE){
            K::FT s=mesh.unbounded_face()->data().s;
            Arrangement2::Face_iterator Global;
            for(Arrangement2::Face_iterator it=mesh.faces_begin();it!=mesh.faces_end();it++){
                if(it->is_unbounded())continue;
                //find the global face via indexing
                Global=(std::find_if(globalArrangement.faces_begin(),globalArrangement.faces_end(),[&it](auto i){return i.data().index==it->data().index&&!i.is_unbounded();}));
                if(Global->data().index!=it->data().index){
                    std::cout<<"LOL"<<std::endl;
                }
                Arrangement2::Ccb_halfedge_circulator hc=Global->outer_ccb();
                globals[0]=Point(exactToK(hc->target()->point().x()),exactToK(hc->target()->point().y()),hc->target()->data().height);hc++;
                globals[1]=Point(exactToK(hc->target()->point().x()),exactToK(hc->target()->point().y()),hc->target()->data().height);hc++;
                globals[2]=Point(exactToK(hc->target()->point().x()),exactToK(hc->target()->point().y()),hc->target()->data().height);
                std::sort(globals,globals+3,[](auto i,auto j){if(i.x()==j.x())return i.y() <j.y();else return i.x()<j.x();});
                hc = it->outer_ccb();
                locals[0]=Point(exactToK(hc->target()->point().x()),exactToK(hc->target()->point().y()),hc->target()->data().height);hc++;
                locals[1]=Point(exactToK(hc->target()->point().x()),exactToK(hc->target()->point().y()),hc->target()->data().height);hc++;
                locals[2]=Point(exactToK(hc->target()->point().x()),exactToK(hc->target()->point().y()),hc->target()->data().height);
                //sort the locals same way as the globals
                std::sort(locals,locals+3,[](auto i,auto j){if(i.x()==j.x())return i.y() <j.y();
                    else return i.x()<j.x();});
                //check if the orientation of the 3rd point relative to line from 1st and 2nd point is the same as the globals
                Line2 gl(Point2(globals[0].x(),globals[0].y()),Point2(globals[1].x(),globals[1].y()));
                CGAL::Oriented_side gos=gl.oriented_side(Point2(globals[2].x(),globals[2].y()));
                Line2 ll(Point2(locals[0].x(),locals[0].y()),Point2(locals[1].x(),locals[1].y()));
                CGAL::Oriented_side los=ll.oriented_side(Point2(locals[2].x(),locals[2].y()));
                if(los!=gos)std::reverse(globals,globals+3);
                transformations.push_back(Affine3(locals,globals,s));
                (transformations.end()-1)->vst=vst;(transformations.end()-1)->ct=ct;
            }
        }
        else if(vst==Arrangement2::VerticalScalingType::FACES){
            Arrangement2::Face_handle Global;
            for(Arrangement2::Face_iterator it=mesh.faces_begin();it!=mesh.faces_end();it++){
                if(it->is_unbounded())continue;
                K::FT s = it->data().s;
                //find the global face via indexing
                Global=(std::find_if(globalArrangement.faces_begin(),globalArrangement.faces_end(),[&it](auto i){return i.data().index==it->data().index&&!i.is_unbounded();}));
                Arrangement2::Ccb_halfedge_circulator hc=Global->outer_ccb();
                globals[0]=Point(exactToK(hc->target()->point().x()),exactToK(hc->target()->point().y()),hc->target()->data().height);hc++;
                globals[1]=Point(exactToK(hc->target()->point().x()),exactToK(hc->target()->point().y()),hc->target()->data().height);hc++;
                globals[2]=Point(exactToK(hc->target()->point().x()),exactToK(hc->target()->point().y()),hc->target()->data().height);
                std::sort(globals,globals+3,[](auto i,auto j){if(i.x()==j.x())return i.y() <j.y();else return i.x()<j.x();});
                hc = it->outer_ccb();
                locals[0]=Point(exactToK(hc->target()->point().x()),exactToK(hc->target()->point().y()),hc->target()->data().height);hc++;
                locals[1]=Point(exactToK(hc->target()->point().x()),exactToK(hc->target()->point().y()),hc->target()->data().height);hc++;
                locals[2]=Point(exactToK(hc->target()->point().x()),exactToK(hc->target()->point().y()),hc->target()->data().height);
                //sort the locals same way as the globals
                std::sort(locals,locals+3,[](auto i,auto j){if(i.x()==j.x())return i.y() <j.y();
                    else return i.x()<j.x();});
                //check if the orientation of the 3rd point relative to line from 1st and 2nd point is the same as the globals
                Line2 gl(Point2(globals[0].x(),globals[0].y()),Point2(globals[1].x(),globals[1].y()));
                CGAL::Oriented_side gos=gl.oriented_side(Point2(globals[2].x(),globals[2].y()));
                Line2 ll(Point2(locals[0].x(),locals[0].y()),Point2(locals[1].x(),locals[1].y()));
                CGAL::Oriented_side los=ll.oriented_side(Point2(locals[2].x(),locals[2].y()));
                if(los!=gos)std::reverse(globals,globals+3);
                transformations.push_back(Affine3(locals,globals,s));
                (transformations.end()-1)->vst=vst;(transformations.end()-1)->ct=ct;
            }
        }
        else if(vst==Arrangement2::VerticalScalingType::VERTICES){
            Arrangement2::Face_handle Global;
            K::FT s[3];
            for(Arrangement2::Face_iterator it=mesh.faces_begin();it!=mesh.faces_end();it++){
                if(it->is_unbounded())continue;
                //find the global face via indexing
               Global=(std::find_if(globalArrangement.faces_begin(),globalArrangement.faces_end(),[&it](auto i){return i.data().index==it->data().index&&!i.is_unbounded();}));
                Arrangement2::Ccb_halfedge_circulator hc=Global->outer_ccb();
                globals[0]=Point(exactToK(hc->target()->point().x()),exactToK(hc->target()->point().y()),hc->target()->data().height);hc++;
                globals[1]=Point(exactToK(hc->target()->point().x()),exactToK(hc->target()->point().y()),hc->target()->data().height);hc++;
                globals[2]=Point(exactToK(hc->target()->point().x()),exactToK(hc->target()->point().y()),hc->target()->data().height);
                std::sort(globals,globals+3,[](auto i,auto j){if(i.x()==j.x())return i.y() <j.y();else return i.x()<j.x();});
                hc = it->outer_ccb();
                locals[0]=Point(exactToK(hc->target()->point().x()),exactToK(hc->target()->point().y()),hc->target()->data().height);s[0]=hc->target()->data().s;hc++;
                locals[1]=Point(exactToK(hc->target()->point().x()),exactToK(hc->target()->point().y()),hc->target()->data().height);s[1]=hc->target()->data().s;hc++;
                locals[2]=Point(exactToK(hc->target()->point().x()),exactToK(hc->target()->point().y()),hc->target()->data().height);s[2]=hc->target()->data().s;
                //sort the locals same way as the globals
                std::sort(locals,locals+3,[](auto i,auto j){if(i.x()==j.x())return i.y() <j.y();
                    else return i.x()<j.x();});
                //check if the orientation of the 3rd point relative to line from 1st and 2nd point is the same as the globals
                Line2 gl(Point2(globals[0].x(),globals[0].y()),Point2(globals[1].x(),globals[1].y()));
                CGAL::Oriented_side gos=gl.oriented_side(Point2(globals[2].x(),globals[2].y()));
                Line2 ll(Point2(locals[0].x(),locals[0].y()),Point2(locals[1].x(),locals[1].y()));
                CGAL::Oriented_side los=ll.oriented_side(Point2(locals[2].x(),locals[2].y()));
                if(los!=gos)std::reverse(globals,globals+3);
                transformations.push_back(Affine3(locals,globals,s));
                (transformations.end()-1)->vst=vst;(transformations.end()-1)->ct=ct;
            }

        }
        
    }
    else if(ct==Arrangement2::ColoringType::CONSISTENT){
        //get the globals in a vector
        std::vector<Arrangement2::Vertex_handle> Globals;
        for(Arrangement2::Vertex_iterator it=globalArrangement.vertices_begin();it!=globalArrangement.vertices_end();it++){
            Globals.push_back(it);
        }
        //the mapping from I to J is done via vertice colors//
        int indices[3];
        //sort the globals based on their triangulation index
        std::sort(Globals.begin(),Globals.end(),[](auto i,auto j){return i->data().triangulation_index<j->data().triangulation_index;});
        //Globals.resize(std::distance(Globals.begin(),std::unique(Globals.begin(),Globals.end(),
        //                                                       [](auto i,auto j){return i->data().triangulation_index==j->data().triangulation_index;})));
        if(vst==Arrangement2::VerticalScalingType::SINGLE){
            K::FT s=mesh.unbounded_face()->data().s;
            for(Arrangement2::Face_iterator it=mesh.faces_begin();it!=mesh.faces_end();it++){
                if(it->is_unbounded())continue;
                Arrangement2::Ccb_halfedge_circulator hc = it->outer_ccb();
                locals[0]=Point(exactToK(hc->target()->point().x()),exactToK(hc->target()->point().y()),hc->target()->data().height);
                indices[0]=hc->target()->data().triangulation_index;
                globals[0]=Point(exactToK(Globals[indices[0]]->point().x()),exactToK(Globals[indices[0]]->point().y()),Globals[indices[0]]->data().height);
                hc++;
                locals[1]=Point(exactToK(hc->target()->point().x()),exactToK(hc->target()->point().y()),hc->target()->data().height);
                indices[1]=hc->target()->data().triangulation_index;
                globals[1]=Point(exactToK(Globals[indices[1]]->point().x()),exactToK(Globals[indices[1]]->point().y()),Globals[indices[1]]->data().height);
                hc++;
                locals[2]=Point(exactToK(hc->target()->point().x()),exactToK(hc->target()->point().y()),hc->target()->data().height);
                indices[2]=hc->target()->data().triangulation_index;
                globals[2]=Point(exactToK(Globals[indices[2]]->point().x()),exactToK(Globals[indices[2]]->point().y()),Globals[indices[2]]->data().height);
                //create and add the transformation to the container
                transformations.push_back(Affine3(locals,globals,s));
                (transformations.end()-1)->vst=vst;(transformations.end()-1)->ct=ct;
            }
        }
        else if(vst==Arrangement2::VerticalScalingType::FACES){
            for(Arrangement2::Face_iterator it=mesh.faces_begin();it!=mesh.faces_end();it++){
                if(it->is_unbounded())continue;
                K::FT s=it->data().s;
                Arrangement2::Ccb_halfedge_circulator hc = it->outer_ccb();
                locals[0]=Point(exactToK(hc->target()->point().x()),exactToK(hc->target()->point().y()),hc->target()->data().height);
                indices[0]=hc->target()->data().triangulation_index;
                globals[0]=Point(exactToK(Globals[indices[0]]->point().x()),exactToK(Globals[indices[0]]->point().y()),Globals[indices[0]]->data().height);
                hc++;
                locals[1]=Point(exactToK(hc->target()->point().x()),exactToK(hc->target()->point().y()),hc->target()->data().height);
                indices[1]=hc->target()->data().triangulation_index;
                globals[1]=Point(exactToK(Globals[indices[1]]->point().x()),exactToK(Globals[indices[1]]->point().y()),Globals[indices[1]]->data().height);
                hc++;
                locals[2]=Point(exactToK(hc->target()->point().x()),exactToK(hc->target()->point().y()),hc->target()->data().height);
                indices[2]=hc->target()->data().triangulation_index;
                globals[2]=Point(exactToK(Globals[indices[2]]->point().x()),exactToK(Globals[indices[2]]->point().y()),Globals[indices[2]]->data().height);
                //create and add the transformation to the container
                transformations.push_back(Affine3(locals,globals,s));
                (transformations.end()-1)->vst=vst;(transformations.end()-1)->ct=ct;
            }
            
        }
        else if(vst==Arrangement2::VerticalScalingType::VERTICES){
            K::FT s[3];
            for(Arrangement2::Face_iterator it=mesh.faces_begin();it!=mesh.faces_end();it++){
                if(it->is_unbounded())continue;
                Arrangement2::Ccb_halfedge_circulator hc = it->outer_ccb();
                locals[0]=Point(exactToK(hc->target()->point().x()),exactToK(hc->target()->point().y()),hc->target()->data().height);
                indices[0]=hc->target()->data().triangulation_index;
                globals[0]=Point(exactToK(Globals[indices[0]]->point().x()),exactToK(Globals[indices[0]]->point().y()),Globals[indices[0]]->data().height);
                s[0]=hc->target()->data().s;hc++;
                locals[1]=Point(exactToK(hc->target()->point().x()),exactToK(hc->target()->point().y()),hc->target()->data().height);
                indices[1]=hc->target()->data().triangulation_index;
                globals[1]=Point(exactToK(Globals[indices[1]]->point().x()),exactToK(Globals[indices[1]]->point().y()),Globals[indices[1]]->data().height);
                s[1]=hc->target()->data().s;
                hc++;
                locals[2]=Point(exactToK(hc->target()->point().x()),exactToK(hc->target()->point().y()),hc->target()->data().height);
                indices[2]=hc->target()->data().triangulation_index;
                globals[2]=Point(exactToK(Globals[indices[2]]->point().x()),exactToK(Globals[indices[2]]->point().y()),Globals[indices[2]]->data().height);
                s[2]=hc->target()->data().s;
                //create and add the transformation to the container
                transformations.push_back(Affine3(locals,globals,s));
                (transformations.end()-1)->vst=vst;(transformations.end()-1)->ct=ct;
            }
        }
    }
    //create the connection matrix
    connection_matrix.reserve(transformations.size());
    for(size_t i=0;i<transformations.size();i++){
        connection_matrix.push_back(std::vector<bool>());
        connection_matrix[connection_matrix.size()-1].reserve(transformations.size());
        for(size_t j=0;j<transformations.size();j++){
            //compute the median of the local points
            Point2 median((transformations[j].locals[0].x()+transformations[j].locals[1].x()+transformations[j].locals[2].x())/3,
                          (transformations[j].locals[0].y()+transformations[j].locals[1].y()+transformations[j].locals[2].y())/3);
            K::Triangle_2 t(Point2(transformations[i].globals[0].x(),transformations[i].globals[0].y()),
                            Point2(transformations[i].globals[1].x(),transformations[i].globals[1].y()),
                            Point2(transformations[i].globals[2].x(),transformations[i].globals[2].y()));
            if(((t.oriented_side(median)!=CGAL::ON_POSITIVE_SIDE)&&t.orientation()==CGAL::NEGATIVE)||
               ((t.oriented_side(median)!=CGAL::ON_NEGATIVE_SIDE)&&t.orientation()==CGAL::POSITIVE)){
                connection_matrix[i].push_back(true);
            }
            else{
                connection_matrix[i].push_back(false);
            }
        }
    }
    //create the probability matrix
    probability_matrix.reserve(transformations.size());
    for(size_t i=0;i<transformations.size();i++){
        probability_matrix.push_back(std::vector<float>());
        probability_matrix[probability_matrix.size()-1].reserve(transformations.size());
        float accumulator=0.0;
        for(size_t j=0;j<transformations.size();j++){//accumulate the row of the cmatrix
            if(connection_matrix[j][i])accumulator+=1.0f;
        }
        for(size_t j=0;j<transformations.size();j++){//init the values of pmatrix based on cmatric
            if(connection_matrix[j][i])probability_matrix[i].push_back(1.0f/accumulator);
            else probability_matrix[i].push_back(0.0f);
        }
    }
    //create the data vector
    buffer1.reserve(transformations.size());
    buffer2.reserve(transformations.size());
    for(size_t i=0;i<transformations.size();i++){
        buffer1.push_back(std::vector<Point>());
        buffer2.push_back(std::vector<Point>());
        buffer1[i].push_back(transformations[i].locals[0]);
        buffer1[i].push_back(transformations[i].locals[1]);
        buffer1[i].push_back(transformations[i].locals[2]);
    }
    points=std::vector<std::vector<Point>>(buffer1);
    
}
