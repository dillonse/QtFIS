//
//  RAFIS.h
//  OpenGL_CGAL
//
//  Created by Sean Dillon on 3/16/16.
//  Copyright (c) 2016 Sean Dillon. All rights reserved.
//

#ifndef __OpenGL_CGAL__RAFIS__
#define __OpenGL_CGAL__RAFIS__

#include "RFIS.h"
#include "KernelConstructions.h"
#include "AffineTransformation.h"
class RAFIS:public RFIS<Point,Affine3>{
public:
    RAFIS(){;};
    RAFIS(Arrangement2& mesh,Arrangement2& globalsArrangements);
};

#endif /* defined(__OpenGL_CGAL__RAFIS__) */
