//
//  RBFIM.h
//  OpenGL_CGAL
//
//  Created by Sean Dillon on 3/18/16.
//  Copyright (c) 2016 Sean Dillon. All rights reserved.
//

#ifndef __OpenGL_CGAL__RBFIM__
#define __OpenGL_CGAL__RBFIM__
#include "BivariateTransformation.h"
#include "KernelConstructions.h"
#include "RFIS.h"
class RBFIM:public RFIS<Arrangement2,Bivariate2Arrangement>{
public:
    RBFIM(Arrangement2& mesh ,Arrangement2& globals);
};

#endif /* defined(__OpenGL_CGAL__RBFIM__) */
