//
//  RBFIS.h
//  OpenGL_CGAL
//
//  Created by Sean Dillon on 3/18/16.
//  Copyright (c) 2016 Sean Dillon. All rights reserved.
//

#ifndef __OpenGL_CGAL__RBFIS__
#define __OpenGL_CGAL__RBFIS__
#include "RFIS.h"
#include "KernelConstructions.h"
#include "BivariateTransformation.h"
class RBFIS:public RFIS<Point,Bivariate3>{
public:
    RBFIS(Arrangement2& mesh,Arrangement2& globalsArrangements);
};
#endif /* defined(__OpenGL_CGAL__RBFIS__) */
