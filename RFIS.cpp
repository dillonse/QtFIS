//
//  RFIS.cpp
//  OpenGL_CGAL
//
//  Created by Sean Dillon on 3/4/16.
//  Copyright (c) 2016 Sean Dillon. All rights reserved.
//

#include "RFIS.h"
#include "RIFS.h"
/*
RFIS::RFIS(std::vector<Arrangement2>& mesh,std::vector<Arrangement2>& globalsArrangements){
    Point globals[3];
    Point locals[3];
    K::FT s[3];
    int indices[3];
    std::vector<Arrangement2::Vertex_handle> Globals;
    //get the globals in a vector and sort them based on their triangulation index
    for(std::vector<Arrangement2>::iterator it=globalsArrangements.begin();it!=globalsArrangements.end();it++){
        for(Arrangement2::Vertex_iterator itt=it->vertices_begin();itt!=it->vertices_end();itt++){
            Globals.push_back(itt);
        }
    }
    //sorting and deduplication
    std::sort(Globals.begin(),Globals.end(),triangulation_order);
    Globals.resize(std::distance(Globals.begin(),std::unique(Globals.begin(),Globals.end(),triangulation_unique)));
    //create the triangulations
    std::vector<Point> temp;
    for(std::vector<Arrangement2>::iterator it=mesh.begin();it!=mesh.end();it++){
        for(Arrangement2::Face_iterator itt=it->faces_begin();itt!=it->faces_end();itt++){
            if(itt->is_unbounded())continue;
            Arrangement2::Ccb_halfedge_circulator hc = itt->outer_ccb();
            locals[0]=Point(hc->target()->point().x(),hc->target()->point().y(),hc->target()->data().height);
            indices[0]=hc->target()->data().triangulation_index;
            globals[0]=Point(Globals[indices[0]]->point().x(),Globals[indices[0]]->point().y(),Globals[indices[0]]->data().height);
            s[0]=hc->target()->data().s;hc++;
            locals[1]=Point(hc->target()->point().x(),hc->target()->point().y(),hc->target()->data().height);
            indices[1]=hc->target()->data().triangulation_index;
            globals[1]=Point(Globals[indices[1]]->point().x(),Globals[indices[1]]->point().y(),Globals[indices[1]]->data().height);
            s[1]=hc->target()->data().s;
            hc++;
            locals[2]=Point(hc->target()->point().x(),hc->target()->point().y(),hc->target()->data().height);
            indices[2]=hc->target()->data().triangulation_index;
            globals[2]=Point(Globals[indices[2]]->point().x(),Globals[indices[2]]->point().y(),Globals[indices[2]]->data().height);
            s[2]=hc->target()->data().s;
            //create and add the transformation to the container
            transformations.push_back(Affine3(locals,globals,s));
        }
    }
    //create the connection matrix
    connection_matrix.reserve(transformations.size());
    for(int i=0;i<transformations.size();i++){
        connection_matrix.push_back(std::vector<bool>());
        connection_matrix[connection_matrix.size()-1].reserve(transformations.size());
        for(int j=0;j<transformations.size();j++){
            //compute the median of the local points
            Point2 median((transformations[j].locals[0].x()+transformations[j].locals[1].x()+transformations[j].locals[2].x())/3,
                          (transformations[j].locals[0].y()+transformations[j].locals[1].y()+transformations[j].locals[2].y())/3);
            K::Triangle_2 t(Point2(transformations[i].globals[0].x(),transformations[i].globals[0].y()),
                            Point2(transformations[i].globals[1].x(),transformations[i].globals[1].y()),
                            Point2(transformations[i].globals[2].x(),transformations[i].globals[2].y()));
            if(((t.oriented_side(median)!=CGAL::ON_POSITIVE_SIDE)&&t.orientation()==CGAL::NEGATIVE)||
               ((t.oriented_side(median)!=CGAL::ON_NEGATIVE_SIDE)&&t.orientation()==CGAL::POSITIVE)){
                connection_matrix[i].push_back(true);
            }
            else{
                connection_matrix[i].push_back(false);
            }
        }
    }
    //create the probability matrix
    probability_matrix.reserve(transformations.size());
    for(int i=0;i<transformations.size();i++){
        probability_matrix.push_back(std::vector<float>());
        probability_matrix[probability_matrix.size()-1].reserve(transformations.size());
        float accumulator=0.0;
        for(int j=0;j<transformations.size();j++){//accumulate the row of the cmatrix
            if(connection_matrix[j][i])accumulator+=1.0f;
        }
        for(int j=0;j<transformations.size();j++){//init the values of pmatrix based on cmatric
            if(connection_matrix[j][i])probability_matrix[i].push_back(1.0f/accumulator);
            else probability_matrix[i].push_back(0.0f);
        }
    }
    //create the data vector
    buffer1.reserve(transformations.size());
    buffer2.reserve(transformations.size());
    for(int i=0;i<transformations.size();i++){
        buffer1.push_back(std::vector<Point>());
        buffer2.push_back(std::vector<Point>());
        buffer1[i].push_back(transformations[i].locals[0]);
        buffer1[i].push_back(transformations[i].locals[1]);
        buffer1[i].push_back(transformations[i].locals[2]);
    }
    points=std::vector<std::vector<Point>>(buffer1);
    
}
*/