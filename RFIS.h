//
//  RFIS.h
//  OpenGL_CGAL
//
//  Created by Sean Dillon on 3/4/16.
//  Copyright (c) 2016 Sean Dillon. All rights reserved.
//

#ifndef __OpenGL_CGAL__RFIS__
#define __OpenGL_CGAL__RFIS__

#include <vector>
#include "RIFS.h"
#include "FractalInterpolationObject.h"
template<typename P,typename T>
class RFIS:public FIO<P>{
public:
    std::vector<std::vector<bool>> connection_matrix;
    std::vector<std::vector<float>> probability_matrix;
    std::vector<T> transformations;
    std::vector<std::vector<P>> buffer1;
    std::vector<std::vector<P>> buffer2;
    std::vector<std::vector<P>> points;
    void iterate_deterministic(){
        points.clear();
        if(buffer1[0].empty()){
            RIFS::iterate_deterministic(buffer2,transformations,buffer1,connection_matrix);
            points= std::vector<std::vector<P>>(buffer1);
            for(size_t i=0;i<buffer2.size();i++)buffer2[i].clear();
        }
        else{
            RIFS::iterate_deterministic(buffer1,transformations,buffer2,connection_matrix);
            points= std::vector<std::vector<P>>(buffer2);
            for(size_t i=0;i<buffer1.size();i++)buffer1[i].clear();
        }
    }

    void iterate_stochastic(){
        if(buffer1[0].empty()){
            RIFS::iterate_stochastic(buffer2,transformations,buffer1,probability_matrix);
            for(size_t i=0;i<buffer1.size();i++)
                points[i].insert(points[i].end(),buffer1[i].begin(),buffer1[i].end());
            for(size_t i=0;i<buffer2.size();i++)
                buffer2[i].clear();
        }
        else{
            RIFS::iterate_stochastic(buffer1,transformations,buffer2,probability_matrix);
            for(size_t i=0;i<buffer2.size();i++)
                points[i].insert(points[i].end(),buffer2[i].begin(),buffer2[i].end());
            for(size_t i=0;i<buffer1.size();i++)
                buffer1[i].clear();
        }
    }
    
    void getPoints(std::vector<std::vector<P>>& output){
        output=points;
    }
    
    void getPoints(std::vector<P>& output){
        for(typename std::vector<std::vector<P>>::iterator it=points.begin();it!=points.end();it++){
            output.insert(output.end(),it->begin(),it->end());
        }
    }
    void getInitialPoints(std::vector<std::vector<P>>& output){
        if(buffer1[0].empty())output=buffer2;
        else output=buffer1;
    }
    void getTransformations(std::vector<T>& output){
        output=transformations;
    }
    void getConnectionMatrix(std::vector<std::vector<bool>>& output){
        output=connection_matrix;
    }
    void getProbabilityMatrix(std::vector<std::vector<float>>& output){
        output=probability_matrix;
    }
};

#endif /* defined(__OpenGL_CGAL__RFIS__) */
