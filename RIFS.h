//
//  RIFS.h
//  OpenGL_CGAL
//
//  Created by Sean Dillon on 3/14/16.
//  Copyright (c) 2016 Sean Dillon. All rights reserved.
//

#ifndef OpenGL_CGAL_RIFS_h
#define OpenGL_CGAL_RIFS_h
#include "Random.h"
#include <iostream>
class RIFS{
public:
    template <class P,class T>
    static void iterate_deterministic(typename std::vector<std::vector<P>>& points,typename std::vector<T>& transformations,std::vector<std::vector<P>>& result,std::vector<std::vector<bool>> connection_matrix){
        for(size_t i=0;i<points.size();i++){
            for(size_t j=0;j<transformations.size();j++){
                if(connection_matrix[j][i]){//check the connection matrix
                    for(typename std::vector<P>::iterator it = points[i].begin();it!=points[i].end();it++){
                        result[j].push_back(transformations[j].Transform(*it));
                    }
                }
            }
        }
    }
    template <class P,class T>
    static void iterate_stochastic(typename std::vector<std::vector<P>>& points,typename std::vector<T>& transformations,std::vector<std::vector<P>>& result,std::vector<std::vector<float>>& probability_matrix){
        for(size_t i=0;i<points.size();i++){
            for(typename std::vector<P>::iterator it = points[i].begin();it!=points[i].end();it++){
                int dice_index=dice::throw_pmatrix(probability_matrix[i]);
                result[dice_index].push_back(transformations[dice_index].Transform(*it));
            }
        }
    }
};

#endif
