//
//  Random.cpp
//  OpenGL_CGAL
//
//  Created by Sean Dillon on 3/14/16.
//  Copyright (c) 2016 Sean Dillon. All rights reserved.
//

#include "Random.h"
#include "time.h"
#include <random>
time_t seed = time(0);
std::default_random_engine engine(seed);

int dice::throw_pmatrix(std::vector<float>& pmatrix){
    std::uniform_real_distribution<float> dist(0,1);
    float probability = dist(engine);
    float accumulator=0.0;
    for(int i=0;i<pmatrix.size();i++){
        accumulator+=pmatrix[i];
        if(probability<accumulator)return i;
    }
    return -1;
}

float dice::throw_range(float b,float e){
    std::uniform_real_distribution<float> dist(b,e);
    return dist(engine);
}

int dice::throw_range(int b, int e){
    std::uniform_int_distribution<int> dist(b,e);
    return dist(engine);
}

float dice::GaussianNoise(float b,float e){
    std::normal_distribution<float> dist(b,e);
    return dist(engine);
}
