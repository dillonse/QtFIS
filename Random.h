//
//  Random.h
//  OpenGL_CGAL
//
//  Created by Sean Dillon on 3/14/16.
//  Copyright (c) 2016 Sean Dillon. All rights reserved.
//

#ifndef __OpenGL_CGAL__Random__
#define __OpenGL_CGAL__Random__

#include <vector>

class dice{
public:
    static int throw_pmatrix(std::vector<float>& pmatrix);
    static int throw_range(int b,int e);
    static float throw_range(float b,float e);
    static float GaussianNoise(float b,float e);
};

#endif /* defined(__OpenGL_CGAL__Random__) */
