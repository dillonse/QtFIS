//
//  Regression.cpp
//  OpenGL_CGAL
//
//  Created by Sean Dillon on 4/3/16.
//  Copyright (c) 2016 Sean Dillon. All rights reserved.
//

#include "Regression.h"

void Regression::CalculateVSV(Arrangement2& mesh,Arrangement2& global,std::vector<std::vector<Point>>& mesh_points,std::vector<std::vector<Point>>& global_points){
    ExacttoK exactToK;
    //for each face of mesh, create a vector of its isolated vertices and call BivariateMeanArea
    int total = std::distance(mesh.faces_begin(),mesh.faces_end())-1;
    int count=0;
    for(Arrangement2::Face_iterator fi=global.faces_begin();fi!=global.faces_end();fi++){
        if(fi->is_unbounded())continue;
        fi->data().s=BivariateMeanArea(global_points[(int)CGAL::to_double(fi->data().s)]);
    }
    //std::cout<<"0-/"<<total<<std::endl;
    for(Arrangement2::Face_iterator fi=mesh.faces_begin();fi!=mesh.faces_end();fi++){
        if(fi->is_unbounded())continue;
        fi->data().s=BivariateMeanArea(mesh_points[(int)CGAL::to_double(fi->data().s)]);
        //search for the domain
        for(Arrangement2::Face_iterator fii=global.faces_begin();fii!=global.faces_end();fii++){
            if(fii->is_unbounded())continue;
            if(fii->data().index==fi->data().index){
                fi->data().s/=fii->data().s;
                fi->data().s=std::max(-1.0, std::min(CGAL::to_double(fi->data().s),1.0));
                break;
            }
        }
        //
        //std::cout<<++count<<"/"<<total<<std::endl;
    }
    for(Arrangement2::Vertex_iterator vi=mesh.vertices_begin();vi!=mesh.vertices_end();vi++){
         Arrangement2::Halfedge_around_vertex_circulator havc= vi->incident_halfedges(),end(havc);
         vi->data().s=0;
         int counter=0;
         do{
             if(havc->face()->is_unbounded())
                 continue;
             counter++;
             vi->data().s+=havc->face()->data().s;
         }while(++havc!=end);
         vi->data().s/=counter;
    }
}

void Regression::Sample(Arrangement2& mesh,Arrangement2& global,std::vector<Point>& input,std::vector<std::vector<Point>>& mesh_points,
                        std::vector<std::vector<Point>>& global_points){
    //enumerate the faces using the s field
    for(Arrangement2::Face_iterator fi= mesh.faces_begin();fi!=mesh.faces_end();fi++){
        fi->data().s=std::distance(mesh.faces_begin(),fi);
    }
    for(Arrangement2::Face_iterator fi= global.faces_begin();fi!=global.faces_end();fi++){

        fi->data().s=std::distance(global.faces_begin(),fi);
    }
    //sort the input point in xy lexicographical order
    std::sort(input.begin(),input.end(),[](auto i,auto j){if(i.x()==j.x())
            return i.y()<j.y();return i.x()<j.x();});
    //deduplicate the points
    input.resize(std::distance(input.begin(),std::unique(input.begin(),input.end(),[](auto i,auto j){return (i.x()==j.x())&&(i.y()==j.y());})));
    //create a list of the projection
    std::vector<Arrangement2::Point_2> projection;
    projection.resize(input.size());
    std::transform(input.begin(),input.end(),projection.begin(),[](auto i){return Arrangement2::Point_2(i.x(),i.y());});
    //locate the points in batch
    std::vector<std::pair<Arrangement2::Point_2,CGAL::Arr_point_location_result<Arrangement_2>::Type>> results_mesh;
    results_mesh.reserve(input.size());
     locate(mesh, projection.begin(),projection.end(), std::back_inserter(results_mesh));
     std::vector<std::pair<Arrangement2::Point_2,CGAL::Arr_point_location_result<Arrangement_2>::Type>> results_global;
     results_global.reserve(input.size());
      locate(global, projection.begin(),projection.end(), std::back_inserter(results_global));

    //iterate through the points and sort
     mesh_points.resize(mesh.number_of_faces());
     global_points.resize(global.number_of_faces());
     for(int i=0;i<results_mesh.size();i++){
         if (const Arrangement2::Face_const_handle* f = boost::get<Arrangement2::Face_const_handle>(&(results_mesh[i].second))){
             mesh_points[(int)CGAL::to_double((*f)->data().s)].push_back(input[i]);
         }
     }

     for(int i=0;i<results_global.size();i++){
         if (const Arrangement2::Face_const_handle* f = boost::get<Arrangement2::Face_const_handle>(&(results_global[i].second))){
             global_points[(int)CGAL::to_double((*f)->data().s)].push_back(input[i]);
         }
     }

    //if(mesh.ct==Arrangement2::ColoringType::NONE&&mesh.rit==Arrangement2::RecurrentIndexingType::FACES)
      //  return;
    //assign an index to each J

    int index=0;

    for(Arrangement2::Face_iterator fi=global.faces_begin();fi!=global.faces_end();fi++){
        if(fi->is_unbounded())continue;
        fi->data().index=index++;
    }
    //assing the index of each interval I to the appropriate J index
    for(Arrangement2::Face_iterator fi=mesh.faces_begin();fi!=mesh.faces_end();fi++){
        if(fi->is_unbounded())continue;
        Arrangement2::Ccb_halfedge_circulator end(fi->outer_ccb());
        std::vector<int> t_indices;
        Arrangement2::Ccb_halfedge_circulator hc=fi->outer_ccb();
        do{
            t_indices.push_back(hc->target()->data().triangulation_index);
        }while(++hc!=end);
        for(Arrangement2::Face_iterator fii=global.faces_begin();fii!=global.faces_end();fii++){
            if(fii->is_unbounded())continue;
            Arrangement2::Ccb_halfedge_circulator end(fii->outer_ccb());
            bool legit=true;
            Arrangement2::Ccb_halfedge_circulator hc=fii->outer_ccb();
            do{
                if(std::find(t_indices.begin(),t_indices.end(),hc->target()->data().triangulation_index)==t_indices.end()){
                    legit=false;
                    break;
                }
            }while(++hc!=end);
            if(legit){
                fi->data().index=fii->data().index;
                std::cout<<"index is "<<fi->data().index<<std::endl;
                break;
            }
        }
    }
}


K::FT Regression::BivariateMeanLine(std::vector<Point>& points){
    if(points.size()<3)return 0;
    //we asume the points are ordered by one dimension and the second has the same value for all e.x. (<x0,y>,<x1,y>,...,<xN,y>)
    //hold the endpoints
    Point start,end;
    K::FT result=0;
    start=*(points.begin());end=*(points.end()-1);
    for(std::vector<Point>::iterator it=points.begin();it!=points.end();it++){
        //linear interpolate the height value
        K::FT linear=
        CGAL::sqrt(CGAL::to_double(CGAL::squared_distance(Point2(start.x(),start.y()),Point2(it->x(),it->y()))))/
        CGAL::sqrt(CGAL::to_double(CGAL::squared_distance(Point2(start.x(),start.y()),Point2(end.x(),end.y()))))*end.z()+
        CGAL::sqrt(CGAL::to_double(CGAL::squared_distance(Point2(end.x(),end.y()),Point2(it->x(),it->y()))))/
        CGAL::sqrt(CGAL::to_double(CGAL::squared_distance(Point2(start.x(),start.y()),Point2(end.x(),end.y()))))*start.z();
        //get the diff of the height value from the interpolated
        result+=it->z()-linear;
    }
    result/=(double)points.size();
    return result;
}

K::FT Regression::BivariateMeanArea(std::vector<Point>& points){
    CGAL::Iso_cuboid_3<K> c3=CGAL::bounding_box(points.begin(),points.end());
    K::FT result=0;
    //order the points by x and then by y and then vice versa
    //x row
    std::sort(points.begin(),points.end(),[](auto i,auto j){if(i.x()==j.x())
            return i.y()<j.y();return i.x()<j.x();});
    std::vector<Point> unique_x(points);
    unique_x.resize(std::distance(unique_x.begin(),std::unique(unique_x.begin(),unique_x.end(),[](auto i,auto j){return i.x()==j.x();})));
    for(std::vector<Point>::iterator it=unique_x.begin();it!=unique_x.end();it++){
        std::vector<Point> x_row;
        //find elements in points that hold that value
        std::pair<std::vector<Point>::iterator,std::vector<Point>::iterator> bounds =
        std::equal_range(points.begin(),points.end(),*it,[](auto i,auto j){return i.x()<j.x();});
        //copy them on x row
        x_row.resize(std::distance(bounds.first,bounds.second));
        std::copy(bounds.first,bounds.second,x_row.begin());
        //find the mean of the x_row
        K::FT local_result=BivariateMeanLine(x_row);
        if(local_result==local_result)
            result+=local_result;
    }
    //y row
    std::sort(points.begin(),points.end(),[](auto i,auto j){if(i.y()==j.y())
            return i.x()<j.x();return i.y()<j.y();});
    std::vector<Point> unique_y(points);
    unique_y.resize(std::distance(unique_y.begin(),std::unique(unique_y.begin(),unique_y.end(),[](auto i,auto j){return i.y()==j.y();})));
    for(std::vector<Point>::iterator it=unique_y.begin();it!=unique_y.end();it++){
        std::vector<Point> y_row;
        //find elements in points that hold that value
        std::pair<std::vector<Point>::iterator,std::vector<Point>::iterator> bounds =
        std::equal_range(points.begin(),points.end(),*it,[](auto i,auto j){return i.y()<j.y();});
        //copy them on y row
        y_row.resize(std::distance(bounds.first,bounds.second));
        std::copy(bounds.first,bounds.second,y_row.begin());
        //find the mean of the y_row
        K::FT local_result=BivariateMeanLine(y_row);
        if(local_result==local_result)
            result+=local_result;
    }
    return result/(double)(unique_x.size()+unique_y.size());
}
