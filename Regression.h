//
//  Regression.h
//  OpenGL_CGAL
//
//  Created by Sean Dillon on 4/3/16.
//  Copyright (c) 2016 Sean Dillon. All rights reserved.
//

#ifndef __OpenGL_CGAL__Regression__
#define __OpenGL_CGAL__Regression__

#include <stdio.h>
#include "KernelConstructions.h"
class Regression{
private:
    static K::FT BivariateMeanLine(std::vector<Point>& points);
    static K::FT BivariateMeanArea(std::vector<Point>& points);
public:
    static void Sample(Arrangement2& mesh,Arrangement2& global,std::vector<Point>& data,std::vector<std::vector<Point>>& mesh_points,
                       std::vector<std::vector<Point>>& global_points);
    static void CalculateVSV(Arrangement2& mesh, Arrangement2& global, std::vector<std::vector<Point> > &mesh_points, std::vector<std::vector<Point> > &global_points);
};

#endif /* defined(__OpenGL_CGAL__Regression__) */
