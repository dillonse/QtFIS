//
//  SurfaceSampler.cpp
//  OpenGL_CGAL
//
//  Created by Sean Dillon on 1/15/16.
//  Copyright (c) 2016 Sean Dillon. All rights reserved.
//

#include "Sampling.h"
#include "TimePerformance.h"
#include "Random.h"
#include <vector>

void Sampling::SampleHeights(Arrangement2& mesh,Arrangement2& globals,std::vector<Point>& input){
    //compute AABB of points
    ExacttoK exactToK;
    K::Iso_cuboid_3 c3=CGAL::bounding_box(input.begin(),input.end());
    //create dt2 triangulation with input points
    Delaunay2 delaunay(input.begin(),input.end());
    //interpolate geometric values of mesh and query each node of the mesh against the dt2
    for(Arrangement2::Vertex_iterator it=mesh.vertices_begin();it!=mesh.vertices_end();it++){
        it->data().height=delaunay.nearest_vertex(Point(exactToK(it->point().x())*(c3.max().x()-c3.min().x()),
                                                        exactToK(it->point().y())*(c3.max().y()-c3.min().y()),
                                                        it->data().height))->point().z();
        it->data().s=0.5;
    }
    for(Arrangement2::Vertex_iterator it=globals.vertices_begin();it!=globals.vertices_end();it++){
        it->data().height=delaunay.nearest_vertex(Point(exactToK(it->point().x())*(c3.max().x()-c3.min().x()),
                                                        exactToK(it->point().y())*(c3.max().y()-c3.min().y()),
                                                        it->data().height))->point().z();
    }
}

void Sampling::NormalizeSurfaceCoodrinates(std::vector<Point>& input){
    K::Iso_cuboid_3 c3=CGAL::bounding_box(input.begin(),input.end());
    for(size_t i=0;i<input.size();i++){
        input[i]=Point((input[i].x()-c3.min().x())/(c3.max().x()-c3.min().x()),
                       (input[i].y()-c3.min().y())/(c3.max().y()-c3.min().y()),
                       input[i].z());
    }
}






