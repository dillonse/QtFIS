//
//  SurfaceSampler.h
//  OpenGL_CGAL
//
//  Created by Sean Dillon on 1/15/16.
//  Copyright (c) 2016 Sean Dillon. All rights reserved.
//

#ifndef __OpenGL_CGAL__SurfaceSampler__
#define __OpenGL_CGAL__SurfaceSampler__

#include <stdio.h>
#include <vector>
#include "KernelConstructions.h"

class Sampling{
private:
public:
    static void SampleHeights(Arrangement2& mesh,Arrangement2& globals,std::vector<Point>& data);
    static void NormalizeSurfaceCoodrinates(std::vector<Point>& data);
};

#endif /* defined(__OpenGL_CGAL__SurfaceSampler__) */
