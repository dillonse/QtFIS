//
//  SceneInteractor.cpp
//  OpenGL_CGAL
//
//  Created by Sean Dillon on 3/27/16.
//  Copyright (c) 2016 Sean Dillon. All rights reserved.
//

#include "SceneInteractor.h"

#define LMB 0
#define RMB 1
#define MMB 2
#define WHEEL 3

bool SceneInteractor::MouseButtons[4];
int SceneInteractor::MouseX;
int SceneInteractor::MouseY;

float SceneInteractor::RotationOffsetX;
float SceneInteractor::RotationOffsetY;
float SceneInteractor::RotationSpeedX=10000000;
float SceneInteractor::RotationSpeedY=10000000;

float SceneInteractor::TranslationOffsetX;
float SceneInteractor::TranslationOffsetY;
float SceneInteractor::TranslationSpeedX=100000;
float SceneInteractor::TranslationSpeedY=100000;

float SceneInteractor::TranslationOffsetZ;
float SceneInteractor::TranslationSpeedZ=100000;

void SceneInteractor::readMouseButtons(int button, int state,int x, int y){
    if(state==GLUT_DOWN){
        if(button==GLUT_LEFT_BUTTON)MouseButtons[LMB]=true;
        if(button==GLUT_RIGHT_BUTTON)MouseButtons[RMB]=true;
        if(button==GLUT_MIDDLE_BUTTON)MouseButtons[MMB]=true;
        if(button==3||button==4){
            MouseButtons[WHEEL]=true;
            if(button==3)SceneInteractor::TranslationOffsetZ=0.001*y;
            if(button==4)SceneInteractor::TranslationOffsetZ=-0.001*y;
        }
    }
    else if(state==GLUT_UP){
        if(button==GLUT_LEFT_BUTTON)MouseButtons[LMB]=false;
        if(button==GLUT_RIGHT_BUTTON)MouseButtons[RMB]=false;
        if(button==GLUT_MIDDLE_BUTTON)MouseButtons[MMB]=false;
        if(button==3||button==4)MouseButtons[WHEEL]=false;
    }
    MouseX=x;MouseY=y;
}

void SceneInteractor::readMouseDrag(int x,int y){
    if(MouseButtons[LMB]){
        SceneInteractor::RotationOffsetX=((float)(x-MouseX))/((float)SceneRender::width);
        SceneInteractor::RotationOffsetY=((float)(y-MouseY))/((float)SceneRender::height);
    }
    if(MouseButtons[RMB]){
        SceneInteractor::TranslationOffsetX=((float)(x-MouseX))/((float)SceneRender::width);
        SceneInteractor::TranslationOffsetY=((float)(MouseY-y))/((float)SceneRender::height);
    }
    if(MouseButtons[MMB]){
        SceneInteractor::TranslationOffsetZ=((float)(MouseY-y))/((float)SceneRender::height);
    }
    MouseX=x;MouseY=y;
}

void SceneInteractor::readKeyboard(unsigned char key,int x, int y){//mimics the wheel functionality for now
    if(key=='m'||key=='n'){
        MouseButtons[WHEEL]=true;
        if(key=='m')SceneInteractor::TranslationOffsetZ=0.1;
        if(key=='n')SceneInteractor::TranslationOffsetZ=-0.1;
    }
    if(key=='l'){
        glEnable(GL_COLOR_MATERIAL);
    }
}

void SceneInteractor::readKeyboardUp(unsigned char key,int x,int y){
    if(key=='m'||key=='n'){
        MouseButtons[WHEEL]=false;
        TranslationOffsetZ=0;
    }
}

void SceneInteractor::updateScene(){
    if(MouseButtons[LMB]){
        SceneRender::RotationOffsetY+=RotationOffsetX*SceneRender::deltaTime*RotationSpeedX;
        SceneRender::RotationOffsetX+=RotationOffsetY*SceneRender::deltaTime*RotationSpeedY;
        RotationOffsetX=0;RotationOffsetY=0;
    }
    if(MouseButtons[RMB]){
        SceneRender::TranslationOffsetY+=TranslationOffsetY*SceneRender::deltaTime*TranslationSpeedY;
        SceneRender::TranslationOffsetX+=TranslationOffsetX*SceneRender::deltaTime*TranslationSpeedX;
        TranslationOffsetX=0;TranslationOffsetY=0;
    }
    if(MouseButtons[MMB]||MouseButtons[WHEEL]){
        SceneRender::TranslationOffsetZ+=TranslationOffsetZ*SceneRender::deltaTime*TranslationSpeedZ;
    }
}
