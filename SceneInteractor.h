//
//  SceneInteractor.h
//  OpenGL_CGAL
//
//  Created by Sean Dillon on 3/27/16.
//  Copyright (c) 2016 Sean Dillon. All rights reserved.
//

#ifndef __OpenGL_CGAL__SceneInteractor__
#define __OpenGL_CGAL__SceneInteractor__

#include <stdio.h>
#include "SceneRender.h"

class SceneInteractor{
    friend class SceneRender;
protected:
    //member functions
    static void readMouseButtons(int button,int state,int x, int y);
    static void readMouseDrag(int x,int y);
    static void readKeyboard(unsigned char key,int x, int y);
    static void readKeyboardUp(unsigned char key,int x,int y);
    static void updateScene();
    //data members
    static bool MouseButtons[4];
    static int MouseX;
    static int MouseY;
    static float RotationSpeedX;
    static float RotationSpeedY;
    static float RotationOffsetX;
    static float RotationOffsetY;
    static float TranslationSpeedX;
    static float TranslationSpeedY;
    static float TranslationOffsetX;
    static float TranslationOffsetY;
    static float TranslationSpeedZ;
    static float TranslationOffsetZ;
    
};

#endif /* defined(__OpenGL_CGAL__SceneInteractor__) */
