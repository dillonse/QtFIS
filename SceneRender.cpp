//
//  SceneRender.cpp
//  OpenGL_CGAL
//
//  Created by Sean Dillon on 2/29/16.
//  Copyright (c) 2016 Sean Dillon. All rights reserved.
//

#include "SceneRender.h"
#include "SceneInteractor.h"
#include <tuple>
#include "OGL.h"




float SceneRender::TranslationOffsetX=0;
float SceneRender::TranslationOffsetY=0;
float SceneRender::TranslationOffsetZ=0;


float SceneRender::RotationOffsetX=0;
float SceneRender::RotationOffsetY=0;
float SceneRender::RotationOffsetZ=0;

float SceneRender::ScalingOffsetX=1;
float SceneRender::ScalingOffsetY=0.4;
float SceneRender::ScalingOffsetZ=1;

std::vector<object> SceneRender::objects;

float SceneRender::deltaTime=0;

int SceneRender::width=0;
int SceneRender::height=0;

float SceneRender::maxHeight=std::numeric_limits<float>().min();
float SceneRender::minHeight=std::numeric_limits<float>().max();

bool SceneRender::drawAxisParameter=true;
bool SceneRender::drawHeighValuesParameter=true;

void SceneRender::initGLUTWindow(){
    char** argv=NULL;
    int argc=0;
     glutInit(&argc,argv); // Initializes glut//    int argc=0;//    char** argv=NULL;
     glutInitDisplayMode(GLUT_DOUBLE | GLUT_DEPTH | GLUT_RGBA);// Sets up a double buffer with RGBA components and a depth component
     glutInitWindowSize(1024, 1024);    // Sets the window size to 512*512 square pixels
     glutInitWindowPosition(0, 0);    // Sets the window position to the upper left
    glutCreateWindow("Midpoint Displacement & Fractal Interpolation visualization");// Creates a window using internal glut functionality
}

void SceneRender::initGLUTFunctions(){
    glutReshapeFunc(SceneRender::reshape);
    glutDisplayFunc(SceneRender::display);
    glutIdleFunc(SceneRender::display);
    //interaction
    glutMouseFunc(SceneInteractor::readMouseButtons);
    glutMotionFunc(SceneInteractor::readMouseDrag);
    glutKeyboardFunc(SceneInteractor::readKeyboard);
    glutKeyboardUpFunc(SceneInteractor::readKeyboardUp);
    glutIdleFunc(SceneRender::idle);
    width=glutGet(GLUT_WINDOW_WIDTH);
    height=glutGet(GLUT_WINDOW_HEIGHT);
    glutPostRedisplay();
    #ifdef _WIN32
	if (GLEW_OK != glewInit()) {
		std::cout << "problem with GLEW initialization" << std::endl;
		exit(0);
	}
    #endif
}

void SceneRender::initRender(){
    glClearColor(1,1,1,1.0);
    //glEnable(GL_DEPTH_TEST);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    float lightAmbient[] = { 0.4f, 0.7f, 0.8f, 1.0f };
    float lightDiffuse[] = { 0.4f, 0.7f, 0.8f, 1.0f };
    float lightPosition[] = {0,0,-10};
    glLightfv(GL_LIGHT0, GL_AMBIENT,lightAmbient);	//Setup The Ambient Light
    glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDiffuse);	//Setup The Diffuse Light
    glLightfv(GL_LIGHT0,GL_POSITION, lightPosition);	//Position The Light
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_COLOR_MATERIAL);
    glEnable(GL_NORMALIZE);
    glColor3f(0,0,0);
    //glEnable(GL_LINE_SMOOTH);
    //glEnable(GL_CULL_FACE);
    //glCullFace(GL_FRONT);
}

void SceneRender::initTransformations(){
    RotationOffsetY=-60;
    RotationOffsetX=60;
    RotationOffsetZ=0;
    TranslationOffsetX=0;
    TranslationOffsetY=0;
    TranslationOffsetZ=-5;
}

double SceneRender::getDeltaTime(){
    static double oldElapsedTime=glutGet(GLUT_ELAPSED_TIME);
    double elapsedTime=glutGet(GLUT_ELAPSED_TIME);
    double deltaTime=(elapsedTime-oldElapsedTime)/1000.0;
    oldElapsedTime=elapsedTime;
    return  deltaTime/1000.0;
}

void SceneRender::display(){
     glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glDisable(GL_DEPTH_TEST);
    if(SceneRender::drawAxisParameter){
        glPushMatrix();
            glTranslatef(TranslationOffsetX,TranslationOffsetY,TranslationOffsetZ);;
            glRotatef(RotationOffsetX,1,0,0);
            glRotatef(RotationOffsetY,0,1,0);
            glRotatef(RotationOffsetZ,0,0,1);
            glTranslatef(-0.5*ScalingOffsetX,-0.5*ScalingOffsetY,-0.5*ScalingOffsetZ);
            glScalef(1.4,1,1.4);
            drawAxis();
        glPopMatrix();
    }
    if(SceneRender::drawHeighValuesParameter){
        glPushMatrix();
            glTranslatef(TranslationOffsetX,TranslationOffsetY,TranslationOffsetZ);;
            glRotatef(RotationOffsetX,1,0,0);
            glRotatef(RotationOffsetY,0,1,0);
            glRotatef(RotationOffsetZ,0,0,1);
            glTranslatef(-0.5*ScalingOffsetX,-0.5*ScalingOffsetY,-0.5*ScalingOffsetZ);
            glScalef(ScalingOffsetX,ScalingOffsetY,ScalingOffsetZ);
            drawLines(0.25);
        glPopMatrix();
    }
    glEnable(GL_DEPTH_TEST);
    for(std::vector<object>::iterator it=objects.begin();it!=objects.end();it++){
        glPushMatrix();
            glTranslatef(CGAL::to_double(std::get<1>(*it).x()),CGAL::to_double(std::get<1>(*it).y()),CGAL::to_double(std::get<1>(*it).z()));
            glTranslatef(TranslationOffsetX,TranslationOffsetY,TranslationOffsetZ);
            glRotatef(RotationOffsetX,1,0,0);
            glRotatef(RotationOffsetY,0,1,0);
            glRotatef(RotationOffsetZ,0,0,1);
            glScalef(ScalingOffsetX,ScalingOffsetY,ScalingOffsetZ);
            glScalef(std::get<0>(*it)->NormalizedScaleX,std::get<0>(*it)->NormalizedScaleY,std::get<0>(*it)->NormalizedScaleZ);
            glTranslatef(std::get<0>(*it)->NormalizedTranslateX,std::get<0>(*it)->NormalizedTranslateY,std::get<0>(*it)->NormalizedTranslateZ);
            std::get<0>(*it)->RenderVBO();
        glPopMatrix();
    }
    glutSwapBuffers();
    glFlush();
}
// Called every time a window is resized to resize the projection matrix
void SceneRender::reshape(int w, int h){
    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glFrustum(-0.1, 0.1, -float(h)/(10.0*float(w)), float(h)/(10.0*float(w)), 0.5, 1000.0);
    glMatrixMode(GL_MODELVIEW);
    width=glutGet(GLUT_WINDOW_WIDTH);
    height=glutGet(GLUT_WINDOW_HEIGHT);
}

void SceneRender::idle(){
    deltaTime=getDeltaTime();
    SceneInteractor::updateScene();
    glutPostRedisplay();
}

void SceneRender::initScene(){
    initGLUTWindow();
    initGLUTFunctions();
    initRender();
    initTransformations();
}

void SceneRender::renderScene(){
    normalize();
    glutMainLoop();
}

void SceneRender::addRender(RenderObject& ro,Point position,Point rotation){
    objects.push_back(std::make_tuple(&ro,position,rotation));
}

void SceneRender::normalize(){
    K::FT scaleX,scaleY,scaleZ,transX,transY,transZ;
    scaleX=scaleY=scaleZ=std::numeric_limits<K::FT>().min();
    transX=0;transY=0;transZ=0;
    K::FT candidateX,candidateY,candidateZ;
    maxHeight=std::numeric_limits<float>().min();
    minHeight=std::numeric_limits<float>().max();
    for(std::vector<object>::iterator it=objects.begin();it!=objects.end();it++){
        candidateX=std::get<0>(*it)->AABB.max().x()-std::get<0>(*it)->AABB.min().x();
        candidateY=std::get<0>(*it)->AABB.max().y()-std::get<0>(*it)->AABB.min().y();
        candidateZ=std::get<0>(*it)->AABB.max().z()-std::get<0>(*it)->AABB.min().z();
        if(candidateX>scaleX)scaleX=candidateX;
        if(candidateY>scaleY)scaleY=candidateY;
        if(candidateZ>scaleZ)scaleZ=candidateZ;
        if(std::abs(CGAL::to_double(std::get<0>(*it)->AABB.max().x()))>std::abs(CGAL::to_double(transX)))transX=std::get<0>(*it)->AABB.max().x();
        if(std::abs(CGAL::to_double(std::get<0>(*it)->AABB.min().x()))>std::abs(CGAL::to_double(transX)))transX=std::get<0>(*it)->AABB.min().x();
        if(std::abs(CGAL::to_double(std::get<0>(*it)->AABB.max().y()))>std::abs(CGAL::to_double(transY)))transY=std::get<0>(*it)->AABB.max().y();
        if(std::abs(CGAL::to_double(std::get<0>(*it)->AABB.min().y()))>std::abs(CGAL::to_double(transY)))transY=std::get<0>(*it)->AABB.min().y();
        if(std::abs(CGAL::to_double(std::get<0>(*it)->AABB.max().z()))>std::abs(CGAL::to_double(transZ)))transZ=std::get<0>(*it)->AABB.max().z();
        if(std::abs(CGAL::to_double(std::get<0>(*it)->AABB.min().z()))>std::abs(CGAL::to_double(transZ)))transZ=std::get<0>(*it)->AABB.min().z();
        if(std::get<0>(*it)->AABB.max().z()>maxHeight)maxHeight=CGAL::to_double(std::get<0>(*it)->AABB.max().z());
        if(std::get<0>(*it)->AABB.min().z()<minHeight)minHeight=CGAL::to_double(std::get<0>(*it)->AABB.min().z());
    }
    if(scaleX==std::numeric_limits<K::FT>().min())scaleX=1;
    if(scaleY==std::numeric_limits<K::FT>().min())scaleY=1;
    if(scaleZ==std::numeric_limits<K::FT>().min())scaleZ=1;
    transX=-transX+copysign(1.0,CGAL::to_double(transX))*0.5*scaleX;
    transY=-transY+copysign(1.0,CGAL::to_double(transY))*0.5*scaleY;
    transZ=-transZ+copysign(1.0,CGAL::to_double(transZ))*0.5*scaleZ;
    scaleX=1/scaleX;scaleY=1/scaleY;scaleZ=1/scaleZ;

    for(std::vector<object>::iterator it=objects.begin();it!=objects.end();it++){
        std::get<0>(*it)->NormalizedTranslateX=CGAL::to_double(transX);
        std::get<0>(*it)->NormalizedTranslateY=CGAL::to_double(transZ);
        std::get<0>(*it)->NormalizedTranslateZ=CGAL::to_double(transY);
        std::get<0>(*it)->NormalizedScaleX=CGAL::to_double(scaleX);
        std::get<0>(*it)->NormalizedScaleY=CGAL::to_double(scaleZ);
        std::get<0>(*it)->NormalizedScaleZ=CGAL::to_double(scaleY);
    }
}

void SceneRender::drawLines(float interval){
    glColor3f(0.5,0.5,0.5);
    glLineWidth(0.1);
    for(float p=0;p<=1.01;p+=interval){
        for(float x=0;x<=1.0;x+=1.0){
            for(float z=0;z<=1.0;z+=1.0){
                glRasterPos3f(x,p,z);
                std::stringstream stream;
                stream << std::fixed << std::setprecision(1) << (minHeight+(maxHeight-minHeight)*p);
                std::string y= stream.str();
                for(std::string::iterator it=y.begin();it!=y.end();it++){
                    char c=*it;
                    glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12,c);
                }
            }
        }
        glBegin(GL_LINE_STRIP);
        glVertex3d(0.0,p,0.0);
        glVertex3d(1.0,p,0.0);
        glVertex3d(1.0,p,1.0);
        glVertex3d(0.0,p,1.0);
        glVertex3d(0.0,p,0.0);
        glEnd();
    }
    glColor3f(0,0,0);
    glBegin(GL_LINES);
    glVertex3d(0.0,1.0,0.0);
    glVertex3d(0.0,0.0,0.0);
    
    glVertex3d(1.0,1.0,0.0);
    glVertex3d(1.0,0.0,0.0);
    
    glVertex3d(1.0,1.0,1.0);
    glVertex3d(1.0,0.0,1.0);
    
    glVertex3d(0.0,1.0,1.0);
    glVertex3d(0.0,0.0,1.0);
    glEnd();
}

void SceneRender::drawAxis(){
    glLineWidth(3);
    glColor3f(1,0,0);
    glBegin(GL_LINE_STRIP);
    glVertex3f(0.0f, 0.0f, 0.0f);
    glVertex3f(1.0, 0.0f, 0.0f);
    glVertex3f(1.0*0.9 , 1.0*0.05, 0.0f);
    glVertex3f(1.0*0.9, -1.0*0.05, 0.0f);
    glVertex3f(1.0, 0.0f, 0.0f);
    glVertex3f(1.0*0.9, 0.0f,1.0*0.05);
    glVertex3f(1.0*0.9, 0.0f,-1.0*0.05);
    glVertex3f(1.0, 0.0f, 0.0f);
    glEnd();
    glColor3f(0,1,0);
    glBegin(GL_LINE_STRIP);
    glVertex3f(0.0f, 0.0f, 0.0f);
    glVertex3f(0.0f, 1.0, 0.0f);
    glVertex3f(0.0f, 1.0*0.9, 1.0*0.05);
    glVertex3f(0.0f, 1.0*0.9, -1.0*0.05);
    glVertex3f(0.0f, 1.0, 0.0f);
    glVertex3f(1.0*0.05, 1.0*0.9, 0.0f);
    glVertex3f(-1.0*0.05, 1.0*0.9, 0.0f);
    glVertex3f(0.0f,1.0, 0.0f);
    glEnd();
    glColor3f(0,0,1);
    glBegin(GL_LINE_STRIP);
    glVertex3f(0.0f, 0.0f, 0.0f);
    glVertex3f(0.0f, 0.0f, 1.0);
    glVertex3f(1.0*0.05, 0.0f, 1.0*0.9);
    glVertex3f(-1.0*0.05, 0.0f,1.0*0.9);
    glVertex3f(0.0f, 0.0f,1.0);
    glVertex3f(0.0f, 1.0*0.05,1.0*0.9);
    glVertex3f(0.0f, -1.0*0.05,1.0*0.9);
    glVertex3f(0.0f, 0.0f,1.0);
    glEnd();
    glColor3f(0,0,0);
    glRasterPos3f(1.0+1.0*0.05, 0.0f, 0.0f);
    glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, 'x');
    glRasterPos3f(0.0f, 1.0+1.0*0.05, 0.0f);
    glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, 'y');
    glRasterPos3f(0.0f, 0.0f, 1.0+1.0*0.05);
    glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, 'z');
    
}

void SceneRender::removeRenders(){
    objects.clear();
}
