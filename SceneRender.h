//
//  SceneRender.h
//  OpenGL_CGAL
//
//  Created by Sean Dillon on 2/29/16.
//  Copyright (c) 2016 Sean Dillon. All rights reserved.
//

#ifndef __OpenGL_CGAL__SceneRender__
#define __OpenGL_CGAL__SceneRender__

#include <stdio.h>
#include "KernelConstructions.h"
#include "SurfaceRender.h"
#include <vector>
#include <tuple>

/*Warning: for compilation reasons we cannot pass a reference as memeber to tuple so we hold a pointer (for polymorphism)
 Thus , the memory management must be consistent, do not delete the rendering object before the rendering is done*/
typedef std::tuple<RenderObject*,Point,Point> object;

class SceneRender{
    friend class SceneInteractor;
protected:
    //functions
    static void initGLUTWindow();
    static void initGLUTFunctions();
    static void initRender();
    static void reshape(int w,int h);
    static void idle();
    static void display();
    static void normalize();
    static double getDeltaTime();
    static void drawAxis();
    static void drawLines(float interval);
    //data members
    static float TranslationOffsetX;
    static float TranslationOffsetY;
    static float TranslationOffsetZ;
    static float RotationOffsetX;
    static float RotationOffsetY;
    static float RotationOffsetZ;
    static float ScalingOffsetX;
    static float ScalingOffsetY;
    static float ScalingOffsetZ;
    static std::vector<object> objects;
    static float deltaTime;
    static int width;
    static int height;
    static float maxHeight;
    static float minHeight;
public:
    //member function
    static void initScene();
    static void renderScene();
    static void addSurface(SurfaceRender& sr,Point position,Point rotation);
    static void addRender(RenderObject& ro,Point position,Point rotation);
    static void removeRenders();
    static void initTransformations();
    //data members
    static bool drawAxisParameter;
    static bool drawHeighValuesParameter;
};
#endif /* defined(__OpenGL_CGAL__SceneRender__) */
