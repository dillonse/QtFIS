//
//  TriangleRO.cpp
//  OpenGL_CGAL
//
//  Created by Sean Dillon on 12/26/15.
//  Copyright (c) 2015 Sean Dillon. All rights reserved.
//

#include "SurfaceRender.h"
#include <iterator>

void SurfaceRender::BindBuffers(){
    //buffers generation
    glGenBuffers(1,&VBO_pointsID);                                  //generate the points buffer and return its id//
    glGenBuffers(1,&VBO_normalsID);
    glGenBuffers(1,&VBO_indexID);
    glGenBuffers(1,&VBO_AABBpointsID);
    glGenBuffers(1,&VBO_floorIndexID);
    glGenBuffers(1,&VBO_colourIndexID);

    //buffers binding
    //points
    glBindBuffer(GL_ARRAY_BUFFER,VBO_pointsID);                 //bind the buffer with the vbo id//
    glBufferData(GL_ARRAY_BUFFER,points.size()*sizeof(points[0]),&(points[0]),GL_STATIC_DRAW);//set the binded buffer with the points buffer//
    //normals
    glBindBuffer(GL_ARRAY_BUFFER,VBO_normalsID);
    glBufferData(GL_ARRAY_BUFFER,normals.size()*sizeof(normals[0]),&(normals[0]),GL_STATIC_DRAW);
    //indexes
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,VBO_indexID);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER,indexes.size()*sizeof(indexes[0]),&(indexes[0]),GL_STATIC_DRAW);
    //AABB points//
    glBindBuffer(GL_ARRAY_BUFFER,VBO_AABBpointsID);                 //bind the buffer with the vbo id//
    glBufferData(GL_ARRAY_BUFFER,AABB_points.size()*sizeof(AABB_points[0]),&(AABB_points[0]),GL_STATIC_DRAW);//set the binded buffer with the points buffer//
    //floor indexes//
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,VBO_floorIndexID);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER,floor_indexes.size()*sizeof(floor_indexes[0]),&(floor_indexes[0]),GL_STATIC_DRAW);
    //colour indexes//
    glBindBuffer(GL_ARRAY_BUFFER,VBO_colourIndexID);
    glBufferData(GL_ARRAY_BUFFER,colour_indexes.size()*sizeof(colour_indexes[0]),&(this->colour_indexes[0]),GL_STATIC_DRAW);

    //buffers activation//
    //points
    glEnableClientState(GL_VERTEX_ARRAY);                   //Activate the points buffer//

    glBindBuffer(GL_ARRAY_BUFFER,VBO_AABBpointsID);
    glVertexPointer(3, GL_FLOAT,0, 0);                     // Stride of 3 GLfloats//

    glBindBuffer(GL_ARRAY_BUFFER,VBO_pointsID);
    glVertexPointer(3, GL_FLOAT,0, 0);                     // Stride of 3 GLfloats//
    //normals
    glEnableClientState(GL_NORMAL_ARRAY);
    glBindBuffer(GL_ARRAY_BUFFER,VBO_normalsID);
    glNormalPointer(GL_FLOAT,0,0);
    //indexes
    glEnableClientState(GL_INDEX_ARRAY);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,VBO_indexID);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,VBO_floorIndexID);
    //colours//
    glEnableClientState(GL_COLOR_ARRAY);
    glBindBuffer(GL_ARRAY_BUFFER,VBO_colourIndexID);
    glColorPointer(3,GL_FLOAT,0,0);
}

void SurfaceRender::RenderVBO(){
    if(!this->drawEnabled)return;
    //enable common resources
    glDisableClientState(GL_COLOR_ARRAY);
    /*glEnableClientState(GL_COLOR_ARRAY);
    glBindBuffer(GL_ARRAY_BUFFER,VBO_colourIndexID);
    glColorPointer(3,GL_FLOAT,0,0);*/
    glBindBuffer(GL_ARRAY_BUFFER,VBO_normalsID);
    glNormalPointer(GL_FLOAT,0,0);
    glBindBuffer(GL_ARRAY_BUFFER,VBO_pointsID);
    glVertexPointer(3, GL_FLOAT,0, 0);
    if(this->drawWireframeParameter){
        //draw wirefame without lights//
        ///close the lights
        glDisable(GL_LIGHTING);
        ///set the polygone mode
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        ///set the line width
        glLineWidth(0.1);
        ////set the indices
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,VBO_indexID);
        ////draw the triangles
        glDrawElements(GL_TRIANGLES,(GLsizei)indexes.size(),GL_UNSIGNED_INT, 0);
    }
    if(this->drawFloorLinesParameter){
        //draw floor lines without lights//
        ///set the polygon mode
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        ///close the lights
        glDisable(GL_LIGHTING);
        ////set line width
        glLineWidth(2);
        ////set the indices
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,VBO_floorIndexID);
        ////draw the lines//
        glDrawElements(GL_LINES,(GLsizei)floor_indexes.size(),GL_UNSIGNED_INT, 0);
    }
    if(this->drawFillParameter){
        glEnableClientState(GL_COLOR_ARRAY);
        glBindBuffer(GL_ARRAY_BUFFER,VBO_colourIndexID);
        glColorPointer(3,GL_FLOAT,0,0);
        //draw filled triangles with lights//
        ///open lights and change polygon mode
        glEnable(GL_LIGHTING);
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        ////set the indices
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,VBO_indexID);
        ///draw the triangles//
        glDrawElements(GL_TRIANGLES,(GLsizei)indexes.size(),GL_UNSIGNED_INT, 0);
    }
}


void SurfaceRender::DelBuffers(){
    //delete point buffers
    glDeleteBuffers(1,&VBO_pointsID);
    glDeleteBuffers(1,&VBO_AABBpointsID);
    //delte normal buffers
    glDeleteBuffers(1,&VBO_normalsID);
    //delete index buffers
    glDeleteBuffers(1,&VBO_indexID);
    glDeleteBuffers(1,&VBO_floorIndexID);
    //delete colour buffers
    glDeleteBuffers(1,&VBO_colourIndexID);
}

void SurfaceRender::_SetData(std::vector<Triangulation>& dt){
    //compute the AABB on xy from CH and the z from min max
    std::vector<Point> convexHull;
    for(size_t i=0;i<dt.size();i++){
        Triangulation::Vertex_circulator vc = dt[i].incident_vertices(dt[i].infinite_vertex()),done(vc);
        if(vc!=0){
            do{convexHull.push_back(vc->point());}while(++vc!=done);
        }
    }

    K::FT max=std::numeric_limits<K::FT>().min();
    K::FT min=std::numeric_limits<K::FT>().max();
    for(size_t i=0;i<dt.size();i++){
        for(Triangulation::Finite_vertices_iterator it=dt[i].finite_vertices_begin();it!=dt[i].finite_vertices_end();it++){
            if(it->point().z()>max)max=it->point().z();
            if(it->point().z()<min)min=it->point().z();
        }
    }
    convexHull[0]=Point(convexHull[0].x(),convexHull[0].y(),max);
    convexHull[1]=Point(convexHull[1].x(),convexHull[1].y(),min);
    K::Iso_cuboid_3 c3=CGAL::bounding_box(convexHull.begin(),convexHull.end());
    AABB=c3;
    this->floor = CGAL::to_double(c3.zmin());
    this->ceiling = CGAL::to_double(c3.zmax());
    this->AABB_points.reserve(30);
    //5
    AABB_points.push_back(CGAL::to_double(c3[5].x()));AABB_points.push_back(CGAL::to_double(c3[5].z()));AABB_points.push_back(CGAL::to_double(c3[5].y()));
    //0
    AABB_points.push_back(CGAL::to_double(c3[0].x()));AABB_points.push_back(CGAL::to_double(c3[0].z()));AABB_points.push_back(CGAL::to_double(c3[0].y()));
    //6
    AABB_points.push_back(CGAL::to_double(c3[6].x()));AABB_points.push_back(CGAL::to_double(c3[6].z()));AABB_points.push_back(CGAL::to_double(c3[6].y()));
    //1
    AABB_points.push_back(CGAL::to_double(c3[1].x()));AABB_points.push_back(CGAL::to_double(c3[1].z()));AABB_points.push_back(CGAL::to_double(c3[1].y()));
    //7
    AABB_points.push_back(CGAL::to_double(c3[7].x()));AABB_points.push_back(CGAL::to_double(c3[7].z()));AABB_points.push_back(CGAL::to_double(c3[7].y()));
    //2
    AABB_points.push_back(CGAL::to_double(c3[2].x()));AABB_points.push_back(CGAL::to_double(c3[2].z()));AABB_points.push_back(CGAL::to_double(c3[2].y()));
    //4
    AABB_points.push_back(CGAL::to_double(c3[4].x()));AABB_points.push_back(CGAL::to_double(c3[4].z()));AABB_points.push_back(CGAL::to_double(c3[4].y()));
    //3
    AABB_points.push_back(CGAL::to_double(c3[3].x()));AABB_points.push_back(CGAL::to_double(c3[3].z()));AABB_points.push_back(CGAL::to_double(c3[3].y()));
    //5
    AABB_points.push_back(CGAL::to_double(c3[5].x()));AABB_points.push_back(CGAL::to_double(c3[5].z()));AABB_points.push_back(CGAL::to_double(c3[5].y()));
    //0
    AABB_points.push_back(CGAL::to_double(c3[0].x()));AABB_points.push_back(CGAL::to_double(c3[0].z()));AABB_points.push_back(CGAL::to_double(c3[0].y()));
    size_t unique_points=0;
    size_t unique_faces=0;
    for(size_t i=0;i<dt.size();i++){
        unique_points+=std::distance(dt[i].finite_vertices_begin(),dt[i].finite_vertices_end());
        unique_faces+=std::distance(dt[i].finite_faces_begin(),dt[i].finite_faces_end());
    }
    points.reserve(unique_points*3*2);
    normals.reserve(unique_points*3);
    int dividor=1;
    if(unique_points*2>std::numeric_limits<unsigned short>::max()){
        dividor=(std::ceil(((float)unique_points*2)/((float)std::numeric_limits<unsigned short>::max())));
        floor_indexes.reserve(unique_points*2/dividor);
    }
    else{
        floor_indexes.reserve(unique_points*2);
    }
    colour_indexes.reserve(unique_points*3);
    Triangulation::Vertex_handle vh;
    Vector normal;
    int j=0;
    int k=0;
    std::vector<GLfloat> floor_points;
    floor_points.reserve(unique_points/dividor);
    for(size_t i=0;i<dt.size();i++){
        for(Triangulation::Finite_vertices_iterator it=dt[i].finite_vertices_begin();it!=dt[i].finite_vertices_end();it++){
            vh=it;
            //normal points
            points.push_back(CGAL::to_double(vh->point().x()));
            points.push_back(CGAL::to_double(vh->point().z()));
            points.push_back(CGAL::to_double(vh->point().y()));
            //floor points
            if(j%dividor==0){

                floor_points.push_back(CGAL::to_double(vh->point().x()));
                floor_points.push_back((GLfloat)this->floor);
                floor_points.push_back(CGAL::to_double(vh->point().y()));
                //floor points indexes//
                floor_indexes.push_back(j);
                floor_indexes.push_back((GLuint)unique_points+k);
                k++;
            }
            normals.push_back(0);
            normals.push_back(0);
            normals.push_back(0);
            vh->info().index=j;
            j++;
        }
    }
    points.insert(points.end(),floor_points.begin(),floor_points.end());
    indexes.reserve(unique_faces*3);

    j=0;
    Triangulation::Face_handle fc;
    for(size_t i=0;i<dt.size();i++){
        for(Triangulation::Finite_faces_iterator it=dt[i].finite_faces_begin();it!=dt[i].finite_faces_end();it++){
            fc=it;
            indexes.push_back((GLuint)fc->vertex(0)->info().index);
            indexes.push_back((GLuint)fc->vertex(1)->info().index);
            indexes.push_back((GLuint)fc->vertex(2)->info().index);
            //compute the normals
            //v0
            normal=CGAL::normal(fc->vertex(0)->point(),fc->vertex(2)->point(),fc->vertex(1)->point());
            normals[indexes[j*3]*3]+=CGAL::to_double(normal.x());
            normals[indexes[j*3]*3+2]+=CGAL::to_double(normal.z());
            normals[indexes[j*3]*3+1]+=CGAL::to_double(normal.y());
            //v1
            normal=CGAL::normal(fc->vertex(1)->point(),fc->vertex(0)->point(),fc->vertex(2)->point());
            normals[indexes[j*3+1]*3]+=CGAL::to_double(normal.x());
            normals[indexes[j*3+1]*3+2]+=CGAL::to_double(normal.z());
            normals[indexes[j*3+1]*3+1]+=CGAL::to_double(normal.y());
            //v2
            normal=CGAL::normal(fc->vertex(2)->point(),fc->vertex(1)->point(),fc->vertex(0)->point());
            normals[indexes[j*3+2]*3]+=CGAL::to_double(normal.x());
            normals[indexes[j*3+2]*3+2]+=CGAL::to_double(normal.z());
            normals[indexes[j*3+2]*3+1]+=CGAL::to_double(normal.y());
            j++;
        }
    }
    for(size_t i=0;i<unique_points;i++){
        colour_indexes.push_back((points[i*3+1]-this->floor)/(this->ceiling-this->floor));
        colour_indexes.push_back(1 - colour_indexes[i*3]);
        colour_indexes.push_back(0);
    }
    BindBuffers();
}
