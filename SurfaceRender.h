//
//  TriangleRO.h
//  OpenGL_CGAL
//
//  Created by Sean Dillon on 12/26/15.
//  Copyright (c) 2015 Sean Dillon. All rights reserved.
//

#ifndef __OpenGL_CGAL__TriangleRO__
#define __OpenGL_CGAL__TriangleRO__

#include "OGL.h"
#include "KernelConstructions.h"
#include <stdio.h>
#include <iterator>


class SurfaceRender:public RenderObject{
protected:
    bool IVBO_setup=false;
    ////IVBO IDs//
    GLuint VBO_pointsID=0;
    GLuint VBO_indexID=0;
    GLuint VBO_normalsID=0;
    GLuint VBO_AABBpointsID=0;
    GLuint VBO_floorIndexID=0;
    GLuint VBO_colourIndexID=0;
    ////Data buffers//
    std::vector<GLfloat> points;
    std::vector<GLfloat> normals;
    std::vector<GLuint> indexes;
    std::vector<GLfloat> AABB_points;
    std::vector<GLuint> floor_indexes;
    std::vector<GLfloat> colour_indexes;
    ////IVBO functions
    void BindBuffers();
    void DelBuffers();
    ////counters and value placeholders//
    GLfloat floor=0;
    GLfloat ceiling=0;
    void _SetData(std::vector<Triangulation>& t);
public:
    bool drawEnabled=true;
    bool drawWireframeParameter=true;
    bool drawFloorLinesParameter=true;
    bool drawFillParameter=true;
    void RenderImmediate();      //render the points in immediate mode//
    void RenderVBO();           //render the points in VBO mode//
    template<class T>
    void SetData(std::vector<T>& t){
        std::vector<Triangulation> t2;
        t2.reserve(t.size());
        for(size_t i=0;i<t.size();i++)
            t2.push_back(Triangulation(t[i]));
        _SetData(t2);
    }
    template<class T>
    void SetData(T& t){
        std::vector<Triangulation> t2;
        t2.reserve(1);
        t2.push_back(Triangulation(t));
        _SetData(t2);
    }
    
    void Normalize(Point scale,Point trans);
};


#endif /* defined(__OpenGL_CGAL__TriangleRO__) */
