//
//  TimePerformance.cpp
//  OpenGL_CGAL
//
//  Created by Sean Dillon on 1/9/16.
//  Copyright (c) 2016 Sean Dillon. All rights reserved.
//

#include "TimePerformance.h"

std::ostream& operator<<(std::ostream& stream,timer& a){
    stream<<std::fixed<<(double)(a.getInterval())/(CLOCKS_PER_SEC)<<" s";
    return stream;
}