//
//  TimePerfomance.h
//  OpenGL_CGAL
//
//  Created by Sean Dillon on 1/9/16.
//  Copyright (c) 2016 Sean Dillon. All rights reserved.
//

#ifndef __OpenGL_CGAL__TimePerfomance__
#define __OpenGL_CGAL__TimePerfomance__

#include <time.h>
#include <iostream>

class timer{
private:
    std::clock_t t1,t2;
public:
    void setStart(){t1=std::clock();}
    void setEnd(){t2=std::clock();}
    std::clock_t getInterval(){return t2-t1;}
};

std::ostream& operator<<(std::ostream& stream,timer& a);
#endif /* defined(__OpenGL_CGAL__TimePerfomance__) */
