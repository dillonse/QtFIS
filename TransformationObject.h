//
//  TransformationObject.h
//  OpenGL_CGAL
//
//  Created by Sean Dillon on 4/13/16.
//  Copyright (c) 2016 Sean Dillon. All rights reserved.
//

#ifndef OpenGL_CGAL_TransformationObject_h
#define OpenGL_CGAL_TransformationObject_h
#include "KernelConstructions.h"
class TO{
public:
    Arrangement2::VerticalScalingType vst;
    Arrangement2::ColoringType ct;
    Arrangement2::RecurrentIndexingType rit;
};

#endif
