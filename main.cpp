
//
//  main.cpp
//  OpenGL_CGAL
//
//  Created by Sean Dillon on 5/22/15.
//  Copyright (c) 2015 Sean Dillon. All rights reserved.
//
#include "MidpointDisplacement.h"
#include "FileIO.h"
#include "SurfaceRender.h"
#include "SceneRender.h"
#include "AFIM.h"
#include "AFIS.h"
#include "BFIM.h"
#include "BFIS.h"
#include "RAFIM.h"
#include "RAFIS.h"
#include "RBFIM.h"
#include "RBFIS.h"
#include "FIS.h"
#include "Mesh.h"
#include "MeshRender.h"
#include "Sampling.h"
#include "Regression.h"
#include <vector>
#include <regex>
#ifdef _WIN32
    #include <direct.h>
#else
    #include <sys/stat.h>
#endif

void createMeshFolders(std::string path){
    #if defined(_WIN32)
        _mkdir(std::string(path).append("/Triangular.AFIM").c_str());
        _mkdir(std::string(path).append("/Rectangular.BFIM").c_str());
        _mkdir(std::string(path).append("/Wang.BFIM").c_str());
        _mkdir(std::string(path).append("/Recurrent").c_str());
        _mkdir(std::string(path).append("/Recurrent/Rectangular1.RAFIM").c_str());
        _mkdir(std::string(path).append("/Recurrent/Rectangular2.RAFIM").c_str());
        _mkdir(std::string(path).append("/Recurrent/Rectangular3.RBFIM").c_str());
    #else
        mkdir(std::string(path).append("/Triangular.AFIM").c_str(), 0777);
        mkdir(std::string(path).append("/Rectangular.BFIM").c_str(), 0777);
        mkdir(std::string(path).append("/Wang.BFIM").c_str(), 0777);
        mkdir(std::string(path).append("/Recurrent").c_str(), 0777);
        mkdir(std::string(path).append("/Recurrent/Rectangular1.RAFIM").c_str(), 0777);
        mkdir(std::string(path).append("/Recurrent/Rectangular2.RAFIM").c_str(), 0777);
        mkdir(std::string(path).append("/Recurrent/Rectangular3.RBFIM").c_str(), 0777);
    #endif
    Arrangement2 I,J;
    std::vector<Arrangement2> AVI,AVJ;
    
    Mesh::createTriangular(I,J);
    //FileIO::Write(I,std::string(path).append("/Triangular.AFIM/Triangular.AFIMI"),VerticalScalingType::VERTICES,ColoringType::CONSISTENT);
//    FileIO::Write(J,std::string(path).append("/Triangular.AFIM/Triangular.AFIMJ"));
    
  /*  I=Arrangement2();J=Arrangement2();
    Mesh::createRectangular3(I,J);
    FileIO::Write(I,std::string(path).append("/Rectangular.BFIM/Rectangular.BFIMI"));
    FileIO::Write(J,std::string(path).append("/Rectangular.BFIM/Rectangular.BFIMJ"));
    
    I=Arrangement2();J=Arrangement2();
    Mesh::createWang1(I,J);
    FileIO::Write(I,std::string(path).append("/Wang.BFIM/Wang.BFIMI"));
    FileIO::Write(J,std::string(path).append("/Wang.BFIM/Wang.BFIMJ"));
    
    I=Arrangement2();J=Arrangement2();
    AVI.clear();AVJ.clear();
    Mesh::createRectangular1(AVI,AVJ);
    Mesh::unify(AVI,I);Mesh::unify(AVJ,J);
    FileIO::Write(I,std::string(path).append("/Recurrent/Rectangular1.RAFIM/Rectangular1.RAFIMI"));
    FileIO::Write(J,std::string(path).append("/Recurrent/Rectangular1.RAFIM/Rectangular1.RAFIMJ"));
    
    I=Arrangement2();J=Arrangement2();
    AVI.clear();AVJ.clear();
    Mesh::createRectangular2(AVI,AVJ);
    Mesh::unify(AVI,I);Mesh::unify(AVJ,J);
    FileIO::Write(I,std::string(path).append("/Recurrent/Rectangular2.RAFIM/Rectangular2.RAFIMI"));
    FileIO::Write(J,std::string(path).append("/Recurrent/Rectangular2.RAFIM/Rectangular2.RAFIMJ"));
    
    I=Arrangement2();J=Arrangement2();
    AVI.clear();AVJ.clear();
    Mesh::createRectangular4(AVI,AVJ);
    Mesh::unify(AVI,I);
    Mesh::unify(AVJ,J);
    FileIO::Write(I,std::string(path).append("/Recurrent/Rectangular3.RBFIM/Rectangular3.RBFIMI"));
    FileIO::Write(J,std::string(path).append("/Recurrent/Rectangular3.RBFIM/Rectangular3.RBFIMJ"));*/
    
}

bool parseInputInterpolation(int argc,char* argv[],std::string& meshfile,std::string& datafile,int& iterations){
    if(argc<2){
        return false;
    }
    std::string input;
    for(int i=1;i<argc-1;i++){
        input.append(argv[i]).append(" ");
    }
    input.append(argv[argc-1]);
    std::regex base_regex("\\x2D\\x6D\\s([A-Za-z1-9/\.-_]+)(\\s\\x2D\\x64\\s([A-Za-z1-9/\.-_]+))?\\s\\x2D\\x69\\s([0-9])+");
    std::smatch base_match;
    if(!std::regex_match(input,base_match, base_regex)){
        return false; //std::cout<<"Undefined input: Usage -m <meshfile> -d <datafile>(optional)"<<std::endl;
    }
    meshfile=argv[2];
    if(argc>5){
        datafile=argv[4];
        iterations=std::stod(argv[6]);
    }
    else{
        iterations=std::stod(argv[4]);
    }
    return true;
}

bool parseInputRender(int argc, char* argv[],std::string& datafile){
    std::string input;
    for(int i=1;i<argc-1;i++){
        input.append(argv[i]).append(" ");
    }
    input.append(argv[argc-1]);
    std::regex base_regex("(\\x2D\\x64\\s([A-Za-z1-9/\.-_]+))");
    std::smatch base_match;
    if(!std::regex_match(input,base_match, base_regex)){
        return false; //std::cout<<"Undefined input: Usage -m <meshfile> -d <datafile>(optional)"<<std::endl;
    }
    datafile=argv[2];
    return true;
}

bool parseInputDataCreation(int argc,char* argv[],std::string& path){
    if(argc<2){
        return false;
    }
    std::string input;
    for(int i=1;i<argc-1;i++){
        input.append(argv[i]).append(" ");
    }
    input.append(argv[argc-1]);
    std::regex base_regex("\\x2D\\x63\\s([A-Za-z1-9/\.-_]+)");
    std::smatch base_match;
    if(!std::regex_match(input,base_match, base_regex)){
        return false;//std::cout<<"Undefined input: Usage -m <meshfile> -d <datafile>(optional)"<<std::endl;
    }
    path=argv[2];
    return true;
}

int main(int argc,char* argv[]){
    //parse the input arguments
    std::string meshfile;
    std::string datafile;
    std::string datapath;
    int iterations;
    /*
    Arrangement2 I,J;
    std::vector<Arrangement2> IV,JV;
    Mesh::createWang1(I,J);
//    Mesh::unify(IV,I);Mesh::unify(JV,J);
    std::string path ="/Users/seandillon/thesis/data/meshes";
    //1
    mkdir(std::string(path).append("/Triangular1.RBFIM").c_str(), 0777);
    FileIO::Write(I,"/Users/seandillon/thesis/data/meshes/Triangular1.RBFIM/Triangular1.RBFIMI",VerticalScalingType::SINGLE,ColoringType::NONE,RecurrentIndexingType::NONE);
    FileIO::Write(J,"/Users/seandillon/thesis/data/meshes/Triangular1.RBFIM/Triangular1.RBFIMJ",VerticalScalingType::SINGLE,ColoringType::NONE,RecurrentIndexingType::NONE);
    //2
    mkdir(std::string(path).append("/Triangular2.RBFIM").c_str(), 0777);
    FileIO::Write(I,"/Users/seandillon/thesis/data/meshes/Triangular2.RBFIM/Triangular2.RBFIMI",VerticalScalingType::SINGLE,ColoringType::CONSISTENT,RecurrentIndexingType::NONE);
    FileIO::Write(J,"/Users/seandillon/thesis/data/meshes/Triangular2.RBFIM/Triangular2.RBFIMJ",VerticalScalingType::SINGLE,ColoringType::CONSISTENT,RecurrentIndexingType::NONE);
    //3
    mkdir(std::string(path).append("/Triangular3.RBFIM").c_str(), 0777);
    FileIO::Write(I,"/Users/seandillon/thesis/data/meshes/Triangular3.RBFIM/Triangular3.RBFIMI",VerticalScalingType::FACES,ColoringType::NONE,RecurrentIndexingType::NONE);
    FileIO::Write(J,"/Users/seandillon/thesis/data/meshes/Triangular3.RBFIM/Triangular3.RBFIMJ",VerticalScalingType::FACES,ColoringType::NONE,RecurrentIndexingType::NONE);
    //4
    mkdir(std::string(path).append("/Triangular4.RBFIM").c_str(), 0777);
    FileIO::Write(I,"/Users/seandillon/thesis/data/meshes/Triangular4.RBFIM/Triangular4.RBFIMI",VerticalScalingType::FACES,ColoringType::CONSISTENT,RecurrentIndexingType::NONE);
    FileIO::Write(J,"/Users/seandillon/thesis/data/meshes/Triangular4.RBFIM/Triangular4.RBFIMJ",VerticalScalingType::FACES,ColoringType::CONSISTENT,RecurrentIndexingType::NONE);
    //5
    mkdir(std::string(path).append("/Triangular5.RBFIM").c_str(), 0777);
    FileIO::Write(I,"/Users/seandillon/thesis/data/meshes/Triangular5.RBFIM/Triangular5.RBFIMI",VerticalScalingType::VERTICES,ColoringType::NONE,RecurrentIndexingType::NONE);
    FileIO::Write(J,"/Users/seandillon/thesis/data/meshes/Triangular5.RBFIM/Triangular5.RBFIMJ",VerticalScalingType::VERTICES,ColoringType::NONE,RecurrentIndexingType::NONE);
    //6
    mkdir(std::string(path).append("/Triangular6.RBFIM").c_str(), 0777);
    FileIO::Write(I,"/Users/seandillon/thesis/data/meshes/Triangular6.RBFIM/Triangular6.RBFIMI",VerticalScalingType::VERTICES,ColoringType::CONSISTENT,RecurrentIndexingType::NONE);
    FileIO::Write(J,"/Users/seandillon/thesis/data/meshes/Triangular6.RBFIM/Triangular6.RBFIMJ",VerticalScalingType::VERTICES,ColoringType::CONSISTENT,RecurrentIndexingType::NONE);
    //7
    mkdir(std::string(path).append("/Triangular7.RBFIM").c_str(), 0777);
    FileIO::Write(I,"/Users/seandillon/thesis/data/meshes/Triangular7.RBFIM/Triangular7.RBFIMI",VerticalScalingType::SINGLE,ColoringType::NONE,RecurrentIndexingType::FACES);
    FileIO::Write(J,"/Users/seandillon/thesis/data/meshes/Triangular7.RBFIM/Triangular7.RBFIMJ",VerticalScalingType::SINGLE,ColoringType::NONE,RecurrentIndexingType::FACES);
    //8
    mkdir(std::string(path).append("/Triangular8.RBFIM").c_str(), 0777);
    FileIO::Write(I,"/Users/seandillon/thesis/data/meshes/Triangular8.RBFIM/Triangular8.RBFIMI",VerticalScalingType::SINGLE,ColoringType::CONSISTENT,RecurrentIndexingType::FACES);
    FileIO::Write(J,"/Users/seandillon/thesis/data/meshes/Triangular8.RBFIM/Triangular8.RBFIMJ",VerticalScalingType::SINGLE,ColoringType::CONSISTENT,RecurrentIndexingType::FACES);
    //9
    mkdir(std::string(path).append("/Triangular9.RBFIM").c_str(), 0777);
    FileIO::Write(I,"/Users/seandillon/thesis/data/meshes/Triangular9.RBFIM/Triangular9.RBFIMI",VerticalScalingType::FACES,ColoringType::NONE,RecurrentIndexingType::FACES);
    FileIO::Write(J,"/Users/seandillon/thesis/data/meshes/Triangular9.RBFIM/Triangular9.RBFIMJ",VerticalScalingType::FACES,ColoringType::NONE,RecurrentIndexingType::FACES);
    //10
    mkdir(std::string(path).append("/Triangular10.RBFIM").c_str(), 0777);
    FileIO::Write(I,"/Users/seandillon/thesis/data/meshes/Triangular10.RBFIM/Triangular10.RBFIMI",VerticalScalingType::FACES,ColoringType::CONSISTENT,RecurrentIndexingType::FACES);
    FileIO::Write(J,"/Users/seandillon/thesis/data/meshes/Triangular10.RBFIM/Triangular10.RBFIMJ",VerticalScalingType::FACES,ColoringType::CONSISTENT,RecurrentIndexingType::FACES);
    //11
    mkdir(std::string(path).append("/Triangular11.RBFIM").c_str(), 0777);
    FileIO::Write(I,"/Users/seandillon/thesis/data/meshes/Triangular11.RBFIM/Triangular11.RBFIMI",VerticalScalingType::VERTICES,ColoringType::NONE,RecurrentIndexingType::FACES);
    FileIO::Write(J,"/Users/seandillon/thesis/data/meshes/Triangular11.RBFIM/Triangular11.RBFIMJ",VerticalScalingType::VERTICES,ColoringType::NONE,RecurrentIndexingType::FACES);
    //12
    mkdir(std::string(path).append("/Triangular12.RBFIM").c_str(), 0777);
    FileIO::Write(I,"/Users/seandillon/thesis/data/meshes/Triangular12.RBFIM/Triangular12.RBFIMI",VerticalScalingType::VERTICES,ColoringType::CONSISTENT,RecurrentIndexingType::FACES);
    FileIO::Write(J,"/Users/seandillon/thesis/data/meshes/Triangular12.RBFIM/Triangular12.RBFIMJ",VerticalScalingType::VERTICES,ColoringType::CONSISTENT,RecurrentIndexingType::FACES);
    VerticalScalingType vst;ColoringType ct; RecurrentIndexingType rit;
    exit(0);
     */
    if(parseInputDataCreation(argc,argv,datapath)){
        createMeshFolders(datapath);
    }
    else if(parseInputInterpolation(argc, argv,meshfile,datafile,iterations)){
        //find the file name from the path
        size_t pos=meshfile.find_last_of("/\\");
        std::cout << " path: " << meshfile.substr(0,pos) << '\n';
        std::cout << " file: " << meshfile.substr(pos+1) << '\n';
        //read the data
        Arrangement2 I,J;
        VerticalScalingType vst;ColoringType ct; RecurrentIndexingType rit;
        #ifdef _WIN32
        FileIO::Read(I,std::string(meshfile).append("\\").append(meshfile.substr(pos+1)).append("I"),vst,ct,rit);
        FileIO::Read(J,std::string(meshfile).append("\\").append(meshfile.substr(pos+1)).append("J"));
        #else
        FileIO::Read(I,std::string(meshfile).append("/").append(meshfile.substr(pos+1)).append("I"),vst,ct,rit);
        FileIO::Read(J,std::string(meshfile).append("/").append(meshfile.substr(pos+1)).append("J"));
        #endif
        size_t pos1=std::string(meshfile.substr(pos+1)).find_last_of(".");
        std::vector<Point> Data;
        std::vector<std::vector<Point>> interpolatedData;
        //interpolate the data
        Arrangement2 originalI(I);
        Mesh::addCollinearFrame(I);
        //exit(0);
        if(meshfile.substr(pos+1).substr(pos1+1)=="BFIM"){
            BFIM bfim(I,J);
            for(int i=0;i<0;i++);
            if(!datafile.empty()){
               // FileIO::Read(Data,datafile);
                Arrangement2 IR(I);Arrangement2 JR(J);
                Regression::Sample(IR,JR,Data);
                Regression::CalculateVSV(IR,JR);
            }
            BFIS bfis(I,J,vst,ct);
            for(int i=0;i<iterations;i++)bfis.iterate();
            interpolatedData.push_back(std::vector<Point>());
            bfis.getPoints(interpolatedData[0]);
        }
        else if(meshfile.substr(pos+1).substr(pos1+1)=="AFIM"){
            AFIM afim(I,J);
            for(int i=0;i<0;i++);
            if(!datafile.empty()){
                FileIO::Read(Data,datafile);
                Sampling::Sample(I,J,Data);
            }
            AFIS afis(I,J,vst,ct);
            for(int i=0;i<iterations;i++)afis.iterate();
            interpolatedData.push_back(std::vector<Point>());
            afis.getPoints(interpolatedData[0]);
        }
        else if(meshfile.substr(pos+1).substr(pos1+1)=="RBFIM"){
            RBFIM rbfim(I,J);
            for(int i=0;i<0;i++);
            if(!datafile.empty()){
                //FileIO::Read(Data,datafile);
                Sampling::Sample(I,J,Data);
            }
            RBFIS rbfis(I,J,vst,ct);
            for(int i=0;i<iterations;i++)rbfis.iterate();
            rbfis.getPoints(interpolatedData);
        }
        else if(meshfile.substr(pos+1).substr(pos1+1)=="RAFIM"){
            RAFIM rafim(I,J);
            for(int i=0;i<0;i++);
            if(!datafile.empty()){
                FileIO::Read(Data,datafile);
                Sampling::Sample(I,J,Data);
            }
            RAFIS rafis(I,J,vst,ct);
            for(int i=0;i<iterations;i++)rafis.iterate();
            rafis.getPoints(interpolatedData);
        }
        //remove the frame data
        std::vector<Point> centroids;
        for(std::vector<std::vector<Point>>::iterator it=interpolatedData.begin();it!=interpolatedData.end();it++){
            //compute a centroid
            Point centroid =CGAL::centroid(it->begin(),it->end(),CGAL::Dimension_tag<0>());
            centroids.push_back(centroid);
        }
        int side = std::sqrt(centroids.size());
        std::vector<Point> erase_centroids(side*4);
        //sort centroids by x
        std::sort(centroids.begin(),centroids.end(),[](auto i,auto j){return i.x()<j.x();});
        erase_centroids.resize(std::distance(erase_centroids.begin(),std::copy(centroids.begin(),centroids.begin()+side,erase_centroids.begin())));
        std::reverse(centroids.begin(),centroids.end());
        erase_centroids.resize(std::distance(erase_centroids.begin(),std::copy(centroids.begin(),centroids.begin()+side,erase_centroids.end())));
        std::sort(centroids.begin(),centroids.end(),[](auto i,auto j){return i.y()<j.y();});
        erase_centroids.resize(std::distance(erase_centroids.begin(),std::copy(centroids.begin(),centroids.begin()+side,erase_centroids.end())));
        std::reverse(centroids.begin(),centroids.end());
        erase_centroids.resize(std::distance(erase_centroids.begin(),std::copy(centroids.begin(),centroids.begin()+side,erase_centroids.end())));
        interpolatedData.resize(std::distance(interpolatedData.begin(),std::remove_if(interpolatedData.begin(),interpolatedData.end(),
                      [&erase_centroids](auto i){Point c=CGAL::centroid(i.begin(),i.end(),CGAL::Dimension_tag<0>());
                          return std::find(erase_centroids.begin(),erase_centroids.end(),c)!=erase_centroids.end();})));

        //render the data
        std::vector<Delaunay2> DataTriangulation;
        for(int i=0;i<interpolatedData.size();i++){
            DataTriangulation.push_back(Delaunay2(interpolatedData[i].begin(),interpolatedData[i].end()));
        }
        SceneRender::initScene();
        MeshRender m;
        SurfaceRender s;
        m.SetData(originalI,vst);
        s.SetData(DataTriangulation);
        SceneRender::addRender(s,Point(0,0,0),Point(0,0,0));
        SceneRender::addRender(m,Point(0,0,0),Point(0,0,0));
        SceneRender::renderScene();
        
    }
    else if(parseInputRender(argc,argv,datafile)){
        std::vector<Point> Data;
        //FileIO::Read(Data,datafile);
        SceneRender::initScene();
        SurfaceRender s;
        Delaunay2 triangulation(Data.begin(),Data.end());
        s.SetData(triangulation);
        SceneRender::addRender(s,Point(0,0,0),Point(0,0,0));
        SceneRender::renderScene();
    }
    else{
        std::cout<<"Undefined input-Usage:1) -m <meshfile> -d <datafile>(optional) -i <iterations> 2) -c <meshdatapath>"<<std::endl;
    }
}
