//
//  testHausdorff.cpp
//  OpenGL_CGAL
//
//  Created by Sean Dillon on 1/8/16.
//  Copyright (c) 2016 Sean Dillon. All rights reserved.
//

#include <stdio.h>
#include "HausdorffDistance.h"
#include "KernelConstructions.h"
int main(){
    //create a set of a linear surface
    int limit = 20;
    std::vector<Point> v1;
    for(int i=0;i<limit;i++){
        for(int j=0;j<limit;j++){
            v1.push_back(Point(i,j,i+j));
        }
    }
    //create a linear surface with diferent orientation
    std::vector<Point> v2;
    for(int i=0;i<limit;i++){
        for(int j=0;j<limit;j++){
            v2.push_back(Point(i,j,-1));
        }
    }
    
    //compare set with itself
    HausdorffDistance h1(v1,v1);
    std::cout<<"A2B "<<h1.DistanceAtoB()<<std::endl;
    std::cout<<"B2A "<<h1.DistanceBtoA()<<std::endl;
    std::cout<<"H "<<h1.DistanceAB()<<std::endl;
    //compare sets v1 v2
    h1.setB(v2);
    std::cout<<"A2B "<<h1.DistanceAtoB()<<std::endl;
    std::cout<<"B2A "<<h1.DistanceBtoA()<<std::endl;
    std::cout<<"H "<<h1.DistanceAB()<<std::endl;
    
    
}